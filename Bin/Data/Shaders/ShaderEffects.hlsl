#include "CommonStructs.hlsli"

Texture2D fullscreenTexture0 : register(t0);
Texture2D fullscreenTexture1 : register(t1);
SamplerState samplerState : register(s0);

cbuffer GlitchData : register(b0)
{
	float2 resolution;
	float elapsedTime;
	float UVNoiseDistance;
	float lineThresholdMultiplier;
	float blockThresholdMultiplier;
	float distortionLineMultiplier;
	float noiseAbberationAlpha;
}

cbuffer StaticData : register(b1)
{
	float intensity;
	float elapsedTimeStatic;
	float lineDistortionMultiplier;
	float padding;
}

#ifdef GLITCHSCREEN01
PixelOutput PS_GlitchScreen01(TexturedVertexToPixel input)
{
	PixelOutput output;
	float3 resource = 0; // fullscreenTexture0.SampleLevel(samplerState, input.myUV, 0).rgb;

	float2 uv = input.myUV;
	float2 block = float2(floor(uv.x * resolution.x), floor(uv.y * resolution.y)) / float2(16.0f, 800.0f);
	float2 uvNoise = block / float2(64.0f, 64.0f);
	uvNoise += floor(float2(elapsedTime, elapsedTime) * float2(1234.0f, 3542.0f)) / float2(128.0f, 128.0f);

	//uvNoise = input.myUV;

	float3 noiseResource = fullscreenTexture1.SampleLevel(samplerState, uvNoise, 0).rgb;
	float3 singularInvertedNoiseResource = fullscreenTexture1.SampleLevel(samplerState, float2(uvNoise.y, 0.0f), 0).rgb;

	float blockThreshold = pow(frac(elapsedTime * 1236.0453f), 2.0f) * 0.55f * blockThresholdMultiplier;
	float lineThreshold = pow(frac(elapsedTime * 2236.0453f), 3.0f) * 0.7f * lineThresholdMultiplier;

	float2 uvRed = uv, uvGreen = uv, uvBlue = uv;

	//Glitch blocks and lines
	if (noiseResource.r < blockThreshold || singularInvertedNoiseResource.g < lineThreshold)
	{
		float2 dist = (frac(uvNoise) - 0.5f) * UVNoiseDistance;
		uvRed += dist * 0.1f;
		uvGreen += dist * 0.2f;
		uvBlue += dist * 0.125f;
	}

	resource.r = fullscreenTexture0.SampleLevel(samplerState, uvRed, 0).r;
	resource.g = fullscreenTexture0.SampleLevel(samplerState, uvGreen, 0).g;
	resource.b = fullscreenTexture0.SampleLevel(samplerState, uvBlue, 0).b;

	//Chromatically abberate noise texture
	float3 noiseAbberation = float3(0.0f, 0.0f, 0.0f);
	if (noiseAbberationAlpha > 0.0f)
	{
		float2 noiseUvRed = uv, noiseUvGreen = uv, noiseUvBlue = uv;

		float2 distNoise = (frac(uvNoise) - 0.5f) * 0.05f;
		//distNoise.x /= 1000.5f;
		noiseUvRed += distNoise * 0.1f;
		noiseUvGreen += distNoise * 0.2f;
		noiseUvBlue += distNoise * 0.125f;

		noiseAbberation.r = fullscreenTexture0.SampleLevel(samplerState, noiseUvRed, 0).r;
		noiseAbberation.g = fullscreenTexture0.SampleLevel(samplerState, noiseUvGreen, 0).g;
		noiseAbberation.b = fullscreenTexture0.SampleLevel(samplerState, noiseUvBlue, 0).b;

		noiseAbberation.r += fullscreenTexture1.SampleLevel(samplerState, noiseUvRed, 0).r;
		noiseAbberation.g += fullscreenTexture1.SampleLevel(samplerState, noiseUvGreen, 0).g;
		noiseAbberation.b += fullscreenTexture1.SampleLevel(samplerState, noiseUvBlue, 0).b;
	}

	//loose lum for blocks
	if (noiseResource.g < blockThreshold)
	{
		resource.rgb = resource.ggg;
	}

	//Discolor block lines
	if (singularInvertedNoiseResource.b * 3.5f < lineThreshold)
	{
		resource.rgb = float3(dot(resource, float3(5.0f, 1.0f, 1.0f)), 0.0f, 0.0f);
	}

	//Leave lines in some blocks
	if (noiseResource.g * 1.5f < blockThreshold || singularInvertedNoiseResource.g * 1.75f < lineThreshold)
	{
		float distortionLine = frac((block.y * 64.0) / (28.0f * distortionLineMultiplier));
		float3 mask = float3(5.5f, 0.5f, 0.5f);
		if (distortionLine > 0.333)
		{
			mask = float3(1.0f, 3.0f, 1.0f);
		}
		if (distortionLine > 0.666)
		{
			mask = float3(3.0f, 1.0f, 3.0f);
		}

		resource *= mask;
	}

	output.myColor = float4(resource.rgb, 1.0f);
	output.myColor += float4(noiseAbberation.rgb * noiseAbberationAlpha, 1.0f);
	return output;
}
#endif

#ifdef VHSTapeNoise
PixelOutput PS_VHSTapeNoise(TexturedVertexToPixel input)
{
	PixelOutput output;
	float3 resource = fullscreenTexture0.SampleLevel(samplerState, input.myUV, 0).rgb;

	output.myColor = float4(resource.rgb, 1.0f);
	return output;
}
#endif

#ifdef STATIC

static float fNintensity = 1.0f;
static float fSintensity = 0.25f;
static float fScount = 1024;
#define MONOCHROME

PixelOutput PS_Static(TexturedVertexToPixel input)
{
	PixelOutput output;
	float3 resource = fullscreenTexture0.SampleLevel(samplerState, input.myUV, 0).rgb;
	float3 noiseResource = fullscreenTexture1.SampleLevel(samplerState, input.myUV, 0).rgb;
	
	float2 uv = input.myUV;
	uv.x *= elapsedTimeStatic * 10.0f;
	uv.x = fmod(uv.x, 1.0f);

	float x = uv.x * uv.y * elapsedTimeStatic * 1000.0f;
	x = fmod(x, 13.0f) * fmod(x, 123.0f);
	float dx = fmod(x, 0.01f);

	float3 addedNoise = noiseResource * 0.0f + noiseResource * saturate(0.1f + dx.xxx * 100.0f);

	uv.x = input.myUV.x;
	uv.y *= elapsedTimeStatic * 50.0f;
	uv.y = fmod(uv.y, 1.0f);
	float2 sc; sincos(uv.y * fScount, sc.x, sc.y);
	
	////sc.x *= elapsedTimeStatic * 100.0f;
	//sc.y *= elapsedTimeStatic * 100.0f;
	////sc.x = fmod(sc.x, 5.0f);
	//sc.y = fmod(sc.y, 6.0f);

	addedNoise += noiseResource * float3(sc.x, sc.y, sc.y) * fSintensity;

	//addedNoise = lerp(noiseResource, addedNoise, saturate(fNintensity));

#ifdef MONOCHROME
	addedNoise.rgb = dot(addedNoise, float3(0.3f, 0.59f, 0.11f));
#endif

	output.myColor = float4(resource + addedNoise * intensity, 1.0f);
	return output;

	//float2 uv = input.myUV;
	//float2 block = float2(floor(uv.x * 1920.0f), floor(uv.y * 1080.0f)) / float2(16.0f, 16.0f);
	//float2 uvNoise = block / float2(64.0f, 64.0f);
	//uvNoise += floor(float2(elapsedTimeStatic, elapsedTimeStatic) * float2(1234.0f, 3542.0f)) / float2(128.0f, 128.0f);
	//uvNoise.x = fmod(uvNoise.x, 10.0f);
	//uvNoise.y = fmod(uvNoise.y, 10.0f);
	//float3 noiseResource = fullscreenTexture1.SampleLevel(samplerState, uvNoise, 0).rgb;
	//
	//if (dot(noiseResource, noiseResource) < 0.5f)
	//{
	//	noiseResource = 0.0f;
	//}
	//else
	//{
	//	noiseResource = 1.0f;
	//}

	//output.myColor = float4(resource.rgb, 1.0f);
	//output.myColor += float4(noiseResource.rgb, 1.0f);
	//return output;
}
#endif

#ifdef CHROMATICABBERATION
PixelOutput PS_ChromaticAbberation(TexturedVertexToPixel input)
{
	PixelOutput output;

	float3 resource = fullscreenTexture0.SampleLevel(samplerState, input.myUV, 0).rgb;

	//float yOffset = abs(sin(iTime)*4.0)*vertMovementOn + vertJerk*vertJerk2*0.3;
	//float y = mod(uv.y + yOffset, 1.0);

	//float xOffset = (fuzzOffset + largeFuzzOffset) * horzFuzzOpt;

	//float staticVal = 0.0;

	//for (float y = -1.0; y <= 1.0; y += 1.0) {
	//	float maxDist = 5.0 / 200.0;
	//	float dist = y / 200.0;
	//	staticVal += staticV(vec2(uv.x, uv.y + dist))*(maxDist - abs(dist))*1.5;
	//}

	//staticVal *= bottomStaticOpt;

	//float red = texture(iChannel0, vec2(uv.x + xOffset - 0.01*rgbOffsetOpt, y)).r + staticVal;
	//float green = texture(iChannel0, vec2(uv.x + xOffset, y)).g + staticVal;
	//float blue = texture(iChannel0, vec2(uv.x + xOffset + 0.01*rgbOffsetOpt, y)).b + staticVal;

	//vec3 color = vec3(red, green, blue);


	output.myColor = float4(resource.rgb, 1.0f);
	return output;
}
#endif

/*PixelOutput output;
float3 resource = 0; // fullscreenTexture0.SampleLevel(samplerState, input.myUV, 0).rgb;

float2 uv = input.myUV;
float2 block = float2(floor(uv.x * resolution.x), floor(uv.y * resolution.y)) / float2(16.0, 16.0f);
float2 uvNoise = block / float2(64.0f, 64.0f);
uvNoise += floor(float2(elapsedTime, elapsedTime) * float2(1234.0f, 3542.0f)) / float2(64.0f, 64.0f);

float3 noiseResource = fullscreenTexture1.SampleLevel(samplerState, uvNoise, 0).rgb;
float3 singularInvertedNoiseResource = fullscreenTexture1.SampleLevel(samplerState, float2(uvNoise.y, 0.0f), 0).rgb;

float blockThreshold = pow(frac(elapsedTime * 1236.0453f), 2.0f) * 0.35f;
float lineThreshold = pow(frac(elapsedTime * 2236.0453f), 3.0f) * 0.5f;

float2 uvRed = uv, uvGreen = uv, uvBlue = uv;

//Glitch blocks and lines
if (noiseResource.r < blockThreshold || singularInvertedNoiseResource.g < lineThreshold)
{
float2 dist = (frac(uvNoise) - 0.5f) * 0.0125f;
//uvRed += dist * 0.1f;
//uvGreen += dist * 0.2f;
//uvBlue += dist * 0.125f;
}

resource.r = fullscreenTexture0.SampleLevel(samplerState, uvRed, 0).r;
resource.g = fullscreenTexture0.SampleLevel(samplerState, uvGreen, 0).g;
resource.b = fullscreenTexture0.SampleLevel(samplerState, uvBlue, 0).b;

//loose lum for blocks
if (noiseResource.g < blockThreshold)
{
//resource.rgb = resource.ggg;
}

//Discolor block lines
if (singularInvertedNoiseResource.b * 3.5f < lineThreshold)
{
//resource.rgb = float3(dot(resource, float3(1.0f, 1.0f, 1.0f)), 0.0f, 0.0f);
}

//Leave lines in some blocks
if (noiseResource.g * 1.5f < blockThreshold || singularInvertedNoiseResource.g * 2.5f < lineThreshold)
{
float distortionLine = frac((block.y * 64.0f) / 10.0f);
float3 mask = float3(1.0f, 0.0f, 0.0f);
if (distortionLine > 0.333)
{
mask = float3(0.0f, 1.0f, 0.0f);
}
if (distortionLine > 0.666)
{
mask = float3(0.0f, 1.0f, 0.0f);
}

//resource *= mask;
}

output.myColor = float4(resource.rgb, 1.0f);
return output;
*/

