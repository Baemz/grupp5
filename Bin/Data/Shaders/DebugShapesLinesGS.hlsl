#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
	float4x4 cameraOrientation;
	float4x4 toCamera;
	float4x4 projection;
	float4 camPosition;
}

[maxvertexcount(2)]
void GSMain(line LineVertexToGeometry input[2], inout LineStream<LineVertexToGeometry> output)
{
	for (int i = 0; i < 2; i++)
	{
		LineVertexToGeometry vertex;
		vertex.myPosition.xyz = input[i].myPosition.xyz;
		vertex.myPosition.w = 1.f;
		vertex.myPosition = mul(toCamera, vertex.myPosition);
		vertex.myPosition = mul(projection, vertex.myPosition);
		vertex.myColor = input[i].myColor;
		vertex.myThickness = input[i].myThickness;

		output.Append(vertex);
	}

	output.RestartStrip();
}