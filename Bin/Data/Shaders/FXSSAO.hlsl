#include "CommonStructs.hlsli"

SamplerState samplerState : register(s0);

cbuffer CameraData : register(b0)
{
    float4x4 cameraOrientation;
    float4x4 toCamera;
    float4x4 projection;
    float4 camPosition;
}

cbuffer Resolution : register(b1)
{
    float2 myResolution;
}

#ifdef FX_SSAO
Texture2D positionTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D randomNormalTexture : register(t6);

static const float globalScale = 0.8f;
static const float globalIntensity = 1.4f;       
static const float globalBias = 0.6f;
static const float globalRadius = 0.02f;
static const float2 globalRandomTextureSize = float2(64.f, 64.f);

float DoAmbientOcclusion(float2 tcoord, float2 uv, float3 pos, float3 cnorm)
{
    float3 diff = positionTexture.Sample(samplerState, tcoord + uv).xyz - pos; 
    float3 v = normalize(diff);
    const float d = length(diff) * globalScale;
    const float rangeCheck = ((diff.z - pos.z) < globalRadius) ? 1.f : 0.f;
    return max(0.0f, dot(cnorm, v) - globalBias) * (1.0 / (1.0 + d)) * globalIntensity * rangeCheck;
}

PixelOutput PS_FXSSAO(TexturedVertexToPixel input)
{
    PixelOutput output;
    output.myColor.rgb = float3(1.f, 1.f, 1.f);

    const float2 vec[4] = { float2(1.f, 0.f), float2(-1.f, 0.f), float2(0.f, 1.f), float2(0.f, -1.f) };
    float3 normal = normalTexture.Sample(samplerState, input.myUV).xyz;
    float3 position = positionTexture.Sample(samplerState, input.myUV).xyz;
    float2 random = normalize(randomNormalTexture.Sample(samplerState, myResolution * input.myUV / globalRandomTextureSize).xy);

    float ambientOcclusion = 0.f;
    float sampleRadius = globalRadius / position.z;

    uint iterations = 4;
    for (uint i = 0; i < iterations; ++i)
    {
        float2 firstCoord = reflect(vec[i], random) * sampleRadius;
        float2 secondCoord = float2((firstCoord.x * 0.707f) - (firstCoord.y * 0.707f), (firstCoord.x * 0.707f) - (firstCoord.y * 0.707f));
       
        ambientOcclusion += DoAmbientOcclusion(input.myUV, firstCoord * 0.25f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, secondCoord * 0.5f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, firstCoord * 0.75f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, secondCoord, position, normal);

        ambientOcclusion += DoAmbientOcclusion(input.myUV, secondCoord * 0.25f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, firstCoord * 0.5f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, secondCoord * 0.75f, position, normal);
        ambientOcclusion += DoAmbientOcclusion(input.myUV, firstCoord, position, normal);
    }

    ambientOcclusion /= (float) iterations * 4.f;
    
    output.myColor.r = 1.f - ambientOcclusion;
    return output;
}
#endif

#ifdef FX_SSAOBLUR
static const int globalBlurSize = 4;

Texture2D ssaoTexture : register(t0);
PixelOutput PS_FXSSAOBlur(TexturedVertexToPixel input)
{
    const float2 texelSize = float2((1.f / myResolution.x), (1.f / myResolution.y));
    PixelOutput output;

    float2 hlim = float2(float(-globalBlurSize) * 0.5f + 0.5f, float(-globalBlurSize) * 0.5f + 0.5f);
    float result = 0.f;
    for (int i = 0; i < globalBlurSize; ++i)
    {
        for (int j = 0; j < globalBlurSize; ++j)
        {
            float2 offset = (hlim + float2(float(i), float(j))) * texelSize;
            result += ssaoTexture.Sample(samplerState, input.myUV + offset).r;
        }
    }

    output.myColor.r = result / float(globalBlurSize * globalBlurSize);
    return output;
}
#endif