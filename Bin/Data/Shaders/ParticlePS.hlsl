#include "CommonStructs.hlsli"

Texture2D instanceTexture : register(t0);
SamplerState instanceSampler : register(s0);

PixelOutput PSMain(GeometryToPixel input)
{
	PixelOutput output;
	output.myColor = instanceTexture.Sample(instanceSampler, input.myUV) * input.myColor;

	return output;
};