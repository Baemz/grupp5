#pragma once

cbuffer CameraData : register(b0)
{
    float4x4 cameraOrientation;
    float4x4 toCamera;
    float4x4 projection;
    float4 camPosition;
}
cbuffer InstanceData : register(b1)
{
    float4x4 toWorld;
    float useFog;
}
//cbuffer BoneData : register(b2)
//{
//    float4x4 modelBones[64];
//}