struct PBRData
{
    float3 lightColor;
    float3 lambert;
    float3 fresnel;
    float3 reflectionFresnel;
    float distribution;
    float visibility;
};

struct Attributes
{
    float4 diffuseAndRoughness;
    float4 ambientOcclusion;
    float4 metallic;
    float4 emissive;
    float4 objectNormal;
    float4 worldPosition;
    float4 toEyeAndFogFactor;
};

float3 GetLambert(Attributes attributes, float3 toLight)
{
	// Lambert
    float3 lightDir = normalize(toLight.xyz);
    float3 lambert = saturate(dot(attributes.objectNormal.xyz, lightDir));

    return lambert;
}

float3 GetFresnel(Attributes attributes, float3 toLight)
{
    float3 substance = (float3(0.04f, 0.04f, 0.04f) - (float3(0.04f, 0.04f, 0.04f) * attributes.metallic.rgb)) + attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb;
    float3 halfvec = normalize(toLight + attributes.toEyeAndFogFactor.xyz);
    float LdotH = dot(toLight, halfvec);
    LdotH = saturate(LdotH);
    LdotH = 1.0f - LdotH;
    LdotH = pow(LdotH, 5);
    float3 fresnel = LdotH * (1.f - substance);
    fresnel = substance + fresnel;

    return fresnel;
}

float3 GetReflectionFresnel(Attributes attributes)
{
	// Reflection-fresnel
    float VdotN = dot(attributes.toEyeAndFogFactor.xyz, attributes.objectNormal.xyz);
    VdotN = saturate(VdotN);
    VdotN = 1.0f - VdotN;
    VdotN = pow(VdotN, 5);

    float3 substance = (float3(0.04f, 0.04f, 0.04f) - (float3(0.04f, 0.04f, 0.04f) * attributes.metallic.rgb)) + attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb;
    float3 refFresnel = VdotN * (1.f - substance);
    refFresnel = refFresnel / (6 - 5 * attributes.diffuseAndRoughness.a);
    refFresnel = substance + refFresnel;

    return refFresnel;
}

float GetDistribution(Attributes attributes, float3 toLight)
{
	// Distribution
    float3 halfvec = normalize(toLight + attributes.toEyeAndFogFactor.xyz);
    float HdotN = saturate(dot(halfvec, attributes.objectNormal.xyz));
    float m = attributes.diffuseAndRoughness.a * attributes.diffuseAndRoughness.a;
    float m2 = m * m;
    float denominator = HdotN * HdotN * (m2 - 1.f) + 1.f;
    float distribution = m2 / (3.14159 * denominator * denominator);

    return distribution;
}

float GetVisibility(Attributes attributes, float3 toLight)
{
	// Visability
    float roughnessRemapped = (attributes.diffuseAndRoughness.a + 1.f) / 2.f;
    float NdotL = saturate(dot(attributes.objectNormal.xyz, toLight));
    float NdotV = saturate(dot(attributes.objectNormal.xyz, attributes.toEyeAndFogFactor.xyz));
    float k = roughnessRemapped * roughnessRemapped * 1.7724f;
    float G1V = NdotV * (1.f - k) + k;
    float G1L = NdotL * (1.f - k) + k;
    float visability = (NdotV * NdotL) / (G1V * G1L);

    return visability;
}

float3 GetDirectDiffuse(Attributes attributes, PBRData pbrData)
{
    float3 metallicAlbedo = attributes.diffuseAndRoughness.rgb - (attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb);
    
    float3 returnColor = metallicAlbedo * pbrData.lightColor * pbrData.lambert * (float3(1.f, 1.f, 1.f) - pbrData.fresnel);
    return saturate(returnColor);

}

float3 GetDirectSpecular(Attributes attributes, PBRData pbrData)
{
    return saturate(float3(pbrData.lightColor * pbrData.lambert * pbrData.fresnel * pbrData.distribution * pbrData.visibility));
}