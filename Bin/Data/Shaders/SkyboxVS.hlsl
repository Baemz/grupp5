#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
    float4x4 cameraOrientation;
    float4x4 toCamera;
    float4x4 projection;
	float4 camPosition;
}
cbuffer InstanceData : register(b1)
{
    float4x4 toWorld;
}

SkyboxVertexToPixel VSMain(ColoredVertexInput input)
{
    SkyboxVertexToPixel output;

    float4 vertexObjectPos = float4(input.myPosition.xyz, 1.f);
    float4 vertexWorldPos = mul(toWorld, vertexObjectPos);
    float4 vertexViewPos = mul(toCamera, vertexWorldPos);
    float4 vertexProjectionPos = mul(projection, vertexViewPos);
    vertexProjectionPos.z = vertexProjectionPos.w; // Set Z-value to w to make the depth-test always be furthest away from camera. (z/w == 1.f)

    output.myPosition = vertexProjectionPos;
	output.myWPosition = vertexWorldPos;
	output.myCameraPosition = camPosition.xyz;

    return output;
};