#include "CommonStructs.hlsli"
#include "StandardBuffersVS.hlsli"

PBLVertexToPixel VSMain(PBLVertexInput input)
{
    PBLVertexToPixel output;
    
    //float4 vertexWeights = input.myWeights;
    //uint4 vertexBones = uint4((uint) input.myBones.x, (uint) input.myBones.y, (uint) input.myBones.z, (uint) input.myBones.w);
    

    float4 vertexObjectPos = float4(input.myPosition.xyz, 1);
    //
    //float4x4 finalMatrix;
    //finalMatrix = vertexWeights.x * modelBones[vertexBones.x];
    //finalMatrix += vertexWeights.y * modelBones[vertexBones.y];
    //finalMatrix += vertexWeights.z * modelBones[vertexBones.z];
    //finalMatrix += vertexWeights.w * modelBones[vertexBones.w];
    //float4 vertexAnimatedPos = mul(finalMatrix, float4(input.myPosition.xyz, 1.0f));
    float4 vertexWorldPos = mul(toWorld, vertexObjectPos);
    float4 vertexViewPos = mul(toCamera, vertexWorldPos);
    float4 vertexProjectionPos = mul(projection, vertexViewPos);

    float3x3 worldWithoutTranslation = (float3x3) toWorld;
    float3x3 viewWithoutTranslation = (float3x3) toCamera;

    output.myPosition = vertexProjectionPos;
    output.myViewPos = vertexViewPos;
    output.myWPosition = vertexWorldPos;
    output.myNormals = normalize(mul(worldWithoutTranslation, (float3) input.myNormals));
    output.myViewNormal = normalize(mul(viewWithoutTranslation, (float3) output.myNormals));
    output.myTangents = normalize(mul(worldWithoutTranslation, (float3) input.myTangents));
    output.myBinormal = normalize(mul(worldWithoutTranslation, (float3) input.myBinormal));
    output.myUV = input.myUV;
    output.myBones = input.myBones;
    output.myWeights = input.myWeights;
    output.myCameraPosition = camPosition.xyz;
    output.myUseFog = useFog;
    return output;
}

