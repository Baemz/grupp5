#include "CommonStructs.hlsli"

cbuffer LightData : register(b0)
{
    float3 fromLightDirection;
    float4 lightColor;
}

TextureCube skyboxTexture : register(t0);
Texture2D cullingMask : register(t1);
SamplerState instanceSampler : register(s0);

PixelOutput PSMain(SkyboxVertexToPixel input)
{
    PixelOutput output;

	float3 toEye = normalize(input.myWPosition - input.myCameraPosition);
    output.myColor = skyboxTexture.SampleLevel(instanceSampler, toEye, 0);
    return output;
};