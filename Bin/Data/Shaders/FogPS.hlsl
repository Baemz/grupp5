#include "CommonStructs.hlsli"

SamplerState samplerState : register(s0);
Texture2D sceneTexture : register(t0);
Texture2D toEyeAndFogFactorTexture : register(t2);

PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    float4 sceneColor = sceneTexture.Sample(samplerState, input.myUV);
    float fogFactor = toEyeAndFogFactorTexture.Sample(samplerState, input.myUV).a;
    float4 fogColor = float4(0.603f, 0.270f, 0.764f, 1.0f);
    
    output.myColor = fogFactor * sceneColor + (1.0f - fogFactor) * fogColor;
    return output;
}