#include "CommonStructs.hlsli"

LineVertexToGeometry VSMain(LineVertexInput input)
{

	//float4 vertexObjectPos = float4(input.myPosition.xyz, 1);
	//float4 vertexWorldPos = float4(input.myPosition.xyz, 1);

	LineVertexToGeometry output;

	output.myPosition = float4((input.myPosition.xy * 2.f - 1.f), 1.0f, 1.0f);
	output.myPosition.y = -output.myPosition.y;
	output.myColor = input.myColor;
	output.myThickness = input.myThickness;

	return output;
};