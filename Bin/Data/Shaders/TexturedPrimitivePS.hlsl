#include "CommonStructs.hlsli"

cbuffer LightData : register(b0)
{
    float3 fromLightDirection;
    float4 lightColor;
}

Texture2D instanceTexture : register(t0);
SamplerState instanceSampler : register(s0);

PixelOutput PSMain(TexturedVertexToPixel input)
{
    PixelOutput output;
    output.myColor = instanceTexture.Sample(instanceSampler, input.myUV);
    
    return output;
};