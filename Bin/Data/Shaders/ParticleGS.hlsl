#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
	float4x4 cameraOrientation;
	float4x4 toCamera;
	float4x4 projection;
	float4 camPosition;
}

float2x2 ComputeRotation(float aRotation)
{
    float c = cos(aRotation);
    float s = sin(aRotation);
    return float2x2(c, -s, s, c);
}

[maxvertexcount(4)]
void GSMain(point ParticleToGeometry input[1], inout TriangleStream<GeometryToPixel> output)
{
	const float4 offset[4] =
	{
		{ -input[0].mySize, input[0].mySize, 0, 0 },
		{ input[0].mySize, input[0].mySize, 0, 0 },
		{ -input[0].mySize, -input[0].mySize, 0, 0 },
		{ input[0].mySize, -input[0].mySize, 0, 0 }
	};

	const float2 uv[4] = 
	{
		{ 0, 1 },
		{ 1, 1 },
		{ 0, 0 },
		{ 1, 0 }
	};
    float2x2 rotation = ComputeRotation(input[0].myRotation);

    for (int i = 0; i < 4; i++)
    {
        GeometryToPixel vertex;
        vertex.myPosition.xyz = input[0].myPosition.xyz;
        float2 totalOffset = mul(rotation, offset[i].xy);
        vertex.myPosition.w = 1.f;
        vertex.myPosition.xy += totalOffset;
        vertex.myPosition = mul(projection, vertex.myPosition);
        vertex.myColor = input[0].myColor;
        vertex.myUV = uv[i];
        output.Append(vertex);
    }

	output.RestartStrip();
}