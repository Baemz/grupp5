#include "CommonStructs.hlsli"

SamplerState samplerState : register(s0);
Texture2D emissiveAndMetallicTexture : register(t4);

PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    float3 emissive = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).rgb;

    output.myColor.rgb = max(0.f, emissive);
    output.myColor.a = 1.f;
    return output;
}