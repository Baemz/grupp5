#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
	float4x4 cameraOrientation;
	float4x4 toCamera;
	float4x4 projection;
	float4 camPosition;
}

[maxvertexcount(4)]
void GSMain(lineadj ParticleToGeometry input[4], inout TriangleStream<GeometryToPixel> output)
{
    float3 toEye = camPosition.xyz - input[1].myPosition.xyz;

    float3 line1 = input[2].myPosition.xyz - input[0].myPosition.xyz;
    float3 dir1 = normalize(line1);
    float3 normal1 = normalize(cross(normalize(toEye), dir1));

    float3 line2 = input[3].myPosition.xyz - input[1].myPosition.xyz;
    float3 dir2 = normalize(line2);
    float3 normal2 = normalize(cross(normalize(toEye), dir2));
    

    GeometryToPixel pOut[4];
    pOut[0].myPosition = float4(input[1].myPosition.xyz - normal1 * input[1].mySize, 1);
    pOut[0].myPosition = mul(projection, pOut[0].myPosition);
    pOut[0].myUV = float2(0, 1);
    pOut[0].myColor = input[1].myColor;

    pOut[1].myPosition = float4(input[2].myPosition.xyz - normal2 * input[2].mySize, 1);
    pOut[1].myPosition = mul(projection, pOut[1].myPosition);
    pOut[1].myUV = float2(1, 1);
    pOut[1].myColor = input[2].myColor;

    pOut[2].myPosition = float4(input[1].myPosition.xyz + normal1 * input[1].mySize, 1);
    pOut[2].myPosition = mul(projection, pOut[2].myPosition);
    pOut[2].myUV = float2(0, 0);
    pOut[2].myColor = input[1].myColor;

    pOut[3].myPosition = float4(input[2].myPosition.xyz + normal2 * input[2].mySize, 1);
    pOut[3].myPosition = mul(projection, pOut[3].myPosition);
    pOut[3].myUV = float2(1, 0);
    pOut[3].myColor = input[2].myColor;
    
    output.Append(pOut[0]);
    output.Append(pOut[1]);
    output.Append(pOut[2]);
    output.Append(pOut[3]);
    output.RestartStrip();
}