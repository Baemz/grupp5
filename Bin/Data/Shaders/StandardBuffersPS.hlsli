#pragma once

cbuffer LightData : register(b0)
{
    float4 fromLightDirectionAndMipCount;
    float4 lightColor;
    float dirLightintensity;
}

cbuffer PointLightData : register(b1)
{
    uint numberOfUsedPointLights;
    uint3 plpad;
    struct SPointLight
    {
        float4 myPosition;
        float3 myColor;
        float myRange;
        float myIntensity;
    } pointLights[8];
}

cbuffer SpotLightData : register(b2)
{
    uint numberOfUsedSpotLights;
    uint3 slpad;
    struct SSpotLight
    {
        float4 myPosition;
        float3 myColor;
        float myAngle;
        float3 myDirection;
        float myRange;
        float myIntensity;
    } spotLights[8];
}

cbuffer TimeData : register(b3)
{
    float threadDeltaTime;
    float threadTotalTime;
    float applicationTotalTime;
}