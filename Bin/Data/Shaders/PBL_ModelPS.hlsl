#include "CommonStructs.hlsli"
#include "StandardBuffersPS.hlsli"

SamplerState samplerState : register(s0);

Texture2D diffuseTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D roughnessTexture : register(t2);
Texture2D metallicTexture : register(t3);
Texture2D emissiveTexture : register(t4);
Texture2D ambientOcclusionTexture : register(t5);
TextureCube environmentalTexture : register(t6);


struct NormalData
{
    float3 normal;
    float3 binormal;
    float3 tangent;
};

struct Attributes
{
    float4 diffuse;
    float3 ambientOcclusion;
    float3 metallic;
	float3 emissive;
    float roughness;
    float3 objectNormal;
    float3 toEye;
};

struct PBRData
{
	float3 lightColor;
	float3 lambert;
	float3 fresnel;
	float3 reflectionFresnel;
	float distribution;
	float visibility;
};

float3x3 CreateTangentSpaceMatrix(NormalData normalData)
{
    float3x3 mat =
    {
        normalData.tangent.x, normalData.tangent.y, normalData.tangent.z,
        normalData.binormal.x, normalData.binormal.y, normalData.binormal.z,
        normalData.normal.x, normalData.normal.y, normalData.normal.z
    };

    return mat;
}

float RoughToSPow(float roughness)
{
    return (2.f / (roughness * roughness)) - 2.f;
}

static const float k0 = 0.00098f;
static const float k1 = 0.9921f;
static const float fakeLysMaxSpecularPower = (2.f / (0.0014f * 0.0014f)) -2.f;
static const float fMaxT = (exp2(-10.f / sqrt((2.f / (0.0014f * 0.0014f)) - 2.f)) - 0.00098f) / 0.9921f;
float GetSpecPowToMip(float fSpecPow, int nMips)
{
    float fSmulMaxT = (exp2(-10.f / sqrt(fSpecPow)) - k0) / k1;
    return float(nMips - 1 - 0) * (1.0f - clamp(fSmulMaxT / fMaxT, 0.0f, 1.0f));
}

float3 GetAmbientDiffuse(Attributes attributes, PBRData pbrData)
{
    float3 metallicAlbedo = attributes.diffuse.rgb - (attributes.diffuse.rgb * attributes.metallic);
    float3 ambientLight = environmentalTexture.SampleLevel(samplerState, attributes.objectNormal, fromLightDirectionAndMipCount.w - 2).rgb;
    return metallicAlbedo * ambientLight * attributes.ambientOcclusion * (float3(1.f, 1.f, 1.f) - pbrData.reflectionFresnel);
}

float3 GetAmbientSpecular(Attributes attributes, PBRData pbrData)
{
    float3 reflectionVector = -reflect(attributes.toEye, attributes.objectNormal);
    float fakeLysSpecularPower = RoughToSPow(attributes.roughness);
    float lysMipMap = GetSpecPowToMip(fakeLysSpecularPower, fromLightDirectionAndMipCount.w);

    float3 ambientLight = environmentalTexture.SampleLevel(samplerState, reflectionVector.xyz, lysMipMap).xyz;
    return ambientLight * attributes.ambientOcclusion * pbrData.reflectionFresnel;
}

float3 GetDirectDiffuse(Attributes attributes, PBRData pbrData)
{
    float3 metallicAlbedo = attributes.diffuse.rgb - (attributes.diffuse.rgb * attributes.metallic);
    
    float3 returnColor = metallicAlbedo * pbrData.lightColor * pbrData.lambert * (float3(1.f, 1.f, 1.f) - pbrData.fresnel);
    return returnColor;

}

float3 GetDirectSpecular(Attributes attributes, PBRData pbrData)
{
    return float3(pbrData.lightColor * pbrData.lambert * pbrData.fresnel * pbrData.distribution * pbrData.visibility);
}

float3 GetLambert(Attributes attributes, float3 toLight)
{
	// Lambert
	float3 lightDir = normalize(toLight.xyz);
	float3 lambert = saturate(dot(attributes.objectNormal, lightDir));

	return lambert;
}

float3 GetFresnel(Attributes attributes, float3 toLight)
{
	float3 substance = (float3(0.04f, 0.04f, 0.04f) - (float3(0.04f, 0.04f, 0.04f) * attributes.metallic)) + attributes.diffuse.rgb * attributes.metallic;
	float3 halfvec = normalize(toLight + attributes.toEye);
	float LdotH = dot(toLight, halfvec);
	LdotH = saturate(LdotH);
	LdotH = 1.0f - LdotH;
	LdotH = pow(LdotH, 5);
	float3 fresnel = LdotH * (1.f - substance);
	fresnel = substance + fresnel;

	return fresnel;
}

float3 GetReflectionFresnel(Attributes attributes)
{
	// Reflection-fresnel
	float VdotN = dot(attributes.toEye.xyz, attributes.objectNormal);
	VdotN = saturate(VdotN);
	VdotN = 1.0f - VdotN;
	VdotN = pow(VdotN, 5);

	float3 substance = (float3(0.04f, 0.04f, 0.04f) - (float3(0.04f, 0.04f, 0.04f) * attributes.metallic)) + attributes.diffuse.rgb * attributes.metallic;
	float3 refFresnel = VdotN * (1.f - substance);
	refFresnel = refFresnel / (6 - 5 * attributes.roughness);
	refFresnel = substance + refFresnel;

	return refFresnel;
}

float GetDistribution(Attributes attributes, float3 toLight)
{
	// Distribution
	float3 halfvec = normalize(toLight + attributes.toEye);
	float HdotN = saturate(dot(halfvec, attributes.objectNormal));
	float m = attributes.roughness * attributes.roughness;
	float m2 = m * m;
	float denominator = HdotN * HdotN * (m2 - 1.f) + 1.f;
	float distribution = m2 / (3.14159 * denominator * denominator);

	return distribution;
}

float GetVisibility(Attributes attributes, float3 toLight)
{
	// Visability
	float roughnessRemapped = (attributes.roughness + 1.f) / 2.f;
	float NdotL = saturate(dot(attributes.objectNormal, toLight));
	float NdotV = saturate(dot(attributes.objectNormal, attributes.toEye));
	float k = roughnessRemapped * roughnessRemapped * 1.7724f;
	float G1V = NdotV * (1.f - k) + k;
	float G1L = NdotL * (1.f - k) + k;
	float visability = (NdotV * NdotL) / (G1V * G1L);

	return visability;
}

float4 GetPointLightData(Attributes attributes, PBLVertexToPixel input)
{
	float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);

	for (unsigned int index = 0; index < numberOfUsedPointLights; ++index)
	{
		float3 toLight = pointLights[index].myPosition.xyz - input.myWPosition.xyz;
		float toLightDistance = toLight.x * toLight.x + toLight.y * toLight.y + toLight.z * toLight.z;
		toLight = normalize(toLight);
		float lightRange2 = pointLights[index].myRange * pointLights[index].myRange;

		float lambertAttenuation = saturate(dot(input.myNormals.xyz, toLight));
		float linearAttenuation = toLightDistance / lightRange2;
		linearAttenuation = 1.f - linearAttenuation;
		linearAttenuation = saturate(linearAttenuation);
		float physicalAttenuation = 1.f / toLightDistance;

		float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation;
        float4 resColor = float4(attributes.diffuse.rgb * attenuation * (pointLights[index].myColor.rgb * pointLights[index].myIntensity), 1.0f);

		// Lambert
		float3 lightDir = toLight;
		float3 lambert = GetLambert(attributes, lightDir);

		// Fresnel
		float3 fresnel = GetFresnel(attributes, lightDir);

		// Reflection-fresnel
		float3 refFresnel = GetReflectionFresnel(attributes);

		// Distribution
		float distribution = GetDistribution(attributes, lightDir);

		// Visability
		float visibility = GetVisibility(attributes, lightDir);

		PBRData pbrData;
		pbrData.lightColor = pointLights[index].myColor.rgb * pointLights[index].myIntensity;
		pbrData.fresnel = fresnel;
		pbrData.reflectionFresnel = refFresnel;
		pbrData.lambert = lambert;
		pbrData.distribution = distribution;
		pbrData.visibility = visibility;


		float3 ambientDiffuse = GetAmbientDiffuse(attributes, pbrData);
		float3 ambientSpecular = GetAmbientSpecular(attributes, pbrData);
		float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
		float3 directSpecular = GetDirectSpecular(attributes, pbrData);

		resColor *= float4(ambientDiffuse + ambientSpecular + directDiffuse + directSpecular + attributes.emissive, 1.0f);
		returnColor += resColor;
	}
	return float4(returnColor.xyz, 1.f);
}

float4 GetSpotLightData(Attributes attributes, PBLVertexToPixel input)
{
	float4 returnColor = float4(0.f, 0.f, 0.f, 1.f);

	for (unsigned int index = 0; index < numberOfUsedSpotLights; ++index)
	{
		float3 toLight = spotLights[index].myPosition.xyz - input.myWPosition.xyz;
		float toLightDistance = toLight.x * toLight.x + toLight.y * toLight.y + toLight.z * toLight.z;
		toLight = normalize(toLight);
		float lightRange2 = spotLights[index].myRange * spotLights[index].myRange;

		float lambertAttenuation = saturate(dot(input.myNormals.xyz, toLight));
		float linearAttenuation = toLightDistance / lightRange2;
		linearAttenuation = 1.f - linearAttenuation;
		linearAttenuation = saturate(linearAttenuation);
		float physicalAttenuation = 1.f / toLightDistance;

		float halfangle = spotLights[index].myAngle / 2.f;
		float cutPoint = 1.f - (halfangle / 90.f);

		float3 fromLight = normalize(-toLight);
		float3 dir = normalize(spotLights[index].myDirection);
		float x = saturate(dot(fromLight, dir));

		float angleAttenuation = (x - cutPoint) * (1.f / (1.f - cutPoint));

		float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation * angleAttenuation;
		float4 resColor = float4(attributes.diffuse.rgb * attenuation * (spotLights[index].myColor.rgb * spotLights[index].myIntensity), 1.0f);

		// Lambert
		float3 lightDir = toLight;
		float3 lambert = GetLambert(attributes, lightDir);

		// Fresnel
		float3 fresnel = GetFresnel(attributes, lightDir);

		// Reflection-fresnel
		float3 refFresnel = GetReflectionFresnel(attributes);

		// Distribution
		float distribution = GetDistribution(attributes, lightDir);

		// Visability
		float visibility = GetVisibility(attributes, lightDir);

		PBRData pbrData;
		pbrData.lightColor = spotLights[index].myColor.rgb * spotLights[index].myIntensity;
		pbrData.fresnel = fresnel;
		pbrData.reflectionFresnel = refFresnel;
		pbrData.lambert = lambert;
		pbrData.distribution = distribution;
		pbrData.visibility = visibility;


		float3 ambientDiffuse = GetAmbientDiffuse(attributes, pbrData);
		float3 ambientSpecular = GetAmbientSpecular(attributes, pbrData);
		float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
		float3 directSpecular = GetDirectSpecular(attributes, pbrData);

		resColor *= float4(ambientDiffuse + ambientSpecular + directDiffuse + directSpecular + attributes.emissive, 1.0f);
		returnColor += resColor;
	}
	return saturate(float4(returnColor.xyz, 1.f));
}

PixelOutput PSMain(PBLVertexToPixel input)
{
    PixelOutput output;
    output.myColor = diffuseTexture.Sample(samplerState, input.myUV).rgba;
    return output;
}