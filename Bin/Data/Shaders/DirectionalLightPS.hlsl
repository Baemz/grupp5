#include "CommonStructs.hlsli"
#include "DeferredLightFunc.hlsli"

cbuffer LightData : register(b0)
{
    float4 fromLightDirectionAndMipCount;
    float4 lightColor;
    float dirLightintensity;
}

SamplerState samplerState : register(s0);

Texture2D depthTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D toEyeTexture : register(t2);
Texture2D diffuseAndRoughnessTexture : register(t3);
Texture2D emissiveAndMetallicTexture : register(t4);
Texture2D ambientOcclusionTexture : register(t5);
Texture2D shadowMap : register(t8);


cbuffer CameraData : register(b1)
{
    float4x4 cameraOrientation;
    float4x4 inversetoCamera;
    float4x4 inverseProjection;
    float4 camPosition;
}

cbuffer shadowData : register(b2)
{
	float4x4 depthMVP;
}

static const float bias = 0.005f;
static const float4x4 shadowBias = {
	0.5f, 0.0f, 0.0f, 0.0f,
	0.0f, 0.5f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.5f, 0.0f,
	0.5f, 0.5f, 0.5f, 1.0f
};

PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    Attributes attributes;

    //float4 position = depthTexture.Sample(samplerState, input.myUV);
    //float4x4 mvp = mul(depthMVP, shadowBias);
	//////mvp = mul(mvp, depthProj);
	//position = mul(depthMVP, position);
	//float2 uv = position.xy * 0.5f + 0.5f;
	////
	//float zVal = saturate((position.z / position.w) * 0.5f + 0.5f);
	//float depth = shadowMap.Sample(samplerState, uv).r;
	//float shadow = (zVal < depth);

    attributes.diffuseAndRoughness = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV);
    attributes.ambientOcclusion = ambientOcclusionTexture.Sample(samplerState, input.myUV);
    attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).xxxx;
    attributes.objectNormal = normalTexture.Sample(samplerState, input.myUV);
    attributes.toEyeAndFogFactor = toEyeTexture.Sample(samplerState, input.myUV);
    
    // Lambert
    float3 lightDir = normalize(fromLightDirectionAndMipCount.xyz);
    float3 lambert = GetLambert(attributes, lightDir);

    // Fresnel
    float3 fresnel = GetFresnel(attributes, lightDir);

    // Reflection-fresnel
    float3 refFresnel = GetReflectionFresnel(attributes);

    // Distribution
    float distribution = GetDistribution(attributes, lightDir);

    // Visability
    float visibility = GetVisibility(attributes, lightDir);

    PBRData pbrData;
    pbrData.lightColor = lightColor.rgb * dirLightintensity;
    pbrData.fresnel = fresnel;
    pbrData.reflectionFresnel = refFresnel;
    pbrData.lambert = lambert;
    pbrData.distribution = distribution;
    pbrData.visibility = visibility;
    
    float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
    float3 directSpecular = GetDirectSpecular(attributes, pbrData);
    

    output.myColor = float4((directDiffuse + directSpecular), 1.0f);
    return output;
}