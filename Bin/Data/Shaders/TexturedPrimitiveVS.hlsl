#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
    float4x4 cameraOrientation;
    float4x4 toCamera;
    float4x4 projection;
}
cbuffer InstanceData : register(b1)
{
    float4x4 toWorld;
}

TexturedVertexToPixel VSMain(TexturedVertexInput input)
{
    TexturedVertexToPixel output;

    float4 vertexObjectPos = float4(input.myPosition.xyz, 1);
    float4 vertexWorldPos = mul(toWorld, vertexObjectPos);
    float4 vertexViewPos = mul(toCamera, vertexWorldPos);
    float4 vertexProjectionPos = mul(projection, vertexViewPos);

    output.myPosition = vertexProjectionPos;
    output.myUV = input.myUV;

    return output;
};