#include "CommonStructs.hlsli"
#include "StandardBuffersPS.hlsli"

SamplerState samplerState : register(s0);

Texture2D diffuseTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D roughnessTexture : register(t2);
Texture2D metallicTexture : register(t3);
Texture2D emissiveTexture : register(t4);
Texture2D ambientOcclusionTexture : register(t5);
TextureCube environmentalTexture : register(t6);

static const float globalFogStart = 300.0f;
static const float globalFogEnd = 1000.0f;


struct NormalData
{
	float3 normal;
	float3 binormal;
	float3 tangent;
};

struct Attributes
{
	float4 diffuseAndRoughness;
	float3 ambientOcclusion;
	float3 metallic;
	float3 emissive;
	float3 objectNormal;
	float3 toEye;
};

float3x3 CreateTangentSpaceMatrix(NormalData normalData)
{
	float3x3 mat =
	{
		normalData.tangent.x, normalData.tangent.y, normalData.tangent.z,
		normalData.binormal.x, normalData.binormal.y, normalData.binormal.z,
		normalData.normal.x, normalData.normal.y, normalData.normal.z
	};

	return mat;
}

DeferredDataOutput PSMain(PBLVertexToPixel input)
{
    DeferredDataOutput output;

	Attributes attributes;
	attributes.diffuseAndRoughness.rgb = diffuseTexture.Sample(samplerState, input.myUV).rgb;
	attributes.metallic = metallicTexture.Sample(samplerState, input.myUV).rgb;
	attributes.diffuseAndRoughness.a = roughnessTexture.Sample(samplerState, input.myUV).x;
	attributes.ambientOcclusion = ambientOcclusionTexture.Sample(samplerState, input.myUV).xxx;
	attributes.emissive = emissiveTexture.Sample(samplerState, input.myUV).xyz;
    attributes.toEye = normalize(input.myCameraPosition - input.myWPosition);

	NormalData normalData;
	normalData.normal = input.myNormals;
	normalData.binormal = input.myBinormal;
	normalData.tangent = input.myTangents;
	float3x3 tangentSpaceMatrix = CreateTangentSpaceMatrix(normalData);
	float3 normalSample = normalTexture.Sample(samplerState, input.myUV).xyz;
	normalSample = (normalSample * 2) - 1;
	attributes.objectNormal = normalize(mul(normalSample, tangentSpaceMatrix));

    
    float fogFactor = saturate((globalFogEnd - input.myViewPos.z) / (globalFogEnd - globalFogStart));
    if (input.myUseFog == 0.f)
    {
        fogFactor = 1.f;
    }

    output.myPosition = float4(input.myWPosition, 1.f);
    output.myNormal = float4(attributes.objectNormal, 1.f);
    output.myToEyeAndFogFactor = float4(attributes.toEye, fogFactor);
    output.myAlbedoAndRoughness = attributes.diffuseAndRoughness;
    output.myEmissiveAndMetallic = float4(attributes.emissive, attributes.metallic.r);
    output.myAO = float4(attributes.ambientOcclusion, 1.f);
    output.mySSAOViewNormal = float4(input.myViewNormal, 1.f);
    output.mySSAOViewPosition = float4(input.myViewPos, 1.f);

    return output;
}