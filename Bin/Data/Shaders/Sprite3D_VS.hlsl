#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
	float4x4 cameraOrientation;
	float4x4 toCamera;
	float4x4 projection;
	float4 camPosition;
}

cbuffer SpriteData : register(b1)
{
	float4x4 toView;
    float4 myPositionAndRotation;
    float4 myTextureSizeAndTargetSize;
	float4 myColor;
    float4 mySizeAndPivot;
    float4 myCrop;
}

float2x2 ComputeRotation(float aRotation)
{
    float c = cos(aRotation);
    float s = sin(aRotation);
    return float2x2(c, -s, s, c);
}

SpriteVertexToPixel VSMain(TexturedVertexInput input)
{
	SpriteVertexToPixel output;
    
	//float4 vertexObjectPos = float4(input.myPosition.xyz, 1.0f);

	//float4 vertexViewPos = mul(toCamera, vertexObjectPos);
	//float4 vertexProjectionPos = mul(projection, vertexViewPos);

    float2x2 rotation = ComputeRotation(myPositionAndRotation.z);

	//float2 projectionPos = vertexProjectionPos.xy;
    float4 position = input.myPosition;

    position.w = 1.f;

    position.x -= (mySizeAndPivot.z * 2) - 1.f;
    position.y += (mySizeAndPivot.w * 2) - 1.f;

    position.xy *= float2(mySizeAndPivot.xy);
    position.xy = mul(rotation, position.xy);

    position.x *= myTextureSizeAndTargetSize.x / myTextureSizeAndTargetSize.z;
    position.y *= myTextureSizeAndTargetSize.y / myTextureSizeAndTargetSize.w;


    position.x += (myPositionAndRotation.x * 2) - 1.f;
    position.y += (myPositionAndRotation.y * 2) - 1.f;

	//position.x += (0.5f * 2) - 1.f;
	//position.y += (0.5f * 2) - 1.f;

	//position.x += 0.0f;

    output.myPosition = position;
    output.myUV.x = min(max(input.myUV.x, myCrop.x), myCrop.z);
    output.myUV.y = min(max(input.myUV.y, myCrop.y), myCrop.w);

	output.myColor = myColor;

    return output;
};