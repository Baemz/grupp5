#include "CommonStructs.hlsli"

cbuffer CameraData : register(b0)
{
	float4x4 cameraOrientation;
	float4x4 toCamera;
	float4x4 projection;
}

LineVertexToGeometry VSMain(LineVertexInput input)
{
	LineVertexToGeometry output;

	output.myPosition = input.myPosition;
	output.myColor = input.myColor;
	output.myThickness = input.myThickness;

	return output;
};