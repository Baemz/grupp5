#include "CommonStructs.hlsli"

cbuffer LightData : register(b0)
{
    float3 fromLightDirection;
    float4 lightColor;
}

PixelOutput PSMain(ColoredVertexToPixel input)
{
    PixelOutput output;
    output.myColor = input.myColor;
    
    return output;
};