
struct ParticleInput
{
	float4 myPosition : POSITION;
	float4 myColor : COLOR;
    float mySize : PSIZE;
    float myRotation : PROTATION;
	float myLifetime : EMPTYFLOAT;
    float3 myDirection : PARTICLEDIRECTION;
    bool myGradient : PARTICLEGRADIENT;
    float myPRotationVel : PARTICLERVELOCITY;
};

struct ParticleToGeometry
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
    float mySize : PSIZE;
    float myRotation : PROTATION;
};

struct GeometryToPixel
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
	float2 myUV : UV;
};

struct ColoredVertexInput
{
    float4 myPosition : POSITION;
    float4 myColor : COLOR;
};

struct ColoredVertexToPixel
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
};

struct LineVertexInput
{
	float4 myPosition : POSITION;
	float4 myColor : COLOR;
	float myThickness : PSIZE;
};

struct LineVertexToGeometry
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
	float myThickness : PSIZE;
};

struct TexturedVertexInput
{
    float4 myPosition : POSITION;
    float2 myUV : UV;
};

struct TexturedVertexToPixel
{
    float4 myPosition : SV_POSITION;
    float2 myUV : UV;
};

struct PBLVertexInput
{
    float4 myPosition : POSITION;
    float4 myNormals : NORMAL;
    float4 myTangents : TANGENT;
    float4 myBinormal : BINORMAL;
    float2 myUV : UV;
    float4 myBones : BONE;
    float4 myWeights : WEIGHT;
};

struct PBLVertexToPixel
{
    float4 myPosition : SV_POSITION;
    float3 myWPosition : POSITION;
    float3 myNormals : NORMAL;
    float3 myTangents : TANGENT;
    float3 myBinormal : BINORMAL;
    float2 myUV : UV;
    float4 myBones : BONE;
    float4 myWeights : WEIGHT;
    float3 myCameraPosition : CAMERAPOSITION;
    float3 myViewPos : VIEWPOS;
    float3 myViewNormal : VIEWNORMAL;
    float myUseFog : USEFOG;
};

struct SkyboxVertexToPixel
{
    float4 myPosition : SV_POSITION;
	float3 myWPosition : POSITION;
    float3 myCameraPosition : CAMERAPOSITION;
    float2 myUV : UV;
};

struct SpriteVertexToPixel
{
	float4 myPosition : SV_POSITION;
	float4 myColor : COLOR;
	float2 myUV : UV;
};

struct PixelOutput
{
    float4 myColor : SV_Target;
};

struct DeferredDataOutput
{
    float4 myPosition : SV_TARGET0;
    float4 myNormal : SV_TARGET1;
    float4 myToEyeAndFogFactor : SV_TARGET2;
    float4 myAlbedoAndRoughness : SV_TARGET3;
    float4 myEmissiveAndMetallic : SV_TARGET4;
    float4 myAO : SV_TARGET5;
    float4 mySSAOViewPosition : SV_TARGET6;
    float4 mySSAOViewNormal : SV_TARGET7;
};