#include "CommonStructs.hlsli"
#include "DeferredLightFunc.hlsli"

SamplerState samplerState : register(s0);

Texture2D depthTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D toEyeTexture : register(t2);
Texture2D diffuseAndRoughnessTexture : register(t3);
Texture2D emissiveAndMetallicTexture : register(t4);
Texture2D ambientOcclusionTexture : register(t5);
Texture2D ssaoTexture : register(t6);
TextureCube environmentalTexture : register(t7);

cbuffer LightData : register(b0)
{
    float4 fromLightDirectionAndMipCount;
    float4 lightColor;
    float dirLightintensity;
}

cbuffer CameraData : register(b1)
{
    float4x4 cameraOrientation;
    float4x4 inversetoCamera;
    float4x4 inverseProjection;
    float4 camPosition;
}

float RoughToSPow(float roughness)
{
    return (2.f / (roughness * roughness)) - 2.f;
}

static const float k0 = 0.00098f;
static const float k1 = 0.9921f;
static const float fakeLysMaxSpecularPower = (2.f / (0.0014f * 0.0014f)) - 2.f;
static const float fMaxT = (exp2(-10.f / sqrt((2.f / (0.0014f * 0.0014f)) - 2.f)) - 0.00098f) / 0.9921f;
float GetSpecPowToMip(float fSpecPow, int nMips)
{
    float fSmulMaxT = (exp2(-10.f / sqrt(fSpecPow)) - k0) / k1;
    return float(nMips - 1 - 0) * (1.0f - clamp(fSmulMaxT / fMaxT, 0.0f, 1.0f));
}

float3 GetAmbientDiffuse(Attributes attributes, PBRData pbrData, float3 ssao)
{
    float3 metallicAlbedo = attributes.diffuseAndRoughness.rgb - (attributes.diffuseAndRoughness.rgb * attributes.metallic.rgb);
    float3 ambientLight = environmentalTexture.SampleLevel(samplerState, attributes.objectNormal.xyz, fromLightDirectionAndMipCount.w - 4).rgb;
    return metallicAlbedo * ambientLight * attributes.ambientOcclusion.rgb * ssao * (float3(1.f, 1.f, 1.f) - pbrData.reflectionFresnel);
}

float3 GetAmbientSpecular(Attributes attributes, PBRData pbrData, float3 ssao)
{
    float3 reflectionVector = -reflect(attributes.toEyeAndFogFactor.xyz, attributes.objectNormal.xyz);
    float fakeLysSpecularPower = RoughToSPow(attributes.diffuseAndRoughness.a);
    float lysMipMap = GetSpecPowToMip(fakeLysSpecularPower, fromLightDirectionAndMipCount.w);

    float3 ambientLight = environmentalTexture.SampleLevel(samplerState, reflectionVector.xyz, lysMipMap).xyz;
    return ambientLight * attributes.ambientOcclusion.rgb * ssao * pbrData.reflectionFresnel;
}

PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    Attributes attributes;
    
    attributes.diffuseAndRoughness = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV);
    attributes.ambientOcclusion = ambientOcclusionTexture.Sample(samplerState, input.myUV);
    attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).xxxx;
    attributes.objectNormal = normalTexture.Sample(samplerState, input.myUV);
    attributes.toEyeAndFogFactor = toEyeTexture.Sample(samplerState, input.myUV);
    
    // Lambert
    float3 lightDir = normalize(fromLightDirectionAndMipCount.xyz);
    float3 lambert = GetLambert(attributes, lightDir);

    // Fresnel
    float3 fresnel = GetFresnel(attributes, lightDir);

    // Reflection-fresnel
    float3 refFresnel = GetReflectionFresnel(attributes);

    // Distribution
    float distribution = GetDistribution(attributes, lightDir);

    // Visability
    float visibility = GetVisibility(attributes, lightDir);

    PBRData pbrData;
    pbrData.lightColor = lightColor.rgb;
    pbrData.fresnel = fresnel;
    pbrData.reflectionFresnel = refFresnel;
    pbrData.lambert = lambert;
    pbrData.distribution = distribution;
    pbrData.visibility = visibility;

    const float3 ssao = ssaoTexture.Sample(samplerState, input.myUV).rrr;
    float3 ambientDiffuse = GetAmbientDiffuse(attributes, pbrData, ssao);
    float3 ambientSpecular = GetAmbientSpecular(attributes, pbrData, ssao);
    
    output.myColor = float4(ambientDiffuse + ambientSpecular, 1.0f);
    return output;
}