#include "CommonStructs.hlsli"
#include "DeferredLightFunc.hlsli"

SamplerState samplerState : register(s0);

Texture2D depthTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D toEyeTexture : register(t2);
Texture2D diffuseAndRoughnessTexture : register(t3);
Texture2D emissiveAndMetallicTexture : register(t4);
Texture2D ambientOcclusionTexture : register(t5);

cbuffer SpotLightData : register(b0)
{
    float4 myPosition;
    float3 myColor;
    float myAngle;
    float3 myDirection;
    float myRange;
    float myIntensity;
}

cbuffer CameraData : register(b1)
{
    float4x4 cameraOrientation;
    float4x4 inversetoCamera;
    float4x4 inverseProjection;
    float4 camPosition;
}

PixelOutput PSMain(TexturedVertexInput input)
{
    PixelOutput output;
    Attributes attributes;
    
    attributes.diffuseAndRoughness = diffuseAndRoughnessTexture.Sample(samplerState, input.myUV);
    attributes.ambientOcclusion = ambientOcclusionTexture.Sample(samplerState, input.myUV);
    attributes.metallic = emissiveAndMetallicTexture.Sample(samplerState, input.myUV).xxxx;
    attributes.objectNormal = normalTexture.Sample(samplerState, input.myUV);
    attributes.toEyeAndFogFactor = toEyeTexture.Sample(samplerState, input.myUV);

    /*float depth = depthTexture.SampleLevel(samplerState, input.myUV, 0).r;
    float4 h = float4((input.myUV.x * 2.0f - 1.0f), (input.myUV.y * 2.0f - 1.0f), depth, 1.0f);
    float4 pos = mul(inverseProjection, h);
    pos = mul(inversetoCamera, pos);
    pos.xyz = pos.xyz / pos.w;
    pos.y *= -1.f;*/

    attributes.worldPosition = depthTexture.Sample(samplerState, input.myUV);
    
    float3 toLight = myPosition.xyz - attributes.worldPosition.xyz;
    float toLightDistance = distance(myPosition.xyz, attributes.worldPosition.xyz);
    toLight = normalize(toLight);
    float lightRange2 = myRange * myRange;

    float lambertAttenuation = saturate(dot(attributes.objectNormal.xyz, toLight));
    float linearAttenuation = toLightDistance / lightRange2;
    linearAttenuation = 1.f - linearAttenuation;
    linearAttenuation = saturate(linearAttenuation);
    float physicalAttenuation = 1.f / toLightDistance;

    float halfangle = myAngle / 2.f;
    float cutPoint = 1.f - (halfangle / 90.f);

    float3 fromLight = normalize(-toLight);
    float3 dir = normalize(myDirection);
    float a = saturate(dot(fromLight, dir));

    float angleAttenuation = saturate((a - cutPoint) * (1.f / (1.f - cutPoint)));

    float attenuation = lambertAttenuation * linearAttenuation * physicalAttenuation * angleAttenuation;
    float4 resColor = float4(attributes.diffuseAndRoughness.rgb * attenuation * (myColor.rgb * myIntensity), 1.0f);

    // Lambert
    float3 lightDir = toLight;
    float3 lambert = GetLambert(attributes, lightDir);

    // Fresnel
    float3 fresnel = GetFresnel(attributes, lightDir);

    // Reflection-fresnel
    float3 refFresnel = GetReflectionFresnel(attributes);

    // Distribution
    float distribution = GetDistribution(attributes, lightDir);

    // Visability
    float visibility = GetVisibility(attributes, lightDir);

    PBRData pbrData;
    pbrData.lightColor = myColor.rgb * myIntensity;
    pbrData.fresnel = fresnel;
    pbrData.reflectionFresnel = refFresnel;
    pbrData.lambert = lambert;
    pbrData.distribution = distribution;
    pbrData.visibility = visibility;
    
    float3 directDiffuse = GetDirectDiffuse(attributes, pbrData);
    float3 directSpecular = GetDirectSpecular(attributes, pbrData);
    
    resColor *= float4(directDiffuse + directSpecular, 1.0f);
    output.myColor = resColor;
    return output;
}