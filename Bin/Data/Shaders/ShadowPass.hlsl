#include "CommonStructs.hlsli"
cbuffer DepthMVP : register(b1)
{
	float4x4 depthMVP;
}


TexturedVertexToPixel VSMain(PBLVertexInput input)
{
	TexturedVertexToPixel output;

	float4 vertexPos = mul(depthMVP, float4(input.myPosition.xyz, 1));
	output.myPosition = vertexPos;
	return output;
}

PixelOutput PSMain(TexturedVertexToPixel input)
{
	PixelOutput output;
	return output;
}