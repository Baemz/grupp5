#pragma once

enum class EButtonState
{
	Pressed,
	Released,
	Down,
	Up,
	NONE
};

enum class EMessageType
{
	KeyInput,
	MouseInput,
	MouseClickInput,
	RawMouseInput,
	StoryEvent,
	NONE,
};

struct SMessage
{
	EMessageType messageType = EMessageType::NONE;
};

struct SInputMessage : public SMessage
{
	unsigned char key;
};

struct SKeyMessage : public SInputMessage
{
	EButtonState state = EButtonState::NONE;
};

struct SMouseDataMessage : public SInputMessage
{
	unsigned long x;
	unsigned long y;
	long deltaX;
	long deltaY;
	long scrollDelta;
};

struct SMouseClickMessage : public SMouseDataMessage
{
	SMouseClickMessage(const SMouseDataMessage& aMouseDataMsg)
	{
		messageType = EMessageType::MouseClickInput;
		x = aMouseDataMsg.x;
		y = aMouseDataMsg.y;
		deltaX = aMouseDataMsg.deltaX;
		deltaY = aMouseDataMsg.deltaY;
		scrollDelta = aMouseDataMsg.scrollDelta;
	}

	EButtonState state = EButtonState::NONE;
};

struct SRawMouseMoveMessage : public SInputMessage
{
	long x;
	long y;
};