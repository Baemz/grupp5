#pragma once
#include "MessageStructs.h"
class IEventListener
{
public:
	IEventListener() {};
	virtual ~IEventListener() {};

	virtual void ReceieveEvent(SMessage& aMessage) = 0;
};