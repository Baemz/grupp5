#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "MessageStructs.h"
#include <mutex>

class IEventListener;
namespace CommonUtilities
{
	class InputManager;
}
namespace hse
{
	class CEventManager
	{
	public:
		~CEventManager();

		static void Create();
		static void Destroy();
		static CEventManager* Get();

		void Update();
		void ResetInput();

		void AttachInputListener(IEventListener* aListener);
		void DetachInputListener(IEventListener* aListener);

		void AttachInputManager(CU::InputManager* aInputManager);

	private:
		CEventManager();

		static CEventManager* ourInstance;

		void UpdateInputEvents();

		CU::InputManager* myInputManager;
		CU::GrowingArray<IEventListener*> myEventListeners;
		CU::GrowingArray<IEventListener*> myInputEventListeners;

		std::mutex myMutex;
	};
}
