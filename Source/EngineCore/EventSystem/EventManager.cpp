#include "stdafx.h"
#include "EventManager.h"
#include "EventListener.h"
#include "../CommonUtilities/InputManager.h"

namespace hse
{
	CEventManager* CEventManager::ourInstance = nullptr;

	CEventManager::CEventManager()
		: myInputManager(nullptr)
	{
	}

	CEventManager::~CEventManager()
	{
	}

	void CEventManager::UpdateInputEvents()
	{
		if (myInputManager == nullptr)
		{
			return;
		}

		if (myInputManager->LostFocus())
		{
			return;
		}

		for (unsigned char key = static_cast<unsigned char>(CU::Keys::START); key < static_cast<unsigned char>(CU::Keys::COUNT); ++key)
		{
			SKeyMessage keyMsg;
			keyMsg.messageType = EMessageType::KeyInput;
			keyMsg.key = key;
			if (myInputManager->WasKeyJustPressed(key))
			{
				keyMsg.state = EButtonState::Pressed;
			}
			else if (myInputManager->IsKeyDown(key))
			{
				keyMsg.state = EButtonState::Down;
			}
			else if (myInputManager->WasKeyJustReleased(key))
			{
				keyMsg.state = EButtonState::Released;
			}
			else if (myInputManager->IsKeyUp(key))
			{
				keyMsg.state = EButtonState::Up;
			}
			if (keyMsg.state != EButtonState::NONE)
			{
				for (unsigned short i = 0; i < myInputEventListeners.Size(); ++i)
				{
					myInputEventListeners[i]->ReceieveEvent(keyMsg);
				}
			}
		}

		SRawMouseMoveMessage rawMouseMsg;
		rawMouseMsg.messageType = EMessageType::RawMouseInput;
		rawMouseMsg.x = myInputManager->GetRawMouseXMovementSinceLastFrame();
		rawMouseMsg.y = myInputManager->GetRawMouseYMovementSinceLastFrame();

		if (rawMouseMsg.x != 0 || rawMouseMsg.y != 0)
		{
			for (unsigned short i = 0; i < myInputEventListeners.Size(); ++i)
			{
				myInputEventListeners[i]->ReceieveEvent(rawMouseMsg);
			}
		}

		SMouseDataMessage mouseDataMsg;
		mouseDataMsg.messageType = EMessageType::MouseInput;
		mouseDataMsg.x = myInputManager->GetMousePositionX();
		mouseDataMsg.y = myInputManager->GetMousePositionY();
		mouseDataMsg.deltaX = myInputManager->GetMouseXMovementSinceLastFrame();
		mouseDataMsg.deltaY = myInputManager->GetMouseYMovementSinceLastFrame();
		mouseDataMsg.scrollDelta = myInputManager->GetScrollMovementSinceLastFrame();

		for (unsigned short i = 0; i < myInputEventListeners.Size(); ++i)
		{
			myInputEventListeners[i]->ReceieveEvent(mouseDataMsg);
		}

		for (unsigned char mButton = 1; mButton <= 3; ++mButton)
		{
			SMouseClickMessage mouseClickMsg(mouseDataMsg);
			mouseClickMsg.messageType = EMessageType::MouseClickInput;
			mouseClickMsg.key = mButton;

			if (myInputManager->WasMouseButtonJustPressed(mButton))
			{
				mouseClickMsg.state = EButtonState::Pressed;
			}
			else if (myInputManager->IsMouseButtonDown(mButton))
			{
				mouseClickMsg.state = EButtonState::Down;
			}
			else if (myInputManager->WasMouseButtonJustReleased(mButton))
			{
				mouseClickMsg.state = EButtonState::Released;
			}

			if (mouseClickMsg.state != EButtonState::NONE)
			{
				for (unsigned short i = 0; i < myInputEventListeners.Size(); ++i)
				{
					myInputEventListeners[i]->ReceieveEvent(mouseClickMsg);
				}
			}
		}
	}

	void CEventManager::Create()
	{
		if (ourInstance == nullptr)
		{
			ourInstance = hse_new(CEventManager());
		}
	}

	void CEventManager::Destroy()
	{
		SAFE_DELETE(ourInstance);
	}

	CEventManager* CEventManager::Get()
	{
		return ourInstance;
	}

	void CEventManager::Update()
	{
		UpdateInputEvents();
	}

	void CEventManager::AttachInputListener(IEventListener* aListener)
	{
		myInputEventListeners.Add(aListener);
	}

	void CEventManager::DetachInputListener(IEventListener* aListener)
	{
		myInputEventListeners.RemoveCyclic(aListener);
	}
	void CEventManager::AttachInputManager(CU::InputManager* aInputManager)
	{
		myInputManager = aInputManager;
	}
}