#pragma once

// STD-includes
#include <map>
#include <vector>
#include <array>
#include <algorithm>
#include <string>
#include <functional>
#include <cassert>

// EngineCore-includes
#include "MemoryPool/MemoryPool.h"
#include "Logger/Logger.h"

// CommonUtilities-includes
#include "../CommonUtilities/Stopwatch.h"

// Defines
#define SAFE_RELEASE(ptr) if (ptr != nullptr){ ptr->Release(); ptr = nullptr; }
#define SAFE_LRELEASE(ptr) if (ptr != nullptr){ ptr->release(); ptr = nullptr; }
#define SAFE_DELETE(ptr) hse_delete(ptr)