﻿#include "stdafx.h"
#include "MemoryPool.h"
#include <mutex>
#ifdef _DEBUG
#include <iostream>
#endif // _DEBUG
#ifdef HSE_CMEMORYPOOL_SAFEGUARD_BYTES
#include "..\CommonUtilities\ThreadHelper.h"
#endif // HSE_CMEMORYPOOL_SAFEGUARD_BYTES

#ifdef max
#undef max
#endif // max


namespace hse
{
	namespace MemoryPool🔒
	{
		std::mutex ourMemoryPoolMutex;
	}

	CMemoryPool* CMemoryPool::ourInstance(nullptr);

	CMemoryPool::CMemoryPool()
		: myIsInitiated(false)
		, myDataMemory(nullptr)
		, mySize(0)
#ifdef HSE_CMEMORYPOOL_SAFEGUARD_BYTES
		, mySafeGuardThread(nullptr)
		, mySafeGuardForceQuit(false)
#endif // HSE_CMEMORYPOOL_SAFEGUARD_BYTES
	{
	}

	CMemoryPool::~CMemoryPool()
	{
		bool somethingLeft(!Destroy(false));

		assert("Memory Leak!!!!!111!!" && (somethingLeft == false));

		Destroy(true);
	}

	bool CMemoryPool::Init(unsigned long long aMaxBytes)
	{
		if (myIsInitiated == false)
		{
			myIsInitiated = true;

			myDataMemory = new char[aMaxBytes];
			mySize = aMaxBytes;

			myDataFree.insert({ mySize, 0 });

#ifdef HSE_CMEMORYPOOL_SAFEGUARD_BYTES
			mySafeGuardThread = new std::thread(&CMemoryPool::SafeGuardUpdate, this);
			CU::ThreadHelper::SetThreadName(mySafeGuardThread, "HSE | MemoryPool SafeGuard");
#endif // HSE_CMEMORYPOOL_SAFEGUARD_BYTES

			return true;
		}

		return false;
	}

	bool CMemoryPool::Destroy(bool aForced)
	{
		if (myIsInitiated == false)
		{
			return true;
		}

#ifdef _DEBUG
		std::cout << "Shutting down memory pool... (might take some time, something is wrong if it does)" << std::endl;
#endif // _DEBUG

#ifdef HSE_CMEMORYPOOL_SAFEGUARD_BYTES
		if (mySafeGuardThread != nullptr)
		{
			mySafeGuardForceQuit = true;
			mySafeGuardThread->join();
			delete mySafeGuardThread;
			mySafeGuardThread = nullptr;
		}
#endif // HSE_CMEMORYPOOL_SAFEGUARD_BYTES

		std::lock_guard<std::mutex> lockGuard(MemoryPool🔒::ourMemoryPoolMutex);

		if ((aForced == false) && (myDataSaved.empty() == false))
		{
#ifdef _DEBUG
			std::cout << std::endl;
			std::cout << "Memory leaked! " << myDataSaved.size() << " leaks" << std::endl;
			for (auto& currentSavedData : myDataSaved)
			{
				std::cout << "\t{" << std::endl;
				std::cout << "\t\tFileName: \"" << currentSavedData.second.myFileName << "\"" << std::endl;
				std::cout << "\t\tFunctionName: \"" << currentSavedData.second.myFunctionName << "\"" << std::endl;
				std::cout << "\t\tLine: \"" << currentSavedData.second.myLineNumber << "\"" << std::endl;
				std::cout << "\t}," << std::endl;
			}
			std::cout << myDataSaved.size() << " leaks" << std::endl;
#endif // _DEBUG
			return false;
		}

		delete[] myDataMemory;
		myDataMemory = nullptr;

		mySize = 0;

		myDataSaved.clear();
		myDataFree.clear();

		myIsInitiated = false;

		return true;
	}

	unsigned long long CMemoryPool::GetFreeMemoryInBytes()
	{
		std::lock_guard<std::mutex> lockGuard(MemoryPool🔒::ourMemoryPoolMutex);
		unsigned long long amountFree(0);
		for (auto& freeIterator : ourInstance->myDataFree)
		{
			amountFree += freeIterator.first;
		}
		return amountFree;
	}

	unsigned long long CMemoryPool::GetUsedMemoryInBytes()
	{
		return (ourInstance->mySize - GetFreeMemoryInBytes());
	}

	const unsigned long long& CMemoryPool::GetMaxMemoryInBytes()
	{
		return ourInstance->mySize;
	}

	bool CMemoryPool::CreateData(void** aData, const std::size_t& aSize, std::size_t aArraySize, std::size_t aSizeOfType
#ifdef _DEBUG
		, const char* aFileName, const char* aFunctionName, const int aCodeLine
#endif // _DEBUG
	)
	{
		std::lock_guard<std::mutex> lockGuard(MemoryPool🔒::ourMemoryPoolMutex);

		const std::size_t sizeToUse(aSize
#ifdef HSE_CMEMORYPOOL_SAFEGUARD_BYTES
			+ HSE_CMEMORYPOOL_SAFEGUARD_BYTES
#endif // HSE_CMEMORYPOOL_SAFEGUARD_BYTES
		);

		auto firstMemoryIterator(myDataFree.lower_bound(sizeToUse));

		if (firstMemoryIterator == myDataFree.end())
		{
			return false;
		}

		const unsigned long long sizeOfDataFound(firstMemoryIterator->first);
		const unsigned long long idAtDataFound(firstMemoryIterator->second);

		const unsigned long long newDataSize(sizeOfDataFound - sizeToUse);
		const unsigned long long newDataId(idAtDataFound + sizeToUse);

		unsigned long long sizeDifference(0);
		if (aArraySize > 0)
		{
			sizeDifference = aSize - (aArraySize * aSizeOfType);
		}

		myDataFree.erase(firstMemoryIterator);
		if (newDataSize > 0)
		{
			myDataFree.insert({ newDataSize, newDataId });
		}

		*aData = myDataMemory + idAtDataFound;

#ifdef HSE_CMEMORYPOOL_SAFEGUARD_BYTES
		char* dataAsCharPointer((char*)(*aData));
		const int charValue(0xff);
		for (unsigned char byteIndex = 0; byteIndex < HSE_CMEMORYPOOL_SAFEGUARD_BYTES; ++byteIndex)
		{
			dataAsCharPointer[aSize + byteIndex] = (*(char*)(&charValue));
		}
#endif // HSE_CMEMORYPOOL_SAFEGUARD_BYTES

		myDataSaved.insert({ (char*)*aData + sizeDifference, SDataSaved(idAtDataFound, sizeToUse, aSizeOfType, sizeDifference, aArraySize
#ifdef _DEBUG
			, aFileName, aFunctionName, aCodeLine
#endif // _DEBUG
		) });

		return true;
	}

	bool CMemoryPool::RemoveData(void** aData)
	{
		std::lock_guard<std::mutex> lockGuard(MemoryPool🔒::ourMemoryPoolMutex);

		auto dataIterator(myDataSaved.find(*aData));

		if (dataIterator == myDataSaved.end())
		{
			return false;
		}

		const unsigned long long sizeOfData(dataIterator->second.mySize);
		const unsigned long long idOfData(dataIterator->second.myId);

		myDataFree.insert({ sizeOfData, idOfData });
		myDataSaved.erase(dataIterator);

		return true;
	}

	const unsigned long long CMemoryPool::GetAmountOfObjectsAtPointer(void* aData) const
	{
		std::lock_guard<std::mutex> lockGuard(MemoryPool🔒::ourMemoryPoolMutex);

		auto dataIterator(myDataSaved.find(aData));

		if (dataIterator == myDataSaved.end())
		{
			return 0;
		}

		unsigned long long amountOfObjects(1);
		if (dataIterator->second.myArraySize > 0)
		{
			amountOfObjects = dataIterator->second.myArraySize;
		}

		return amountOfObjects;
	}

#ifdef HSE_CMEMORYPOOL_SAFEGUARD_BYTES
	void CMemoryPool::SafeGuardUpdate()
	{
		std::unique_lock<std::mutex> lockGuard(MemoryPool🔒::ourMemoryPoolMutex, std::defer_lock);

		while (mySafeGuardForceQuit == false)
		{
			lockGuard.lock();
			for (auto& currentData : myDataSaved)
			{
				char* dataAsCharPointer((char*)(currentData.first));
				dataAsCharPointer -= currentData.second.mySizeDifference;
				const unsigned int guardBytes(*(int*)&(dataAsCharPointer[currentData.second.mySize - HSE_CMEMORYPOOL_SAFEGUARD_BYTES]));
				assert("Something has accessed memory they did not own!!! very bad" && (guardBytes == std::numeric_limits<unsigned int>::max()));
			}
			lockGuard.unlock();
		}
	}
#endif // HSE_CMEMORYPOOL_SAFEGUARD_BYTES
}
