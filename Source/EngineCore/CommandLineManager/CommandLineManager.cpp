#include "stdafx.h"
#include "CommandLineManager.h"
#include <unordered_map>

using namespace hse;

static std::unordered_map<std::wstring, std::vector<std::wstring>> myCommands;

void hse::CCommandLineManager::Init()
{
	wchar_t** argv(__wargv);
	const int argc(__argc);

	std::wstring lastCommandEntered;

	for (int i = 1; i < argc; ++i)
	{
		std::wstring arg(argv[i]);
		if (!arg.empty())
		{
			if (arg.front() != '-')
			{
				myCommands[lastCommandEntered].push_back(arg);
			}
			else
			{
				if (myCommands.find(arg) == myCommands.end())
				{
					myCommands[arg] = std::vector<std::wstring>();
					lastCommandEntered = arg;
				}
			}
		}
	}
}

bool hse::CCommandLineManager::HasParameter(const wchar_t* aParam)
{
	if (myCommands.find(aParam) != myCommands.end())
	{
		return true;
	}
	return false;
}

bool hse::CCommandLineManager::HasArgument(const wchar_t* aParam, const wchar_t* aArg)
{
	if (myCommands.find(aParam) != myCommands.end())
	{
		for (int index = 0; index < myCommands[aParam].size(); ++index)
		{
			if (myCommands[aParam][index] == aArg)
			{
				return true;
			}
		}
	}
	return false;
}
