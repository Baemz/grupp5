#pragma once
/* W.I.P. */

namespace AudioInterfaceWraper
{
	const unsigned int LoadAudioAndGetID(const char* aAudioPath);
	const bool PlayAudio(const unsigned int aAudioID);
	const bool UnloadAudio(const unsigned int aAudioID);

	void SetSoundPosition(const unsigned int aAudioID, const float aPosX, const float aPosY, const float aPosZ);
	void SetSoundPosition(const unsigned int aAudioID, const float aPos[3]);
	void SetListenerPosition(const float aPosX, const float aPosY, const float aPosZ);
	void SetListenerPosition(const float aPos[3]);
};

namespace AIW = AudioInterfaceWraper;