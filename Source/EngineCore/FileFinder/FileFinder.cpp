#include "stdafx.h"
#include "FileFinder.h"
#include <codecvt>
#include <windows.h>
#include <filesystem>
#include <PathCch.h>
#pragma comment(lib, "Pathcch.lib")

namespace hse
{
	const bool CFileFinder::GetFilePathsRecursive(CU::GrowingArray<std::string>& aPathsToFilesFound, const char* aFileExtension, const char* aRootFolderSearchPath)
	{
		if (aFileExtension == nullptr || aRootFolderSearchPath == nullptr)
		{
			return false;
		}

		std::experimental::filesystem::path rootPath = std::experimental::filesystem::current_path().append(aRootFolderSearchPath);
		GetPathsRecursive(aPathsToFilesFound, aFileExtension, aRootFolderSearchPath, rootPath.c_str());

		return true;
	}
	bool CFileFinder::FindFilesInFolder(CU::GrowingArray<std::string>& aPathsToFilesFound, const char* aFileExtension, const char* aRootFolderSearchPath)
	{
		if (aFileExtension == nullptr)
		{
			return false;
		}

		std::wstring rootFolder;
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

		HMODULE hModule(GetModuleHandleW(nullptr));
		if (hModule == nullptr)
		{
			return false;
		}
		wchar_t path[MAX_PATH];
		DWORD pathSize(GetModuleFileNameW(hModule, path, MAX_PATH));
		if (pathSize == FALSE)
		{
			return false;
		}
		PathCchRemoveFileSpec(path, pathSize);
		rootFolder = path;

		if (aRootFolderSearchPath != nullptr)
		{
			rootFolder += L"\\" + converter.from_bytes(aRootFolderSearchPath);
		}

		CU::GrowingArray<std::wstring> pathsToFilesFoundW;
		if (FindFilesInFolder(pathsToFilesFoundW, converter.from_bytes(aFileExtension).c_str(), rootFolder.c_str()) == false)
		{
			return false;
		}

		for (unsigned short pathIndex = 0; pathIndex < pathsToFilesFoundW.Size(); ++pathIndex)
		{
			aPathsToFilesFound.Add(converter.to_bytes(pathsToFilesFoundW[pathIndex]));
		}

		return true;
	}

	bool CFileFinder::FindFilesInFolder(CU::GrowingArray<std::wstring>& aPathsToFilesFound, const wchar_t* aFileExtension, const wchar_t* aRootFolderSearchPath)
	{
		if (aFileExtension == nullptr)
		{
			return false;
		}
		
		std::wstring rootFolder;
		if (aRootFolderSearchPath == nullptr)
		{
			HMODULE hModule(GetModuleHandleW(nullptr));
			if (hModule == nullptr)
			{
				return false;
			}
			wchar_t path[MAX_PATH];
			DWORD pathSize(GetModuleFileNameW(hModule, path, MAX_PATH));
			if (pathSize == FALSE)
			{
				return false;
			}
			PathCchRemoveFileSpec(path, pathSize);
			rootFolder = path;
		}
		else
		{
			rootFolder = aRootFolderSearchPath;
		}

		return ScanFolder(aPathsToFilesFound, aFileExtension, rootFolder.c_str(), L"");
	}

	const std::string CFileFinder::FindFilePath(const char* aFileName, const char* aFileExtension, const char* aRootFolderSearchPath)
	{
		if (aFileExtension == nullptr)
		{
			return std::string();
		}

		std::wstring rootFolder;
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

		HMODULE hModule(GetModuleHandleW(nullptr));
		if (hModule == nullptr)
		{
			return std::string();
		}
		wchar_t path[MAX_PATH];
		DWORD pathSize(GetModuleFileNameW(hModule, path, MAX_PATH));
		if (pathSize == FALSE)
		{
			return std::string();
		}
		PathCchRemoveFileSpec(path, pathSize);
		rootFolder = path;

		if (aRootFolderSearchPath != nullptr)
		{
			rootFolder += L"\\" + converter.from_bytes(aRootFolderSearchPath);
		}

		CU::GrowingArray<std::wstring> pathsToFilesFoundW;
		if (FindFilesInFolder(pathsToFilesFoundW, converter.from_bytes(aFileExtension).c_str(), rootFolder.c_str()) == false)
		{
			return std::string();
		}

		for (unsigned short pathIndex = 0; pathIndex < pathsToFilesFoundW.Size(); ++pathIndex)
		{
			std::string npath(converter.to_bytes(pathsToFilesFoundW[pathIndex]));
			if (npath.find(aFileName) != npath.npos)
			{
				return npath;
			}
		}

		return std::string();
	}

	/* PRIVATE FUNCTIONS */

	void CFileFinder::GetPathsRecursive(CU::GrowingArray<std::string>& aPathsToFilesFound, const char* aFileExtension, const char* aRootFolderSearchPath, const wchar_t* aFolderPath)
	{
		std::experimental::filesystem::directory_iterator modelFolderIt(aFolderPath);
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
		std::string cutOffPath("");
		size_t cutIndex(0);

		for (auto& file : modelFolderIt)
		{
			if (file.path().has_extension())
			{
				if (converter.to_bytes(file.path().extension().c_str()) == aFileExtension)
				{
					cutOffPath = converter.to_bytes(file.path().c_str());
					cutIndex = cutOffPath.find(aRootFolderSearchPath);
					if (cutIndex != std::string::npos)
					{
						cutOffPath = cutOffPath.substr(cutIndex);
					}

					aPathsToFilesFound.Add(cutOffPath);
				}
			}
			else
			{
				GetPathsRecursive(aPathsToFilesFound, aFileExtension, aRootFolderSearchPath, file.path().c_str());
			}
		}
	}

	bool CFileFinder::ScanFolder(CU::GrowingArray<std::wstring>& aPathsToFilesFound, const wchar_t* aFileExtension, const wchar_t* aFolderSearchPath, const wchar_t* aFoldersIn)
	{
		WIN32_FIND_DATAW fileData;
		const std::wstring folderPath(aFolderSearchPath + std::wstring(L"\\*."));
		HANDLE fileHandle(FindFirstFileW(folderPath.c_str(), &fileData));
		if (fileHandle == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		do
		{
			const std::wstring fileName(fileData.cFileName);
			if ((fileName != L".") && (fileName != L".."))
			{
				const std::wstring relativeFilePath(std::wstring(L"\\") + fileName);
				if (ScanFolder(aPathsToFilesFound, aFileExtension, (aFolderSearchPath + relativeFilePath).c_str(), (aFoldersIn + relativeFilePath).c_str()) == false)
				{
					return false;
				}
			}
		} while (FindNextFileW(fileHandle, &fileData) == TRUE);

		if (FindClose(fileHandle) == FALSE)
		{
			return false;
		}

		const std::wstring filePath(aFolderSearchPath + std::wstring(L"\\*.") + aFileExtension);
		AddFiles(aPathsToFilesFound, filePath.c_str(), aFoldersIn);

		return true;
	}

	void CFileFinder::AddFiles(CU::GrowingArray<std::wstring>& aPathsToFilesFound, const wchar_t* aFilePath, const wchar_t* aFoldersIn)
	{
		WIN32_FIND_DATAW fileData;
		HANDLE fileHandle(FindFirstFileW(aFilePath, &fileData));
		if (fileHandle == INVALID_HANDLE_VALUE)
		{
			return;
		}

		std::wstring foldersIn(aFoldersIn + std::wstring(L"\\"));
		foldersIn = foldersIn.substr(foldersIn.find_first_of(L"\\") + 1);
		do
		{
			aPathsToFilesFound.Add(foldersIn + fileData.cFileName);
		} while (FindNextFileW(fileHandle, &fileData) == TRUE);

		FindClose(fileHandle);
	}
}
