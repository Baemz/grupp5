#pragma once
#ifdef FILEWATCHER_WRAPPER_USED
#include <thread>
#include "../CommonUtilities\GrowingArray.h"
#include <mutex>

namespace hse
{
	using FileWatchFunction = std::function<bool(void*, const char*)>;

	class CFileWatcher
	{
	public:
		CFileWatcher();
		~CFileWatcher();

		bool Init();

		bool AddFileToWatch(void* aThisCallback, const char* aFilePath, FileWatchFunction aFileWatchFunctionCallback, const bool aAddIfNotExisting = false);
		void RemoveFileToWatch(const char* aFilePath);

	private:
		struct SWatchData
		{
			SWatchData()
				: myThisCallback(nullptr)
				, myCallbackFunction(nullptr)
				, myLastTimeChanged(0)
			{};

			void* myThisCallback;
			std::string myFilePath;
			FileWatchFunction myCallbackFunction;
			long long myLastTimeChanged;
		};

		void WatchingUpdate(const volatile bool& aForceQuit);
		bool CheckIfFileHasChanged(SWatchData& aWatchData);

		CU::GrowingArray<SWatchData> myFilesToUpdateWatch;
		CU::GrowingArray<SWatchData> myFilesToAddToWatch;
		CU::GrowingArray<std::string> myFilesToRemoveToWatch;
		void* myChangeHandle;
		
		std::thread myWatchingThread;
		std::mutex myFilesToAddToWatchMutex;
		std::mutex myFilesToRemoveToWatchMutex;
		volatile bool myWatchingThreadForceQuit;

	};
}
#else
#error You need to use the FileWatcherWrapper instead
#endif // FILEWATCHER_WRAPPER_USED
