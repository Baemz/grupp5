#include "stdafx.h"
#define FILEWATCHER_WRAPPER_USED
#include "FileWatcher.h"
#undef FILEWATCHER_WRAPPER_USED
#include <windows.h>
#include <codecvt>
#include "..\CommonUtilities\ThreadHelper.h"
#include "WorkerPool\WorkerPool.h"

namespace hse
{
	CFileWatcher::CFileWatcher()
		: myChangeHandle(nullptr)
		, myWatchingThreadForceQuit(false)
	{
	}

	CFileWatcher::~CFileWatcher()
	{
		myWatchingThreadForceQuit = true;
		myWatchingThread.join();

		FindCloseChangeNotification(myChangeHandle);
	}

	bool CFileWatcher::Init()
	{
		myFilesToUpdateWatch.Init(32);
		myFilesToAddToWatch.Init(32);
		myFilesToRemoveToWatch.Init(32);

		HMODULE hModule = GetModuleHandleW(nullptr);
		wchar_t path[MAX_PATH];
		unsigned long pathSize(GetModuleFileNameW(hModule, path, MAX_PATH));

		if (pathSize == 0)
		{
			return false;
		}

		const std::wstring workingDirectoryPathWStringWithExe(path);
		const std::wstring workingDirectoryPathWString(workingDirectoryPathWStringWithExe.substr(0, (workingDirectoryPathWStringWithExe.find_last_of('\\') + 1)));

		myChangeHandle = FindFirstChangeNotificationW(workingDirectoryPathWString.c_str(), TRUE, FILE_NOTIFY_CHANGE_LAST_WRITE);
		if ((myChangeHandle == INVALID_HANDLE_VALUE) || (myChangeHandle == nullptr))
		{
			return false;
		}

		myWatchingThread = std::thread(&CFileWatcher::WatchingUpdate, this, std::cref<const volatile bool>(myWatchingThreadForceQuit));
		CU::ThreadHelper::SetThreadName(myWatchingThread, "HSE | FileWatcher: Checking&Callback thread");

		return true;
	}

	bool CFileWatcher::AddFileToWatch(void* aThisCallback, const char* aFilePath, FileWatchFunction aFileWatchFunctionCallback, const bool aAddIfNotExisting)
	{
		SWatchData watchData;
		watchData.myThisCallback = aThisCallback;
		watchData.myFilePath = aFilePath;
		watchData.myCallbackFunction = aFileWatchFunctionCallback;
		
		struct stat result;
		if (stat(watchData.myFilePath.c_str(), &result) == 0)
		{
			watchData.myLastTimeChanged = result.st_mtime;
		}
		else if (aAddIfNotExisting == false)
		{
			return false;
		}

		std::unique_lock<std::mutex> uniqueLock(myFilesToAddToWatchMutex);
		myFilesToAddToWatch.Add(watchData);
		uniqueLock.unlock();

		return true;
	}

	void CFileWatcher::RemoveFileToWatch(const char* aFilePath)
	{
		std::unique_lock<std::mutex> uniqueLock(myFilesToRemoveToWatchMutex);
		myFilesToRemoveToWatch.Add(aFilePath);
		uniqueLock.unlock();
	}

	void CFileWatcher::WatchingUpdate(const volatile bool& aForceQuit)
	{
		while (aForceQuit == false)
		{
			std::unique_lock<std::mutex> uniqueLockAdd(myFilesToAddToWatchMutex);
			myFilesToUpdateWatch.Add(myFilesToAddToWatch);
			myFilesToAddToWatch.RemoveAll();
			uniqueLockAdd.unlock();

			std::unique_lock<std::mutex> uniqueLockRemove(myFilesToRemoveToWatchMutex);
			for (unsigned short removeIndex(0); removeIndex < myFilesToRemoveToWatch.Size(); ++removeIndex)
			{
				const std::string& currentPath(myFilesToRemoveToWatch[removeIndex]);

				for (unsigned short fileIndex(0); fileIndex < myFilesToUpdateWatch.Size(); ++fileIndex)
				{
					if (myFilesToUpdateWatch[fileIndex].myFilePath == currentPath)
					{
						myFilesToUpdateWatch.RemoveCyclicAtIndex(fileIndex);
					}
				}
			}
			uniqueLockRemove.unlock();

			for (unsigned short fileIndex(0); fileIndex < myFilesToUpdateWatch.Size(); ++fileIndex)
			{
				SWatchData& watchData(myFilesToUpdateWatch[fileIndex]);

				if (CheckIfFileHasChanged(watchData) == false)
				{
					continue;
				}

				WORKER_POOL_QUEUE_WORK(watchData.myCallbackFunction, watchData.myThisCallback, watchData.myFilePath.c_str());
			}

			std::this_thread::yield();
		}
	}

	bool CFileWatcher::CheckIfFileHasChanged(SWatchData& aWatchData)
	{
		struct stat result;
		if ((stat(aWatchData.myFilePath.c_str(), &result) == 0) && (result.st_mtime != aWatchData.myLastTimeChanged))
		{
			aWatchData.myLastTimeChanged = result.st_mtime;
			return true;
		}

		return false;
	}
}
