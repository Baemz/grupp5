#pragma once
#include <functional>
#include <atomic>
#include "FileWatcher\FileWatcherWrapper.h"
#include "MemoryPool\MemoryPool.h"

namespace std
{
	class thread;
}

namespace hse {

	using callback_function_init = std::function<void()>;
	using callback_function_update = std::function<void(const float aDeltaTime)>;

	struct SEngineStartParams
	{
		callback_function_init initCallback;
		callback_function_update updateCallback;
	};

	namespace gfx
	{
		struct SWindowData;
		class CGraphicsEngine;
	}
	
	class CEngine
	{
	private:
		CMemoryPool myMemoryPool; // IMPORTANT: Needs to be first or crash (for destructor order last)

	public:
		CEngine();
		~CEngine();

		void Start();
		void Shutdown();

		void Init(const SEngineStartParams& aStartParams);
		void InitWithWindowData(const gfx::SWindowData& aWindowData, const SEngineStartParams& aStartParams);

	private:
		void RenderLoop();
		bool LoadConfig(gfx::SWindowData* aWindowData); 

		hse::gfx::CGraphicsEngine* graphicsEngine;
		CFileWatcherWrapper myFileWatcher;
		std::thread* myRenderThread;
		std::atomic_bool myShouldRun;
		volatile bool myShouldLoadConfig;

		callback_function_init myGameInitCallback;
		callback_function_update myGameUpdateCallback;
	};
}