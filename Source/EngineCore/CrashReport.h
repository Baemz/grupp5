#pragma once
#pragma warning(disable: 4091)
#include <windows.h>
#include <algorithm>
#include <string>
#include <fstream>
#include <dbghelp.h>
#include <psapi.h>
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "zip/ZipArchive.h"
#include "zip/ZipArchiveEntry.h"
#include "zip/ZipFile.h"
#pragma comment(lib, "dbghelp.lib")
#pragma comment(lib, "version.lib")

#ifdef max
#undef max
#endif // max

namespace hse {
	static inline void GetAdditionalInfo(std::wstring& fileName, SYSTEMTIME& t);
	inline bool FileExists(const char* aFileName) {
		struct stat buffer;
		return (stat(aFileName, &buffer) == 0);
	}
	inline void CreateMiniDump(EXCEPTION_POINTERS* someExceptionPointers)
	{
		HANDLE process = GetCurrentProcess();
		DWORD processID = GetCurrentProcessId();


		SYSTEMTIME t;
		GetLocalTime(&t);
		wchar_t name[4096];
		{
			auto nameEnd = name + GetModuleFileNameW(GetModuleHandleW(0), name, 4096);
			wsprintfW(nameEnd - strlen(".exe"),
				L"_%4d%02d%02d_%02d%02d.dmp",
				t.wYear, t.wMonth, t.wDay, t.wHour, t.wMinute);
		}

		wchar_t dirName[4096];
		GetModuleFileNameW(GetModuleHandleW(0), dirName, 4096);
		std::wstring nameStr(name);
		nameStr = nameStr.substr(nameStr.find_last_of(L"/\\") + 1);
		nameStr = L"\\" + nameStr;
		std::wstring dirStr(dirName); 
		auto pos = dirStr.find_last_of(L"/\\");
		if (pos != std::string::npos) {
			dirStr.erase(pos);
		}
		std::wstring finalDir(L"\\Crashes");
		finalDir = dirStr + finalDir;
		BOOL result = CreateDirectoryW(finalDir.c_str(), nullptr);
		std::wstring finalPath(finalDir + nameStr);
		//DWORD test = GetLastError();

		auto dumpFile = CreateFileW(finalPath.c_str(), GENERIC_WRITE,
			FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

		if (dumpFile != INVALID_HANDLE_VALUE && result != ERROR_PATH_NOT_FOUND)
		{

			MINIDUMP_TYPE type = MINIDUMP_TYPE(MINIDUMP_TYPE::MiniDumpNormal | MINIDUMP_TYPE::MiniDumpWithCodeSegs);
			MINIDUMP_EXCEPTION_INFORMATION info;
			info.ThreadId = GetCurrentThreadId();
			info.ExceptionPointers = someExceptionPointers;
			info.ClientPointers = FALSE;


			result = MiniDumpWriteDump(process, processID, dumpFile, type, (someExceptionPointers != nullptr) ? &info : 0, 0, 0);
			if (result)
			{
				CloseHandle(dumpFile);
				pos = nameStr.find_last_of(L".");
				if (pos != std::string::npos) {
					nameStr.erase(pos);
				}
				GetAdditionalInfo(nameStr, t);
				std::wstring screenshot(L"Crashes\\" + nameStr + L".dds");
				hse::gfx::CGraphicsEngineInterface::TakeScreenshot(screenshot.c_str());

				std::string nameString(nameStr.begin(), nameStr.end());
				std::string archiveName("Crashes" + nameString + ".zip");
				std::string dmpName("Crashes" + nameString + ".dmp");
				std::string addInfoName("Crashes" + nameString + "_AdditionalInfo.txt");
				std::string screenshotName("Crashes" + nameString + ".dds");
				std::string engineLogName("Crashes" + nameString + "_log_engine.log");
				std::string resourceLogName("Crashes" + nameString + "_log_resource.log");
				std::string gameLogName("Crashes" + nameString + "_log_game.log");
				
				ZipFile::AddFile(archiveName, dmpName);
				ZipFile::AddFile(archiveName, addInfoName);

				if (FileExists(screenshotName.c_str()))
				{
					ZipFile::AddFile(archiveName, screenshotName);
				}

				if (FileExists(engineLogName.c_str()))
				{
					ZipFile::AddFile(archiveName, engineLogName);
				}

				if (FileExists(resourceLogName.c_str()))
				{
					ZipFile::AddFile(archiveName, resourceLogName);
				}

				if (FileExists(gameLogName.c_str()))
				{
					ZipFile::AddFile(archiveName, gameLogName);
				}


				std::remove(dmpName.c_str());
				std::remove(addInfoName.c_str());
				std::remove(screenshotName.c_str());
				std::remove(engineLogName.c_str());
				std::remove(resourceLogName.c_str());
				std::remove(gameLogName.c_str());
				MessageBox(nullptr, TEXT("The program has crashed. A dump-file has been generated in [Bin/Crashes/...]. Please show this file for your programmers."), TEXT("ERROR"), MB_OK | MB_ICONERROR);
			}
			else
			{
				CloseHandle(dumpFile);
				// PRINT ERROR
			}
			std::exit(1);
		}
		else
		{
			// PRINT ERROR
		}
	}

	static inline void GetExeVersion(std::wstring& aFileName, std::wstring& aVersionString)
	{
		DWORD  verHandle = 0;
		UINT   size = 0;
		LPBYTE lpBuffer = NULL;
		DWORD  verSize = GetFileVersionInfoSizeW(aFileName.c_str(), &verHandle);

		if (verSize != NULL)
		{
			LPSTR verData = new char[verSize];

			if (GetFileVersionInfoW(aFileName.c_str(), verHandle, verSize, verData))
			{
				if (VerQueryValueW(verData, L"\\", (VOID FAR* FAR*)&lpBuffer, &size))
				{
					if (size)
					{
						VS_FIXEDFILEINFO *verInfo = (VS_FIXEDFILEINFO *)lpBuffer;
						if (verInfo->dwSignature == 0xfeef04bd)
						{
							aVersionString += std::to_wstring(static_cast<int>((verInfo->dwFileVersionMS >> 16) & 0xffff)) + L".";
							aVersionString += std::to_wstring(static_cast<int>((verInfo->dwFileVersionMS >> 0) & 0xffff)) + L".";
							aVersionString += std::to_wstring(static_cast<int>((verInfo->dwFileVersionLS >> 16) & 0xffff)) + L".";
							aVersionString += std::to_wstring(static_cast<int>((verInfo->dwFileVersionLS >> 0) & 0xffff));
						}
					}
				}
			}
			delete[] verData;
		}
	}

	inline const std::wstring GetCpuInfo()
	{
		int CPUInfo[4] = { -1 };
		unsigned   nExIds, i = 0;
		char CPUBrandString[0x40];
		// Get the information associated with each extended ID.
		__cpuid(CPUInfo, 0x80000000);
		nExIds = CPUInfo[0];
		for (i = 0x80000000; i <= nExIds; ++i)
		{
			__cpuid(CPUInfo, i);
			// Interpret CPU brand string
			if (i == 0x80000002)
				memcpy(CPUBrandString, CPUInfo, sizeof(CPUInfo));
			else if (i == 0x80000003)
				memcpy(CPUBrandString + 16, CPUInfo, sizeof(CPUInfo));
			else if (i == 0x80000004)
				memcpy(CPUBrandString + 32, CPUInfo, sizeof(CPUInfo));
		}
		std::string string(CPUBrandString);
		std::wstring ws(string.begin(), string.end());
		return ws;
	}

	static inline void CopyLog(const std::wstring& aFrom, const std::wstring& aTo)
	{
		std::ifstream fromLog;
		std::ofstream toLog;

		fromLog.open(aFrom, std::ifstream::binary);
		toLog.open(aTo, std::ofstream::binary);
		if (fromLog.good())
		{
			fromLog.ignore(std::numeric_limits<std::streamsize>::max());
			const std::streamsize byteLength = fromLog.gcount();

			fromLog.clear();
			fromLog.seekg(0, std::ios_base::beg);

			char* rawData = new char[byteLength];

			fromLog.read(rawData, byteLength);

			toLog.write(rawData, byteLength);
			delete[] rawData;
		}
		toLog.close();
		fromLog.close();
	}

	inline void GetAdditionalInfo(std::wstring& fileName, SYSTEMTIME& t)
	{
		std::wstring additionalInfoFileName(L"Crashes" + fileName + L"_AdditionalInfo.txt");
		std::wofstream outputFile;
		outputFile.open(additionalInfoFileName, std::ofstream::out);

		wchar_t userName[4096];
		DWORD userNameLength(4096);
		GetUserNameW(userName, &userNameLength);
		std::wstring usrName(userName);
		std::wstring version;

		wchar_t exe[4096];
		GetModuleFileNameW(GetModuleHandleW(0), exe, 4096);
		std::wstring exeName(exe);
		GetExeVersion(exeName, version);

		SYSTEM_INFO sysInfo; 
		GetSystemInfo(&sysInfo);

		MEMORYSTATUSEX statex; 
		statex.dwLength = sizeof(statex);
		GlobalMemoryStatusEx(&statex);

		PROCESS_MEMORY_COUNTERS_EX pmc;
		GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
		SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;

		std::wstring architecture;
		switch (sysInfo.wProcessorArchitecture)
		{
		case PROCESSOR_ARCHITECTURE_AMD64:
			architecture = L"x64";
			break;
		case PROCESSOR_ARCHITECTURE_INTEL:
			architecture = L"x86";
			break;
		case PROCESSOR_ARCHITECTURE_ARM:
			architecture = L"ARM";
			break;
		case PROCESSOR_ARCHITECTURE_UNKNOWN:
			architecture = L"UNKNOWN";
			break;
		}

		std::wstring cpuModel = GetCpuInfo();

		outputFile << "CRASH REPORT" << std::endl;
		outputFile << "------------" << std::endl;
		outputFile << "  Username: " << usrName << std::endl;
		outputFile << "  Time: " << t.wYear << "-" << t.wMonth << "-" << t.wDay << "_" << t.wHour << ":" << t.wMinute << std::endl;
		outputFile << "  Exe-version: " << version << std::endl;
		outputFile << std::endl;
		outputFile << "------------" << std::endl;
		outputFile << "SYSTEM INFO" << std::endl;
		outputFile << "------------" << std::endl;
		outputFile << "  CPU: " << cpuModel << std::endl;
		outputFile << "  CPU-architecture: " << architecture << std::endl;
		outputFile << "  #CPU Cores: " << sysInfo.dwNumberOfProcessors << std::endl;
		outputFile << "  Total RAM: " << statex.ullTotalPhys / 1048576 << "MB" << std::endl;
		outputFile << "  Available RAM: " << (statex.ullTotalPhys / 1048576) - (statex.ullAvailPageFile / 1048576) << "MB" << std::endl;
		outputFile << "  RAM used by this process: " << virtualMemUsedByMe / 1048576 << "MB /" << statex.ullTotalPhys / 1048576 << "MB" << std::endl;
		outputFile.close();

		CopyLog(L"log_engine.log", (L"Crashes" + fileName + L"_log_engine.log"));
		CopyLog(L"_log_resource.log", (L"Crashes" + fileName + L"_log_resource.log"));
		CopyLog(L"_log_game.log", (L"Crashes" + fileName + L"_log_game.log"));
	}
	
}


#pragma warning(default: 4091)