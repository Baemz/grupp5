#include "stdafx.h"
#include "Engine.h"
#include <windows.h>
#include <shlobj.h>
#include <thread>
#include "../GraphicsEngine/GraphicsEngine.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "../CommonUtilities/Timer.h"
#include "../CommonUtilities/InputManager.h"
#include "EventSystem/EventManager.h"

#include "JSON/json.hpp"
//#include "../ComponentSystem/ComponentFactory.h"
#include "WorkerPool/WorkerPool.h"
#include "../CommonUtilities/ThreadHelper.h"
#include "../CommonUtilities/Random.h"
#include "DebugHUD.h"

#include "../AudioEngine/AudioManager.h"
#include "../Script/ScriptManager.h"
static float globalPrevCPUpercent = 0.0f; // HACK
static float globalCurrCPUpercent = 0.0f; // HACK


// Use this define if you want to disable threaded rendering.
// If enabled the rendering will occur on the same thread as the game-logic
// and will be executed after the logic.
//#define DISABLE_THREADED_RENDERING


using namespace hse;

using json = nlohmann::json;

void ErrorCallback(const char* aError)
{
#ifndef _RETAIL
	ENGINE_LOG(aError);
#else
	aError;
#endif
}

hse::gfx::SWindowData globalWindowData;

CEngine::CEngine()
	: myShouldRun(false)
	, myGameInitCallback(nullptr)
	, myRenderThread(nullptr)
	, graphicsEngine(nullptr)
	, myGameUpdateCallback(nullptr)
	, myShouldLoadConfig(true)
{
}


CEngine::~CEngine()
{
	if (myRenderThread != nullptr && myRenderThread->joinable())
	{
		myRenderThread->join();
	}
	hse_delete(myRenderThread);
	hse_delete(graphicsEngine);
	//CComponentFactory::Destroy();
	CEventManager::Destroy();
	CLogger::Destroy();
}

#define KB * 1024llu
#define MB * 1024llu KB
#define GB * 1024llu MB

void CEngine::Start()
{
	myMemoryPool.Init(512 MB);
	CMemoryPool::ourInstance = &myMemoryPool;

	graphicsEngine = hse_new(gfx::CGraphicsEngine());

	CLogger::Create();
	ENGINE_LOG("Starting engine...");

	CWorkerPool workerPool;
	workerPool.Init();
	ENGINE_LOG("SUCCESS! Initiated worker pool");
	CWorkerPool::ourInstance = &workerPool;

	if (myShouldLoadConfig)
	{
		if (LoadConfig(&globalWindowData) == false)
		{
			globalWindowData.myWidth = 1280;
			globalWindowData.myHeight = 720;
			globalWindowData.myStartInFullScreen = false;
			globalWindowData.myWindowName = "[DEFAULT] Necrobyte Engine";
			globalWindowData.myUseBloom = true;
			globalWindowData.myUseFXAA = true;
			globalWindowData.myUseColorGrading = true;
			globalWindowData.myUseVSync = false;
		}
	}

	myFileWatcher.Init();
	CFileWatcherWrapper::ourInstance = &myFileWatcher;

	hse::gfx::CGraphicsEngineInterface::myGraphicsEngine = graphicsEngine;
	myShouldRun = graphicsEngine->Init(globalWindowData, myFileWatcher);

	CDebugHUD debugHUD;
	debugHUD.Init(0.5, 20.0, 1.0, 0.75);

	CEventManager::Create();
	CU::InputManager inputManager(true);
	CEventManager::Get()->AttachInputManager(&inputManager);;

	ENGINE_LOG("SUCCESS! Sucessfully initialized engine.");

	CU::InitRandom();

	myGameInitCallback();

	CU::ThreadHelper::SetThreadName(std::this_thread::get_id(), std::string("Main / HSE | Update/Logic")
#ifdef DISABLE_THREADED_RENDERING
	+ " and Renderer");
#else
	);
	myRenderThread = hse_new(std::thread(&CEngine::RenderLoop, this));
	CU::ThreadHelper::SetThreadName(myRenderThread, "HSE | Renderer");
#endif

	CU::Timer timer;
	MSG windowsMsg = {};
	while (myShouldRun)
	{
		while (PeekMessage(&windowsMsg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&windowsMsg);
			DispatchMessage(&windowsMsg);

			inputManager.HandleInput(windowsMsg.message, windowsMsg.wParam, windowsMsg.lParam);

			if (windowsMsg.message == WM_QUIT)
			{
				myShouldRun = false;
			}
		}
		CEventManager::Get()->Update();
		//CScriptManager::Get()->Update();

		//if (inputManager.WasKeyJustPressed('E')) // HACK
		//{
		//	CAudioManager::PostEventAndGetID("Fire_Plasma_Player", 1);
		//}
		//if (inputManager.WasKeyJustPressed('Q'))
		//{
		//	CAudioManager::PostEventAndGetID("Mouse_Click", 1);
		//}
		//if (inputManager.WasKeyJustPressed('P'))
		//{
		//	CAudioManager::SetState("ToggleMusic", "Pause");
		//	//CAudioManager::PostEventAndGetID("Pause_Music", 1);
		//}
		//if (inputManager.WasKeyJustPressed('O'))
		//{
		//	CAudioManager::SetState("ToggleMusic", "Play");
		//	//CAudioManager::PostEventAndGetID("Resume_Music", 1);
		//}
		//if (inputManager.IsKeyDown('Z') && globalCurrCPUpercent > 0.0f)
		//{
		//	globalCurrCPUpercent -= timer.GetDeltaTime() * 10.0f;
		//	if (globalCurrCPUpercent < 0.0f) globalCurrCPUpercent = 0.0f;
		//}
		//if (inputManager.IsKeyDown('X') && globalCurrCPUpercent < 100.0f)
		//{
		//	globalCurrCPUpercent += timer.GetDeltaTime() * 10.0f;
		//	if (globalCurrCPUpercent > 100.0f) globalCurrCPUpercent = 100.0f;
		//}
		//if (globalPrevCPUpercent != globalCurrCPUpercent)
		//{
		//	globalPrevCPUpercent = globalCurrCPUpercent;
		//	CAudioManager::SetRTPC("AlarmState", globalCurrCPUpercent, 1);

		//	printf("Current CPU percent: %f\n", globalCurrCPUpercent);
		//}

		//time = clock.now(); // Limit max fps for game-loop
		timer.Update();
		float deltaTime(timer.GetDeltaTime());
		if (deltaTime > 0.05f)
		{
			deltaTime = 0.05f;
		}

		myGameUpdateCallback(deltaTime);

		debugHUD.Update(deltaTime);
		debugHUD.Render();
		graphicsEngine->EndGameUpdateFrame();

#ifdef DISABLE_THREADED_RENDERING
		graphicsEngine.BeginFrame();
		graphicsEngine.RenderFrame(deltaTime, (float)timer.GetTotalTime());
		graphicsEngine.EndFrame();
#endif

		inputManager.Update();

		debugHUD.SetMS(CDebugHUD::eMSTexts::Update);
		//std::this_thread::yield();
	}

	ENGINE_LOG("Shutting down worker pool... (might take some time, something is wrong if it does)");
	workerPool.Destroy();
}

void CEngine::Shutdown()
{
	myShouldRun = false;
}

void hse::CEngine::Init(const SEngineStartParams& aStartParams)
{
	myGameInitCallback = aStartParams.initCallback;
	myGameUpdateCallback = aStartParams.updateCallback;
	myShouldRun = false;
}

void hse::CEngine::InitWithWindowData(const gfx::SWindowData& aWindowData, const SEngineStartParams& aStartParams)
{
	myShouldLoadConfig = false;
	globalWindowData = aWindowData;
	Init(aStartParams);
}

void hse::CEngine::RenderLoop()
{
	CU::Timer timer;
	while (myShouldRun)
	{
		float deltaTime(timer.GetDeltaTime());
		if (deltaTime > 0.05f)
		{
			deltaTime = 0.05f;
		}
		graphicsEngine->BeginFrame();
		graphicsEngine->RenderFrame(deltaTime, (float)timer.GetTotalTime());
		graphicsEngine->EndFrame(); 
		timer.Update();
	}
}

bool hse::CEngine::LoadConfig(gfx::SWindowData* aWindowData)
{
	CHAR documentsPath[MAX_PATH];
	HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, documentsPath);
	if (FAILED(result))
	{
		return false;
	}

	std::string configPath(documentsPath);
	configPath = configPath + "\\The Game Assembly\\Save Game\\config.json";
	std::ifstream configFile(configPath);

	if (configFile.good() == false)
	{
#ifdef _DEBUG
		printf("Didnt use documents-config.\n");
#endif
		configFile.open("Data/config.json");
		if (configFile.good() == false)
		{
			configFile.close();
			return false;
		}
	}

	json configJSON;
	configFile >> configJSON;
	configFile.close();

	try
	{
		aWindowData->myWindowName = configJSON["windowName"].get<std::string>();
		aWindowData->myWidth = configJSON["windowSize"]["width"];
		aWindowData->myHeight = configJSON["windowSize"]["height"];
		aWindowData->myStartInFullScreen = configJSON["startInFullscreen"];
		aWindowData->myUseBloom = configJSON["useBloom"];
		aWindowData->myUseColorGrading = configJSON["useColorGrading"];
		aWindowData->myUseVSync = configJSON["useVSync"];
	}
	catch (...)
	{
		std::cout << "Failed to load engine config, using default" << std::endl;
		std::cout << std::endl;
		return false;
	}

	return true;
}
