#include "stdafx.h"
#include "GameOverScreen.h"
//#include "EntityManager.h"


CGameOverScreen::CGameOverScreen()
{
	myMenu.Disable();
}


CGameOverScreen::~CGameOverScreen()
{
}

void CGameOverScreen::Init()
{
}

void CGameOverScreen::Update(const float aDeltaTime)
{
	const bool exitMenu(!myMenu.Update(aDeltaTime));
}

void CGameOverScreen::Render()
{
	myMenu.Render(myState == CState::EStateCommand::eActive);
}

void CGameOverScreen::OnActivation()
{
	myMenu.Enable();
}
