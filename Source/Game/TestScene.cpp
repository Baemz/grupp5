#include "stdafx.h"
#include "TestScene.h"
#include "..\GraphicsEngine\Camera\CameraFactory.h"
#include "..\EngineCore\EventSystem\EventManager.h"
#include "..\GraphicsEngine\GraphicsEngineInterface.h"
#include "..\GraphicsEngine\Model\Loading\ModelFactory.h"


CTestScene::CTestScene(EManager& aEntityManager)
	: CState(aEntityManager)
	, usePointLight(false)
{
	hse::CEventManager::Get()->AttachInputListener(this);
}


CTestScene::~CTestScene()
{
	hse::CEventManager::Get()->DetachInputListener(this);
}

void CTestScene::Init()
{
	hse::gfx::CGraphicsEngineInterface::EnableCursor();
	myCamInst = hse::gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.f, 0.f, -10.f });
	myCamInst.SetPosition({ 0.f, 0.f, -10.f });
	mymdl.SetModelPath("Data/Models/Player/Player/mdl_player.fbx");
	mymdl.SetPosition({ 0.f, -0.1f, 0.f });
	mymdl.SetIsCullable(false);
	mymdl.Init();

	mymdl2.SetModelPath("Data/Models/Misc/plane_debug.fbx");
	mymdl2.SetMaterialPath("Data/Materials/cobblestone.material");
	mymdl2.SetScale({ 10.f, 10.f, 10.f });
	mymdl2.SetPosition({ 0.f, -1.f, 0.f });
	mymdl2.SetIsCullable(false);
	mymdl2.Init();


	knife.SetModelPath("Data/Models/Knife_fbx.fbx");
	knife.SetMaterialPath("Data/Materials/knife.material");
	knife.SetScale({ 0.01f, 0.01f, 0.01f });
	knife.SetPosition({ 4.f, -0.4f, 0.f });
	knife.SetIsCullable(false);
	knife.Init(); 

	couch.SetModelPath("Data/Models/couch.fbx");
	couch.SetMaterialPath("Data/Materials/couch.material");
	couch.SetScale({ 0.01f, 0.01f, 0.01f });
	couch.SetPosition({ -3.f, -1.0f, 0.f });
	couch.SetIsCullable(false);
	couch.Init();
	
	myEnvLight.Init("Data/Models/Misc/field_skybox.dds");
	myPointLight.Init({ -3.f, 1.0f, 0.f }, { 1.f, 1.f, 1.f }, 10.f, 2.f);

	myskybox = hse::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/field_skybox1.dds");
}

void CTestScene::Update(const float aDeltaTime)
{
	dt = aDeltaTime;
	knife.Rotate(CU::Vector3f(0.0f, 0.4f, 0.f) * dt);
}

void CTestScene::Render()
{
	myCamInst.UseForRendering(); 
	myEnvLight.UseForRendering();
	if (usePointLight)
	{
		myPointLight.Render();
	}
	myskybox.Render();
	mymdl.Render();
	mymdl2.Render();
	knife.Render();
	couch.Render();
}

void CTestScene::OnActivation()
{
}

void CTestScene::ReceieveEvent(SMessage & aMessage)
{
	if (aMessage.messageType == EMessageType::KeyInput)
	{
		SKeyMessage* keyMsgPtr = reinterpret_cast<SKeyMessage*>(&aMessage);

		if (keyMsgPtr->state == EButtonState::Down)
		{
			if (keyMsgPtr->key == 'W')
			{
				myCamInst.MoveForward(dt);
			}
			else if (keyMsgPtr->key == 'S')
			{
				myCamInst.MoveForward(-dt);
			}
			else if (keyMsgPtr->key == 'A')
			{
				myCamInst.Strafe(-dt);
			}
			else if (keyMsgPtr->key == 'D')
			{
				myCamInst.Strafe(dt);
			}
			else if (keyMsgPtr->key == 'Q')
			{
				myCamInst.Rotate(CU::Vector3f(0.f, -1.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == 'E')
			{
				myCamInst.Rotate(CU::Vector3f(0.f, 1.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == '1')
			{
				myCamInst.VerticalMovement(-dt);
			}
			else if (keyMsgPtr->key == '2')
			{
				myCamInst.VerticalMovement(dt);
			}
			else if (keyMsgPtr->key == 'M')
			{
				couch.Move(CU::Vector3f(0.f, -1.f, 0.f) * dt);
			}
			else if (keyMsgPtr->key == 'N')
			{
				couch.Move(CU::Vector3f(0.f, 1.f, 0.f) * dt);
			}
		}
		else if (keyMsgPtr->state == EButtonState::Pressed)
		{
			if (keyMsgPtr->key == '3')
			{
				hse::gfx::CGraphicsEngineInterface::ToggleFXAA();
			}
			else if (keyMsgPtr->key == '4')
			{
				hse::gfx::CGraphicsEngineInterface::ToggleSSAO();
			}
			else if (keyMsgPtr->key == '5')
			{
				hse::gfx::CGraphicsEngineInterface::ToggleBloom();
			}
			else if (keyMsgPtr->key == 'P')
			{
				usePointLight = !usePointLight;
			}
		}
	}
}
