#pragma once
#include "../CommonUtilities/Vector.h"
#include <map>
#include <queue>

#include "../GraphicsEngine/Sprite/Sprite.h"
#include "../GraphicsEngine/Text/Text.h"

#include "../EngineCore/EventSystem/EventListener.h"

namespace hse { namespace gfx { class CSprite; } }

struct STextContent
{
	struct SBox
	{
		SBox()
		{
			myTimer = 2.0f;
		}

		//std::queue<std::string> myLines;
		std::string myText;
		CU::Vector3f myColor;
		float myTimer;
	};

	std::queue<SBox> myBoxes;
};

class CTextBox : public IEventListener
{
public:
	CTextBox();
	~CTextBox();

	void Init(const char* aSpritePath, const char* aTextFilePath);
	
	void Update(const float aDeltaTime);

	void Close();

	void Render();

	void ReceieveEvent(SMessage& aMessage) override;

private:
	bool LoadText(const char* aTextFilePath);

	std::queue<STextContent> myQueuedTextContents;
	std::string myCurrentString;
	const CU::Vector2f myClosedPosition;
	const CU::Vector2f myOpenedPosition;
	const CU::Vector2f myTextOffset;

	int myCharacterIndex;

	const float myTimeBetweenCharacters;
	float myCharacterTimer;
	float myStateTimer;

	float myPopDownBoxTimer;

	hse::gfx::CText myText;
	hse::gfx::CSprite mySprite;
};