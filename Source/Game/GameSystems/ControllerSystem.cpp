#include "stdafx.h"
#include "ControllerSystem.h"

#include "../../EngineCore/EventSystem/EventManager.h"

#include "../../GraphicsEngine/GraphicsEngineInterface.h"
#include "../../GraphicsEngine/Camera/CameraInstance.h"

#include "../../GraphicsEngine/DebugTools.h"

#include "../../CommonUtilities/Intersection.h"

CControllerSystem::CControllerSystem()
	: IEventListener()
	, myEntityManager(nullptr)
{
}

void CControllerSystem::Init(EManager & aEntityManager, const SEntityHandle & aCameraHandle, const SEntityHandle & aInputHandle, const SEntityHandle& aGridHandle)
{
	myCameraHandle = aCameraHandle;
	myInputHandle = aInputHandle;
	myGridHandle = aGridHandle;
	myEntityManager = &aEntityManager;
	myCameraProjection = &myEntityManager->GetComponent<CompCameraInstance>(myEntityManager->GetEntityIndex(myCameraHandle)).myInstance.GetProjection();

	hse::CEventManager::Get()->AttachInputListener(this);
}

void CControllerSystem::Update(EManager&, const float aDeltaTime)
{
	//HACK_ if controller system is not initiated
	if (myEntityManager != nullptr)
	{
		const CU::Vector3f playerPosition(myEntityManager->GetComponent<CompPosition>(myEntityManager->GetEntityIndex(myInputHandle)).value);
		auto targetPlayer = [aDeltaTime, playerPosition](auto& aEntityIndex, CompPosition& aPosition, CompAIController& aController)
		{
			aController.myTarget = (playerPosition - aPosition.value);
		};

		myEntityManager->ForEntitiesMatching<SigAIController>(targetPlayer);
	}
}

void CControllerSystem::ReceieveEvent(SMessage & aMessage)
{
	if (aMessage.messageType == EMessageType::MouseClickInput)
	{
		SMouseClickMessage* clickMsg = reinterpret_cast<SMouseClickMessage*>(&aMessage);
		if (clickMsg->key == 1 && clickMsg->state == EButtonState::Down)
		{
			const CU::Vector2f resolution = hse::gfx::CGraphicsEngineInterface::GetResolution();
			const CU::Matrix44f& camOrientation(myEntityManager->GetComponent<CompCameraInstance>(myEntityManager->GetEntityIndex(myCameraHandle)).myInstance.GetOrientation());
			auto movePlayer = [this, &camOrientation, &clickMsg, &resolution](auto& aEntityIndex, CompPosition& aPosition, CompInputController& aController)
			{
				float normalizedMouseX = (((static_cast<float>(clickMsg->x) * 2.0f) / resolution.x) - 1.0f);
				float normalizedMouseY = (((static_cast<float>(clickMsg->y) * 2.0f) / resolution.y) - 1.0f);

				CU::Vector2f projPos = { normalizedMouseX, normalizedMouseY };
				projPos.x /= (*myCameraProjection)[0];
				projPos.y /= (*myCameraProjection)[5];

				CU::Vector3f screenPosition =
				{
					(projPos.x * camOrientation[0]) + (-projPos.y * camOrientation[4]) + camOrientation[8],
					(projPos.x * camOrientation[1]) + (-projPos.y * camOrientation[5]) + camOrientation[9],
					(projPos.x * camOrientation[2]) + (-projPos.y * camOrientation[6]) + camOrientation[10]
				};

				CU::Collision::Ray cameraToWorld(camOrientation.GetPosition(), screenPosition.GetNormalized() * 1000.0f);

				CU::Vector3f color = { 1.0f, 1.0f, 1.0f };

				DT_DRAW_LINE_4_ARGS(cameraToWorld.myOrigin, cameraToWorld.myOrigin + cameraToWorld.myDirection, color, color); // , 3.0f, 15.0f);

				CU::GrowingArray<CU::Collision::AABB3D>& cells(myEntityManager->GetComponent<CompSimpleAABBGrid>(myEntityManager->GetEntityIndex(myGridHandle)).myCells);

				for (unsigned short i = 0; i < cells.Size(); ++i)
				{
					if (CU::Collision::IntersectionRayAABB(cameraToWorld, cells[i]))
					{
						cells[i].myIsTargeted = true;
						CU::Vector3f center = (cells[i].myMin + cells[i].myMax) * 0.5f;
						aController.myTarget = center;
						DT_DRAW_LINE_2_ARGS(aController.myTarget, CU::Vector3f(aController.myTarget.x, 0.0f, aController.myTarget.z));
						break;
					}
					else
					{
						cells[i].myIsTargeted = false;
					}
				}
			};

			myEntityManager->RunIfMatching<SigReadPlayerInput>(myEntityManager->GetEntityIndex(myInputHandle), movePlayer);
		}
	}
	else if (aMessage.messageType == EMessageType::MouseInput)
	{
		SMouseDataMessage* mouseMsg = reinterpret_cast<SMouseDataMessage*>(&aMessage);

		//hse::gfx::CDebugTools::Get()->DrawCube({mouseMsg->
	}
}

void CControllerSystem::ReadPlayerInput(EManager & aEntityManager, const float aDeltaTime)
{
	//const CU::Vector2f resolution = hse::gfx::CGraphicsEngineInterface::GetResolution();
	//hse::gfx::CCameraInstance& camInstance(aEntityManager.GetComponent<CompCameraInstance>(aEntityManager.GetEntityIndex(myCameraHandle)).myInstance);
	//auto readPlayerInput = [&aEntityManager, &camInstance, &resolution] (auto& aEntityIndex, CompPosition& aPosition, CompInputController& aController)
	//{		


	//};

	//aEntityManager.RunIfMatching<SigReadPlayerInput>(myInputHandle, readPlayerInput);
}
