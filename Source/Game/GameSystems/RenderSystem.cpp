#include "stdafx.h"
#include "RenderSystem.h"

#include "../../GraphicsEngine/Camera/CameraFactory.h"
//#include "../../GraphicsEngine/Camera/CameraInstance.h"

#include "../../GraphicsEngine/DebugTools.h"

#include <iostream>

void CRenderSystem::Init(EManager& aEntityManager)
{
	auto createCamera = [](auto&, auto& aCameraComponent)
	{
		aCameraComponent.myInstance = hse::gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.0f, 0.0f, 0.0f });
	};

	aEntityManager.ForEntitiesMatching<SigCamera>(createCamera);
}

void CRenderSystem::Update(EManager& aEntityManager, const float aDeltaTime)
{
	//auto sendHpToHud = [aEntityManager](auto& aEntityIndex, auto& aHPComponent)
	//{
	//	float hp = aHPComponent.Hp;
	//	std::size_t enemyIndex = aEntityIndex;

	//	aEntityManager.ForEntitiesMatching<SigReceiveHP>([hp, enemyIndex](auto& aEntityIndex, auto& aHudHPComponent)
	//	{
	//		aHudHPComponent.myEnemyHPList[enemyIndex] = hp;
	//	});
	//};


	//aEntityManager.ForEntitiesMatching<SigSendHPToHUD>(sendHpToHud);

	RenderModels(aEntityManager, aDeltaTime);
	RenderCamera(aEntityManager);
	RenderPointLights(aEntityManager);
	RenderParticles(aEntityManager, aDeltaTime);

	hse::gfx::CDebugTools* debugDrawer(hse::gfx::CDebugTools::Get());
	auto renderObjectsAsDebugCubes = [&aEntityManager, debugDrawer](auto& aEntityIndex, CompPosition& aPosition)
	{
		if (aEntityManager.HasTag<TagInputController>(aEntityIndex))
		{
			debugDrawer->DrawCube(aPosition.value, 1.0f, { 0.0f, 0.25f, 1.0f });
		}
		if (aEntityManager.HasTag<TagScriptController>(aEntityIndex))
		{
			debugDrawer->DrawCube({ aPosition.value.x, 1.0f, aPosition.value.z }, 1.0f, { 0.0f, 1.0f, 0.0f });
		}
	};

	auto renderAIAsDebugCubes = [&aEntityManager, debugDrawer](auto& aEntityIndex, CompPosition& aPosition, CompAIController& aController)
	{
		if (aController.myTag == CompAIController::Polling)
		{
			debugDrawer->DrawCube(aPosition.value, 1.0f, { 1.0f, 0.25f, 0.0f });
			debugDrawer->DrawText2D("P", { (aPosition.value.x / 20.0f) + 0.05f, (aPosition.value.z / 80.0f) }, 14.0, { 1.0f, 0.25f, 0.0f });
		}
		if (aController.myTag == CompAIController::Events)
		{
			debugDrawer->DrawCube(aPosition.value, 1.0f, { 0.5f, 0.25f, 0.5f });
			debugDrawer->DrawText2D("E", { (aPosition.value.x / 80.0f) + 0.05f, (aPosition.value.z / 60.0f) }, 14.0f, { 0.5f, 0.25f, 0.5f });
		}
	};

	auto renderSimpleGrid = [&debugDrawer](auto& aEntityIndex, CompSimpleAABBGrid& aGrid)
	{
		for (unsigned short i = 0; i < aGrid.myCells.Size(); ++i)
		{
			const CU::Vector3f center = (aGrid.myCells[i].myMin + aGrid.myCells[i].myMax) * 0.5f;
			const CU::Vector3f color = (aGrid.myCells[i].myIsTargeted) ? CU::Vector3f(1.0f, 0.0, 0.0f) : CU::Vector3f(0.2f);
			debugDrawer->DrawRectangle3D(center, { aGrid.myCellSize, 0.02f, aGrid.myCellSize }, color);
		}
	};

	aEntityManager.ForEntitiesMatching<SigRenderDebugCubes>(renderObjectsAsDebugCubes);
	aEntityManager.ForEntitiesMatching<SigAIController>(renderAIAsDebugCubes);

	aEntityManager.ForEntitiesMatching<SigRenderSimpleGrid>(renderSimpleGrid);

	//auto renderModelBosses
	//auto renderModelShieldedBosses

	//aEntityManager.ForEntitiesMatching<SigRender, STBoss>(renderModelBosses);
	//aEntityManager.ForEntitiesMatching<SigRender, STBoss, SCShield>(renderModelShieldedBosses);
}

void CRenderSystem::RenderModels(EManager& aEntityManager, const float aDeltaTime)
{
	auto renderModels = [aDeltaTime](auto& eIndex, CompPosition& aPositionComponent, CompRender& aRenderComponent)
	{
		auto& instance(aRenderComponent.myModel);

		if (!instance.IsLoaded())
		{
			instance.Init();
		}
		if (instance.IsLoaded())
		{
			static float rotation = 0.f;
			rotation += aDeltaTime / 10.f;
			instance.SetOrientation(CU::Matrix44f::CreateRotateAroundY(rotation));

			instance.SetPosition(aPositionComponent.value);
			//CU::Vector3f position(aPositionComponent.value);
			//printf("Position: X: %f, Y: %f, Z: %f\n", position.x, position.y, position.z);
			instance.Render();
		}
	};

	aEntityManager.ForEntitiesMatching<SigRender>(renderModels);
}

void CRenderSystem::RenderCamera(EManager& aEntityManager)
{
	auto createCamera = [](auto&, auto& aCameraComponent)
	{
		aCameraComponent.myInstance.UseForRendering();
	};

	aEntityManager.ForEntitiesMatching<SigCamera>(createCamera);
}

void CRenderSystem::RenderPointLights(EManager & aEntityManager)
{
	auto renderPointLights = [](auto&, CompPosition& aPositionComponent, CompPointLight& aLightComponent)
	{
		auto& instance(aLightComponent.myPointLight);

		instance.SetPosition(aPositionComponent.value);
		instance.Render();
	};

	aEntityManager.ForEntitiesMatching<SigRenderPointLight>(renderPointLights);
}

void CRenderSystem::RenderParticles(EManager & aEntityManager, const float aDeltaTime)
{
	auto renderParticles = [aDeltaTime](auto& aEntityIndex, CompParticleEmitter& aParticleComponent)
	{
		//printf("%i\n", aEntityIndex);

		auto& instance(aParticleComponent.myParticleEmitter);

		instance.Update(aDeltaTime);
		instance.Render();
	};

	aEntityManager.ForEntitiesMatching<SigRenderParticles>(renderParticles);
}
