#include "stdafx.h"
#include "GameSystems.h"
#include "GameSystems/GameSystemsInclude.h"

CSystemAbstractAbstract::SystemCounter CSystemAbstractAbstract::abstractSystemCount = 0;
CGameSystems* CGameSystems::ourInstance = nullptr;

void CGameSystems::Create(EManager & aEntityManager)
{
	assert(ourInstance == nullptr && "Instance has already been created.");
	ourInstance = hse_new(CGameSystems(aEntityManager));
	ourInstance->Init();
}

void CGameSystems::Destroy()
{
	hse_delete(ourInstance);
}

void CGameSystems::Init()
{
	myEntityManager.Refresh();
	//myDuringUpdateFunctions.Init(256);
	Add<CMovementSystem>();
	Add<CRenderSystem>();
	Add<CControllerSystem>();
}

void CGameSystems::UpdateAllSystems(const float aDeltaTime)
{
	for (auto& pair : mySystems)
	{
		pair.second->Update(myEntityManager, aDeltaTime);
	}

	//for (std::size_t fIndex = myDuringUpdateFunctions.Size(); fIndex-- > 0;)
	//{
	//	myDuringUpdateFunctions[fIndex]();
	//	myDuringUpdateFunctions.RemoveCyclicAtIndex(fIndex);
	//}
	myEntityManager.Refresh();
}
