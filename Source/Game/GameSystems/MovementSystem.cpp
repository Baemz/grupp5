#include "stdafx.h"
#include "MovementSystem.h"

void CMovementSystem::Update(EManager& aEntityManager, const float aDeltaTime)
{
	auto moveInput = [aDeltaTime] (auto&, CompPosition& aPosition, CompInputController& aInputController)
	{
		const CU::Vector3f direction(aInputController.myTarget - aPosition.value);

		if (direction.Length2() < 0.125f) return;

		CU::Vector3f velocity = direction.GetNormalized() * aInputController.mySpeed * aDeltaTime;
		aPosition.value += velocity;
		//aPosition.value += aInputController.myTarget.GetNormalized() * aInputController.mySpeed * aDeltaTime;
	};

	auto moveAI = [aDeltaTime](auto&, CompPosition& aPosition, CompAIController& aAIController)
	{
		float targetLength2(aAIController.myTarget.Length2());
		if (targetLength2 == 0.0f || (aAIController.myTarget).Length2() < 0.125f) return;

		aPosition.value += aAIController.myTarget.GetNormalized() * aAIController.mySpeed * aDeltaTime;
	};

	aEntityManager.ForEntitiesMatching<SigReadPlayerInput>(moveInput);
	aEntityManager.ForEntitiesMatching<SigAIController>(moveAI);
}
