#pragma once
#include "SystemAbstract.h"

class CMenuSystem : public CSystemAbstract<CMenuSystem>
{
public:

	void Init() { }

	//Update every signature with anything menu-related
	void Update(EManager& aEntityManager, const float aDeltaTime) override;

private:

	auto OnHoverButton(EManager& aEntityManager);
};