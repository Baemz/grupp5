#pragma once
#include "SystemAbstract.h"

class CRenderSystem : public CSystemAbstract<CRenderSystem>
{
public:
	//Load models etc.
	void Init(EManager& aEntityManager);

	//Update every signature with anything render-related
	void Update(EManager& aEntityManager, const float aDeltaTime) override;

private:

	void RenderModels(EManager& aEntityManager, const float aDeltaTime);
	void RenderCamera(EManager& aEntityManager);
	void RenderPointLights(EManager& aEntityManager);
	void RenderParticles(EManager& aEntityManager, const float aDeltaTime);
};