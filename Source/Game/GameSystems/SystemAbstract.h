#pragma once
#include "../EntitySystemLink/EntitySystemLink.h"

//Fairly self-explanatory
class CSystemAbstractAbstract
{
public:
	using SystemCounter = std::size_t;
	virtual ~CSystemAbstractAbstract() = default;

	static SystemCounter abstractSystemCount;

	template <typename... Args>
	void Init(EManager& aEntityManager, Args&&... aArgs) = 0;

	virtual void Update(EManager& aEntityManager, const float aDeltaTime) = 0;
};


template <typename AbstractAbstract>
class CSystemAbstract : public CSystemAbstractAbstract
{
	friend class CGameSystems;

	static size_t NewSystemIndex()
	{
		static std::size_t count = abstractSystemCount++;
		myIndex = count;
		return count;
	}

	static SystemCounter myIndex;

public:
	virtual ~CSystemAbstract() = default;
};

template<typename System>
CSystemAbstractAbstract::SystemCounter CSystemAbstract<System>::myIndex = 0;

