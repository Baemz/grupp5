#pragma once
#include "../EntitySystemLink/EntitySystemLink.h"
#include "SystemAbstract.h"

#include <unordered_map>
#include <memory>

//Warning: regular NEWs data-less types, get me dead if we overstep our bounds by like 20 bytes

//FWD
class CGame;

class CGameSystems
{
public:
	~CGameSystems() = default;

	static CGameSystems* Get() { return ourInstance; }

	template<typename System, typename... Args>
	void InitSystem(Args&&... aArgs)
	{
		GetSystem<System>()->Init(std::forward<Args>(aArgs)...);
	}

	//Add new system with constructor arguments
	template <typename System, typename... Args>
	std::shared_ptr<System> Add(Args &&... aArgs)
	{
		std::shared_ptr<System> system(new System(std::forward<Args>(aArgs) ...));
		mySystems.insert(std::make_pair(System::NewSystemIndex(), system));
		return system;
	}

	//Delete system
	template <typename System>
	void Delete()
	{
		auto it = mySystems.find(System::myIndex);
		assert(it != mySystems.end());
		mySystems.erase(it->first);
	}
private:
	//Don'THICC let anyone else operate the manager.
	friend CGame;
	static void Create(EManager& aEntityManager);
	static void Destroy();

	CGameSystems(EManager& aEntityManager)
		: myEntityManager(aEntityManager) { }

	void Init();


	template<typename System>
	std::shared_ptr<System> GetSystem()
	{
		auto it = mySystems.find(System::myIndex);
		assert(it != mySystems.end());
		return std::shared_ptr<System>(std::static_pointer_cast<System>(it->second));
	}


	void UpdateAllSystems(const float aDeltaTime);

	static CGameSystems* ourInstance;

	EManager& myEntityManager;
	//CU::GrowingArray<std::function<void()>, std::size_t> myDuringUpdateFunctions;
	std::unordered_map<CSystemAbstractAbstract::SystemCounter, std::shared_ptr<CSystemAbstractAbstract>> mySystems;
};