#pragma once
#include "SystemAbstract.h"

class CMovementSystem : public CSystemAbstract<CMovementSystem>
{
public:
	void Init() { }
	void Update(EManager& aEntityManager, const float aDeltaTime) override;
};