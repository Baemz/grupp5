#pragma once
#include "SystemAbstract.h"
#include "../EngineCore/EventSystem/EventListener.h"
#include "../../EntitySystem/Handle.h"

class CControllerSystem : public CSystemAbstract<CControllerSystem>, public IEventListener
{
public:
	CControllerSystem();
	~CControllerSystem() = default;

	void Init(EManager& aEntityManager, const SEntityHandle& aCameraHandle, const SEntityHandle& aInputHandle, const SEntityHandle& aGridHandle);

	//Update every signature with anything menu-related
	void Update(EManager& aEntityManager, const float aDeltaTime) override;

	void ReceieveEvent(SMessage& aMessage);

private:
	void ReadPlayerInput(EManager&, const float aDeltaTime);

	SEntityHandle myCameraHandle;
	SEntityHandle myInputHandle;
	SEntityHandle myGridHandle;

	EManager* myEntityManager;
	const CU::Matrix44f* myCameraProjection;
};