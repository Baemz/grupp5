#pragma once
#include "../../EntitySystem/ESSettingsLight.h"
#include "../../EntitySystem/Handle.h"
#include "CommonComponents.h"
#include "RenderComponents.h"
#include "PlayerComponents.h"
#include "AIComponents.h"

//Init data
#include <string>

//-------------------------------------LIST-----------------------------------
using MyComponents = ComponentList
<
	CompPosition,
	CompLife,
	CompRender,
	CompCameraInstance,
	CompCameraControl,
	CompPointLight,
	CompSpotLight,
	CompParticleEmitter,
	CompInputController,
	CompAIController,
	CompSimpleAABBGrid
>;
