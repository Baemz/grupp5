#pragma once
#include "CommonComponents.h"

#include "../../GraphicsEngine/Sprite/Sprite.h"
#include "../../GraphicsEngine/Model/ModelInstance.h"
#include "../../GraphicsEngine/Camera/CameraInstance.h"
#include "../../GraphicsEngine/Lights/PointLightInstance.h"
#include "../../GraphicsEngine/Lights/SpotLightInstance.h"
#include "../../GraphicsEngine/Particles/ParticleEmitterInstance.h"
#include "../../GraphicsEngine/Particles/StreakEmitterInstance.h"

struct CompRender
{
	hse::gfx::CModelInstance myModel;
	hse::gfx::CModelInstance myLODModel;
	bool myIsCullable;

	CompRender()
	{
		myModel.SetModelPath("Data/Models/Misc/nC_wMat.fbx");
		myIsCullable = true;
	}

	CompRender(const std::string& aFilePath, const CU::Vector3f& aScale = CU::Vector3f(1.0f))
	{
		myModel.SetModelPath(aFilePath);
		myModel.SetScale(aScale);
		myIsCullable = true;
	}
};

struct CompCameraInstance
{
	hse::gfx::CCameraInstance myInstance;
};

struct CompPointLight
{
	hse::gfx::CPointLightInstance myPointLight;
	CompPointLight() = default;
	CompPointLight(
		const CU::Vector3f& aPosition,
		const CU::Vector3f& aColor = CU::Vector3f(1.0f),
		float aRange = 100.0f, float aIntensity = 1.0f)
	{
		myPointLight.Init(aPosition, aColor, aRange, aIntensity);
	}
};

struct CompSpotLight
{
	hse::gfx::CSpotLightInstance mySpotLight;
	CompSpotLight() = default;
	CompSpotLight(
		const CU::Vector3f& aPosition,
		const CU::Vector3f& aColor = CU::Vector3f(1.0f),
		const CU::Vector3f& aDirection = CU::Vector3f(0.0f, 0.0f, -1.0f),
		float aAngle = 90.0f, float aRange = 100.0f, float aIntensity = 1.0f)
	{
		mySpotLight.Init(aPosition, aColor, aDirection, aAngle, aRange, aIntensity);
	}
};

struct CompParticleEmitter
{
	hse::gfx::CParticleEmitterInstance myParticleEmitter;
	CompParticleEmitter() = default;
	CompParticleEmitter(const char* aJsonPath, CU::Vector3f* aParentPosition = nullptr)
	{
		myParticleEmitter.Init(aJsonPath, aParentPosition);
	}
};

struct CompStreakEmitter
{
	//hse::gfx::CStreakEmitterInstance myStreakEmitter;
};
