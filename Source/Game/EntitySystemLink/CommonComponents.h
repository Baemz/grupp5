#pragma once
#include "../../CommonUtilities/Vector.h"
#include "../../CommonUtilities/Matrix.h"
#include "../../CommonUtilities/Quaternion.h"

#include "../../CommonUtilities/Intersection.h"

struct CompPosition
{
	CompPosition() = default;
	CompPosition(const CU::Vector3f& aValue) { 
		value = aValue; }
	CU::Vector3f value;
};

struct CompLife { float value; };

struct CompSimpleAABBGrid
{
	CompSimpleAABBGrid()
		: myCellSize(0)
	{}

	CompSimpleAABBGrid(const float aCellSize, const CU::Vector2f& aGridWorldSize)
		: myCellSize(aCellSize)
	{
		const unsigned short cellAmountX = (unsigned short)std::ceil(aGridWorldSize.x / aCellSize);
		const unsigned short cellAmountY = (unsigned short)std::ceil(aGridWorldSize.y / aCellSize);
		
		myCells.Resize(cellAmountX * cellAmountY);

		for (unsigned short i = 0; i < (cellAmountX * cellAmountY); ++i)
		{
			myCells[i].myMin = { (i % cellAmountX) * aCellSize, -myCellSize, (i / cellAmountY) * aCellSize };
			myCells[i].myMax = { ((i % cellAmountX) + 1) * aCellSize, -myCellSize + 0.1f, ((i / cellAmountY) + 1) * aCellSize };
			myCells[i].myIsTargeted = false;
		}
	}

	CompSimpleAABBGrid& operator=(const CompSimpleAABBGrid& aCompSimpleAABBGrid)
	{
		assert("can't just assign a grid to another grid like that, for real." && myCellSize == aCompSimpleAABBGrid.myCellSize);
		myCells = aCompSimpleAABBGrid.myCells;
		return *this;
	}

	const float myCellSize;
	CU::GrowingArray<CU::Collision::AABB3D> myCells;
};