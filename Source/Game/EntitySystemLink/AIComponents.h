#pragma once

struct CompAIController
{
	enum eReceiveDataType 
	{
		Polling,
		Events
	};
	CU::Vector3f myTarget;
	float mySpeed;

	eReceiveDataType myTag;

	CompAIController() = default;
	CompAIController(eReceiveDataType aReceiveType) : myTag(aReceiveType), mySpeed(0.25f) { }
};