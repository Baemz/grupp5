#pragma once
#include "Components.h"
#include "Tags.h"
#include "../../EntitySystem/ESSettingsLight.h"

using SigRender = Signature<CompPosition, CompRender>;
using SigRenderPointLight = Signature<CompPosition, CompPointLight>;
using SigRenderParticles = Signature<CompParticleEmitter>;
using SigGrow = Signature<CompRender, CompLife>;
using SigReduceLife = Signature<CompLife>;

using SigCamera = Signature<CompCameraInstance, TagCamera>;

using SigRenderDebugCubes = Signature<CompPosition>;
using SigAIController = Signature<CompPosition, CompAIController>;

using SigReadPlayerInput = Signature<CompPosition, CompInputController, TagInputController>;

using SigRenderSimpleGrid = Signature<CompSimpleAABBGrid>;

using MySignatures = SignatureList
<
	SigRender,
	SigGrow,
	SigCamera,
	SigRenderPointLight,
	SigRenderParticles,
	SigRenderDebugCubes,
	SigAIController,
	SigReadPlayerInput,
	SigRenderSimpleGrid
>;

//Parameter list for all signature lambda functions:
//1: Index of this entity
//Rest: The required components and tags, in the order listed in the template arguments above.
//Capture list is optional, you can pass things like deltatime or the manager, for killing of objects. Remember to specify the entity manager as a reference.

//auto LAMBDAFUNCTION = [aDeltaTime, &aEntityManager] (auto& ENTITYINDEX, auto& COMPONENT1, auto& COMPONENT2, auto& COMPONENT...)
//{
//	//code to run
//
//	if (aComponent1.myTimer <= 0) aEntityManager.Kill(aEntityIndex);
//};

//aEntityManager.ForEntitiesMatching<SIGNATURE>(LAMBDAFUNCTION);