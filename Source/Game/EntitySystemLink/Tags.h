#pragma once
#include "../../EntitySystem/ESSettingsLight.h"

struct TagPlayer {};
struct TagEnemy {};
struct TagCamera {};
struct TagParticle {};
struct TagMenu {};
struct TagButton {};

struct TagInputController {};
struct TagAIController {};
struct TagScriptController {};

using MyTags = TagList 
<
	TagPlayer,
	TagEnemy,
	TagCamera,
	TagParticle,
	TagMenu,
	TagButton,
	TagInputController,
	TagAIController,
	TagScriptController
>;