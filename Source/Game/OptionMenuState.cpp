#include "stdafx.h"
#include "OptionMenuState.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"

COptionMenuState::COptionMenuState(EManager& aEntityManager)
	: CState(aEntityManager)
{
	myMenu.Disable();
	myPrevResolutionIndex = UINT_MAX;
	myCurrResolutionIndex = UINT_MAX;
}

void COptionMenuState::Init()
{
}

void COptionMenuState::Update(const float aDeltaTime)
{
	aDeltaTime;
}

void COptionMenuState::Render()
{
	myMenu.Render(myState == CState::EStateCommand::eActive);
}

void COptionMenuState::OnActivation()
{
	myMenu.Enable();
}

void COptionMenuState::UpdateFullscreenState()
{
	hse::gfx::CGraphicsEngineInterface::ToggleFullscreen();
}

void COptionMenuState::UpdateWindowResolution()
{
	myPrevResolutionIndex = myCurrResolutionIndex;
	myCurrResolutionIndex = myMenu.GetNavigationButtonIndex("resolution");
	if (myCurrResolutionIndex == myPrevResolutionIndex) return;

	if (myCurrResolutionIndex == 4)
	{
		hse::gfx::CGraphicsEngineInterface::SetResolution({ 1920.0f, 1080.0f });
	}
	else if (myCurrResolutionIndex == 3)
	{
		hse::gfx::CGraphicsEngineInterface::SetResolution({ 1600.0f, 900.0f });
	}
	else if (myCurrResolutionIndex == 2)
	{
		hse::gfx::CGraphicsEngineInterface::SetResolution({ 1280.0f, 720.0f });
	}
	else if (myCurrResolutionIndex == 1)
	{
		hse::gfx::CGraphicsEngineInterface::SetResolution({ 960.0f, 540.0f });
	}
	else if (myCurrResolutionIndex == 0)
	{
		hse::gfx::CGraphicsEngineInterface::SetResolution({ 640.0f, 360.0f });
	}
}

const unsigned short COptionMenuState::GetUsedResolutionIndex() const
{
	CU::Vector2f resolution( hse::gfx::CGraphicsEngineInterface::GetFullWindowedResolution() );

	if (resolution.x == 1920.0f && resolution.y == 1080.0f)
	{
		return 4;
	}
	else if (resolution.x == 1600.0f && resolution.y == 900.0f)
	{
		return 3;
	}
	else if (resolution.x == 1280.0f && resolution.y == 720.0f)
	{
		return 2;
	}
	else if (resolution.x == 960.0f && resolution.y == 540.0f)
	{
		return 1;
	}
	else if (resolution.x == 640.0f && resolution.y == 360.0f)
	{
		return 0;
	}

	GAME_LOG("[COptionMenuState] WARNING! Found no matching resolutions (Current full windowed resolution: X = %f		Y = %f). Continues with default.", resolution.x, resolution.y);
	return 4;
}
