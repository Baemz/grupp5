#pragma once
#include "State.h"
#include "../EngineCore/EventSystem/EventListener.h"

namespace hse
{
	namespace gfx
	{
		class CSprite;
		class CText;
	}
}

class CCreditsState : private CState, public IEventListener
{
public:
	CCreditsState(EManager& aEntityManager, bool aExitMainstate);
	~CCreditsState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;

	void ReceieveEvent(SMessage& aMessage) override;

private:
	hse::gfx::CSprite* myBlackCanvasSprite;
	hse::gfx::CSprite* myCreditsImage;
	hse::gfx::CText* myText;

	const float myTimeToRollCredits;
	float myElapsedTime;
	bool myExitMainstate;
};