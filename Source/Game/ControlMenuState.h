#pragma once
#include "State.h"
#include "Menu.h"

class CControlMenuState : private CState
{
public:
	CControlMenuState(EManager& aEntityManager);
	~CControlMenuState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;

private:
	CMenu myMenu;
};