#include "stdafx.h"
#include "StateStack.h"

#define sc_i(var)						static_cast<int>(var)
#define sc_ui(var)						static_cast<unsigned int>(var)
#define sc_us(var)						static_cast<unsigned short>(var)
#define SAFE_HSE_DELETE(ptr)			if ((ptr) != nullptr) { hse_delete(ptr); (ptr) = nullptr; }
#define STATE_IS_ACTIVE(aStatePtr)		((aStatePtr)->myState == CState::EStateCommand::eKeepState || (aStatePtr)->myState == CState::EStateCommand::eActive)

CStateStack* CStateStack::myInstancePtr = nullptr;

const bool CStateStack::Create()
{
	if (myInstancePtr != nullptr)
	{
		GAME_LOG("FAIL! Failed to create new instance of State Stack because it was already created.");
		return false;
	}

	myInstancePtr = hse_new(CStateStack());
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("FAIL! Failed to allocate memory for the State Stack.");
		return false;
	}

	return true;
}

void CStateStack::Destroy()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Could not destroy State Stack instance because it does not exist.");
		return;
	}

	for (unsigned short i(0); i < myInstancePtr->myStates.Size(); ++i)
	{
		SAFE_HSE_DELETE(myInstancePtr->myStates[i]);
	}

	myInstancePtr->myPrevActiveStatePtr = nullptr;
	myInstancePtr->myCurrActiveStatePtr = nullptr;
	myInstancePtr->myStates.RemoveAll();
	myInstancePtr->myNotInitializedStateList.RemoveAll();

	SAFE_HSE_DELETE(myInstancePtr);
}

void CStateStack::UpdateStateOnTop(const float aDeltaTime)
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	for (int i(sc_i(myInstancePtr->myStates.Size()) - 1); i >= 0; --i)
	{
		if (myInstancePtr->myStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveState ||
			myInstancePtr->myStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveMainState)
		{
			SAFE_HSE_DELETE(myInstancePtr->myStates[sc_us(i)]);
			myInstancePtr->myStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}

		if (myInstancePtr->CheckUnderStatesForRemoval( myInstancePtr->myStates[sc_us(i)] ))
		{
			SAFE_HSE_DELETE(myInstancePtr->myStates[sc_us(i)]);
			myInstancePtr->myStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}
	}

	if (myInstancePtr->myStates.Size() == 0) return;
	myInstancePtr->UpdateStateOnTop(myInstancePtr->myStates.GetLast(), aDeltaTime);
}

void CStateStack::RenderApprovedStates()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	if (myInstancePtr->myStates.Size() == 0) return;
	myInstancePtr->RenderAvailableStates(myInstancePtr->myStates.GetLast());
}

const bool CStateStack::IsEmpty()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return true;
	}

	return myInstancePtr->myStates.Size() == 0;
}

void CStateStack::PushMainStateOnTop(void* aMainStatePtr)
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	if (myInstancePtr->myFreeID == UINT_MAX)
	{
		GAME_LOG("ERROR! Created too many states. No more states will be created.");
		return;
	}

	CState* statePtr = (CState*)aMainStatePtr;
	statePtr->InitState(myInstancePtr->myFreeID, false);

	myInstancePtr->myStates.Add(statePtr);
	myInstancePtr->myNotInitializedStateList.Add(statePtr);

	++myInstancePtr->myFreeID;
}

void CStateStack::PushUnderStateOnTop(void* aMainStatePtr, void* aUnderStatePtr, const bool aCanRenderMainState)
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}

	if (myInstancePtr->myFreeID == UINT_MAX)
	{
		GAME_LOG("ERROR! Created too many states. No more states will be created.");
		return;
	}

	CState* mainStatePtr = (CState*)aMainStatePtr;
	CState* underStatePtr = (CState*)aUnderStatePtr;
	underStatePtr->InitState(myInstancePtr->myFreeID, aCanRenderMainState);

	mainStatePtr->myUnderStates.Add(underStatePtr);
	myInstancePtr->myNotInitializedStateList.Add(underStatePtr);

	++myInstancePtr->myFreeID;
}

void CStateStack::InitStatesCreatedFromPreviousFrame()
{
	if (myInstancePtr == nullptr)
	{
		GAME_LOG("WARNING! Trying to call State Stack's function before State Stack was created.");
		return;
	}
	myInstancePtr->CheckAndActivateStateOnTop();

	if (myInstancePtr->myNotInitializedStateList.Size() <= 0) return;
	for (unsigned short i(0); i < myInstancePtr->myNotInitializedStateList.Size(); ++i)
	{
		myInstancePtr->myNotInitializedStateList[i]->Init();
		myInstancePtr->myNotInitializedStateList[i]->myState = CState::EStateCommand::eKeepState;
	}

	myInstancePtr->myNotInitializedStateList.RemoveAll();
}

/* PRIVATE FUNCTIONS */

CStateStack::CStateStack()
{
	myStates.Init(16);
	myNotInitializedStateList.Init(16);
	myPrevActiveStatePtr = nullptr;
	myCurrActiveStatePtr = nullptr;
	myFreeID = 0;
}

const bool CStateStack::CheckUnderStatesForRemoval(CState*& aStatePtr)
{
	bool foundMainStateRemove(false);

	for (int i(sc_i(aStatePtr->myUnderStates.Size()) - 1); i >= 0; --i)
	{
		if (aStatePtr->myUnderStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveState)
		{
			SAFE_HSE_DELETE(aStatePtr->myUnderStates[sc_us(i)]);
			aStatePtr->myUnderStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}
		else if (aStatePtr->myUnderStates[sc_us(i)]->myState == CState::EStateCommand::eRemoveMainState)
		{
			foundMainStateRemove = true;
		}

		if (CheckUnderStatesForRemoval(aStatePtr->myUnderStates[sc_us(i)]))
		{
			SAFE_HSE_DELETE(aStatePtr->myUnderStates[sc_us(i)]);
			aStatePtr->myUnderStates.RemoveCyclicAtIndex(sc_us(i));
			continue;
		}
	}

	return foundMainStateRemove;
}

void CStateStack::UpdateStateOnTop(CState*& aStatePtr, const float aDeltaTime)
{
	if (aStatePtr->myUnderStates.Size() > 0)
	{
		UpdateStateOnTop(aStatePtr->myUnderStates.GetLast(), aDeltaTime);
	}
	else if (STATE_IS_ACTIVE(aStatePtr))
	{
		if ((myCurrActiveStatePtr == nullptr) ? (true) : (myCurrActiveStatePtr->myID != aStatePtr->myID))
		{
			myPrevActiveStatePtr = myCurrActiveStatePtr;
			myCurrActiveStatePtr = aStatePtr;
		}

		aStatePtr->Update(aDeltaTime);
	}
}

void CStateStack::RenderAvailableStates(CState*& aStatePtr)
{
	if (aStatePtr->myUnderStates.Size() > 0)
	{
		if (aStatePtr->myUnderStates.GetLast()->myCanRenderMainState)
		{
			aStatePtr->Render();
		}

		RenderAvailableStates(aStatePtr->myUnderStates.GetLast());
	}
	else if (STATE_IS_ACTIVE(aStatePtr))
	{
		aStatePtr->Render();
	}
}

void CStateStack::CheckAndActivateStateOnTop()
{
	if (myCurrActiveStatePtr == nullptr) return;

	if (myCurrActiveStatePtr->myState == CState::EStateCommand::eKeepState)
	{
		if (myPrevActiveStatePtr != nullptr)
		{
			myPrevActiveStatePtr->myState = CState::EStateCommand::eKeepState;
		}

		myCurrActiveStatePtr->myState = CState::EStateCommand::eActive;
		myCurrActiveStatePtr->OnActivation();
	}
}
