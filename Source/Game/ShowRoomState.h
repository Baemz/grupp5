#pragma once
#include "State.h"
#include "..\EngineCore\EventSystem\EventListener.h"
#include "..\CommonUtilities\Vector3.h"

#define DEBUG_SHOWROOM

namespace hse
{
	namespace gfx
	{
		class CModelInstance;
		class CCameraInstance;
		class CEnvironmentalLight;
	};
};

class CShowRoomState: private CState, private IEventListener
{
private:
	enum ECommands
	{
		eNone = 0 << 0
		, eZoomIn = 1 << 0
		, eZoomOut = 1 << 1
		, eRotateXClockWise = 1 << 2
		, eRotateXCClockWise = 1 << 3
		, eRotateYClockWise = 1 << 4
		, eRotateYCClockWise = 1 << 5
		, eRotateZClockWise = 1 << 6
		, eRotateZCClockWise = 1 << 7
		, eResetRotation = 1 << 8
		, eIncreaseRotationSpeed = 1 << 9
		, eDecreaseRotationSpeed = 1 << 10
		, eMoveCamera = 1 << 11
	};
	enum class EMoveMode: unsigned char
	{
		eStatic
		, eFree
	};

	struct SModels
	{
		SModels(): myLODList(nullptr), myLODsCount(0), myPrevLODIndex(0), myCurrLODIndex(0) {}

		hse::gfx::CModelInstance* myLODList;
		unsigned short myLODsCount;
		unsigned short myPrevLODIndex;
		unsigned short myCurrLODIndex;
	};

public:
	CShowRoomState(EManager& aEntityManager);
	~CShowRoomState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void ReceieveEvent(SMessage& aMessageRef) override;

private:
	void ReorderModelsWhenReady();
	void MoveCameraToTargetPosition(const float aDeltaTime);
	void UpdateStates();
	void UpdateControls(const float aDeltaTime);
	void LoadAllModels();
	void GetEveryModelPathAndLatestModelIndex(CU::GrowingArray<CU::GrowingArray<std::string>>& aAllFilePathList,
		unsigned short& aLatestModModelIndexRef, unsigned short& aLatestModLODIndexRef);
	void DeleteAllModels();

private:
	SModels* myModelList;
	//hse::gfx::CModelInstance* myDebugSpherePtr;
	hse::gfx::CModelInstance* myCurrModelPtr;
	hse::gfx::CModelInstance* mySkyboxPtr;
	hse::gfx::CCameraInstance* myCamInstancePtr;
	hse::gfx::CEnvironmentalLight* myEnvLightPtr;
	CU::Vector3f myCamTargetPosition;
	CU::Vector3f myCamTargetDirection;
	CU::Vector3f myCurrObjRotation;
	CU::Vector3f myDeltaFromMouse;
	float myCurrCamMoveSpeed;
	float myObjRotationSpeed;
	float myZoomFromMouse;
	unsigned int myCommandState;
	unsigned short myModelsCount;
	unsigned short myMaxModelsCount;
	unsigned short myPrevModelIndex;
	unsigned short myCurrModelIndex;
	EMoveMode myMode; // Requires a "player".
	bool myDoneReordering;
};