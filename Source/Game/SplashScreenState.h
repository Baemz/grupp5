#pragma once
#include "State.h"
#include "../GraphicsEngine/Sprite/Sprite.h"

class CSplashScreenState: private CState
{
public:
	CSplashScreenState(EManager& aEntityManager);
	~CSplashScreenState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;

private:
	const float GetAlpha(const float aEndTime, const float aStartTime) const;

private:
	hse::gfx::CSprite myTGALogoSprite;
	hse::gfx::CSprite myGroupLogoSprite;
	hse::gfx::CSprite myBackgroundSprite;
	float myTimeLeftToPop;
};