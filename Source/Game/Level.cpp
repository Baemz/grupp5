#include "stdafx.h"
#include "Level.h"
#include "MapReader/MapReader.h"
#include "../EngineCore/WorkerPool/WorkerPool.h"
#include "../CommonUtilities/Stopwatch.h"

CLevel::CLevel()
	: myTimeSpentOnLevel(0.0)
	, myIsLoading(false)
{
}

CLevel::~CLevel()
{
	Destroy();
}

bool CLevel::Init(const char* aLevelPath)
{
	CMapReader mapReader;
	CMapData* mapData(hse_new(CMapData));
	auto future = WORKER_POOL_QUEUE_WORK_FUTURE(&CMapReader::LoadMap, &mapReader, aLevelPath, std::ref<CMapData>(*mapData), -1l);
	// Do stuff while level is loading from disk

	myEnvLight.Init("Data/Models/Misc/test_cubemap1.dds");
	myEnvLight.UseForRendering();

	myCurrentLevelName = aLevelPath;
	myTimeSpentOnLevel = 0.0f;
	myIsLoading = false;

	constexpr unsigned short quadTreeMaxAmountOfObjectsInNode(16);
	const CU::Vector4f quadTreeRootSize(-100, -100, 100, 100);
	myQuadTree.Init(quadTreeMaxAmountOfObjectsInNode, quadTreeRootSize);

	QuadTreeData quadTreeObject;
	quadTreeObject.myCenterPosition = CU::Vector2f(13.37f, -100.9f);
	quadTreeObject.mySize = CU::Vector2f(1.0f, 1.0f);

	for (unsigned short objectIndex(0); objectIndex < 150; ++objectIndex)
	{
		quadTreeObject.myCenterPosition.x = 80.0f * sinf(objectIndex / 100.0f * 3.141f * 2);
		quadTreeObject.myCenterPosition.y += 1.0f;
		myQuadTree.AddObject(quadTreeObject);
	}

	const bool mapLoaded(future.get());

	bool shouldIDeleteMap(false);
	if (mapData != nullptr)
	{
		if (mapLoaded == true)
		{
			myIsLoading = true;
			WORKER_POOL_QUEUE_WORK(&CLevel::LoadLevel, this, mapData);
		}
		else
		{
			GAME_LOG("ERROR! Failed to read level \"%s\"", myCurrentLevelName.c_str());
			shouldIDeleteMap = true;
		}
	}
	else
	{
		GAME_LOG("ERROR! Invalid map data");
		shouldIDeleteMap = true;
	}

	if (shouldIDeleteMap == true)
	{
		hse_delete(mapData);
	}
	return mapLoaded;
}

void CLevel::Destroy()
{
}

bool CLevel::Update(const float aDeltaTime)
{
	if (myIsLoading == true)
	{
		return true;
	}

	myTimeSpentOnLevel += aDeltaTime;
	myEnvLight.UseForRendering();

	static float size(0.0f);
	const CU::Vector4f cullingAABB(-size, -size, size, size);
	size += aDeltaTime;
	if (size > 100.0f)
	{
		size = 100.0f;
	}

	CU::GrowingArray<QuadTreeData> entitiesInAABB(myQuadTree.GetObjectsCloseToAABB(cullingAABB));

#ifndef _RETAIL
	myQuadTree.DebugRenderNodes(cullingAABB);
	//myQuadTree.DebugRenderObjects(cullingAABB);
#endif // !_RETAIL

	return true;
}

const char* CLevel::GetCurrentLevelName() const
{
	return myCurrentLevelName.c_str();
}

double CLevel::GetTimeSpentOnLevel() const
{
	if (myIsLoading == true)
	{
		return -1.0;
	}

	return myTimeSpentOnLevel;
}

void CLevel::LoadLevel(CMapData* aMapData)
{
	START_TIMER(loadTimer);
	int time(0);

	const auto& objectsReadFromMap(aMapData->GetObjects());
	const unsigned int objectsFromMapSize(objectsReadFromMap.Size());

	for (unsigned int objectIndex = 0; objectIndex < objectsFromMapSize; ++objectIndex)
	{
		const CMapDataObject& currentDataObject(objectsReadFromMap[objectIndex]);

		currentDataObject; // Do stuff when we have stuff
	}

	aMapData->Destroy();
	hse_delete(aMapData);
	END_TIMER(loadTimer, time);
	GAME_LOG("Level \"%s\" took %i ms to load"
		, myCurrentLevelName.c_str(), time);

	myIsLoading = false;
}
