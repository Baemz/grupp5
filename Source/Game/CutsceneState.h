#pragma once
#include "State.h"
#include "../EngineCore/EventSystem/EventListener.h"
#include "../GraphicsEngine/ShaderEffects/ShaderEffectInstance.h"

namespace hse
{
	namespace gfx
	{
		class CSprite;
		class CText;
	}
}

class CCutsceneState : private CState, public IEventListener
{
public:
	CCutsceneState(EManager& aEntityManager, const std::string& aCutsceneFolderPath, bool aFadeOutPrevious, float& aCutsceneMusicVolume);
	~CCutsceneState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;

	void ReceieveEvent(SMessage& aMessage) override;

private:
	std::string myFolderPath;
	hse::gfx::CSprite* myCutsceneImages;
	hse::gfx::CSprite* myBlackCanvasSprite;
	hse::gfx::CText* myPressToContinueText;

	enum class EFadeState : char
	{
		None,
		FadeOutPrevious,
		FadeIn,
		FadeOutCutscene,
	};
	const float myTimeToFade;
	float myFadeTimer;
	EFadeState myFadeState;
	char myCurrentCutsceneIndex;
	char myMaxCutsceneIndex;
	char myRenderStaticIndex;

	float& myCutsceneMusicVolume;
};