#pragma once
#include "../GraphicsEngine/Lights/EnvironmentalLight.h"
#include "QuadTreeNode.h"

class CMapData;
class CMapDataObject;

class CLevel
{
public:
	CLevel();
	~CLevel();

	bool Init(const char* aLevelPath);
	void Destroy();

	bool Update(const float aDeltaTime);

	//
	const char* GetCurrentLevelName() const;
	double GetTimeSpentOnLevel() const;

private:
	void LoadLevel(CMapData* aMapData);

	std::string myCurrentLevelName;
	double myTimeSpentOnLevel;
	volatile bool myIsLoading;

	QuadTreeNode myQuadTree;

	// NOTE: Load from level?
	hse::gfx::CEnvironmentalLight myEnvLight;
};
