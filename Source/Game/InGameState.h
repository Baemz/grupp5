#pragma once
#include "State.h"
#include "../EngineCore/EventSystem/EventListener.h"
#include "../GraphicsEngine/Camera/CameraInstance.h"
#include "../GraphicsEngine/Model/ModelInstance.h"
#include "Level.h"

#include "../EntitySystem/Handle.h"

namespace hse { namespace gfx {
	class CText;
}}  


class CInGameState: private CState, public IEventListener
{
public:
	CInGameState(EManager& aEntityManager);
	~CInGameState();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;
	void ReceieveEvent(SMessage& aMessage) override;

private:
	bool RegisterFunctionsFunction();

	SEntityHandle myCamera;
	SEntityHandle myModel;

	SEntityHandle myInputCharacter;

	//hse::gfx::CCameraInstance myCamera;
	//hse::gfx::CModelInstance myModel;

	CLevel myLevel;

};