#pragma once
#include "State.h"
#include "..\EngineCore\EventSystem\EventListener.h"
#include "..\GraphicsEngine\Camera\CameraInstance.h"
#include "..\GraphicsEngine\Model\ModelInstance.h"
#include "..\GraphicsEngine\Lights\EnvironmentalLight.h"
class CTestScene : private CState, public IEventListener
{
public:
	CTestScene(EManager& aEntityManager);
	~CTestScene();

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;

	void ReceieveEvent(SMessage& aMessage) override;

private:

	hse::gfx::CCameraInstance myCamInst;
	hse::gfx::CModelInstance mymdl;
	hse::gfx::CModelInstance mymdl2;
	hse::gfx::CModelInstance knife;
	hse::gfx::CModelInstance couch;
	hse::gfx::CModelInstance myskybox;
	hse::gfx::CEnvironmentalLight myEnvLight;
	hse::gfx::CPointLightInstance myPointLight;
	float dt;
	bool usePointLight;
};

