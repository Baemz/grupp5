#include "stdafx.h"
#include "State.h"

#define SAFE_HSE_DELETE(ptr) if (ptr != nullptr) { hse_delete(ptr); ptr = nullptr; }

CState::CState(EManager& aEntityManager)
	: myEntityManager(aEntityManager)
{
	myUnderStates.Init(2);
	myID = UINT_MAX;
	myState = EStateCommand::eMustInit;
	myCanRenderMainState = false;
}

CState::~CState()
{
	for (unsigned short i(0); i < myUnderStates.Size(); ++i)
	{
		SAFE_HSE_DELETE(myUnderStates[i]);
	}
	myUnderStates.RemoveAll();
}

/* PRIVATE FUNCTIONS */

void CState::InitState(const unsigned int aID, const bool aCanRenderMainState)
{
	myID = aID;
	myCanRenderMainState = aCanRenderMainState;
}
