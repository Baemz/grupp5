#include "stdafx.h"
#include "ShowRoomState.h"

#include "..\EngineCore\FileFinder\FileFinder.h"
#include "..\EngineCore\EventSystem\EventManager.h"
#include "..\EngineCore\Logger\Logger.h"

#include "..\GraphicsEngine\Model\ModelInstance.h"
#include "..\GraphicsEngine\Model\Loading\ModelFactory.h"
#include "..\GraphicsEngine\Camera\CameraInstance.h"
#include "..\GraphicsEngine\Camera\CameraFactory.h"
#include "..\GraphicsEngine\Lights\EnvironmentalLight.h"
#include "..\GraphicsEngine\GraphicsEngineInterface.h"

#ifdef DEBUG_SHOWROOM
#include <sys\types.h>
#include <sys\stat.h>
#endif
#include <Windows.h>
#include "..\GraphicsEngine\DirectXFramework\Direct3D11.h"

#include "..\GraphicsEngine\DebugTools.h"

#define EVENT_MANAGER_PTR (hse::CEventManager::Get())
#define DEFAULT_DISTANCE_BETWEEN_OBJECTS (50.0f)
#define MIN_DISTANCE_BETWEEN_OBJECTS (10.0f)
#define MIN_ZOOM_DISTANCE (5.0f)
#define INCREASE_OBJ_ROTATION_SPEED_MOD (1.2f)
#define DECREASE_OBJ_ROTATION_SPEED_MOD (0.8f)
#define MOUSE_OBJ_ROTATION_MOD (0.001f)
#define MOUSE_SCROLL_ZOOM_SPEED_MOD (100.0f)
#define MOUSE_ZOOM_SPEED_MOD (0.01f)
#define ZOOM_SPEED_MOD (10.0f)

CShowRoomState::CShowRoomState(EManager& aEntityManager)
	: CState(aEntityManager)
	//, myDebugSpherePtr(nullptr)
	, myModelList(nullptr)
	, myCurrModelPtr(nullptr)
	, mySkyboxPtr(nullptr)
	, myCamInstancePtr(nullptr)
	, myEnvLightPtr(nullptr)
	, myCommandState(ECommands::eNone)
	, myCurrCamMoveSpeed(0.0f)
	, myObjRotationSpeed(5.0f)
	, myZoomFromMouse(0.0f)
	, myModelsCount(0)
	, myMaxModelsCount(0)
	, myPrevModelIndex(0)
	, myCurrModelIndex(0)
	, myMode(EMoveMode::eStatic)
	, myDoneReordering(false)
{
}

CShowRoomState::~CShowRoomState()
{
	hse::gfx::CGraphicsEngineInterface::DisableCursor();

	if (EVENT_MANAGER_PTR != nullptr)
	{
		EVENT_MANAGER_PTR->DetachInputListener(this);
	}
	else
	{
		GAME_LOG("[CShowRoomState] ERROR! Failed to detach show room state from event listening because event manager was nullptr.");
	}

	myCurrModelPtr = nullptr;
	hse_delete(myEnvLightPtr);
	hse_delete(myCamInstancePtr);
	hse_delete(mySkyboxPtr);
	//hse_delete(myDebugSpherePtr);
	DeleteAllModels();
}

void CShowRoomState::Init()
{
	hse::gfx::CGraphicsEngineInterface::EnableCursor();

	if (EVENT_MANAGER_PTR != nullptr)
	{
		EVENT_MANAGER_PTR->AttachInputListener(this);
	}
	else
	{
		GAME_LOG("[CShowRoomState] ERROR! Failed to attach show room state to event listening because event manager was nullptr.");
	}

	hse_delete(myCamInstancePtr);
	myCamInstancePtr = hse_new(hse::gfx::CCameraInstance);
	*myCamInstancePtr = hse::gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.0f, 0.f, 0.f });

	hse_delete(mySkyboxPtr);
	mySkyboxPtr = hse_new(hse::gfx::CModelInstance);
	*mySkyboxPtr = hse::gfx::CModelFactory::Get()->CreateSkyboxCube();
	mySkyboxPtr->Init();

	hse_delete(myEnvLightPtr);
	myEnvLightPtr = hse_new(hse::gfx::CEnvironmentalLight);
	myEnvLightPtr->Init("Data/Models/Misc/test_cubemap1.dds");
	myEnvLightPtr->UseForRendering();

	LoadAllModels();

	//myDebugSpherePtr = hse_new(hse::gfx::CModelInstance);
	//*myDebugSpherePtr = hse::gfx::CModelFactory::Get()->CreateCustomSphere({ 1.0f }, 1.0f);
	//myDebugSpherePtr->SetPosition({ 0.0f, 0.0f, 0.0f });

	myDoneReordering = false;
	myPrevModelIndex = USHRT_MAX;
}

void CShowRoomState::Update(const float aDeltaTime)
{
	ReorderModelsWhenReady();

	if (myPrevModelIndex != myCurrModelIndex || (myCurrModelPtr != nullptr && myModelList[myCurrModelIndex].myPrevLODIndex != myModelList[myCurrModelIndex].myCurrLODIndex))
	{
		if (myPrevModelIndex != myCurrModelIndex)
		{
			myModelList[myCurrModelIndex].myCurrLODIndex = 0;
		}

		myPrevModelIndex = myCurrModelIndex;
		myModelList[myCurrModelIndex].myPrevLODIndex = myModelList[myCurrModelIndex].myCurrLODIndex;
		myCurrModelPtr = &myModelList[myCurrModelIndex].myLODList[myModelList[myCurrModelIndex].myCurrLODIndex];

		myCamTargetPosition = myCurrModelPtr->GetPosition() - CU::Vector3f( 0.0f, 0.0f, MIN_ZOOM_DISTANCE + myCurrModelPtr->GetRadious() );
	}

	MoveCameraToTargetPosition(aDeltaTime);
	UpdateControls(aDeltaTime);
	UpdateStates();

	myCommandState = ECommands::eNone;
	myCamInstancePtr->Update(aDeltaTime);
}

void CShowRoomState::Render()
{
	printf("Cam pos = x: %f  y: %f  z: %f\n", myCamInstancePtr->GetPosition().x, myCamInstancePtr->GetPosition().y, myCamInstancePtr->GetPosition().z);

	if (mySkyboxPtr->IsLoaded() && mySkyboxPtr->GetModelID() != UINT_MAX)
	{
		mySkyboxPtr->Render();
	}

	myCamInstancePtr->UseForRendering();
	myEnvLightPtr->UseForRendering();

	//DT_DRAW_SPHERE_5_ARGS({ 0.0f }, {1.0f, 1.0f, 0.0}, 1.0f, 10, 10);
	//DT_DRAW_SPHERE_DEF_COLOR_5_ARGS({ 0.0f }, { myRotationValue, 0.0f, 0.0f }, 1.0f, 10, 10);

	//if (myDebugSpherePtr->IsLoaded() && myDebugSpherePtr->GetModelID() != UINT_MAX)
	//{
	//	myDebugSpherePtr->Render();
	//}

	for (unsigned short i(0); i < myModelsCount; ++i)
	{
		for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
		{
			if (myModelList[i].myLODList[j].IsLoaded() && myModelList[i].myLODList[j].GetModelID() != UINT_MAX)
			{
				myModelList[i].myLODList[j].Render();
			}
		}
	}
}

void CShowRoomState::OnActivation()
{
}

void CShowRoomState::ReceieveEvent(SMessage& aMessageRef)
{
	switch (aMessageRef.messageType)
	{
	case EMessageType::KeyInput:
	{
		SKeyMessage* keyMsgPtr = reinterpret_cast<SKeyMessage*>(&aMessageRef);

		if (keyMsgPtr->state == EButtonState::Pressed)
		{
			if (keyMsgPtr->key == VK_F1)
			{
				myMode = (myMode == EMoveMode::eStatic) ? EMoveMode::eFree : EMoveMode::eStatic;
			}

			if (keyMsgPtr->key == VK_F1)
			{
				if (hse::gfx::CDirect3D11::GetAPI() != nullptr)
				{
					hse::gfx::CDirect3D11::GetAPI()->SetShouldRenderWireFrame();
				}
				else
				{
					GAME_LOG("[CShowRoomState] ERROR! direct 3d11 was not initialized! Idk how tf...");
				}
			}

			if (keyMsgPtr->key == VK_LEFT)
			{
				if (myCurrModelIndex > 0)
				{
					--myCurrModelIndex;
				}
				else
				{
					myCurrModelIndex = myModelsCount - 1;
				}
			}
			else if (keyMsgPtr->key == VK_RIGHT)
			{
				if (myCurrModelIndex < myModelsCount - 1)
				{
					++myCurrModelIndex;
				}
				else
				{
					myCurrModelIndex = 0;
				}
			}

			if (myCurrModelPtr != nullptr)
			{
				if (keyMsgPtr->key == VK_DOWN)
				{
					if (myModelList[myCurrModelIndex].myCurrLODIndex > 0)
					{
						--myModelList[myCurrModelIndex].myCurrLODIndex;
					}
					else
					{
						myModelList[myCurrModelIndex].myCurrLODIndex = myModelList[myCurrModelIndex].myLODsCount - 1;
					}
				}
				else if (keyMsgPtr->key == VK_UP)
				{
					if (myModelList[myCurrModelIndex].myCurrLODIndex < myModelList[myCurrModelIndex].myLODsCount - 1)
					{
						++myModelList[myCurrModelIndex].myCurrLODIndex;
					}
					else
					{
						myModelList[myCurrModelIndex].myCurrLODIndex = 0;
					}
				}
			}

			if (keyMsgPtr->key == VK_RETURN)
			{
				myCommandState |= ECommands::eResetRotation;
			}

			if (keyMsgPtr->key == VK_RETURN)
			{
				myCommandState |= ECommands::eResetRotation;
			}
			if (keyMsgPtr->key == VK_OEM_PLUS)
			{
				myCommandState |= ECommands::eIncreaseRotationSpeed;
			}
			if (keyMsgPtr->key == VK_OEM_MINUS)
			{
				myCommandState |= ECommands::eDecreaseRotationSpeed;
			}
		}

		if (keyMsgPtr->state == EButtonState::Down)
		{
			if (keyMsgPtr->key == VK_LCONTROL) // DOESN'T WORK
			{
				myCommandState |= ECommands::eMoveCamera;
			}

			if (keyMsgPtr->key == 'A' || keyMsgPtr->key == VK_NUMPAD4)
			{
				myCommandState |= ECommands::eRotateXClockWise;
			}
			else if (keyMsgPtr->key == 'D' || keyMsgPtr->key == VK_NUMPAD6)
			{
				myCommandState |= ECommands::eRotateXCClockWise;
			}
			if (keyMsgPtr->key == 'W' || keyMsgPtr->key == VK_NUMPAD8)
			{
				myCommandState |= ECommands::eRotateYClockWise;
			}
			else if (keyMsgPtr->key == 'S' || keyMsgPtr->key == VK_NUMPAD2)
			{
				myCommandState |= ECommands::eRotateYCClockWise;
			}
			if (keyMsgPtr->key == 'Q' || keyMsgPtr->key == VK_NUMPAD7)
			{
				myCommandState |= ECommands::eRotateZClockWise;
			}
			else if (keyMsgPtr->key == 'E' || keyMsgPtr->key == VK_NUMPAD9)
			{
				myCommandState |= ECommands::eRotateZCClockWise;
			}
		}
		break;
	}
	case EMessageType::MouseClickInput:
	{
		SMouseClickMessage* mouseMsgPtr = reinterpret_cast<SMouseClickMessage*>(&aMessageRef);

		if (mouseMsgPtr->key == 1 && mouseMsgPtr->state == EButtonState::Down)
		{
			myDeltaFromMouse.x += -(float)mouseMsgPtr->deltaX;
			myDeltaFromMouse.y += -(float)mouseMsgPtr->deltaY;
		}
		if (mouseMsgPtr->key == 2 && mouseMsgPtr->state == EButtonState::Down)
		{
			myZoomFromMouse += (float)(mouseMsgPtr->deltaX + mouseMsgPtr->deltaY);
		}
		break;
	}
	case EMessageType::MouseInput:
	{
		SMouseDataMessage* mouseMsgPtr = reinterpret_cast<SMouseDataMessage*>(&aMessageRef);

		if (mouseMsgPtr->scrollDelta != 0)
		{
			myZoomFromMouse += (float)mouseMsgPtr->scrollDelta * MOUSE_SCROLL_ZOOM_SPEED_MOD;
		}

		break;
	}
	}
}

/* PRIVATE FUNCTIONS */

void CShowRoomState::ReorderModelsWhenReady()
{
	if (myDoneReordering) return;

	myDoneReordering = true;
	for (unsigned short i(0); i < myModelsCount; ++i)
	{
		for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
		{
			if (!myModelList[i].myLODList[j].IsLoaded() && !myModelList[i].myLODList[j].LoadingFailed())
			{
				myDoneReordering = false;
				break;
			}
		}
	}

	if (myDoneReordering)
	{
		unsigned short lastLODIndex(0);
		hse::gfx::CModelInstance* currModelPtr(nullptr);

		// Remove unloaded models before reordering.
		for (unsigned short i(myModelsCount); i-- > 0;)
		{
			for (unsigned short j(myModelList[i].myLODsCount); j-- > 0;)
			{
				currModelPtr = &myModelList[i].myLODList[j];

				if (currModelPtr->LoadingFailed())
				{
					if (myModelList[i].myCurrLODIndex == j && (myModelList[i].myPrevLODIndex != USHRT_MAX || myModelList[i].myCurrLODIndex != 0))
					{
						myModelList[i].myPrevLODIndex = USHRT_MAX;
						myModelList[i].myCurrLODIndex = 0;
					}

					--myModelList[i].myLODsCount;
					lastLODIndex = myModelList[i].myLODsCount;

					if (lastLODIndex > 0)
					{
						myModelList[i].myLODList[j] = myModelList[i].myLODList[lastLODIndex];
						myModelList[i].myLODList[lastLODIndex] = *currModelPtr;
					}
					else
					{
						--myModelsCount;
						SModels modelToRemove = myModelList[i];
						myModelList[i] = myModelList[myModelsCount];
						myModelList[myModelsCount] = modelToRemove;

						if (myCurrModelIndex == i && myCurrModelIndex != 0)
						{
							myCurrModelIndex = 0;
						}
					}
				}
			}
		}

		float objRadious(0.0f);
		for (unsigned short i(0); i < myModelsCount; ++i)
		{
			for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
			{
				currModelPtr = &myModelList[i].myLODList[j];
				objRadious = currModelPtr->GetRadious() * (float)(j > 0);

				currModelPtr->SetPosition({ MIN_DISTANCE_BETWEEN_OBJECTS * i + objRadious, MIN_DISTANCE_BETWEEN_OBJECTS * j + objRadious, 0.0f });
			}
		}

		myPrevModelIndex = USHRT_MAX;
	}
}

void CShowRoomState::MoveCameraToTargetPosition(const float aDeltaTime)
{
	if (myCamTargetPosition.x == 0.0f && myCamTargetPosition.y == 0.0f && myCamTargetPosition.z == 0.0f) return;

#ifdef DEBUG_SHOWROOM
	myCamInstancePtr->SetPosition(myCamTargetPosition);
	myCamTargetPosition.x = 0.0f;
	myCamTargetPosition.y = 0.0f;
	myCamTargetPosition.z = 0.0f;
	aDeltaTime;
#endif
#ifndef DEBUG_SHOWROOM
	myCamTargetDirection = myCamTargetPosition - myCamInstancePtr->GetPosition();

	if (myCamTargetDirection.Length2() > 8.0f)
	{
		myCurrCamMoveSpeed = myCamTargetDirection.Length2();
	}

	if (myCamTargetDirection.Length2() > 0.01f)
	{
		myCamTargetDirection.Normalize();

		myCamInstancePtr->Move(myCamTargetDirection, myCurrCamMoveSpeed * aDeltaTime);
	}
	else
	{
		myCamInstancePtr->SetPosition(myCamTargetPosition);
		myCamTargetPosition.x = 0.0f;
		myCamTargetPosition.y = 0.0f;
		myCamTargetPosition.z = 0.0f;
		myCurrCamMoveSpeed = 0.0f;
	}
#endif
}

void CShowRoomState::UpdateStates()
{
	// Rotation reset
	if ((myCommandState & ECommands::eResetRotation) != 0)
	{
		CU::Vector3f prevPosition(myCurrModelPtr->GetPosition());

		myCurrModelPtr->SetOrientation(CU::Matrix44f::Identity);
		myCurrModelPtr->SetPosition(prevPosition);
	}

	if ((myCommandState & ECommands::eIncreaseRotationSpeed) != 0)
	{
		myObjRotationSpeed *= INCREASE_OBJ_ROTATION_SPEED_MOD;
	}
	if ((myCommandState & ECommands::eDecreaseRotationSpeed) != 0)
	{
		myObjRotationSpeed *= DECREASE_OBJ_ROTATION_SPEED_MOD;
	}
}

void CShowRoomState::UpdateControls(const float aDeltaTime)
{
	if (myCurrCamMoveSpeed != 0.0f) return;

	// Zoom
	if ((myCommandState & ECommands::eZoomOut) != 0)
	{
		myCamInstancePtr->Move({ 0.0f, 0.0, -1.0f }, aDeltaTime * ZOOM_SPEED_MOD);
	}
	if ((myCommandState & ECommands::eZoomIn) != 0)
	{
		myCamInstancePtr->Move({ 0.0f, 0.0f, 1.0f }, aDeltaTime * ZOOM_SPEED_MOD);
	}
	if (myZoomFromMouse != 0.0f)
	{
		myCamInstancePtr->Move({ 0.0f, 0.0f, myZoomFromMouse }, MOUSE_ZOOM_SPEED_MOD);
		myZoomFromMouse = 0.0f;
	}

	if (myCamInstancePtr->GetPosition().z > MIN_ZOOM_DISTANCE + myCurrModelPtr->GetRadious())
	{
		myCamInstancePtr->SetPosition(CU::Vector3f(myCamInstancePtr->GetPosition().x, myCamInstancePtr->GetPosition().y, MIN_ZOOM_DISTANCE + myCurrModelPtr->GetRadious()));
	}

	// Rotation
	if ((myCommandState & ECommands::eRotateXClockWise) != 0)
	{
		myCurrObjRotation.x = 1.0f;
	}
	else if ((myCommandState & ECommands::eRotateXCClockWise) != 0)
	{
		myCurrObjRotation.x = -1.0f;
	}
	if ((myCommandState & ECommands::eRotateYClockWise) != 0)
	{
		myCurrObjRotation.y = 1.0f;
	}
	else if ((myCommandState & ECommands::eRotateYCClockWise) != 0)
	{
		myCurrObjRotation.y = -1.0f;
	}
	if ((myCommandState & ECommands::eRotateZClockWise) != 0)
	{
		myCurrObjRotation.z = 1.0f;
	}
	else if ((myCommandState & ECommands::eRotateZCClockWise) != 0)
	{
		myCurrObjRotation.z = -1.0f;
	}

	if (myCurrObjRotation.x != 0.0f || myCurrObjRotation.y != 0.0f || myCurrObjRotation.z != 0.0f)
	{
		myCurrModelPtr->Rotate(myCurrObjRotation * myObjRotationSpeed * aDeltaTime);
		myCurrObjRotation.x = 0.0f;
		myCurrObjRotation.y = 0.0f;
		myCurrObjRotation.z = 0.0f;
	}
	

	// Move
	if (myDeltaFromMouse.x != 0.0f || myDeltaFromMouse.y != 0.0f || myDeltaFromMouse.z != 0.0f)
	{
		if ((myCommandState & ECommands::eMoveCamera) != 0)
		{
			myCamInstancePtr->Move(myDeltaFromMouse, myObjRotationSpeed * MOUSE_OBJ_ROTATION_MOD);
			
		}
		else
		{
			myCurrModelPtr->Rotate(myDeltaFromMouse * myObjRotationSpeed * MOUSE_OBJ_ROTATION_MOD);
		}

		myDeltaFromMouse.x = 0.0f;
		myDeltaFromMouse.y = 0.0f;
		myDeltaFromMouse.z = 0.0f;
	}
}

void CShowRoomState::LoadAllModels()
{
	CU::GrowingArray<CU::GrowingArray<std::string>> filePathList;
	unsigned short latestModLODIndex;
	filePathList.Init(32);

	GetEveryModelPathAndLatestModelIndex(filePathList, myCurrModelIndex, latestModLODIndex);

	if (filePathList.Size() <= 0 && myCurrModelIndex == UINT_MAX)
	{
		GAME_LOG("[CShowRoomState] WARNING! Could not find any models inside model folder.");
		return;
	}
	else if (filePathList.Size() > 0 && myCurrModelIndex == UINT_MAX)
	{
		GAME_LOG("[CShowRoomState] ERROR! Failed to get latest modified model even though there are models inside model folder.");
	}

	DeleteAllModels();
	myModelsCount = filePathList.Size();
	myMaxModelsCount = myModelsCount;
	myModelList = hse_newArray(SModels, myModelsCount);
	
	for (unsigned short i(0); i < myModelsCount; ++i)
	{
		myModelList[i].myLODsCount = filePathList[i].Size();
		myModelList[i].myLODList = hse_newArray(hse::gfx::CModelInstance, myModelList[i].myLODsCount);

		for (unsigned short j(0); j < myModelList[i].myLODsCount; ++j)
		{
			if (i == myCurrModelIndex && j == latestModLODIndex)
			{
				myModelList[i].myCurrLODIndex = latestModLODIndex;
			}

			myModelList[i].myLODList[j].SetModelPath(filePathList[i][j]);
			myModelList[i].myLODList[j].SetPosition({ DEFAULT_DISTANCE_BETWEEN_OBJECTS * i, DEFAULT_DISTANCE_BETWEEN_OBJECTS * j, 0.0f });
			myModelList[i].myLODList[j].Init();
		}
	}
}

void CShowRoomState::GetEveryModelPathAndLatestModelIndex(CU::GrowingArray<CU::GrowingArray<std::string>>& aAllFilePathList,
	unsigned short& aLatestModModelIndexRef, unsigned short& aLatestModLODIndexRef)
{
	CU::GrowingArray<std::string> allFilePathList;
	allFilePathList.Init(16);

	hse::CFileFinder::GetFilePathsRecursive(allFilePathList, ".fbx", "Data\\Models\\");

	CU::GrowingArray<std::string> filePathLODList;
	filePathLODList.Init(16);
	std::string cutLODName("");
	size_t namePos(std::string::npos);
	size_t typePos(std::string::npos);
	bool foundPlaceForLODPath(false);

	for (unsigned short i(0); i < allFilePathList.Size(); ++i)
	{
		namePos = allFilePathList[i].find_last_of('\\');
		if (namePos != std::string::npos)
		{
			typePos = allFilePathList[i].rfind("_LOD");
			if (typePos != std::string::npos && typePos > namePos)
			{
				cutLODName = allFilePathList[i].substr(namePos, typePos - namePos);
				foundPlaceForLODPath = false;

				for (unsigned short j(0); j < aAllFilePathList.Size(); ++j)
				{
					if (aAllFilePathList[j][0].find(cutLODName) != std::string::npos)
					{
						aAllFilePathList[j].Add(allFilePathList[i]);
						foundPlaceForLODPath = true;
						break;
					}
				}

				if (!foundPlaceForLODPath)
				{
					filePathLODList.Add(allFilePathList[i]);
				}
			}
			else
			{
				aAllFilePathList.Add(CU::GrowingArray<std::string>());
				aAllFilePathList.GetLast().Init(8);
				aAllFilePathList.GetLast().Add(allFilePathList[i]);
			}
		}
		else
		{
			GAME_LOG("[CShowRoomState] ERROR! Could not find \"\\\" in a file path: \"%s\"", allFilePathList[i].c_str());
		}
	}

	for (unsigned short i(0); i < filePathLODList.Size(); ++i)
	{
		namePos = filePathLODList[i].find_last_of('\\');
		typePos = filePathLODList[i].rfind("_LOD");
		cutLODName = filePathLODList[i].substr(namePos, typePos - namePos);
		foundPlaceForLODPath = false;

		for (unsigned short j(0); j < aAllFilePathList.Size(); ++j)
		{
			if (aAllFilePathList[j][0].find(cutLODName) != std::string::npos)
			{
				aAllFilePathList[j].Add(filePathLODList[i]);
				foundPlaceForLODPath = true;
				break;
			}
		}

		if (!foundPlaceForLODPath)
		{
			GAME_LOG("[CShowRoomState] WARNING! Found a LOD model without main model. It will not be loaded. Path: \"%s\".", filePathLODList[i].c_str());
		}
	}
	allFilePathList.RemoveAll();
	filePathLODList.RemoveAll();

#ifdef DEBUG_SHOWROOM
	long long latestTime(0);
	struct _stat result;

	for (unsigned short i(0); i < aAllFilePathList.Size(); ++i)
	{
		for (unsigned short j(0); j < aAllFilePathList[i].Size(); ++j)
		{
			if (_stat(aAllFilePathList[i][j].c_str(), &result) == 0)
			{
				if (latestTime < result.st_mtime)
				{
					latestTime = result.st_mtime;
					aLatestModModelIndexRef = i;
					aLatestModLODIndexRef = j;
				}
			}
		}
	}
#endif
#ifndef DEBUG_SHOWROOM
	aLatestModModelIndexRef = 0;
	aLatestModLODIndexRef = 0;
#endif
}

void CShowRoomState::DeleteAllModels()
{
	for (unsigned short i(0); i < myMaxModelsCount; ++i)
	{
		hse_delete(myModelList[i].myLODList);
		myModelList[i].myLODsCount = 0;
	}
	hse_delete(myModelList);
	myModelsCount = 0;
}
