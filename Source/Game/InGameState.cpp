#include "stdafx.h"
#include "InGameState.h"
#include "../EngineCore/CommandLineManager/CommandLineManager.h"
#include "../EngineCore/WorkerPool/WorkerPool.h"
#include "../GraphicsEngine/Camera/CameraFactory.h"
#include "../GraphicsEngine/Model/Loading/ModelFactory.h"
#include "../GraphicsEngine/DirectXFramework/Direct3D11.h"
#include "../EngineCore/EventSystem/EventManager.h"
#include "../GraphicsEngine/DebugTools.h"

#include "EntitySystemLink/EntitySystemLink.h"
#include "GameSystems/RenderSystem.h"
#include "GameSystems/ControllerSystem.h"
#include "GameSystems/GameSystems.h"

#include "../Script/ScriptManager.h"

#include "../CommonUtilities/Random.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"

CInGameState::CInGameState(EManager& aEntityManager)
	: CState(aEntityManager)
{

}

CInGameState::~CInGameState()
{
	while (hse::CWorkerPool::GetHasWorkToDo())
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
	hse::CEventManager::Get()->DetachInputListener(this);
}

void CInGameState::Init()
{
	if (hse::CCommandLineManager::HasParameter(L"-showroom"))
	{
	}
	else
	{
		hse::CEventManager::Get()->AttachInputListener(this);
		hse::gfx::CGraphicsEngineInterface::EnableCursor();

		myCamera = myEntityManager.CreateHandle();
		myEntityManager.AddComponent<CompCameraInstance>(myEntityManager.GetEntityIndex(myCamera));
		myEntityManager.AddTag<TagCamera>(myEntityManager.GetEntityIndex(myCamera));
		//myModel = myEntityManager.CreateHandle();
		//myEntityManager.AddComponent<CompRender>(myEntityManager.GetEntityIndex(myModel));
		//myEntityManager.AddComponent<CompPosition>(myEntityManager.GetEntityIndex(myModel));

		myInputCharacter = myEntityManager.CreateHandle();
		myEntityManager.AddTag<TagInputController>(myInputCharacter);
		myEntityManager.AddComponent<CompPosition>(myInputCharacter);
		myEntityManager.AddComponent<CompInputController>(myInputCharacter);

		auto aiEvent1(myEntityManager.CreateIndex());
		auto aiEvent2(myEntityManager.CreateIndex());
		auto aiPoll1(myEntityManager.CreateIndex());
		auto aiPoll2(myEntityManager.CreateIndex());
		myEntityManager.AddTag<TagAIController>(aiEvent1);
		myEntityManager.AddComponent<CompAIController>(aiEvent1, CompAIController::Events);
		myEntityManager.AddTag<TagAIController>(aiEvent2);
		myEntityManager.AddComponent<CompAIController>(aiEvent2, CompAIController::Events);
		myEntityManager.AddTag<TagAIController>(aiPoll1);
		myEntityManager.AddComponent<CompAIController>(aiPoll1, CompAIController::Polling);
		myEntityManager.AddTag<TagAIController>(aiPoll2);
		myEntityManager.AddComponent<CompAIController>(aiPoll2, CompAIController::Polling);

		for (std::size_t i(aiEvent1); i <= aiPoll2; ++i)
		{
			CU::Vector3f pos(CU::GetRandomInRange(-15.0f, 15.0f), 0, CU::GetRandomInRange(-15.0f, 15.0f));
			myEntityManager.AddComponent<CompPosition>(i, pos);
		}

		auto simpleGrid(myEntityManager.CreateHandle());
		myEntityManager.AddComponent<CompSimpleAABBGrid>(simpleGrid, 8.0f, CU::Vector2f(100, 100));

		myLevel.Init("");

		//Functions aren'THICC run on added entities until refresh is called, make sure to call it before calling any init functions.
		myEntityManager.Refresh();
		CGameSystems::Get()->InitSystem<CRenderSystem>(myEntityManager);
		CGameSystems::Get()->InitSystem<CControllerSystem>(myEntityManager, myCamera, myInputCharacter, simpleGrid);

		auto rotateCamera = [](auto&, auto& aCameraComponent)
		{
			aCameraComponent.myInstance.Rotate({ CU::Pif / 6.0f, CU::Pif / 4.0f, 0.0f });
		};
		auto setPosCamera = [](auto&, auto& aCameraComponent)
		{
			aCameraComponent.myInstance.SetPosition({ -30.0f, 40.0f, -30.0f });
		};

		myEntityManager.RunIfMatching<SigCamera>(myCamera, rotateCamera);
		myEntityManager.RunIfMatching<SigCamera>(myCamera, setPosCamera);
	}
}

void CInGameState::Update(const float aDeltaTime)
{
	CScriptManager::Get()->CallFunction("Update", aDeltaTime);
	//hse::gfx::CDebugTools::Get()->DrawLine({ 0.0f, 0.0f, 0.0f }, { 0.0f, 100.0f, 0.0f });
	//hse::gfx::CDebugTools::Get()->DrawRectangle3D(CU::Vector3f::Zero, { 20.f, 100.f, 60.f }, { 0.1f, 0.5f, 0.4f });
	//hse::gfx::CDebugTools::Get()->DrawCube({ 0 }, 30, { 0.2f });
	//hse::gfx::CDebugTools::Get()->DrawText2D("Testing one liner text -> DeltaTime: " + std::to_string(aDeltaTime), { 0.3f, 0.0f }, 24.0f, { 1.0f, 0.0f, 1.0f });
	//hse::gfx::CDebugTools::Get()->DrawLine2D({ 0.0f, 0.0f }, { 0.5f, 0.5f });
	//myLevel.Update(aDeltaTime);



//	auto movePosCamera = [aDeltaTime](auto&, auto& aCameraComponent)
//	{
//		aCameraComponent.myInstance.SetPosition(aCameraComponent.myInstance.GetPosition() - CU::Vector3f(0.0f, 2.5f * aDeltaTime, -5.0f * aDeltaTime));
//	};
//	auto rotateCamera = [aDeltaTime](auto&, auto& aCameraComponent)
//	{
////		aCameraComponent.myInstance.Rotate({ 0.0f, 0.0f, aDeltaTime * 0.5f });
//	};

	//myEntityManager.RunIfMatching<SigCamera>(myCamera, rotateCamera);
	//myEntityManager.RunIfMatchingAssertIfNot<SigCamera>(myCamera, movePosCamera);
}

void CInGameState::Render()
{
	//Deprecated (?)
}

void CInGameState::OnActivation()
{
}

void CInGameState::ReceieveEvent(SMessage& aMessage)
{
	if (aMessage.messageType == EMessageType::KeyInput)
	{
		SKeyMessage* keyMsgPtr = reinterpret_cast<SKeyMessage*>(&aMessage);

		if (keyMsgPtr->key == CU::CKeyBinds::GetHotKeys().myF1 && keyMsgPtr->state == EButtonState::Pressed)
		{
			hse::gfx::CDirect3D11::GetAPI()->SetShouldRenderWireFrame();
		}
	}
}

bool CInGameState::RegisterFunctionsFunction()
{
	//CScriptManager& scriptManager(*CScriptManager::Get());

	//scriptManager.RegisterFunction("Print", PrintMsg,
	//	"Prints a message to the console window. \nTakes in only one parameter (can be a string, number or bool). Returns no values.");

	return true;
}