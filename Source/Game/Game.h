#pragma once
#include "../CommonUtilities/GrowingArray.h"

using EntityManager = void;

class CScriptManager;

class CGame
{
public:
	CGame();
	~CGame();

	void Init();
	void Update(const float aDeltaTime);

private:
	bool RegisterScriptFunctions(CScriptManager& aScriptManager);

	void UpdateMusicPosition(const float aDeltaTime); // HACK
	float myCurrRadian;
	bool myReturn;
	EntityManager* myEntityManagerVoid;
	//CGameSystems* myGameSystems;
};

