#pragma once
#include "../CommonUtilities/GrowingArray.h"

#include "EntitySystemLink/EntitySystemLink.h"

class CState
{
	friend class CStateStack;

public:
	enum class EStateCommand
	{
		eMustInit,
		eActive,
		eKeepState,
		eRemoveState,
		eRemoveMainState
	};

public:
	CState(EManager& aEntityManager);
	virtual ~CState();

	virtual void Init() = 0;
	virtual void Update(const float aDeltaTime) = 0;
	virtual void Render() = 0;

	virtual void OnActivation() = 0;

private:
	void InitState(const unsigned int aID, const bool aCanRenderMainState);

private:
	CU::GrowingArray<CState*> myUnderStates;
	unsigned int myID;
	bool myCanRenderMainState;

protected:
	EStateCommand myState;
	EManager& myEntityManager;
};