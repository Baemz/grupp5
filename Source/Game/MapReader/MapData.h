#pragma once
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/Vector.h"

class CMapReader;
class CMapData;

class CMapDataObject
{
	friend CMapReader;
	friend CMapData;
	friend bool GetMapDataObject(char*, long long&, const long long&, CMapDataObject&);
	friend bool GetChildren(char*, long long&, const long long&, CMapDataObject&);

public:
	struct STransform
	{
		CU::Vector3f myPosition;
		CU::Vector3f myRotation;
		CU::Vector3f myScale;
	};
	template<typename StorageType>
	struct SArray
	{
		friend CMapData;

	public:
		inline const StorageType& operator[](const int& aIndex) const { return myData[aIndex]; };
		int Size() const { return mySize; };

	private:
		int mySize = 0;
		StorageType* myData = nullptr;

	};

	struct STypeData
	{
		enum class eObjectType
		{
			NONE,
		};

		eObjectType myObjectType;
	};

	struct SLight
	{
		enum class eLightType : char
		{
			NOT_SET,
			POINT,
			DIRECTIONAL,
			SPOT,
		};
		struct SPointLight
		{
			float aRange;
		};
		struct SDirectionalLight
		{
			CU::Vector3f myDirection;
		};
		struct SSpotLight
		{
			CU::Vector3f myDirection;
			float myAngle;
			float myRange;
		};
		union
		{
			SPointLight* myPointLightData = nullptr;
			SDirectionalLight* myDirectionalLightData;
			SSpotLight* mySpotLightData;
		};

		SLight()
			: myLightType(nullptr)
			, myColor(nullptr)
			, myIntensity(nullptr)
		{};

		eLightType* myLightType;
		CU::Vector4f* myColor;
		float* myIntensity;

	};

	CMapDataObject();
	~CMapDataObject() = default;

	const char* const GetName() const;
	const CU::GrowingArray<STransform, unsigned int>& GetTransforms() const;
	const CU::GrowingArray<CMapDataObject, unsigned int>& GetChildren() const;
	const CU::GrowingArray<SLight, unsigned int>& GetLights() const;

	int GetInstanceID() const;
	const char* const GetTag() const;
	const char* const GetPrefab() const;
	const STypeData* GetTypeData() const;

private:
	const char* myName;
	CU::GrowingArray<STransform, unsigned int> myTransforms;
	CU::GrowingArray<CMapDataObject, unsigned int> myChildren;
	CU::GrowingArray<SLight, unsigned int> myLights;

	int* myInstanceID;
	const char* myTag;
	const char* myPrefab;
	STypeData* myTypeData;
};

class CMapData
{
	friend CMapReader;

public:
	struct SLevelInfo
	{
		friend CMapData;
		friend CMapReader;
		friend bool GetLevelInfo(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo& aLevelInfo);

	public:

	private:
		SLevelInfo()
		{};

	};

	CMapData();
	~CMapData() = default;

	void Destroy();

	const SLevelInfo& GetLevelInfo() const;
	const CU::GrowingArray<CMapDataObject, unsigned int>& GetObjects() const;

private:
	void DestroyDataObject(CMapDataObject& aMapDataObject);

	char* myRawData;
	long long myRawDataSize;

	SLevelInfo myLevelInfo;
	CU::GrowingArray<CMapDataObject, unsigned int> myObjects;

};
