#include "stdafx.h"
#include "MapData.h"

CMapDataObject::CMapDataObject()
	: myName(nullptr)
	, myInstanceID(nullptr)
	, myTag(nullptr)
	, myPrefab(nullptr)
	, myTypeData(nullptr)
{
}

const char* const CMapDataObject::GetName() const
{
	return myName;
}

const CU::GrowingArray<CMapDataObject::STransform, unsigned int>& CMapDataObject::GetTransforms() const
{
	return myTransforms;
}

const CU::GrowingArray<CMapDataObject, unsigned int>& CMapDataObject::GetChildren() const
{
	return myChildren;
}

const CU::GrowingArray<CMapDataObject::SLight, unsigned int>& CMapDataObject::GetLights() const
{
	return myLights;
}

int CMapDataObject::GetInstanceID() const
{
	return *myInstanceID;
}

const char* const CMapDataObject::GetTag() const
{
	return myTag;
}

const char* const CMapDataObject::GetPrefab() const
{
	return myPrefab;
}

const CMapDataObject::STypeData* CMapDataObject::GetTypeData() const
{
	return myTypeData;
}

CMapData::CMapData()
	: myRawData(nullptr)
	, myRawDataSize(0)
{
}

void CMapData::Destroy()
{
	if (myRawData != nullptr)
	{
		for (unsigned int objectIndex = 0; objectIndex < myObjects.Size(); ++objectIndex)
		{
			CMapDataObject& currentObject(myObjects[objectIndex]);
			DestroyDataObject(currentObject);
		}

		auto objectsDataPointer(myObjects.GetRawData());
		hse_delete(objectsDataPointer);

		hse_delete(myRawData);
		myRawDataSize = 0;
	}
}

const CMapData::SLevelInfo& CMapData::GetLevelInfo() const
{
	return myLevelInfo;
}

const CU::GrowingArray<CMapDataObject, unsigned int>& CMapData::GetObjects() const
{
	return myObjects;
}

void CMapData::DestroyDataObject(CMapDataObject& aMapDataObject)
{
	auto lightDataPointer(aMapDataObject.myLights.GetRawData());
	hse_delete(lightDataPointer);

	for (unsigned int childObjectIndex = 0; childObjectIndex < aMapDataObject.myChildren.Size(); ++childObjectIndex)
	{
		DestroyDataObject(aMapDataObject.myChildren[childObjectIndex]);
	}

	auto childrenDataPointer(aMapDataObject.myChildren.GetRawData());
	hse_delete(childrenDataPointer);
}
