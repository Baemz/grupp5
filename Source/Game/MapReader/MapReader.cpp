#include "stdafx.h"
#include "MapReader.h"
#include <fstream>

#ifdef max
#undef max
#endif // max

#define ALLOWED_TO_READ(aDataIndex, aDataIndexMax, aSize)						\
if (AllowedToReadData((aDataIndex), (aDataIndexMax), (aSize)) == false)			\
{																				\
	return false;																\
}

#define NEXT_IS_TEXT_LOADMAP(aText)						\
NextIsText(*rawData, dataIndex, byteLength, aText)
#define NEXT_IS_TEXT_OTHER(aText)						\
NextIsText(aData, aDataIndex, aDataIndexMax, aText)

#define STOP_THIS "STOP_THIS"

bool OpenFile(const char* aFilePath, std::ifstream& aFileStream, std::ios_base::openmode aOpenMode);

bool GetLevelInfo(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo& aLevelInfo);

bool GetMapDataObject(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject);
bool GetChildren(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject);

bool NextIsText(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char* aText);

bool GetIntString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aNameToFill);
bool GetTransforms(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::GrowingArray<CMapDataObject::STransform, unsigned int>& aArrayToFill);
bool GetType(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject::STypeData** aTypeDataToFill);
bool GetLights(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::GrowingArray<CMapDataObject::SLight, unsigned int>& aLightsToFill);

bool GetInt(char* aData, long long& aDataIndex, const long long& aDataIndexMax, int** aIntToFill);
bool GetString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aStringToFill, const int aSizeToGet);
bool GetIntString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aNameToFill);
bool GetChar(char* aData, long long& aDataIndex, const long long& aDataIndexMax, char** aCharToFill);
bool GetFloat(char* aData, long long& aDataIndex, const long long& aDataIndexMax, float** aFloatToFill);
bool GetBool(char* aData, long long& aDataIndex, const long long& aDataIndexMax, bool** aBoolToFill);

bool MovePointerSize(long long& aDataIndex, const long long& aDataIndexMax);

bool AllowedToReadData(const long long& aDataIndex, const long long& aDataIndexMax, const long long& aSize);

const long long sizeOfInt = sizeof(int);
const long long sizeOfFloat = sizeof(float);
const long long sizeOfPointer = sizeof(void*);
const long long sizeOfChar = sizeof(char);
const long long sizeOfBool = sizeof(bool);

CMapReader::CMapReader()
{
}

CMapReader::~CMapReader()
{
}

bool CMapReader::LoadMap(const char* aMapPath, CMapData& aMapDataToFill, const long long aMaxBytesToRead) const
{
	assert("Tried to load a map into an already used CMapData" && (aMapDataToFill.myObjects.Size() == 0));

	std::ifstream mapFile;
	if (OpenFile(aMapPath, mapFile, std::fstream::in | std::fstream::binary) == false)
	{
		return false;
	}

	mapFile.ignore(std::numeric_limits<std::streamsize>::max());
	const std::streamsize byteLength = mapFile.gcount();

	if ((aMaxBytesToRead < byteLength) && (aMaxBytesToRead != -1))
	{
		return false;
	}

	mapFile.clear();
	mapFile.seekg(0, std::ios_base::beg);

	aMapDataToFill.myRawData = hse_newArray(char, byteLength);
	aMapDataToFill.myRawDataSize = byteLength;

	char** rawData(&aMapDataToFill.myRawData);

	mapFile.read(*rawData, byteLength);
	mapFile.close();

	long long dataIndex(0);

	if (GetLevelInfo(*rawData, dataIndex, byteLength, aMapDataToFill.myLevelInfo) == false)
	{
		return false;
	}

	int* amountOfObjects(nullptr);
	if (GetInt(*rawData, dataIndex, byteLength, &amountOfObjects) == false)
	{
		return false;
	}
	{
		CMapDataObject* mapDataObjects(hse_newArray(CMapDataObject, *amountOfObjects));
		aMapDataToFill.myObjects.SetRawData(mapDataObjects, *amountOfObjects);
	}

	unsigned int objectIndex(0);
	while (NEXT_IS_TEXT_LOADMAP("OBJECT") == true)
	{
		if (GetMapDataObject(*rawData, dataIndex, byteLength, aMapDataToFill.myObjects[objectIndex++]) == false)
		{
			return false;
		}
	}

	return (dataIndex == byteLength);
}

bool OpenFile(const char* aFilePath, std::ifstream& aFileStream, std::ios_base::openmode aOpenMode)
{
	if (aFilePath == nullptr)
	{
		return false;
	}

	aFileStream.open(aFilePath, aOpenMode);

	return aFileStream.good();
}

bool GetLevelInfo(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapData::SLevelInfo& aLevelInfo)
{
	if (NEXT_IS_TEXT_OTHER("LEVELINFO") == false)
	{
		return false;
	}

	aLevelInfo;

	return true;
}

bool GetMapDataObject(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject)
{
	CMapDataObject& newObject(aMapDataObject);

	if (GetIntString(aData, aDataIndex, aDataIndexMax, newObject.myName) == false)
	{
		return false;
	}
	if (GetInt(aData, aDataIndex, aDataIndexMax, &newObject.myInstanceID) == false)
	{
		return false;
	}
	if (GetIntString(aData, aDataIndex, aDataIndexMax, newObject.myTag) == false)
	{
		return false;
	}

	if (NEXT_IS_TEXT_OTHER("PREFAB") == true)
	{
		if (GetIntString(aData, aDataIndex, aDataIndexMax, newObject.myPrefab) == false)
		{
			return false;
		}
	}

	bool wroteSomething(true);
	while (wroteSomething == true)
	{
		wroteSomething = false;

		if (NEXT_IS_TEXT_OTHER(STOP_THIS) == true)
		{
			// Do nothing
		}
		else if (NEXT_IS_TEXT_OTHER("TRANSFORM") == true)
		{
			if (GetTransforms(aData, aDataIndex, aDataIndexMax, newObject.myTransforms) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
		else if (NEXT_IS_TEXT_OTHER("TYPE") == true)
		{
			if (GetType(aData, aDataIndex, aDataIndexMax, &newObject.myTypeData) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
		else if (NEXT_IS_TEXT_OTHER("LIGHT") == true)
		{
			if (GetLights(aData, aDataIndex, aDataIndexMax, newObject.myLights) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
		else if (NEXT_IS_TEXT_OTHER("CHILD") == true)
		{
			if (GetChildren(aData, aDataIndex, aDataIndexMax, newObject) == false)
			{
				return false;
			}
			wroteSomething = true;
		}
	}

	return true;
}

bool GetChildren(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CMapDataObject& aMapDataObject)
{
	int* amountOfChildren(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &amountOfChildren) == false)
	{
		return false;
	}

	{
		CMapDataObject* mapDataObjects(hse_newArray(CMapDataObject, *amountOfChildren));
		aMapDataObject.myChildren.SetRawData(mapDataObjects, static_cast<unsigned int>(*amountOfChildren));
	}

	for (unsigned int childIndex = 0; childIndex < aMapDataObject.myChildren.Size(); ++childIndex)
	{
		if ((NEXT_IS_TEXT_OTHER("OBJECT") && GetMapDataObject(aData, aDataIndex, aDataIndexMax, aMapDataObject.myChildren[childIndex])) == false)
		{
			return false;
		}
	}

	return true;
}

bool NextIsText(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char* aText)
{
	const std::string textToCompare(aText);
	const long long sizeOfText(textToCompare.size());

	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfText);

	if (std::string(&aData[aDataIndex], sizeOfText) == textToCompare)
	{
		aDataIndex += sizeOfText + 1;
		return true;
	}

	return false;
}

bool GetTransforms(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::GrowingArray<CMapDataObject::STransform, unsigned int>& aArrayToFill)
{
	int* amountOfTransforms(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &amountOfTransforms) == false)
	{
		return false;
	}

	const long long amountOfVectorsInTransform(3);
	const long long amountOfFloatsInVector(3);
	const long long amountToReadInBytes((*amountOfTransforms) * (amountOfVectorsInTransform * (amountOfFloatsInVector * sizeOfFloat)));
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytes);

	aArrayToFill.SetRawData((CMapDataObject::STransform*)(&aData[aDataIndex]), (*amountOfTransforms));

	aDataIndex += amountToReadInBytes;

	return true;
}

bool GetType(char* aData, long long & aDataIndex, const long long& aDataIndexMax, CMapDataObject::STypeData** aTypeDataToFill)
{
	const int sizeOfSTypeData(sizeof(CMapDataObject::STypeData));
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfSTypeData);

	*aTypeDataToFill = (CMapDataObject::STypeData*)(&aData[aDataIndex]);

	aDataIndex += sizeOfSTypeData;

	return true;
}

bool GetLights(char* aData, long long& aDataIndex, const long long& aDataIndexMax, CU::GrowingArray<CMapDataObject::SLight, unsigned int>& aLightsToFill)
{
	int* amountOfLights(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &amountOfLights) == false)
	{
		return false;
	}

	if (*amountOfLights > 0)
	{
		{
			CMapDataObject::SLight* lights(hse_newArray(CMapDataObject::SLight, *amountOfLights));
			aLightsToFill.SetRawData(lights, static_cast<unsigned int>(*amountOfLights));
		}

		for (unsigned int lightIndex = 0; lightIndex < aLightsToFill.Size(); ++lightIndex)
		{
			CMapDataObject::SLight& newLight(aLightsToFill[lightIndex]);

			if (GetChar(aData, aDataIndex, aDataIndexMax, (char**)(&newLight.myLightType)) == false)
			{
				return false;
			}

			const long long floatsInVector4(4);
			const long long amountToReadInBytes(sizeOfFloat * floatsInVector4);
			ALLOWED_TO_READ(aDataIndex, aDataIndexMax, amountToReadInBytes);

			newLight.myColor = ((CU::Vector4f*)(&aData[aDataIndex]));

			aDataIndex += amountToReadInBytes;

			if (GetFloat(aData, aDataIndex, aDataIndexMax, &newLight.myIntensity) == false)
			{
				return false;
			}

			if (*newLight.myLightType == CMapDataObject::SLight::eLightType::POINT)
			{
				const long long rangeSizeInBytes(sizeOfFloat);
				const long long pointLightSizeInBytes(rangeSizeInBytes);
				ALLOWED_TO_READ(aDataIndex, aDataIndexMax, pointLightSizeInBytes);

				newLight.myPointLightData = ((CMapDataObject::SLight::SPointLight*)(&aData[aDataIndex]));

				aDataIndex += pointLightSizeInBytes;
			}
			else if (*newLight.myLightType == CMapDataObject::SLight::eLightType::DIRECTIONAL)
			{
				const long long floatsInVector3(3);
				const long long directionSizeInBytes(sizeOfFloat * floatsInVector3);
				const long long directionalLightSizeInBytes(directionSizeInBytes);
				ALLOWED_TO_READ(aDataIndex, aDataIndexMax, directionalLightSizeInBytes);

				newLight.myDirectionalLightData = ((CMapDataObject::SLight::SDirectionalLight*)(&aData[aDataIndex]));

				aDataIndex += directionalLightSizeInBytes;
			}
			else if (*newLight.myLightType == CMapDataObject::SLight::eLightType::SPOT)
			{
				const long long rangeSizeInBytes(sizeOfFloat);
				const long long angleSizeInBytes(sizeOfFloat);
				const long long floatsInVector3(3);
				const long long directionSizeInBytes(sizeOfFloat * floatsInVector3);
				const long long spotLightSizeInBytes(directionSizeInBytes + rangeSizeInBytes + angleSizeInBytes);
				ALLOWED_TO_READ(aDataIndex, aDataIndexMax, spotLightSizeInBytes);

				newLight.mySpotLightData = ((CMapDataObject::SLight::SSpotLight*)(&aData[aDataIndex]));

				aDataIndex += spotLightSizeInBytes;
			}
		}
	}
	else
	{
		return false;
	}

	return true;
}

bool GetInt(char* aData, long long& aDataIndex, const long long& aDataIndexMax, int** aIntToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfInt);

	*aIntToFill = (int*)(&aData[aDataIndex]);

	aDataIndex += sizeOfInt;

	return true;
}

bool GetString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aStringToFill, const int aSizeToGet)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, aSizeToGet);

	aStringToFill = &aData[aDataIndex];

	aDataIndex += aSizeToGet + 1;

	return true;
}

bool GetIntString(char* aData, long long& aDataIndex, const long long& aDataIndexMax, const char*& aNameToFill)
{
	int* sizeOfName(nullptr);
	if (GetInt(aData, aDataIndex, aDataIndexMax, &sizeOfName) == false)
	{
		return false;
	}

	if (GetString(aData, aDataIndex, aDataIndexMax, aNameToFill, *sizeOfName) == false)
	{
		return false;
	}

	return true;
}

bool GetChar(char* aData, long long& aDataIndex, const long long& aDataIndexMax, char** aCharToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfChar);

	*aCharToFill = (char*)(&aData[aDataIndex]);

	aDataIndex += sizeOfChar;

	return true;
}

bool GetFloat(char* aData, long long& aDataIndex, const long long& aDataIndexMax, float** aFloatToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfFloat);

	*aFloatToFill = (float*)(&aData[aDataIndex]);

	aDataIndex += sizeOfFloat;

	return true;
}

bool GetBool(char * aData, long long & aDataIndex, const long long & aDataIndexMax, bool ** aBoolToFill)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfBool);

	*aBoolToFill = (bool*)(&aData[aDataIndex]);

	aDataIndex += sizeOfBool;

	return true;
}

bool MovePointerSize(long long& aDataIndex, const long long& aDataIndexMax)
{
	ALLOWED_TO_READ(aDataIndex, aDataIndexMax, sizeOfPointer);

	aDataIndex += sizeOfPointer;

	return true;
}

bool AllowedToReadData(const long long& aDataIndex, const long long& aDataIndexMax, const long long& aSize)
{
	return ((aDataIndex + aSize) <= aDataIndexMax);
}
