#pragma once
#include "MapData.h"

class CMapReader
{
public:
	CMapReader();
	~CMapReader();

	bool LoadMap(const char* aMapPath, CMapData& aMapDataToFill, const long long aMaxBytesToRead = -1) const;

private:

};
