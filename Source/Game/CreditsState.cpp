#include "stdafx.h"
#include "CreditsState.h"
#include "../GraphicsEngine/Sprite/Sprite.h"
#include "../GraphicsEngine/Text/Text.h"

#define FADEIN_TIME 3.0f
#define FADEOUT_TIME 5.5f


CCreditsState::CCreditsState(EManager& aEntityManager, bool aExitMainstate)
	: CState(aEntityManager)
	, myTimeToRollCredits(10.0f)
	, myElapsedTime(0.0f)
	, myBlackCanvasSprite(nullptr)
	, myText(nullptr)
	, myCreditsImage(nullptr)
	, myExitMainstate(aExitMainstate)
{
}

CCreditsState::~CCreditsState()
{
	if (myBlackCanvasSprite != nullptr)
	{
		hse_delete(myBlackCanvasSprite);
	}

	if (myText != nullptr)
	{
		hse_delete(myText);
	}

	if (myCreditsImage != nullptr)
	{
		hse_delete(myCreditsImage);
	}
}

void CCreditsState::Init()
{

	myBlackCanvasSprite = hse_new(hse::gfx::CSprite);
	myBlackCanvasSprite->Init("Data/UI/ui_blackCanvas.dds");

	myCreditsImage = hse_new(hse::gfx::CSprite);
	myCreditsImage->Init("Data/UI/credits.dds");

	myCreditsImage->SetPosition({ 0.0f, 0.0f });

	myText = hse_new(hse::gfx::CText);
	myText->Init("Data/Fonts/Adventure Subtitles.ttf", "", 36.0f, { 0.0f }, { 0.0f }, hse::gfx::CText::ColorFloatToUint(0.0f, 1.0f, 0.0f), true);
	myText->SetPosition({ 0.5f, 0.0f });
}

void CCreditsState::Update(const float aDeltaTime)
{
	myElapsedTime += aDeltaTime;

	if (myElapsedTime >= (myTimeToRollCredits + FADEIN_TIME + FADEOUT_TIME))
	{
		myState = (myExitMainstate) ? CState::EStateCommand::eRemoveMainState : CState::EStateCommand::eRemoveState;
	}

	if (myElapsedTime <= FADEIN_TIME)
	{
		myBlackCanvasSprite->SetAlpha(1.0f - (myElapsedTime / FADEIN_TIME));
	}
	else if (myElapsedTime >= FADEIN_TIME + myTimeToRollCredits)
	{
		const float calculatedAlpha((myElapsedTime - (FADEIN_TIME + myTimeToRollCredits)) / (myTimeToRollCredits + FADEIN_TIME + FADEOUT_TIME - (FADEIN_TIME + myTimeToRollCredits)));
		myBlackCanvasSprite->SetAlpha(calculatedAlpha);
	}

	if (myElapsedTime > FADEIN_TIME && myElapsedTime < (myTimeToRollCredits + FADEIN_TIME))
	{
		myCreditsImage->SetPosition({ 0.0f, CU::Lerp(0.0f, -1.0f, (myElapsedTime - FADEIN_TIME) / ((FADEIN_TIME + myTimeToRollCredits) - FADEIN_TIME)) });
	}

	myText->SetPosition({ myText->GetPosition().x, CU::Lerp(0.0f, -1.0f, myElapsedTime / myTimeToRollCredits) });
}

void CCreditsState::Render()
{
	myCreditsImage->Render();

	if (myBlackCanvasSprite->GetAlpha() > 0.0f)
	{
		myBlackCanvasSprite->Render();
	}
}

void CCreditsState::OnActivation()
{
}

void CCreditsState::ReceieveEvent(SMessage& aMessage)
{
	aMessage;
}
