#include "stdafx.h"
#include "CutsceneState.h"
#include "../GraphicsEngine/Sprite/Sprite.h"
#include "../EngineCore/FileFinder/FileFinder.h"
#include "../EngineCore/EventSystem/EventManager.h"
#include "../GraphicsEngine/Text/Text.h"

#define FADEDOUT_ALPHA 1.0f
#define FADEDIN_ALPHA 0.0f

CCutsceneState::CCutsceneState(EManager& aEntityManager, const std::string& aCutsceneFolderPath, bool aFadeOutPrevious, float& aCutsceneMusicVolume)
	: CState(aEntityManager)
	, myFolderPath(aCutsceneFolderPath)
	, myCutsceneImages(nullptr)
	, myBlackCanvasSprite(nullptr)
	, myTimeToFade(1.0f)
	, myFadeTimer(0.0f)
	, myCurrentCutsceneIndex(CHAR_MAX)
	, myMaxCutsceneIndex(CHAR_MAX)
	, myPressToContinueText(nullptr)
	, myRenderStaticIndex(CHAR_MAX)
	, myCutsceneMusicVolume(aCutsceneMusicVolume)
{
	myFadeState = (aFadeOutPrevious) ? EFadeState::FadeOutPrevious : EFadeState::FadeIn;
}

CCutsceneState::~CCutsceneState()
{
	if (myCutsceneImages != nullptr)
	{
		hse_delete(myCutsceneImages);
	}
	if (myBlackCanvasSprite != nullptr)
	{
		hse_delete(myBlackCanvasSprite);
	}
	if (myPressToContinueText != nullptr)
	{
		hse_delete(myPressToContinueText);
	}
	hse::CEventManager::Get()->DetachInputListener(this);
}

void CCutsceneState::Init()
{
	SStaticScreenData data;
	data.myIntensity = 0.25f;

	myBlackCanvasSprite = hse_new(hse::gfx::CSprite);
	myBlackCanvasSprite->Init("Data/UI/ui_blackCanvas.dds");

	CU::GrowingArray<std::string> files;
	std::string folderPath = "Data/Cutscenes/" + myFolderPath;
	hse::CFileFinder::FindFilesInFolder(files, "dds", folderPath.c_str());

	myPressToContinueText = hse_new(hse::gfx::CText());
	myPressToContinueText->Init("Data/Fonts/Adventure Subtitles.ttf", "Press \"SPACE\" to continue", 24.0f, { 0.0f }, { 0.42f, 0.02f }, hse::gfx::CText::ColorFloatToUint(0.3f, 0.3f, 0.3f));

	myCurrentCutsceneIndex = 0;
	myMaxCutsceneIndex = static_cast<char>(files.Size());
	myCutsceneImages = hse_newArray(hse::gfx::CSprite, myMaxCutsceneIndex);
	
	for (unsigned short i = 0; i < files.Size(); ++i)
	{
		if (files[i].find_first_of('_') != std::string::npos)
		{
			myRenderStaticIndex = static_cast<char>(i);
		}
		myCutsceneImages[i].Init((folderPath + files[i]).c_str());
	}

	hse::CEventManager::Get()->AttachInputListener(this);
}

void CCutsceneState::Update(const float aDeltaTime)
{
	if (myFadeTimer < myTimeToFade)
	{
		myFadeTimer += aDeltaTime;

		if (myFadeState == EFadeState::FadeOutPrevious)
		{
			myBlackCanvasSprite->SetAlpha(CU::Lerp(FADEDIN_ALPHA, FADEDOUT_ALPHA, myFadeTimer / myTimeToFade));

			if (myFadeTimer >= myTimeToFade)
			{
				myFadeState = EFadeState::FadeIn;
				myFadeTimer = 0.0f;
			}
		}
		else if (myFadeState == EFadeState::FadeIn)
		{
			myBlackCanvasSprite->SetAlpha(CU::Lerp(FADEDOUT_ALPHA, FADEDIN_ALPHA, myFadeTimer / myTimeToFade));
			myCutsceneMusicVolume = CU::Lerp(0.0f, 1.0f, myFadeTimer / myTimeToFade);

		}
		else if (myFadeState == EFadeState::FadeOutCutscene)
		{
			myBlackCanvasSprite->SetAlpha(CU::Lerp(FADEDIN_ALPHA, FADEDOUT_ALPHA, myFadeTimer / myTimeToFade));

			if (myFadeTimer >= myTimeToFade)
			{
				myState = CState::EStateCommand::eRemoveState;
			}
		}
	}
}

void CCutsceneState::Render()
{
	if (myFadeState != EFadeState::FadeOutPrevious)
	{
		myCutsceneImages[myCurrentCutsceneIndex].Render();
	}

	if (myFadeTimer < myTimeToFade)
	{
		myBlackCanvasSprite->Render();
	}

	myPressToContinueText->Render();
}


void CCutsceneState::OnActivation()
{
}

void CCutsceneState::ReceieveEvent(SMessage & aMessage)
{
	if (aMessage.messageType == EMessageType::KeyInput)
	{
		SKeyMessage* msg = reinterpret_cast<SKeyMessage*>(&aMessage);

		if (msg->key == ' ' && msg->state == EButtonState::Pressed)
		{
			if ((myCurrentCutsceneIndex < myMaxCutsceneIndex - 1) && myFadeState != EFadeState::FadeOutPrevious)
			{
				myCurrentCutsceneIndex++;
			}
			else if (myFadeState != EFadeState::FadeOutCutscene)
			{
				myFadeState = EFadeState::FadeOutCutscene;
				myFadeTimer = 0.0f;
			}
		}
	}
}
