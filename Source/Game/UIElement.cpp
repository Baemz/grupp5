#include "stdafx.h"
#include "UIElement.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "Textbox.h"

void CUIElement::Init(const char * aSpritePath)
{
	mySprite.Init(aSpritePath);
}

void CUIElement::Update(const float aPercentage, const EUpdateMode& aUpdateMode)
{
	switch (aUpdateMode)
	{
	case EUpdateMode::HorizontalFade:
		mySprite.SetCrop({ aPercentage, 0.0f });
		break;
	case EUpdateMode::VerticalFadeUp:
		mySprite.SetCrop({ 1.0f, aPercentage });
		mySprite.SetScale({ 1.0f, aPercentage });
		break;
	case EUpdateMode::TotalFade:
		mySprite.SetAlpha(aPercentage);
		break;
	}
}

void CUIElement::Render()
{
	mySprite.Render();
}
