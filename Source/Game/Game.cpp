#include "stdafx.h"
#include "Game.h"
#include "StateStack.h"
#include "MainMenuState.h"
#include "InGameState.h"
#include "ShowRoomState.h"
#include "../GraphicsEngine/GraphicsEngineInterface.h"
#include "..\EngineCore\WorkerPool\WorkerPool.h"
#include "../CommonUtilities/KeyBinds.h"
#include <Windows.h>

#include "EntitySystemLink/EntitySystemLink.h"
#include "GameSystems/GameSystems.h"
#include "../Script/ScriptManager.h"
#include "TestScene.h"

#define myEntityManager (*(CEntityManager<EntitySystemSettings>**)(&myEntityManagerVoid))

CGame::CGame()
	: myEntityManagerVoid(nullptr)
{
}

CGame::~CGame()
{
	hse_delete(myEntityManager);
	CGameSystems::Destroy();
	CStateStack::Destroy();
	CU::CKeyBinds::Destroy();
	CScriptManager::Destroy();
}

void CGame::Init()
{
	CU::CKeyBinds::Create("Data/HotKeys.json");
	//Swap for general savestate manager
	CStateStack::Create();

	myEntityManager = hse_new(CEntityManager<EntitySystemSettings>);
	CGameSystems::Create(*myEntityManager);

	//CStateStack::PushMainStateOnTop(hse_new(CMainMenuState(*myEntityManager)));
	CStateStack::PushMainStateOnTop(hse_new(CTestScene(*myEntityManager)));

	CScriptManager::Create();
	CScriptManager* scriptManager(CScriptManager::Get());

	if (scriptManager == nullptr)
	{
		GAME_LOG("ERROR! Failed to create ScriptManager");
	}
	else
	{
		const auto registerFunction(std::bind(&CGame::RegisterScriptFunctions, this, std::placeholders::_1));
		// This is not an error, just compile and move on
		if (scriptManager->Init(registerFunction, "Data/Scripts/printNumber.lua") == false)
		{
			GAME_LOG("Failed to init script manager");
		}

		scriptManager->CallFunction("Init", 12.34, "stringFromC++ToLua", 1337);
	}

	//WORKER_POOL_QUEUE_WORK(&CGame::PreLoadAudio);
}

void CGame::Update(const float aDeltaTime)
{
	CScriptManager::Get()->CallFunction("Update");

	if (!CStateStack::IsEmpty())
	{
		CStateStack::InitStatesCreatedFromPreviousFrame();
		CStateStack::UpdateStateOnTop(aDeltaTime);

		CGameSystems::Get()->UpdateAllSystems(aDeltaTime);

		CStateStack::RenderApprovedStates();
	}
	else
	{
		PostQuitMessage(0);
	}
}

bool CGame::RegisterScriptFunctions(CScriptManager& aScriptManager)
{
	std::function<void(const char*)> scriptLogMessage(
		[](const char* aMessage) -> void
	{
		aMessage;
		GAME_LOG(aMessage);
	});

	if (aScriptManager.RegisterFunction("Print", scriptLogMessage, "Print\nARGS:\n\t[String]\nPrint to the console.") == false)
	{
		return false;
	}

	std::function<void(double aNumber)> scriptLogNumber(
		[](double aNumber) -> void
	{
		aNumber;
		GAME_LOG("%f", aNumber);
	});

	if (aScriptManager.RegisterFunction("PrintNumber", scriptLogNumber, "PrintNumber\nARGS:\n\t[Number]\nPrint to the console.") == false)
	{
		return false;
	}

	std::function<void(bool aState)> scriptLogBool(
		[](bool aState) -> void
	{
		aState;
		GAME_LOG("%s", (aState ? "true" : "false"));
	});

	if (aScriptManager.RegisterFunction("PrintBool", scriptLogBool, "PrintBool\nARGS:\n\t[Bool]\nPrint to the console.") == false)
	{
		return false;
	}

	std::function<double(double, double)> scriptAdd(
		[](double aNumber0, double aNumber1) -> double
	{
		return (aNumber0 + aNumber1);
	});

	if (aScriptManager.RegisterFunction("Add", scriptAdd, "Add\nARGS:\n\t[Number0], [Number1]\nReturns the sum of [Number0] and [Number1].") == false)
	{
		return false;
	}

	std::function<int()> scriptReturn2(
		[]() -> int
	{
		return 2;
	});

	if (aScriptManager.RegisterFunction("Get2", scriptReturn2, "Get2\nARGS:\n\t\nGet number 2.") == false)
	{
		return false;
	}

	std::function<void(float)> gameUpdateFunction(std::bind(&CGame::Update, this, std::placeholders::_1));
	if (aScriptManager.RegisterFunction("UpdateGame", gameUpdateFunction, "UpdateGame\nARGS:\n\t[Number]\nUpdate game, with deltaTime. Because not using recursive mutex, will crash if placed in Update[LUA]") == false)
	{
		return false;
	}

	return true;
}
