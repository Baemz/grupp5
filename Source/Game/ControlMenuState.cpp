#include "stdafx.h"
#include "ControlMenuState.h"

CControlMenuState::CControlMenuState(EManager& aEntityManager)
	: CState(aEntityManager)
{
}

CControlMenuState::~CControlMenuState()
{
}

void CControlMenuState::Init()
{
	myMenu.AddGraphic("Data/UI/menu/controlsWindow.dds", { 0.22f, 0.0f });
	myMenu.AddButton("Data/UI/menu/options_back.dds", "Data/UI/menu/options_backHover.dds", "back", { 0.3f, 0.605f }, { 1.0f });
}

void CControlMenuState::Update(const float aDeltaTime)
{
	const bool keepMenuState(myMenu.Update(aDeltaTime));

	if (!keepMenuState || myMenu.CheckPressedButton("back"))
	{
		myMenu.Disable();
		myState = CState::EStateCommand::eRemoveState;
	}
}

void CControlMenuState::Render()
{
	myMenu.Render(myState == CState::EStateCommand::eActive);
}

void CControlMenuState::OnActivation()
{
	myMenu.Enable();
}
