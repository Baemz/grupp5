#pragma once
#include "State.h"
#include "Menu.h"
class CGameOverScreen: private CState	
{
public:
	CGameOverScreen();
	~CGameOverScreen() override;

	void Init() override;
	void Update(const float aDeltaTime) override;
	void Render() override;

	void OnActivation() override;

private:
	CMenu myMenu;
};

