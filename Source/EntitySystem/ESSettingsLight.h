#pragma once
#include "MetaProgrammingStuff.h"

template
<
	typename ComponentList,
	typename TagList,
	typename SignatureList
>
struct SESSettings;

template <typename... Ts> using Signature = MPS::CTypeList<Ts...>;
template <typename... Ts> using SignatureList = MPS::CTypeList<Ts...>;
template <typename... Ts> using SignatureCallerList = MPS::CTypeList<Ts...>;

template <typename... Ts> using ComponentList = MPS::CTypeList<Ts...>;
template <typename... Ts> using TagList = MPS::CTypeList<Ts...>;

namespace Matcher
{
	template <typename SettingsType>
	struct SignatureBitsets;

	template <typename Settings>
	struct SignatureBitsetsStorage;
}