#pragma once
#include "ESSettings.h"
#include "Entity.h"
#include "Handle.h"
#include "../CommonUtilities/GrowingArray.h"
#include "ComponentStorage.h"

#include <new>

static constexpr std::size_t EntityCount = 10000;

template <typename ESSettings>
class CEntityManager
{
private:
	template <typename... Ts>
	friend struct ExpandCallHelper;

	using Settings = ESSettings;
	using ThisType = CEntityManager<Settings>;
	using Bitset = typename Settings::Bitset;
	using Entity = SEntity<Settings>;
	using SignatureBitsetsStorage = Matcher::SignatureBitsetsStorage<Settings>;
	using SignatureList = typename Settings::SignatureList;

	//using SignatureCallerList = typename Settings::SignatureCallerList;
	//template <typename... SignatureTypes>
	//using TupleOfSignatures = std::tuple<SignatureTypes...>;
	//MPS::Rename<TupleOfSignatures, SignatureCallerList> mySignatureCallers;
	////MPS::Rename<TupleOfSignatures, SignatureList> mySignatures;
	//SignatureList mySignatures;

	std::size_t mySize;
	std::size_t mySizeNext;
	CU::GrowingArray<Entity, std::size_t> myEntities;
	CU::GrowingArray<SHandleData, size_t> myHandleData;
	SignatureBitsetsStorage mySignatureBitsets;

	CComponentStorage<Settings> myComponentStorage;

	Entity& GetEntity(EntityIndex aIndex) noexcept
	{
		assert(mySizeNext > aIndex);
		return myEntities[aIndex];
	}

	const Entity& GetEntity(EntityIndex aIndex) const noexcept
	{
		assert(mySizeNext > aIndex);
		return myEntities[aIndex];
	}

	SHandleData& GetHandleDataWithHandleDataIndex(HandleDataIndex aHandleIndex) noexcept
	{
		assert(myHandleData.Size() > aHandleIndex && "Handle index was out of bounds!");
		return myHandleData[aHandleIndex];
	}

	const SHandleData& GetHandleDataWithHandleDataIndex(HandleDataIndex aHandleIndex) const noexcept
	{
		assert(myHandleData.Size() > aHandleIndex && "Handle index was out of bounds!");
		return myHandleData[aHandleIndex];
	}

	SHandleData& GetHandleDataWithEntityIndex(EntityIndex aEntityIndex) noexcept
	{
		return GetHandleDataWithHandleDataIndex(GetEntity(aEntityIndex).myHandleDataIndex);
	}

	const SHandleData& GetHandleDataWithEntityIndex(EntityIndex aEntityIndex) const noexcept
	{
		return GetHandleDataWithHandleDataIndex(GetEntity(aEntityIndex).myHandleDataIndex);
	}

	SHandleData& GetHandleData(const SEntityHandle& aHandle) noexcept
	{
		return GetHandleDataWithHandleDataIndex(aHandle.myHandleDataIndex);
	}

	const SHandleData& GetHandleData(const SEntityHandle& aHandle) const noexcept
	{
		return GetHandleDataWithHandleDataIndex(aHandle.myHandleDataIndex);
	}

public:
	CEntityManager()
	{
		Entity* entities(hse_newArray(Entity, EntityCount));
		myEntities.SetRawData(entities, EntityCount);

		SHandleData* handleDatae(hse_newArray(SHandleData, EntityCount));
		myHandleData.SetRawData(handleDatae, EntityCount);

		mySize = 0;
		mySizeNext = 0;

		for (auto index(0); index < EntityCount; ++index)
		{
			auto& e(myEntities[index]);
			auto& h(myHandleData[index]);
			e.myDataIndex = index;
			e.myHandleDataIndex = index;
			e.myBitset.reset();
			e.myAlive = false;

			h.myCounter = 0;
			h.myEntityIndex = index;
		}

		myComponentStorage.InitializeCapacity(EntityCount);
	}

	~CEntityManager()
	{
		decltype(myEntities.GetRawData()) entityPointer(myEntities.GetRawData());
		hse_delete(entityPointer);
		myEntities.SetRawData(nullptr, 0);

		decltype(myHandleData.GetRawData()) handlePointer(myHandleData.GetRawData());
		hse_delete(handlePointer);
		myHandleData.SetRawData(nullptr, 0);
	}

	const inline bool IsHandleValid(const SEntityHandle& aHandle) const noexcept
	{
		return aHandle.myCounter == GetHandleData(aHandle).myCounter;
	}

	const inline std::size_t GetEntityIndex(const SEntityHandle& aHandle) const noexcept
	{
		assert(IsHandleValid(aHandle) && "Tried to get entity index with invalid handle.");
		return GetHandleData(aHandle).myEntityIndex;
	}

	const inline bool IsAlive(EntityIndex aIndex) const noexcept
	{
		return GetEntity(aIndex).myAlive;
	}

	void Kill(EntityIndex aIndex) noexcept
	{
		GetEntity(aIndex).myAlive = false;
	}

	void Kill(const SEntityHandle& aHandle) noexcept
	{
		Kill(GetEntityIndex(aHandle));
	}

	template<typename T>
	const inline bool HasTag(EntityIndex aIndex) const noexcept
	{
		static_assert(Settings::template IsTag<T>(), "HasTag needs a tag struct as template argument.");
		return GetEntity(aIndex).myBitset[Settings::template TagBit<T>()];
	}

	template<typename T>
	void AddTag(const SEntityHandle& aHandle) noexcept
	{
		AddTag<T>(GetEntityIndex(aHandle));
	}

	template<typename T>
	void AddTag(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsTag<T>(), "AddTag needs a tag struct as template argument.");
		GetEntity(aIndex).myBitset[Settings::template TagBit<T>()] = true;
	}

	template<typename T>
	void DelTag(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsTag<T>(), "DelTag needs a tag struct as template argument.");
		GetEntity(aIndex).myBitset[Settings::template TagBit<T>()] = false;
	}

	template<typename T>
	const inline bool HasComponent(EntityIndex aIndex) const noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "HasComponent needs a component struct as template argument.");
		return GetEntity(aIndex).myBitset[Settings::template ComponentBit<T>()];
	}

	template<typename T, typename... TArgs>
	auto& AddComponent(const SEntityHandle& aHandle, TArgs&&... aInitData) noexcept
	{
		return AddComponent<T>(GetEntityIndex(aHandle), aInitData...);
	}

	template<typename T, typename... TArgs>
	auto& AddComponent(EntityIndex aIndex, TArgs&&... aInitData) noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "AddComponent needs a Component struct as template argument.");
		auto& e(GetEntity(aIndex));
		e.myBitset[Settings::template ComponentBit<T>()] = true;

		auto& c(myComponentStorage.template GetComponent<T>(e.myDataIndex));
		new (&c) T(std::forward<decltype(aInitData)>(aInitData)...);
		return c;
	}

	template <typename T>
	auto& GetComponent(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "GetComponent needs a Component struct as template argument.");
		assert(HasComponent<T>(aIndex) && "Entity does not own a component of this type");
		return myComponentStorage.template GetComponent<T>(GetEntity(aIndex).myDataIndex);
	}

	template<typename T>
	void DelComponent(EntityIndex aIndex) noexcept
	{
		static_assert(Settings::template IsComponent<T>(), "DelComponent needs a Component struct as template argument.");
		GetEntity(aIndex).myBitset[Settings::template ComponentBit<T>()] = false;
	}

	EntityIndex CreateIndex()
	{
		assert(mySizeNext + 1 < EntityCount && "Created too many entites.");

		const EntityIndex freeIndex(mySizeNext++);
		assert(!IsAlive(freeIndex) && "Added entity that was still alive.");

		Entity& e(myEntities[freeIndex]);
		e.myAlive = true;
		e.myBitset.reset();

		return freeIndex;
	}

	auto CreateHandle()
	{
		auto freeIndex(CreateIndex());

		auto& e(myEntities[freeIndex]);
		auto& hd(myHandleData[e.myHandleDataIndex]);

		hd.myEntityIndex = freeIndex;
		SEntityHandle handle;
		handle.myHandleDataIndex = e.myHandleDataIndex;

		handle.myCounter = hd.myCounter;
		assert(IsHandleValid(handle) && "Fucked up the handles somehow");
		return handle;
	}

	void Clear() noexcept
	{
		for (auto index(0ull); index < EntityCount; ++index)
		{
			auto& e(myEntities[index]);
			auto& hd(myHandleData[index]);
			e.myDataIndex = index;
			e.myBitset.reset();
			e.myAlive = false;
			e.myHandleDataIndex = index;

			hd.myCounter = 0;
			hd.myEntityIndex = index;
		}

		mySize = 0;
		mySizeNext = 0;
	}

	void Refresh() noexcept
	{
		if (mySizeNext == 0)
		{
			mySize = 0;
			return;
		}

		mySize = mySizeNext = RefreshImpl();
	}

	template<typename T>
	const inline bool MatchesSignature(EntityIndex aIndex) const noexcept
	{
		static_assert(Settings::template IsSignature<T>(), "Matches signature needs a signature as template argument.");

		const auto& entityBitset(GetEntity(aIndex).myBitset);
		const auto& signatureBitset(mySignatureBitsets.template GetSignatureBitset<T>());
		return (entityBitset & signatureBitset) == signatureBitset;
	}

	template <typename Function>
	void ForEntities(Function&& aFunction)
	{
		for (EntityIndex i = 0; i < mySize; ++i)
		{
			aFunction(i);
		}
	}

	template <typename Signature, typename Function>
	void ForEntitiesMatching(Function&& aFunction)
	{
		static_assert(Settings::template IsSignature<Signature>(), "ForEntitiesMatching needs a signature as template argument.");

		ForEntities([this, &aFunction](auto index)
		{
			//std::cout << "Trying to run function on signature: " << typeid(Signature).name() << std::endl;
			if (MatchesSignature<Signature>(index))
			{
				//std::cout << "Ran function on signature: " << typeid(Signature).name() << std::endl << std::endl;
				ExpandSignatureCall<Signature>(index, aFunction);
			}
		});
	}

	template<typename Signature, typename Function>
	void RunIfMatching(const SEntityHandle& aHandle, Function&& aFunction)
	{
		RunIfMatching<Signature>(GetEntityIndex(aHandle), aFunction);
	}

	template<typename Signature, typename Function>
	void RunIfMatching(const EntityIndex& aEntityIndex, Function&& aFunction)
	{
		static_assert(Settings::template IsSignature<Signature>(), "RunIfMatching needs a signature as template argument.");
		if (MatchesSignature<Signature>(aEntityIndex))
		{
			ExpandSignatureCall<Signature>(aEntityIndex, aFunction);
		}
		else
		{
			assert(false && "RunIfMatching needs a signature that fits the entity its being run on.");
		}
	}

	//It was a cool idea, for sure.
	//void UpdateAllSignatures(const float aDeltaTime, CU::GrowingArray<std::function<void()>, size_t> aDuringUpdateFunctionList)
	//{
	//	auto signatureFunction = 
	//		[this, aDeltaTime]
	//		(auto& aSignature)
	//		{
	//			//decltype(aSignature)::Signature;
	//
	//			std::cout << typeid(decltype(aSignature.GetInvalidSignatureType())).name() << std::endl;
	//
	//			//ForEntitiesMatching<decltype(aSignature)::Signature(
	//			ForEntitiesMatching<decltype(aSignature.GetInvalidSignatureType())>(aSignature.myImplementation());
	//		};
	//	
	//	MPS::ForTuple(mySignatureCallers, signatureFunction);
	//}

private:

	template <typename T, typename Function>
	void ExpandSignatureCall(EntityIndex aIndex, Function&& aFunction)
	{
		static_assert(Settings::template IsSignature<T>(), "ExpandSignatureCall needs a signature as template argument.");

		using RequiredComponents = typename Settings::SignatureBitsets::template SignatureComponents<T>;

		using FunctionCallHelper = MPS::Rename<ExpandCallHelper, RequiredComponents>;

		FunctionCallHelper::Call(aIndex, *this, aFunction);
	}

	template <typename... Ts>
	struct ExpandCallHelper
	{
		template <typename Function>
		static void Call(EntityIndex aIndex, ThisType& aManager, Function&& aFunction)
		{
			auto dataIndex(aManager.GetEntity(aIndex).myDataIndex);

			aFunction(aIndex, aManager.myComponentStorage.template GetComponent<Ts>(dataIndex)...);
		}
	};

	void InvalidateHandle(EntityIndex aIndex) noexcept
	{
		auto& hd(myHandleData[myEntities[aIndex].myHandleDataIndex]);
		++hd.myCounter;
	}

	void RefreshHandle(EntityIndex aIndex) noexcept
	{
		auto& hd(myHandleData[myEntities[aIndex].myHandleDataIndex]);
		hd.myEntityIndex = aIndex;
	}

	std::size_t RefreshImpl() noexcept
	{
		EntityIndex indexDead{ 0 };
		EntityIndex indexAlive{ mySizeNext - 1 };

		while (true)
		{
			for (; true; ++indexDead)
			{
				if (indexDead > indexAlive) return indexDead;
				if (!myEntities[indexDead].myAlive) break;
			}

			for (; true; --indexAlive)
			{
				if (myEntities[indexAlive].myAlive) break;
				InvalidateHandle(indexAlive);
				if (indexAlive < indexDead) return indexDead;
			}

			assert(myEntities[indexAlive].myAlive);
			assert(!myEntities[indexDead].myAlive);

			std::swap(myEntities[indexAlive], myEntities[indexDead]);
			//Alive entity's handle is refreshed
			RefreshHandle(indexDead);
			//Dead entity's handle is invalidated and refreshed
			InvalidateHandle(indexAlive);
			RefreshHandle(indexAlive);
			++indexDead; --indexAlive;
		}

		return indexDead;
	}

public:
	const inline size_t GetEntityCount() const noexcept
	{
		return mySize;
	}

	const inline size_t GetCapacity() const noexcept
	{
		return myEntities.Capacity();
	}

	void PrintState() const 
	{
		std::cout
			<< "\nSize: " << mySize
			<< "\nSizeNext: " << mySizeNext
			<< "\n" << std::endl;

		for (auto index(0ull); index < mySizeNext; ++index)
		{
			auto& e(myEntities[index]);
			std::cout << (e.myAlive) ? "A" : "D";
		}

		std::cout << "\n\n" << std::endl;
	}

};