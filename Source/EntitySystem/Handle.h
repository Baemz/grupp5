#pragma once
#include "Entity.h"
#include "ESSettingsLight.h"

struct SHandleData
{
	EntityIndex myEntityIndex;
	int myCounter;
};

struct SEntityHandle
{
	HandleDataIndex myHandleDataIndex;
	int myCounter;
};