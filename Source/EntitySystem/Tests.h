#pragma once
struct comp0 {};
struct comp1 {};
using MyComponents = ComponentList<comp0, comp1>;

struct tag0 {};
struct tag1 {};

using MyTags = TagList<tag0, tag1>;

using MyHorribleList = ComponentList<comp0, tag1>;

using sig0 = typename Signature<>;
using sig1 = typename Signature<comp0, comp1>;
using sig2 = typename Signature<tag0, tag1>;
using sig3 = typename Signature<comp0, tag0, comp1>;

using MySigs = SignatureList<sig0, sig1, sig2, sig3>;

using EntitySystemSettings = SESSettings<MyComponents, MyTags, MySigs>;
using EntityManager = CManager<EntitySystemSettings>;
using EntityComponentStorage = CComponentStorage<EntitySystemSettings>;

static_assert(EntitySystemSettings::IsComponent<comp1>(), "Comp1 is not a component");
static_assert(EntitySystemSettings::IsComponent<tag0>() == false, "tag0 is a component");
static_assert(EntitySystemSettings::IsTag<tag0>(), "tag0 is not a tag");
//static_assert(EntitySystemSettings::IsTag<sig1>() == false, "sig0 is a tag");

//static_assert(EntitySystemSettings::IsSignature<sig1>(), "sig0 is not a sig");
static_assert(EntitySystemSettings::IsSignature<tag0>() == false, "tag0 is a sig");

static_assert(EntitySystemSettings::ComponentCount() == 2, "Component count is not 2");
static_assert(EntitySystemSettings::ComponentCount() != 4, "Component count is 4");
static_assert(EntitySystemSettings::TagCount() == 2, "Tag count is not 2");
static_assert(EntitySystemSettings::TagCount() != 3, "Tag count is 3");
//static_assert(EntitySystemSettings::SignatureCount() == 4, "Signature count is not 4");
//static_assert(EntitySystemSettings::SignatureCount() != 2, "Signature count is 2");

static_assert(EntitySystemSettings::ComponentID<comp0>() == 0, "Component ID is not 0");
static_assert(EntitySystemSettings::ComponentID<comp1>() == 1, "Component ID is not 1");

static_assert(EntitySystemSettings::TagID<tag0>() == 0, "Tag ID is not 0");
static_assert(EntitySystemSettings::TagID<tag1>() == 1, "Tag ID is not 1");

static_assert(EntitySystemSettings::SignatureID<sig0>() == 0, "Sig0 is not 0");


//using signatureComponents = EntitySystemSettings::SignatureBitsets::SignatureComponents<sig0>;
//using signatureTags =		EntitySystemSettings::SignatureBitsets::SignatureTags<sig0>;

//using signatureComponents = typename EntitySystemSettings::SignatureBitsets::template EntitySystemSettings::SignatureList<sig0>;

// using MySignatureBitsets = typename EntitySystemSettings::SignatureBitsets;

//static_assert(std::is_same
//	<
//	EntitySystemSettings::SignatureBitsets::SignatureComponents<sig0>,
//	CTypeList<>
//	>(), "Sig0 is not an empty template signature");
//
//static_assert(std::is_same
//	<
//	EntitySystemSettings::SignatureBitsets::SignatureTags<sig1>,
//	CTypeList<comp0, comp1>
//	>{}, "signature does not contain two components");
//
//static_assert(std::is_same
//	<
//	EntitySystemSettings::SignatureBitsets::SignatureTags<sig2>,
//	CTypeList<tag0, tag1>
//	>{}, "Signature does not contain two tags");
//
//static_assert(std::is_same
//	<
//	EntitySystemSettings::SignatureBitsets::SignatureTags<sig3>,
//	CTypeList<comp0, tag1, comp1>
//	>{}, "Signature does not contain two components and one tag");

void runtimeTests()
{
 	using Bitset = typename EntitySystemSettings::Bitset;
 	Matcher::SignatureBitsetsStorage<EntitySystemSettings> msb;

 	const auto& bS0(msb.GetSignatureBitset<sig0>());
 	const auto& bS1(msb.GetSignatureBitset<sig1>());
 	const auto& bS3(msb.GetSignatureBitset<sig3>());
 
	const auto dasfgsag(Bitset{ "0011" });

 	assert(bS0 == Bitset{ "0000" });
 	assert(bS1 == dasfgsag);
 	assert(bS3 == Bitset{ "0111" });

	EntityManager manager;

	manager.PrintState();
	assert(manager.GetEntityCount() == 0);

	auto i0 = manager.CreateIndex();

	std::cout << "After entity 0" << std::endl;
	manager.PrintState();

	std::cout << "After entity 1" << std::endl;
	auto i1 = manager.CreateIndex();
	manager.PrintState();

	assert(!manager.HasTag<tag0>(i0));
	manager.AddTag<tag0>(i0);
	assert(manager.HasTag<tag0>(i0));

	assert(!manager.HasTag<tag1>(i1));
	manager.AddTag<tag1>(i1);
	assert(manager.HasTag<tag1>(i1));

	assert(manager.HasTag<tag0>(i0));
	manager.DelTag<tag0>(i0);
	assert(!manager.HasTag<tag0>(i0));

	// Notice how the "entity count" stays to zero until we call the
	// `refresh()` method.
	assert(manager.GetEntityCount() == 0);

	manager.Refresh();

	assert(!manager.HasTag<tag0>(i0));
	assert(manager.HasTag<tag1>(i1));

	std::cout << "After refresh\n";
	manager.PrintState();

	assert(manager.GetEntityCount() == 2);

	manager.Clear();

	std::cout << "After clear\n";
	manager.PrintState();

	assert(manager.GetEntityCount() == 0);

	EntityComponentStorage storage;
	storage.InitializeCapacity(manager.GetCapacity());
}