#pragma once
#include <cstddef>

using DataIndex = typename std::size_t;
using EntityIndex = typename std::size_t;
using HandleDataIndex = typename std::size_t;

template <typename ESSettings>
struct CEntity
{
	using Bitset = typename ESSettings::Bitset;

	DataIndex myDataIndex;
	HandleDataIndex myHandleDataIndex;
	Bitset myBitset;

	bool myAlive;
};