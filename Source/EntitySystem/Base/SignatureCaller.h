//#pragma once
//#include "MetaProgrammingStuff.h"
//#include <functional>
//
//template <typename SignatureType, typename... LambdaParameters>
//struct CSignatureCaller
//{
//	//typename for a TYPELIST containing the components and tags for the specified SIGNATURE
//	using Signature = SignatureType;
//
//	//Specific for each created version of CSignatureCaller
//	//AKA, a new static std function for each signature
//	static std::function<void(LambdaParameters...)>* myImplementation;
//
//	static Signature GetInvalidSignatureType()
//	{
//		return *(Signature*(nullptr));
//	};
//};
//
//template <typename SignatureType, typename... LambdaParameters>
//std::function<void(LambdaParameters...)>* CSignatureCaller<SignatureType, LambdaParameters...>::myImplementation = nullptr;