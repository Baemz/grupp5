#pragma once
#include "MetaProgrammingStuff.h"
#include "../../CommonUtilities/GrowingArray.h"

#include <tuple>

template <typename ESSettings>
class CComponentStorage
{
private:
	using Settings = ESSettings;
	using ComponentList = typename Settings::ComponentTypeList;

	template <typename... Components>
	using TupleOfComponentArrays = std::tuple<CU::GrowingArray<Components, size_t>...>;

	//using tupleToName = std::tuple<ComponentList>;
	//using renamed = typename ComponentList::Rename<tupleToName>;

	MPS::Rename<TupleOfComponentArrays, ComponentList> myComponentArrays;

public:
	~CComponentStorage()
	{
		auto lambda =
			[this](auto& aArray)
		{
			decltype(aArray.GetRawData()) dataPointer(aArray.GetRawData());
			hse_delete(dataPointer);
			aArray.SetRawData(nullptr, 0);
		};

		MPS::ForTuple(myComponentArrays, lambda);
	}

	void InitializeCapacity(std::size_t aCapacity)
	{
		auto lambda = 
			[this, aCapacity](auto& aArray)
		{
			//std::cout << typeid(aArray).name() << std::endl;
			aArray.SetRawDataReserve(hse_newArray(decltype(aArray.GetInvalidTypeObject()), aCapacity), aCapacity, aCapacity);
		};

		MPS::ForTuple(myComponentArrays, lambda);
	}

	//void UpdateSignatures()
	//{
	//	lambda = (auto& aSignature) 
	//	{ 
	//		manager.ForEntitiesMatching<decltype(aSignature)>
	//
	//	MPS::ForTuple(mySignatures, lambda);
	//}

	template<typename T>
	auto& GetComponent(std::size_t aIndex) noexcept
	{
		auto& componentArray = std::get<CU::GrowingArray<T, std::size_t>>(myComponentArrays);
		assert((componentArray.Size() >= aIndex) && "(Probably) Tried to get a component that doesn't exist.");
	
		return componentArray[aIndex];
	}
};