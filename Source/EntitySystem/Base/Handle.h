#pragma once
#include "Entity.h"

struct CHandleData
{
	EntityIndex myEntityIndex;
	int myCounter;
};

struct CEntityHandle
{
	HandleDataIndex myHandleDataIndex;
	int myCounter;
};