#pragma once

#ifdef DLL_EXPORT
#define DLLAPI __declspec(dllexport)
#else
#define DLLAPI __declspec(dllimport)
#endif

using namespace System;

namespace hse
{
	class CEngine;
	class CMaterialPreviewer;
}
namespace hseWrap
{
	public ref class CEngineBridge
	{
	public:
		CEngineBridge();
		~CEngineBridge();

		void InitMaterialPreview(System::IntPtr aHWND, unsigned short aWndWidth, unsigned short aWndHeight);
		void Start();
		void Stop();

		void RotateX(float aDelta);
		void RotateY(float aDelta);
		void ResetRotation();
		void SetModel(int aIndex);

	private:
		hse::CEngine* myEngine;
		hse::CMaterialPreviewer* myPreviewer;
	};

}