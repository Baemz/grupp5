#include "MaterialPreviewer.h"
#include "..\GraphicsEngine\Model\ModelInstance.h"
#include "..\GraphicsEngine\Camera\CameraFactory.h"
#include "..\GraphicsEngine\Camera\CameraInstance.h"
#include "../EngineCore/MemoryPool/MemoryPool.h"
#include "..\GraphicsEngine\Model\Loading\ModelFactory.h"


hse::CMaterialPreviewer::CMaterialPreviewer()
	: myCam(nullptr)
	, myMdl(nullptr)
	, myBox(nullptr)
	, mySphere(nullptr)
	, myCylinder(nullptr)
{
}

hse::CMaterialPreviewer::~CMaterialPreviewer()
{
}

void hse::CMaterialPreviewer::Init()
{
	myCam = hse_new(gfx::CCameraInstance());
	*myCam = gfx::CCameraFactory::Get()->CreateCameraAtPosition({ 0.f, 0.f, -2.f });
	myCam->SetPosition({ 0.f, 0.f, -2.f });
	myBox = hse_new(gfx::CModelInstance());
	myBox->SetModelPath("Data/Models/Misc/nC_wMat.fbx");
	myBox->SetMaterialPath("Data/Materials/PreviewTempMaterial/temp_Material.material");
	myBox->SetScale({ 1.3f, 1.3f, 1.3f });
	myBox->Init();
	mySphere = hse_new(gfx::CModelInstance());
	mySphere->SetModelPath("Data/Models/Misc/material_preview_sphere.fbx");
	mySphere->SetMaterialPath("Data/Materials/PreviewTempMaterial/temp_Material.material");
	mySphere->Init();
	myCylinder = hse_new(gfx::CModelInstance());
	myCylinder->SetModelPath("Data/Models/Misc/material_preview_cylinder.fbx");
	myCylinder->SetMaterialPath("Data/Materials/PreviewTempMaterial/temp_Material.material");
	myCylinder->SetScale({ 0.8f, 0.8f, 0.8f });
	myCylinder->Init();

	myMdl = myBox;
	mySky = hse_new(hse::gfx::CModelInstance);
	*mySky = hse::gfx::CModelFactory::Get()->CreateSkyboxCube("Data/Models/Misc/Material_Preview_Skybox.dds");
	mySky->Init();
}

void hse::CMaterialPreviewer::Update(const float aDeltaTime)
{
	myCam->UseForRendering();
	myMdl->Render();
	mySky->Render();
}

void hse::CMaterialPreviewer::RotateX(float aDelta)
{
	CU::Matrix44f mat;
	mat = mat.CreateRotateAroundY(aDelta);
	CU::Matrix44f rotation = myMdl->GetOrientation() * mat;
	myMdl->SetOrientation(rotation);
}

void hse::CMaterialPreviewer::RotateY(float aDelta)
{
	CU::Matrix44f mat;
	mat = mat.CreateRotateAroundX(aDelta);
	CU::Matrix44f rotation = myMdl->GetOrientation() * mat;
	myMdl->SetOrientation(rotation);


}

void hse::CMaterialPreviewer::ResetRotation()
{
	myMdl->SetOrientation(CU::Matrix44f());
}

void hse::CMaterialPreviewer::SetModel(int aIndex)
{
	switch (aIndex)
	{
	case 0:
		myMdl = myBox;
		break;
	case 1:
		myMdl = mySphere;
		break;
	case 2:
		myMdl = myCylinder;
		break;
	default:
		myMdl = myBox;
		break;
	}
}
