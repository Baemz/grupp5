#pragma once

namespace hse
{
	namespace gfx 
	{
		class CModelInstance;
		class CCameraInstance;
	}

	class CMaterialPreviewer
	{
	public:
		CMaterialPreviewer();
		~CMaterialPreviewer();

		void Init();
		void Update(const float aDeltaTime);

		void RotateX(float aDelta);
		void RotateY(float aDelta);
		void ResetRotation();
		void SetModel(int aIndex);
	private:
		gfx::CModelInstance* myMdl;
		gfx::CModelInstance* myBox;
		gfx::CModelInstance* mySphere;
		gfx::CModelInstance* myCylinder;
		gfx::CModelInstance* mySky;
		gfx::CCameraInstance* myCam;
	};
}
