#include "stdafx.h"
#include "WWiseManager.h"

#include <AK/SoundEngine/Common/AkTypes.h>

#include <AK/SoundEngine/Common/AkMemoryMgr.h>		// Memory Manager
#include <AK/SoundEngine/Common/AkModule.h>			// Default memory and stream managers
#include <AK/SoundEngine/Common/IAkStreamMgr.h>		// Streaming Manager
#include <AK/SoundEngine/Common/AkSoundEngine.h>    // Sound engine
#include <AK/MusicEngine/Common/AkMusicEngine.h>	// Music Engine
#include <AK/SoundEngine/Common/AkStreamMgrModule.h>	// AkStreamMgrModule
#include <AK/Comm/AkCommunication.h>	// AkStreamMgrModule
#include "SoundEngine\Win32\AkFilePackageLowLevelIOBlocking.h"
#include <stdio.h>

#define DEMO_DEFAULT_POOL_SIZE 2*1024*1024
#define DEMO_LENGINE_DEFAULT_POOL_SIZE 16*1024*1024


namespace AK
{
	void * AllocHook(size_t in_size)
	{
		return malloc(in_size);
	}
	void FreeHook(void * in_ptr)
	{
		free(in_ptr);
	}
	void * VirtualAllocHook(
		void * in_pMemAddress,
		size_t in_size,
		DWORD in_dwAllocationType,
		DWORD in_dwProtect
	)
	{
		return VirtualAlloc(in_pMemAddress, in_size, in_dwAllocationType, in_dwProtect);
	}
	void VirtualFreeHook(
		void * in_pMemAddress,
		size_t in_size,
		DWORD in_dwFreeType
	)
	{
		VirtualFree(in_pMemAddress, in_size, in_dwFreeType);
	}
}

CAkFilePackageLowLevelIOBlocking* m_pLowLevelIO;
CWwiseManager::CWwiseManager()
{
}


CWwiseManager::~CWwiseManager()
{
	TermWwise();
}

template <class T, size_t N> AkForceInline size_t AK_ARRAYSIZE(T(&)[N])
{
	return N;
}



void CWwiseManager::TermWwise()
{
#ifdef _DEBUG
	AK::Comm::Term();
#endif
	AK::MusicEngine::Term();
	if (AK::SoundEngine::IsInitialized())
	{
		AK::SoundEngine::Term();
	}
	if (AK::IAkStreamMgr::Get())
	{
		m_pLowLevelIO->Term(); // Linker error \/
		/*
		AkDefaultIOHookBlocking.cpp + .h
		AkFileLocationBase.cpp + .h
		*/
		AK::IAkStreamMgr::Get()->Destroy();
	}
	if (AK::MemoryMgr::IsInitialized())
	{
		AK::MemoryMgr::Term();
	}
}

static const AkGameObjectID LISTENER_ID = 10000;
bool CWwiseManager::InitializeSystem(const char* aInitBankPath)
{
	AkMemSettings memSettings;
	AkStreamMgrSettings stmSettings;
	AkDeviceSettings deviceSettings;
	AkInitSettings initSettings;
	AkPlatformInitSettings platformInitSettings;
	AkMusicSettings musicInit;

	memSettings.uMaxNumPools = 20;
	AK::StreamMgr::GetDefaultSettings(stmSettings);

	AK::StreamMgr::GetDefaultDeviceSettings(deviceSettings);

	AK::SoundEngine::GetDefaultInitSettings(initSettings);
	initSettings.uDefaultPoolSize = DEMO_DEFAULT_POOL_SIZE;

	AK::SoundEngine::GetDefaultPlatformInitSettings(platformInitSettings);
	platformInitSettings.uLEngineDefaultPoolSize = DEMO_LENGINE_DEFAULT_POOL_SIZE;

	AK::MusicEngine::GetDefaultInitSettings(musicInit);

	platformInitSettings.hWnd = GetActiveWindow();

	// Init the stuff
	m_pLowLevelIO = new CAkFilePackageLowLevelIOBlocking(); // Linker error \/
	/*
	AkFilePackageLUT.cpp + .h
	AkFilePackage.cpp .h
	*/

	AkOSChar szError[500];
	int in_unErrorBufferCharCount = (unsigned int)AK_ARRAYSIZE(szError);
	AKRESULT res = AK::MemoryMgr::Init(&memSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise:AK::MemoryMgr::Init() returned failed");
		return false;
	}

	if (!AK::StreamMgr::Create(stmSettings))
	{
		AKPLATFORM::SafeStrCpy(szError, AKTEXT("AK::StreamMgr::Create() failed"), in_unErrorBufferCharCount);
		return false;
	}

	res = m_pLowLevelIO->Init(deviceSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise: AK::SoundEngine::Init() returned failed");
		return false;
	}

	res = AK::SoundEngine::Init(&initSettings, &platformInitSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise: AK::SoundEngine::Init() returned failed");
		return false;
	}

	res = AK::MusicEngine::Init(&musicInit);
	if (res != AK_Success)
	{
		printf("%s", "WWise: AK::MusicEngine::Init() returned failed");
		return false;
	}

#ifdef _DEBUG

	AkCommSettings commSettings;
	AK::Comm::GetDefaultInitSettings(commSettings);
	AKPLATFORM::SafeStrCpy(commSettings.szAppNetworkName, "Integration Demo", AK_COMM_SETTINGS_MAX_STRING_SIZE);
	res = AK::Comm::Init(commSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise: Communication between the Wwise authoring application and the game will not be possible.");
		//__AK_OSCHAR_SNPRINTF(in_szErrorBuffer, in_unErrorBufferCharCount, AKTEXT("AK::Comm::Init() returned AKRESULT %d. Communication between the Wwise authoring application and the game will not be possible."), res);
	}
#endif

	AkBankID bankIDInit;
	if (AK::SoundEngine::LoadBank(aInitBankPath, AK_DEFAULT_POOL_ID, bankIDInit) != AK_Success)
	{
		return false;
	}

	AK::SoundEngine::RegisterGameObj(LISTENER_ID, "Listener (Default)");
	AK::SoundEngine::SetDefaultListeners(&LISTENER_ID, 1);

	return true;
}

bool CWwiseManager::LoadBank(const char* aBankPath)
{
	AkBankID bankID; // Not used
	if (AK::SoundEngine::LoadBank(aBankPath, AK_DEFAULT_POOL_ID, bankID) != AK_Success)
	{
		printf("%s%s", "WWise: Bank failed to load :", aBankPath);
		return false;
	}
	return true;
}


void CWwiseManager::RegisterObject(int anObjectID)
{
	AKRESULT result = AK::SoundEngine::RegisterGameObj(anObjectID, "GameObj");
	if (result == AKRESULT::AK_Fail)
	{
		printf("%s%i", "WWise: Gameobject failed to register at ID :", anObjectID);
	}
}

void CWwiseManager::UnRegisterObject(int anObjectID)
{
	AKRESULT result = AK::SoundEngine::UnregisterGameObj(anObjectID);
	if (result == AKRESULT::AK_Fail)
	{
		printf("%s%i", "WWise: Gameobject failed to unregister at ID :", anObjectID);
	}
}

unsigned long CWwiseManager::PostEvent(const char* aEvent, int aObjectID)
{
	AkPlayingID id = AK::SoundEngine::PostEvent(aEvent, aObjectID);
	if (id == AK_INVALID_PLAYING_ID)
	{
		printf("%s%s", "WWise: Failed to post event:", aEvent);
	}
	return id;
}

void CWwiseManager::SetRTPC(const char* aRTPC, int aValue, int aObjectID)
{
	AkRtpcValue val;
	val = static_cast<float>(aValue);
	AKRESULT result = AK::SoundEngine::SetRTPCValue(aRTPC, val, aObjectID);
	if (result == AKRESULT::AK_Fail)
	{
		printf("%s%s%s%d%s%i", "WWise: RTPC:", aRTPC, "Failed to set value :", aValue, " on object: ", aObjectID);
	}
}


void CWwiseManager::SetPosition(float aX, float aY, float aZ, int aObjectID, float aDirectionX, float aDirectionY, float aDirectionZ)
{
	AkVector soundPosition;
	soundPosition.X = aX;
	soundPosition.Y = aY;
	soundPosition.Z = aZ;
	AkVector orientationFront;
	orientationFront.Z = 1;
	orientationFront.Y = orientationFront.X = 0;
	AkVector orientationTop;
	orientationTop.X = orientationTop.Z = 0;
	orientationTop.Y = 1;

	AkSoundPosition soundPos;
	soundPos.SetPosition(soundPosition);
	soundPos.SetOrientation(orientationFront, orientationTop);
	AKRESULT result = AK::SoundEngine::SetPosition(aObjectID, soundPos);
	if (result == AKRESULT::AK_Fail)
	{
		printf("%s%i", "WWise: SetPosition: Failed to set on:", aObjectID);
	}
}

void CWwiseManager::SetListenerPosition(float aX, float aY, float aZ, float aDirectionX, float aDirectionY, float aDirectionZ)
{
	SetPosition(aX, aY, aZ, LISTENER_ID, aDirectionX, aDirectionZ, aDirectionZ);
}

void CWwiseManager::Step()
{
	AKRESULT result = AK::SoundEngine::RenderAudio();
	if (result == AKRESULT::AK_Fail)
	{
		printf("%s", "WWise: Failed to render audio");
	}
}
