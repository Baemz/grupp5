#pragma once
#define DllExport   __declspec( dllexport )  


class DllExport CWwiseManager
{
public:
	CWwiseManager();
	~CWwiseManager();
	bool InitializeSystem(const char* aInitBankPath);
	void TermWwise();
	void Step();

	//Game functions
	bool LoadBank(const char* aBankPath);

	void RegisterObject(int anObjectID);
	void UnRegisterObject(int anObjectID);

	unsigned long PostEvent(const char* aEvent, int aObjectID);
	void SetRTPC(const char* aRTPC, int aValue, int aObjectID);

	void SetListenerPosition(float aX, float aY, float aZ, float aDirectionX, float aDirectionY, float aDirectionZ);
	void SetPosition(float aX, float aY, float aZ, int aObjectID, float aDirectionX, float aDirectionY, float aDirectionZ);
private:

};

