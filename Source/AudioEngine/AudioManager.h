#pragma once
#include "../CommonUtilities/Vector3.h"

class CAudioManager
{
public:
	~CAudioManager();

	static void Create(const char* aInitBankPath = "Data/Audio/Init.bnk");
	static void Destroy();
	static void RenderAudio();

	static void LoadBank(const char* aBankPath);
	static void RegisterObject(const unsigned int aObjectID);
	static void UnRegisterObject(const unsigned int aObjectID);

	static const unsigned long PostEventAndGetID(const char* aEventName, const unsigned int aObjectID);
	static void SetRTPC(const char* aRTPC_Name, const float aValue, const unsigned int aObjectID);
	static void SetState(const char* aStateGroupName, const char* aStateName);

	static void SetListenerPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop);
	static void SetObjectPositionAndOrientation(const CU::Vector3f& aPosition, const CU::Vector3f& aFront, const CU::Vector3f& aTop, const unsigned int aObjectID);

private:
	CAudioManager();
	CAudioManager(const CAudioManager& aAudioManager) = delete;
	void operator=(const CAudioManager& aAudioManager) = delete;

private:
	static CAudioManager* myInstancePtr;

	class CAudioEngine* myAudioEnginePtr;
};

