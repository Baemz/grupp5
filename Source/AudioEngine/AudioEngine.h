#pragma once

class CAudioEngine
{
public:
	CAudioEngine();
	~CAudioEngine();
	const bool InitializeSystem(const char* aInitBankPath);
	void TermWwise();
	const bool RenderAudio() const;

	//Game functions
	const bool LoadBank(const char* aBankPath) const;
	const bool RegisterObject(const unsigned int anObjectID) const;
	const bool UnRegisterObject(const unsigned int anObjectID) const;

	const unsigned long PostEvent(const char* aEventName, const unsigned int aObjectID) const;
	const bool SetRTPC(const char* aRTPC_Name, const float aValue, const unsigned int aObjectID) const;
	const bool SetState(const char* aStateGroupName, const char* aStateName) const;

	const bool SetListenerPositionAndOrientation(const float aX, const float aY, const float aZ,
		const float aFrontX, const float aFrontY, const float aFrontZ,
		const float aTopX, const float aTopY, const float aTopZ) const;
	const bool SetObjectPositionAndOrientation(const unsigned int aObjectID,
		const float aX, const float aY, const float aZ,
		const float aFrontX, const float aFrontY, const float aFrontZ,
		const float aTopX, const float aTopY, const float aTopZ) const;
private:
	const bool SetPositionAndOrientationOnCustomObject(const unsigned int aObjectID,
		const float aX, const float aY, const float aZ,
		const float aFrontX, const float aFrontY, const float aFrontZ,
		const float aTopX, const float aTopY, const float aTopZ) const;
};

