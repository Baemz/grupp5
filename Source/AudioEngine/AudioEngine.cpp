#include "stdafx.h"
#include "AudioEngine.h"

#include <AK/SoundEngine/Common/AkTypes.h>

#include <AK/SoundEngine/Common/AkMemoryMgr.h>		// Memory Manager
#include <AK/SoundEngine/Common/AkModule.h>			// Default memory and stream managers
#include <AK/SoundEngine/Common/IAkStreamMgr.h>		// Streaming Manager
#include <AK/SoundEngine/Common/AkSoundEngine.h>    // Sound engine
#include <AK/MusicEngine/Common/AkMusicEngine.h>	// Music Engine
#include <AK/SoundEngine/Common/AkStreamMgrModule.h>	// AkStreamMgrModule
#include <AK/Comm/AkCommunication.h>	// AkStreamMgrModule
#include "SoundEngine\Win32\AkFilePackageLowLevelIOBlocking.h"
#include <stdio.h>

#define DEMO_DEFAULT_POOL_SIZE 2*1024*1024
#define DEMO_LENGINE_DEFAULT_POOL_SIZE 16*1024*1024

static const AkGameObjectID globalListenerID = 10000;
CAkFilePackageLowLevelIOBlocking* globalLowLevelIOPtr;

namespace AK
{
	void * AllocHook(size_t in_size)
	{
		return malloc(in_size);
	}
	void FreeHook(void * in_ptr)
	{
		free(in_ptr);
	}
	void * VirtualAllocHook(
		void * in_pMemAddress,
		size_t in_size,
		DWORD in_dwAllocationType,
		DWORD in_dwProtect
	)
	{
		return VirtualAlloc(in_pMemAddress, in_size, in_dwAllocationType, in_dwProtect);
	}
	void VirtualFreeHook(
		void * in_pMemAddress,
		size_t in_size,
		DWORD in_dwFreeType
	)
	{
		VirtualFree(in_pMemAddress, in_size, in_dwFreeType);
	}
}

CAudioEngine::CAudioEngine()
{
}

CAudioEngine::~CAudioEngine()
{
	TermWwise();
}

template <class T, size_t N> AkForceInline size_t AK_ARRAYSIZE(T(&)[N])
{
	return N;
}

void CAudioEngine::TermWwise()
{
#ifdef _DEBUG
	AK::Comm::Term();
#endif
	AK::MusicEngine::Term();
	if (AK::SoundEngine::IsInitialized())
	{
		AK::SoundEngine::Term();
	}
	if (AK::IAkStreamMgr::Get())
	{
		globalLowLevelIOPtr->Term(); // Linker error \/
		/*
		AkDefaultIOHookBlocking.cpp + .h
		AkFileLocationBase.cpp + .h
		*/
		AK::IAkStreamMgr::Get()->Destroy();
	}
	if (AK::MemoryMgr::IsInitialized())
	{
		AK::MemoryMgr::Term();
	}
}

const bool CAudioEngine::RenderAudio() const
{
	const AKRESULT result = AK::SoundEngine::RenderAudio();
	if (result != AKRESULT::AK_Success)
	{
		printf("%s", "WWise: Failed to render audio");
		return false;
	}

	return true;
}

const bool CAudioEngine::InitializeSystem(const char* aInitBankPath)
{
	AkMemSettings memSettings;
	AkStreamMgrSettings stmSettings;
	AkDeviceSettings deviceSettings;
	AkInitSettings initSettings;
	AkPlatformInitSettings platformInitSettings;
	AkMusicSettings musicInit;

	memSettings.uMaxNumPools = 20;
	AK::StreamMgr::GetDefaultSettings(stmSettings);

	AK::StreamMgr::GetDefaultDeviceSettings(deviceSettings);

	AK::SoundEngine::GetDefaultInitSettings(initSettings);
	initSettings.uDefaultPoolSize = DEMO_DEFAULT_POOL_SIZE;

	AK::SoundEngine::GetDefaultPlatformInitSettings(platformInitSettings);
	platformInitSettings.uLEngineDefaultPoolSize = DEMO_LENGINE_DEFAULT_POOL_SIZE;

	AK::MusicEngine::GetDefaultInitSettings(musicInit);

	platformInitSettings.hWnd = GetActiveWindow();

	// Init the stuff
	globalLowLevelIOPtr = new CAkFilePackageLowLevelIOBlocking(); // Linker error \/
	/*
	AkFilePackageLUT.cpp + .h
	AkFilePackage.cpp .h
	*/

	AkOSChar szError[500];
	int in_unErrorBufferCharCount = (unsigned int)AK_ARRAYSIZE(szError);
	AKRESULT res = AK::MemoryMgr::Init(&memSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise:AK::MemoryMgr::Init() returned failed");
		return false;
	}

	if (!AK::StreamMgr::Create(stmSettings))
	{
		AKPLATFORM::SafeStrCpy(szError, AKTEXT("AK::StreamMgr::Create() failed"), in_unErrorBufferCharCount);
		return false;
	}

	res = globalLowLevelIOPtr->Init(deviceSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise: AK::SoundEngine::Init() returned failed");
		return false;
	}

	res = AK::SoundEngine::Init(&initSettings, &platformInitSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise: AK::SoundEngine::Init() returned failed");
		return false;
	}

	res = AK::MusicEngine::Init(&musicInit);
	if (res != AK_Success)
	{
		printf("%s", "WWise: AK::MusicEngine::Init() returned failed");
		return false;
	}

#ifdef _DEBUG

	AkCommSettings commSettings;
	AK::Comm::GetDefaultInitSettings(commSettings);
	AKPLATFORM::SafeStrCpy(commSettings.szAppNetworkName, "Integration Demo", AK_COMM_SETTINGS_MAX_STRING_SIZE);
	res = AK::Comm::Init(commSettings);
	if (res != AK_Success)
	{
		printf("%s", "WWise: Communication between the Wwise authoring application and the game will not be possible.");
		//__AK_OSCHAR_SNPRINTF(in_szErrorBuffer, in_unErrorBufferCharCount, AKTEXT("AK::Comm::Init() returned AKRESULT %d. Communication between the Wwise authoring application and the game will not be possible."), res);
	}
#endif

	AkBankID bankIDInit;
	if (AK::SoundEngine::LoadBank(aInitBankPath, AK_DEFAULT_POOL_ID, bankIDInit) != AK_Success)
	{
		return false;
	}

	AK::SoundEngine::RegisterGameObj(globalListenerID, "Listener (Default)");
	AK::SoundEngine::SetDefaultListeners(&globalListenerID, 1);

	return true;
}

const bool CAudioEngine::LoadBank(const char* aBankPath) const
{
	AkBankID bankID; // Not used
	const AKRESULT result = AK::SoundEngine::LoadBank(aBankPath, AK_DEFAULT_POOL_ID, bankID);
	if (result != AKRESULT::AK_Success)
	{
		return false;
	}

	return true;
}

const bool CAudioEngine::RegisterObject(const unsigned int anObjectID) const
{
	const AKRESULT result = AK::SoundEngine::RegisterGameObj(anObjectID, "GameObj");
	if (result != AKRESULT::AK_Success)
	{
		return false;
	}

	return true;
}

const bool CAudioEngine::UnRegisterObject(const unsigned int anObjectID) const
{
	AKRESULT result = AK::SoundEngine::UnregisterGameObj(anObjectID);
	if (result != AKRESULT::AK_Success)
	{
		return false;
	}

	return true;
}

const unsigned long CAudioEngine::PostEvent(const char* aEventName, const unsigned int aObjectID) const
{
	const AkPlayingID id = AK::SoundEngine::PostEvent(aEventName, aObjectID);
	if (id == AK_INVALID_PLAYING_ID)
	{
		return 0;
	}

	return id;
}

const bool CAudioEngine::SetRTPC(const char* aRTPC_Name, const float aValue, const unsigned int aObjectID) const
{
	const AKRESULT result = AK::SoundEngine::SetRTPCValue(aRTPC_Name, (AkRtpcValue)aValue, aObjectID);
	if (result != AKRESULT::AK_Success)
	{
		return false;
	}

	return true;
}

const bool CAudioEngine::SetState(const char* aStateGroupName, const char* aStateName) const
{
	const AKRESULT result = AK::SoundEngine::SetState(aStateGroupName, aStateName);
	if (result != AKRESULT::AK_Success)
	{
		return false;
	}

	return true;
}

const bool CAudioEngine::SetListenerPositionAndOrientation(const float aX, const float aY, const float aZ,
	const float aFrontX, const float aFrontY, const float aFrontZ,
	const float aTopX, const float aTopY, const float aTopZ) const
{
	return SetPositionAndOrientationOnCustomObject(globalListenerID, aX, aY, aZ, aFrontX, aFrontY, aFrontZ, aTopX, aTopY, aTopZ);
}

const bool CAudioEngine::SetObjectPositionAndOrientation(const unsigned int aObjectID,
	const float aX, const float aY, const float aZ,
	const float aFrontX, const float aFrontY, const float aFrontZ,
	const float aTopX, const float aTopY, const float aTopZ) const
{
	//AkVector soundPosition;
	//soundPosition.X = aX;
	//soundPosition.Y = aY;
	//soundPosition.Z = aZ;
	//AkVector orientationFront;
	//orientationFront.Z = 1;
	//orientationFront.Y = orientationFront.X = 0;
	//AkVector orientationTop;
	//orientationTop.X = orientationTop.Z = 0;
	//orientationTop.Y = 1;
	//
	//AkSoundPosition soundPos;
	//soundPos.SetPosition(soundPosition);
	//soundPos.SetOrientation(orientationFront, orientationTop);
	//AKRESULT result = AK::SoundEngine::SetPosition(aObjectID, soundPos);
	//if (result == AKRESULT::AK_Fail)
	//{
	//	printf("%s%i", "WWise: SetPosition: Failed to set on:", aObjectID);
	//}

	return SetPositionAndOrientationOnCustomObject(aObjectID, aX, aY, aZ, aFrontX, aFrontY, aFrontZ, aTopX, aTopY, aTopZ);
}

/* PRIVATE FUNCTIONS */

const bool CAudioEngine::SetPositionAndOrientationOnCustomObject(const unsigned int aObjectID,
	const float aX, const float aY, const float aZ,
	const float aFrontX, const float aFrontY, const float aFrontZ,
	const float aTopX, const float aTopY, const float aTopZ) const
{
	AkVector position;
	position.X = aX;
	position.Y = aY;
	position.Z = aZ;
	AkVector front;
	front.X = aFrontX;
	front.Y = aFrontY;
	front.Z = aFrontZ;
	AkVector top;
	top.X = aTopX;
	top.Y = aTopY;
	top.Z = aTopZ;
	AkSoundPosition soundPosition;
	soundPosition.SetPosition(position);
	soundPosition.SetOrientation(front, top);

	const AKRESULT result = AK::SoundEngine::SetPosition(aObjectID, soundPosition);
	if (result != AKRESULT::AK_Success)
	{
		return false;
	}

	return true;
}