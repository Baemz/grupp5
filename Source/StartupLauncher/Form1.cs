﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace StartupLauncher
{
    public partial class Form1 : Form
    {
        private struct WindowSize
        {
            public int width;
            public int height;
        }
        private struct StartParams
        {
            public string windowName;
            public bool startInFullscreen;
            public bool useBloom;
            public bool useFXAA;
            public bool useColorGrading;
            public bool useVSync;
            public WindowSize windowSize;
        }

        public Form1()
        {
            InitializeComponent();
            resolutionBox.SelectedIndex = 0;
            fullscreenCheck.Checked = true;
            bloomCheck.Checked = true;
            colorGradeCheck.Checked = true;
            fxaaCheck.Checked = true;
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            StartParams startParams = new StartParams();

            string width = resolutionBox.SelectedItem.ToString().Substring(0, resolutionBox.SelectedItem.ToString().LastIndexOf('x'));
            string height = resolutionBox.SelectedItem.ToString().Substring(resolutionBox.SelectedItem.ToString().LastIndexOf('x') + 1);

            startParams.windowName = "Save Game";
            startParams.windowSize.width = Int32.Parse(width);
            startParams.windowSize.height = Int32.Parse(height);
            startParams.startInFullscreen = fullscreenCheck.Checked;
            startParams.useBloom = bloomCheck.Checked;
            startParams.useColorGrading = colorGradeCheck.Checked;
            startParams.useFXAA = fxaaCheck.Checked;
            startParams.useVSync = true;

            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            documentsPath = documentsPath + "\\The Game Assembly\\Save Game";
            Directory.CreateDirectory(documentsPath);
            documentsPath = documentsPath + "\\config.json";
            { 
                using (StreamWriter sw = new StreamWriter(documentsPath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, startParams);
                }
            }

            Process process = new Process();
            process.StartInfo.FileName = "Game.exe";
            process.Start();

            Application.Exit();
        }
    }
}
