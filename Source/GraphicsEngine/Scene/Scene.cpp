#include "stdafx.h"
#include "Scene.h"
#include "..\Model\ModelInstance.h"
#include "Camera\Camera.h"
#include "ResourceManager\ResourceManager.h"
#include "Lights\LightFactory.h"
#include "Sprite\Sprite.h"
#include "Sprite\Sprite3D.h"
#include "Lights\PointLight.h"
#include "Lights\SpotLight.h"

#include <iostream>
#include "Particles\ParticleEmitterInstance.h"
#include "Material.h"
#include "Model\Loading\ModelFactory.h"
using namespace hse::gfx;

CScene::CScene()
	: myWriteIndex(0)
	, myReadIndex(1)
	, myFreeIndex(2)
	, myPWriteIndex(0)
	, myPReadIndex(1)
	, myPFreeIndex(2)
	, myTWriteIndex(0)
	, myTReadIndex(1)
	, myTFreeIndex(2)
{
	myBuffers[myReadIndex].myModelInstances.Init(1000);
	myBuffers[myReadIndex].myPointLightInstances.Init(100);
	myBuffers[myReadIndex].mySpotLightInstances.Init(100);
	myBuffers[myWriteIndex].myModelInstances.Init(1000);
	myBuffers[myWriteIndex].myPointLightInstances.Init(100);
	myBuffers[myWriteIndex].mySpotLightInstances.Init(100);
	myBuffers[myFreeIndex].myModelInstances.Init(1000);
	myBuffers[myFreeIndex].myPointLightInstances.Init(100);
	myBuffers[myFreeIndex].mySpotLightInstances.Init(100);
	myBuffers[myFreeIndex].myShaderEffectInstances.Init(20);
}


CScene::~CScene()
{
}

void CScene::AddModelInstance(const CModelInstance& aModelInstance)
{
	if (aModelInstance.myModelName == "InvertedCube")
	{
		myBuffers[myWriteIndex].mySkybox = aModelInstance;
	}
	else
	{
		myBuffers[myWriteIndex].myModelInstances.Add(aModelInstance);
	}
}

void hse::gfx::CScene::AddPointLightInstance(const CPointLightInstance& aPLInstance)
{
	myBuffers[myWriteIndex].myPointLightInstances.Add(aPLInstance);
}

void hse::gfx::CScene::AddSpotLightInstance(const CSpotLightInstance& aSLInstance)
{
	myBuffers[myWriteIndex].mySpotLightInstances.Add(aSLInstance);
}

void hse::gfx::CScene::AddParticleEmittorInstance(const CParticleEmitterInstance& aParticleEmittorInstance)
{
	myParticleBuffers[myPWriteIndex].myParticleInstances.Add(aParticleEmittorInstance);
}

void hse::gfx::CScene::AddStreakEmittorInstance(const CStreakEmitterInstance& aStreakEmittorInstance)
{
	myParticleBuffers[myPWriteIndex].myStreakInstances.Add(aStreakEmittorInstance);
}

void hse::gfx::CScene::AddShaderEffectInstance(const CShaderEffectInstance & aShaderEffectInstance)
{
	myBuffers[myWriteIndex].myShaderEffectInstances.Add(aShaderEffectInstance);
}

void hse::gfx::CScene::AddText(const CText& aText)
{
	myTextBuffers[myTWriteIndex].myTexts.Add(aText);
}

void hse::gfx::CScene::SetCamera(const CCameraInstance& aCameraInstance)
{
	myBuffers[myWriteIndex].myCurrentCamera = aCameraInstance;
}

void hse::gfx::CScene::SetEnvironmentalLight(const CEnvironmentalLight& aEnvLight)
{
	myBuffers[myWriteIndex].myEnvLight = aEnvLight;
}

CEnvironmentalLight& hse::gfx::CScene::GetEnvironmentalLight()
{
	return myBuffers[myReadIndex].myEnvLight;
}

void hse::gfx::CScene::AddSprite(const CSprite& aSprite)
{
	myBuffers[myWriteIndex].mySprites.push_back(aSprite);
}

void hse::gfx::CScene::AddSprite3D(const CSprite3D & a3DSprite)
{
	myBuffers[myWriteIndex].my3DSprites.push_back(a3DSprite);
}

void hse::gfx::CScene::GetSpriteList(CU::GrowingArray<CSprite, unsigned int>& aListOfSprites)
{
	std::sort(myBuffers[myReadIndex].mySprites.begin(), myBuffers[myReadIndex].mySprites.begin() + myBuffers[myReadIndex].mySprites.size());
	for (auto& sprite : myBuffers[myReadIndex].mySprites)
	{
		aListOfSprites.Add(sprite);
	}
}

void hse::gfx::CScene::GetSprite3DList(CU::GrowingArray<CSprite3D, unsigned int>& aListOf3DSprites)
{
	std::sort(myBuffers[myReadIndex].my3DSprites.begin(), myBuffers[myReadIndex].my3DSprites.begin() + myBuffers[myReadIndex].my3DSprites.size());
	for (auto& sprite3D : myBuffers[myReadIndex].my3DSprites)
	{
		aListOf3DSprites.Add(sprite3D);
	}
}

void hse::gfx::CScene::GetTextList(CU::GrowingArray<CText, unsigned int>& aListOfTexts)
{
	aListOfTexts = myTextBuffers[myTReadIndex].myTexts;
}

bool CScene::Cull(SRenderData& aRenderData)
{
	bool culled(false);
	if (myBuffers[myReadIndex].myCurrentCamera.myCameraID != UINT_MAX)
	{
		culled = true;

		CResourceManager* resourceManager = CResourceManager::Get();
		CCamera& camera(resourceManager->GetCameraWithID(myBuffers[myReadIndex].myCurrentCamera.myCameraID));

		// Skybox should always render first
		if (myBuffers[myReadIndex].mySkybox.IsLoaded())
		{
			myBuffers[myReadIndex].mySkybox.myOrientation.SetPosition(myBuffers[myReadIndex].myCurrentCamera.GetPosition());
			aRenderData.myListToRender.Add(myBuffers[myReadIndex].mySkybox);
		}
		else
		{
			aRenderData.myListToRender.Add(CModelInstance());
		}
		for (unsigned int index = 0; index < myBuffers[myReadIndex].myModelInstances.Size(); ++index)
		{
			CModelInstance& ml(myBuffers[myReadIndex].myModelInstances[index]);
			CMaterial& material(CModelFactory::Get()->GetMaterialWithID(ml.myMaterialID));
			if (material.IsLoaded())
			{
				ml.UpdateColliderPosition();
				if (ml.IsCullable() == false)
				{
					if (material.GetRenderMode() == ERenderMode::Forward)
					{
						aRenderData.myTransparentListToRender.Add(ml);
					}
					else
					{
						aRenderData.myListToRender.Add(ml);
					}
				}
				else if (camera.Intersects(ml.myFrustumCollider))
				{
					if (material.GetRenderMode() == ERenderMode::Forward)
					{
						aRenderData.myTransparentListToRender.Add(ml);
					}
					else
					{
						aRenderData.myListToRender.Add(ml);
					}
				}
			}
		}

		// SORT FORWARD-LIST
		if (aRenderData.myTransparentListToRender.Size() > 1)
		{
			std::sort(&aRenderData.myTransparentListToRender[0], &aRenderData.myTransparentListToRender[aRenderData.myTransparentListToRender.Size()],
				[](const CModelInstance& aFirst, const CModelInstance& aSecond) -> bool
			{
				return (aFirst.GetZValue() < aSecond.GetZValue());
			}
			);
		}
		////////////////////


		for (unsigned int index = 0; index < myBuffers[myReadIndex].myPointLightInstances.Size(); ++index)
		{
			CPointLightInstance& pl(myBuffers[myReadIndex].myPointLightInstances[index]);
			if (camera.Intersects(pl.myPosition, pl.myRange))
			{
				aRenderData.myPointLightData.Add(pl);
			}
		}

		for (unsigned int index = 0; index < myBuffers[myReadIndex].mySpotLightInstances.Size(); ++index)
		{
			CSpotLightInstance& sl(myBuffers[myReadIndex].mySpotLightInstances[index]);
			if (camera.Intersects(sl.myPosition, sl.myRange))
			{
				aRenderData.mySpotLightData.Add(sl);
			}
		}

		for (unsigned int index = 0; index < myBuffers[myReadIndex].myShaderEffectInstances.Size(); ++index)
		{
			aRenderData.myShaderEffectsToRender.Add(myBuffers[myReadIndex].myShaderEffectInstances[index]);
		}

		unsigned int size = myParticleBuffers[myPReadIndex].myParticleInstances.Size();
		if (size > 0)
		{
			for (unsigned int index = 0; index < size; ++index)
			{
				//if (camera.Intersects(myParticleBuffers[myPReadIndex].myEmittorInstances[index].myFrustumCollider))
				//{
				aRenderData.myParticleEmittorsToRender.Add(myParticleBuffers[myPReadIndex].myParticleInstances[index]);
				//}
			}
		}

		size = myParticleBuffers[myPReadIndex].myStreakInstances.Size();
		if (size > 0)
		{
			for (unsigned int index = 0; index < size; ++index)
			{
				//if (camera.Intersects(myParticleBuffers[myPReadIndex].myStreakEmittorsToRender[index].myFrustumCollider))
				//{
				aRenderData.myStreakEmittorsToRender.Add(myParticleBuffers[myPReadIndex].myStreakInstances[index]);
				//}
			}
		}
	}
	else
	{
		for (unsigned int index = 0; index < myBuffers[myReadIndex].myShaderEffectInstances.Size(); ++index)
		{
			aRenderData.myShaderEffectsToRender.Add(myBuffers[myReadIndex].myShaderEffectInstances[index]);
		}
	}
	
	//std::cout << "[" << aRenderData.myListToRender.Size() << "] ml rendered." << std::endl;
	ChangeParticleRenderBuffer();
	ChangeTextRenderBuffer();
	ChangeRenderBuffer();
	return culled;
}

CCameraInstance& hse::gfx::CScene::GetCamera()
{
	return myBuffers[myReadIndex].myCurrentCamera;
}

void hse::gfx::CScene::ChangeUpdateBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myBufferLock);
	Swap(myWriteIndex, myFreeIndex);
	bufferLock.unlock();
	myBuffers[myWriteIndex].myCurrentCamera = CCameraInstance();
	myBuffers[myWriteIndex].myEnvLight = CEnvironmentalLight();
	myBuffers[myWriteIndex].mySkybox = CModelInstance();
	myBuffers[myWriteIndex].myModelInstances.RemoveAll();
	myBuffers[myWriteIndex].myPointLightInstances.RemoveAll();
	myBuffers[myWriteIndex].mySpotLightInstances.RemoveAll();
	myBuffers[myWriteIndex].myShaderEffectInstances.RemoveAll();
	myBuffers[myWriteIndex].mySprites.clear();
	myBuffers[myWriteIndex].my3DSprites.clear();
	myBuffers[myWriteIndex].myIsRead = false;

	ChangeTextBuffer();
}

void hse::gfx::CScene::ChangeParticleBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myPBufferLock);
	Swap(myPWriteIndex, myPFreeIndex);
	bufferLock.unlock();
	myParticleBuffers[myPWriteIndex].myParticleInstances.RemoveAll();
	myParticleBuffers[myPWriteIndex].myStreakInstances.RemoveAll();
	myParticleBuffers[myPWriteIndex].myIsRead = false;

}

void hse::gfx::CScene::ChangeTextBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myTBufferLock);
	Swap(myTWriteIndex, myTFreeIndex);
	bufferLock.unlock();
	myTextBuffers[myTWriteIndex].myTexts.RemoveAll();
	myTextBuffers[myTWriteIndex].myIsRead = false;
}

void hse::gfx::CScene::ChangeRenderBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myBufferLock);
	if (myBuffers[myFreeIndex].myIsRead == false)
	{
		Swap(myReadIndex, myFreeIndex);
	}
	myBuffers[myReadIndex].myIsRead = true;
	bufferLock.unlock();
}

void hse::gfx::CScene::ChangeParticleRenderBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myPBufferLock);
	if (myParticleBuffers[myPFreeIndex].myIsRead == false)
	{
		Swap(myPReadIndex, myPFreeIndex);
	}
	myParticleBuffers[myPReadIndex].myIsRead = true;
	bufferLock.unlock();
}

void hse::gfx::CScene::ChangeTextRenderBuffer()
{
	std::unique_lock<std::mutex> bufferLock(myTBufferLock);
	if (myTextBuffers[myTFreeIndex].myIsRead == false)
	{
		Swap(myTReadIndex, myTFreeIndex);
	}
	myTextBuffers[myTReadIndex].myIsRead = true;
	bufferLock.unlock();
}
