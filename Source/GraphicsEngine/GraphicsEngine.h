#pragma once
#include "WindowHandler/WindowData.h"
#include "Scene/Scene.h"
#include "Renderer/RenderManager.h"
#include "Utilities/RenderStructs.h"
#include "DebugTools.h"

namespace hse
{
	class CFileWatcherWrapper;
}

namespace hse { namespace gfx {
	
	class CWindowHandler;
	class CDirect3D11;
	class CFramework;
	class CCameraInstance;

	class CGraphicsEngine
	{
		friend class CGraphicsEngineInterface;
	public:
		CGraphicsEngine();
		~CGraphicsEngine();

		bool Init(SWindowData& aSomeWindowData, CFileWatcherWrapper& aFileWatcher);

		void BeginFrame();
		void RenderFrame(const float aDeltaTime, const float aTotalTime);
		void EndFrame();

		void EndGameUpdateFrame();


	private:
		CScene myScene;
		CRenderManager myRenderManager;
		SRenderData myRenderData;
		CDebugTools* myDebugTools;
		volatile bool myShouldRender;
		CWindowHandler* myWindowHandler;
		CFramework* myFramework;
		CCameraInstance* myDefaultCameraInstance;

		void TakeScreenshot(const wchar_t* aDestinationPath);
		void ToggleFullscreen();
		void SetResolution(const CU::Vector2f& aNewResolution);
		const CU::Vector2f GetResolution() const;
		const float GetRatio() const;

#ifdef _DEBUG
		void DOFXAATEST() { myRenderManager.FXAA_TEST_BOOL(); };
#endif
		const bool IsSSAOEnabled() const;
		const bool IsColorGradingEnabled() const;
		const bool IsLinearFogEnabled() const;
		const bool IsBloomEnabled() const;
		const bool IsFXAAEnabled() const;
		void ToggleSSAO();
		void ToggleColorGrading();
		void ToggleLinearFog();
		void ToggleBloom();
		void ToggleFXAA();
	};
}}

