#pragma once

namespace hse {	namespace gfx {

	class CSpotLightInstance
	{
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CLightFactory;
		friend class CScene;
	public:
		CSpotLightInstance();
		~CSpotLightInstance();

		void Init(const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const CU::Vector3f& aDirection, const float aAngle, const float aRange, const float aIntensity);
		void SetPosition(const CU::Vector3f& aPos);
		void SetDirection(const CU::Vector3f& aDir);
		void Render();

	private:
		CU::Vector3f myPosition;
		CU::Vector3f myDirection;
		unsigned int mySpotLightID;
		float myIntensity;
		float myRange; // Used for culling.
	};

}}