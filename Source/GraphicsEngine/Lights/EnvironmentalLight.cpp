#include "stdafx.h"
#include "EnvironmentalLight.h"
#include "LightFactory.h"
#include "GraphicsEngineInterface.h"

namespace hse { namespace gfx {

	CEnvironmentalLight::CEnvironmentalLight()
		: myTextureID(UINT_MAX)
	{
	}


	CEnvironmentalLight::~CEnvironmentalLight()
	{
	}

	void CEnvironmentalLight::Init(const char* aCubemap)
	{
		CLightFactory* lf = CLightFactory::Get();
		if (lf)
		{
			myTextureID = lf->CreateEnvironmentLight(aCubemap);
		}
		else
		{
			ENGINE_LOG("WARNING! No LightFactory created.");
		}
	}
	void CEnvironmentalLight::UseForRendering()
	{
		CGraphicsEngineInterface::UseForRendering(*this);
	}
}}