#include "stdafx.h"
#include "SpotLightInstance.h"
#include "LightFactory.h"
#include "GraphicsEngineInterface.h"

namespace hse {	namespace gfx {


	CSpotLightInstance::CSpotLightInstance()
		: mySpotLightID(UINT_MAX)
	{
	}


	CSpotLightInstance::~CSpotLightInstance()
	{
	}



	void CSpotLightInstance::Init(const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const CU::Vector3f& aDirection, const float aAngle, const float aRange, const float aIntensity)
	{
		CLightFactory* lf = CLightFactory::Get();
		if (lf)
		{
			mySpotLightID = lf->CreateSpotLight(aColor, aAngle, aRange);
			myPosition = aPosition;
			myDirection = aDirection;
			myIntensity = aIntensity;
			myRange = aRange;
		}
		else
		{
			ENGINE_LOG("WARNING! No LightFactory created.");
		}
	}

	void CSpotLightInstance::SetPosition(const CU::Vector3f& aPos)
	{
		myPosition = aPos;
	}

	void CSpotLightInstance::SetDirection(const CU::Vector3f& aDir)
	{
		myDirection = aDir;
	}

	void CSpotLightInstance::Render()
	{
		CGraphicsEngineInterface::AddToScene(*this);
	}
}}