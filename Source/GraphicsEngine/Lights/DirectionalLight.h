#pragma once

namespace hse { namespace gfx {

	class CDX11ConstantBuffer;

	struct SDirLightData
	{
		CU::Vector4f myToLightDirection;
		CU::Vector4f myColor;
		float myIntensity;
	private:
		float pad[3];
	};

	class CDirectionalLight
	{
	public:
		CDirectionalLight();
		~CDirectionalLight();

		void SetEnvLightMipCount(const unsigned int aMipCount);
		void SetDataAndBind();

		SDirLightData myLightData;
	private:
		CDX11ConstantBuffer* myCBuffer;
	};
}}
