#pragma once

namespace hse { namespace gfx {

	class CPointLight
	{
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CLightFactory;
		friend class CScene;
	public:
		CPointLight();
		~CPointLight();

	private:
		CU::Vector3f myColor;
		float myRange;
		unsigned int myPixelShader;
	};

}}