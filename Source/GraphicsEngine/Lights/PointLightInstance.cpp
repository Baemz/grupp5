#include "stdafx.h"
#include "PointLightInstance.h"
#include "LightFactory.h"
#include "GraphicsEngineInterface.h"

namespace hse { namespace gfx {

	CPointLightInstance::CPointLightInstance()
		: myPointLightID(UINT_MAX)
	{
	}


	CPointLightInstance::~CPointLightInstance()
	{
	}


	void CPointLightInstance::Init(const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const float aRange, const float aIntensity)
	{
		CLightFactory* lf = CLightFactory::Get();
		if (lf)
		{
			myPointLightID = lf->CreatePointLight(aColor, aRange);
			myPosition = aPosition;
			myIntensity = aIntensity;
			myRange = aRange;
		}
		else
		{
			ENGINE_LOG("WARNING! No LightFactory created.");
		}
	}

	void CPointLightInstance::SetPosition(const CU::Vector3f & aPos)
	{
		myPosition = aPos;
	}

	void CPointLightInstance::Render()
	{
		CGraphicsEngineInterface::AddToScene(*this);
	}

}}