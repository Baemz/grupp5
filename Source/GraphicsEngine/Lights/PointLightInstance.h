#pragma once

namespace hse {	namespace gfx {

	class CPointLightInstance
	{
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CLightFactory;
		friend class CScene;
	public:
		CPointLightInstance();
		~CPointLightInstance();

		void Init(const CU::Vector3f& aPosition, const CU::Vector3f& aColor, const float aRange, const float aIntensity);
		void SetPosition(const CU::Vector3f& aPos);
		const CU::Vector3f GetPosition() const { return myPosition; };
		void Render();

	private:
		CU::Vector3f myPosition;
		unsigned int myPointLightID;
		float myIntensity;
		float myRange; // Used for culling.
	};

}}
