#include "stdafx.h"
#include "LightFactory.h"
#include "../DirectXFramework/API/DX11Texture2D.h"
#include "../ResourceManager/ResourceManager.h"
#include "PointLight.h"
#include "SpotLight.h"

hse::gfx::CLightFactory* hse::gfx::CLightFactory::ourInstance = nullptr;

namespace hse { namespace gfx {

	void CLightFactory::Create()
	{
		if (ourInstance == nullptr)
		{
			ourInstance = hse_new(CLightFactory());
		}
		
		return;
	}

	void CLightFactory::Destroy()
	{
		hse_delete(ourInstance);
	}

	CLightFactory::CLightFactory()
		: myCurrentlyFreePointLightID(0)
		, myCurrentlyFreeSpotLightID(0)
		, myPointLights(nullptr)
		, mySpotLights(nullptr)
	{
		myPointLights = hse_newArray(CPointLight, MAX_POINTLIGHTS);
		mySpotLights = hse_newArray(CSpotLight, MAX_SPOTLIGHTS);
	}


	CLightFactory::~CLightFactory()
	{
		hse_delete(myPointLights);
		hse_delete(mySpotLights);
	}

	unsigned int CLightFactory::CreateEnvironmentLight(const char* aCubemap)
	{
		if (FileExists(aCubemap) == false)
		{
			RESOURCE_LOG("ERROR! Texture [%s] doesn't exist.", aCubemap);
			return UINT_MAX;
		}

		CResourceManager* resourceManager = CResourceManager::Get();

		unsigned int textureID = resourceManager->GetTextureID(aCubemap);
		CDX11Texture& texture(resourceManager->GetTextureWithID(textureID));
		if (texture.CreateTexture(aCubemap, ETextureType::EnvironmentLight) == false)
		{
			RESOURCE_LOG("ERROR! Could not create Environment-texture.");
			return UINT_MAX;
		}

		return textureID;
	}

	unsigned int CLightFactory::CreatePointLight(const CU::Vector3f& aColor, const float aRange)
	{
		std::string identifier = std::to_string(aColor.x) + std::to_string(aColor.y) + std::to_string(aColor.z) + std::to_string(aRange);
		if (PLExists(identifier))
		{
			return GetPointLightID(identifier);
		}

		unsigned int lightID = GetPointLightID(identifier);
		if (lightID == UINT_MAX)
		{
			return lightID;
		}

		CPointLight& pointLight = myPointLights[lightID];

		CResourceManager* resourceManager = CResourceManager::Get();
		if (resourceManager->ShaderExists("Data/Shaders/PointLightPS.hlsl"))
		{
			pointLight.myPixelShader = resourceManager->GetShaderID("Data/Shaders/PointLightPS.hlsl");
		}
		else
		{
			unsigned int pLightShaderID = resourceManager->GetShaderID("Data/Shaders/PointLightPS.hlsl");
			if (pLightShaderID == UINT_MAX)
			{
				return UINT_MAX;
			}
			CDX11Shader& pLightShader = resourceManager->GetShaderWithID(pLightShaderID);
			pLightShader.InitPixel("Data/Shaders/PointLightPS.hlsl", "PSMain");
			pointLight.myPixelShader = pLightShaderID;
		}
		
		pointLight.myColor = aColor;
		pointLight.myRange = aRange;

		return lightID;
	}

	unsigned int CLightFactory::CreateSpotLight(const CU::Vector3f & aColor, const float aAngle, const float aRange)
	{
		std::string identifier = std::to_string(aColor.x) + std::to_string(aColor.y) + std::to_string(aColor.z) + std::to_string(aAngle) + std::to_string(aRange);
		if (SLExists(identifier))
		{
			return GetSpotLightID(identifier);
		}
		unsigned int lightID = GetSpotLightID(identifier);
		if (lightID == UINT_MAX)
		{
			return lightID;
		}

		CSpotLight& spotLight = mySpotLights[lightID];

		CResourceManager* resourceManager = CResourceManager::Get();
		if (resourceManager->ShaderExists("Data/Shaders/SpotLightPS.hlsl"))
		{
			spotLight.myPixelShader = resourceManager->GetShaderID("Data/Shaders/SpotLightPS.hlsl");
		}
		else
		{
			unsigned int sLightShaderID = resourceManager->GetShaderID("Data/Shaders/SpotLightPS.hlsl");
			if (sLightShaderID == UINT_MAX)
			{
				return UINT_MAX;
			}
			CDX11Shader& sLightShader = resourceManager->GetShaderWithID(sLightShaderID);
			sLightShader.InitPixel("Data/Shaders/SpotLightPS.hlsl", "PSMain");
			spotLight.myPixelShader = sLightShaderID;
		}

		spotLight.myColor = aColor;
		spotLight.myAngle = aAngle;
		spotLight.myRange = aRange;

		return lightID;
	}

	CPointLight& CLightFactory::GetPointLightWithID(const unsigned int aID)
	{
		return myPointLights[aID];
	}

	CSpotLight& CLightFactory::GetSpotLightWithID(const unsigned int aID)
	{
		return mySpotLights[aID];
	}

	bool CLightFactory::PLExists(const std::string& aIdentifier)
	{
		if (myAssignedPointLightIDs.find(aIdentifier) != myAssignedPointLightIDs.end())
		{
			return true;
		}
		return false;
	}
	bool CLightFactory::SLExists(const std::string& aIdentifier)
	{
		if (myAssignedSpotLightIDs.find(aIdentifier) != myAssignedSpotLightIDs.end())
		{
			return true;
		}
		return false;
	}
	unsigned int CLightFactory::GetPointLightID(const std::string& aIdentifier)
	{
		if (myAssignedPointLightIDs.find(aIdentifier) != myAssignedPointLightIDs.end())
		{
			return myAssignedPointLightIDs[aIdentifier];
		}
		else
		{
			if (myCurrentlyFreePointLightID > (MAX_POINTLIGHTS - 1))
			{
				assert(false && "Too many PointLights created");
				RESOURCE_LOG("ERROR! Too many PointLights created");
				return UINT_MAX;
			}
			myAssignedPointLightIDs[aIdentifier] = myCurrentlyFreePointLightID;
			return myCurrentlyFreePointLightID++;
		}
	}

	unsigned int CLightFactory::GetSpotLightID(const std::string& aIdentifier)
	{
		if (myAssignedSpotLightIDs.find(aIdentifier) != myAssignedSpotLightIDs.end())
		{
			return myAssignedSpotLightIDs[aIdentifier];
		}
		else
		{
			if (myCurrentlyFreeSpotLightID > (MAX_POINTLIGHTS - 1))
			{
				assert(false && "Too many SpotLights created");
				RESOURCE_LOG("ERROR! Too many SpotLights created");
				return UINT_MAX;
			}
			myAssignedSpotLightIDs[aIdentifier] = myCurrentlyFreeSpotLightID;
			return myCurrentlyFreeSpotLightID++;
		}
	}

}}