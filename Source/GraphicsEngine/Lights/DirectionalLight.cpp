#include "stdafx.h"
#include "DirectionalLight.h"
#include "DirectXFramework\API\DX11ConstantBuffer.h"

namespace hse { namespace gfx {

	CDirectionalLight::CDirectionalLight()
	{
		myCBuffer = hse_new(hse::gfx::CDX11ConstantBuffer());
		myLightData.myColor = { 1.0f, 1.0f, 1.0f, 1.0f };
		myLightData.myToLightDirection = { -0.3f, 0.6f, -0.4f, 0.f };
		myLightData.myIntensity = 0.8f;
		myCBuffer->SetData(myLightData);
	}

	CDirectionalLight::~CDirectionalLight()
	{
		hse_delete(myCBuffer);
	}

	void CDirectionalLight::SetEnvLightMipCount(const unsigned int aMipCount)
	{
		myLightData.myToLightDirection.w = static_cast<float>(aMipCount);
	}

	void hse::gfx::CDirectionalLight::SetDataAndBind()
	{
		myCBuffer->SetData(myLightData);
		myCBuffer->BindPS(0);
	}
}}