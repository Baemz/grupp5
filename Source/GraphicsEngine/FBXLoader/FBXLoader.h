#pragma once
#include "VertexStructs.h"
#include <map>
#include "../CommonUtilities/GrowingArray.h"
#include "../CommonUtilities/Matrix.h"

struct aiMesh;
struct aiScene;
struct aiMaterial;

namespace hse
{
	namespace gfx
	{
		class CModelFactory;
	}

	class CFBXLoader
	{
		friend class gfx::CModelFactory;

	public:

	private:
		CFBXLoader();
		~CFBXLoader();
		CFBXLoader(const CFBXLoader& aOther) = delete;
		CFBXLoader(CFBXLoader&& aOther) noexcept = delete;
		CFBXLoader& CFBXLoader::operator=(const CFBXLoader& aOther) = delete;
		CFBXLoader& CFBXLoader::operator=(CFBXLoader&& aOther) noexcept = delete;

		bool Init();

		bool LoadModel(const char* aPathToModel, class CLoaderModel** aModel) const;

		class CFBXLoaderCustom* myCFXLoaderCustom;
	};

	struct BoneInfo
	{
		CU::Matrix44f BoneOffset;
		CU::Matrix44f FinalTransformation;
	};

	// One model can contain multiple meshes
	class CLoaderMesh
	{
	public:
		CLoaderMesh() { myMaxRadius = 0.f; myShaderType = 0; myVerticies = nullptr; myVertexBufferSize = 0; myVertexCount = 0; myModel = nullptr; }
		~CLoaderMesh() = default;
		CU::GrowingArray<unsigned int, unsigned int> myIndexes;
		CU::GrowingArray<CLoaderMesh*, unsigned int> myChildren;
		unsigned int myShaderType;
		unsigned int myVertexBufferSize;
		int myVertexCount;
		float myMaxRadius;
		class CLoaderModel* myModel;
		char* myVerticies;

	};

	class CLoaderModel
	{
	public:
		CLoaderModel() { myMaxRadius = 0.f; myIsLoaded = false; myAnimationDuration = 0.0f; myNumBones = 0; }
		~CLoaderModel() = default;
		void SetData(const char* aModelPath) { myModelPath = aModelPath; }
		CLoaderMesh* CreateMesh() { CLoaderMesh *model = hse_new(CLoaderMesh()); myMeshes.Add(model); model->myModel = this; return model; }

		void Destroy();

		CU::GrowingArray<CLoaderMesh*, unsigned int> myMeshes;
		CU::GrowingArray<std::string, unsigned int> myTextures;
		CU::GrowingArray<BoneInfo, unsigned int> myBoneInfo;
		std::map<std::string, unsigned int> myBoneNameToIndex;
		std::string myModelPath;
		CU::Matrix44f myGlobalInverseTransform;
		unsigned int myNumBones;
		float myAnimationDuration;
		float myMaxRadius;
		bool myIsLoaded;
		const struct aiScene* myScene;

	};

	class CFBXLoaderCustom
	{
		friend CFBXLoader;
	public:
		~CFBXLoaderCustom();

	private:
		CFBXLoaderCustom();
		CLoaderModel *LoadModel(const char* aModel);

		void* LoadModelInternal(CLoaderModel* someInput);
		int DetermineAndLoadVerticies(aiMesh* aMesh, CLoaderMesh* aLoaderMesh);
		void LoadMaterials(const aiScene* sc, CLoaderModel* aModel);
		void LoadTexture(int aType, CU::GrowingArray<std::string, unsigned int>& someTextures, aiMaterial* aMaterial);

	};
}
