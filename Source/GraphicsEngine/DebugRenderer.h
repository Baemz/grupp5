#pragma once

#define MAX_DEBUGLINES 200000
#define MAX_DEBUGCUBES 100000
#define MAX_DEBUG2DLINES 200000
#define MAX_DEBUGRECTANGLE3D 100000

struct ID3D11DeviceContext;
namespace hse { namespace gfx {

	class CDX11VertexBuffer;
	class CDX11Shader;
	class CDebugRenderer
	{
	private:
		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
		};

	public:
		CDebugRenderer();
		~CDebugRenderer();

		void Destroy();

		void Init();

		void Render(SRenderData& aRenderData);

	private:
		void RenderSpheres(SRenderData& aRenderData);

	private:
		ID3D11DeviceContext* myContext;
		CDX11VertexBuffer* myLineVBuffer;
		CDX11VertexBuffer* myRect3DVBuffer;
		CDX11VertexBuffer* my2DLineVBuffer;
		CDX11VertexBuffer* myCubeVBuffer;
		CDX11Shader* myLinePixelShader;
		CDX11Shader* myLineVertexShader;
		CDX11Shader* my2DLineVertexShader;
		CDX11Shader* my3DRectGShader;
		CDX11Shader* myLineGShader;
		CDX11Shader* my2DLineGShader;
		CDX11Shader* myCubeGShader;
		unsigned int myCBufferID;
	};
}}