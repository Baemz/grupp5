#pragma once

#include "../CommonUtilities/Quaternion.h"
#include "../CommonUtilities/Matrix.h"
#include "../CommonUtilities/Vector.h"

namespace hse { namespace gfx {

	class CCameraInstance
	{
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CSpriteRenderer3D;
		friend class CParticleRenderer;
		friend class CDebugRenderer;
		friend class CSkyboxRenderer;
		friend class CCameraFactory;
		friend class CCamera;
		friend class CScene;
		friend class CGraphicsEngine;

	public:
		CCameraInstance();
		~CCameraInstance();

		void SetPosition(const CU::Vector3f& aPosition);
		void Move(const CU::Vector3f& aAxis, float aSpeed);
		void Strafe(float aSpeed);
		void MoveForward(float aSpeed);
		void VerticalMovement(float aSpeed);
		void Rotate(const CU::Vector3f& aDir);

		//Random direction! Switch up down the line!
		void ShakeOverTime(const float aTime, const CU::Vector3f& aIntensity);
		void StopShake();

		void Update(const float aDeltaTime);
		void UseForRendering();

		const CU::Vector2f GetScreenPosition(const CU::Vector3f& aWorldPosition) const;

		const CU::Vector3f GetPosition() const;
		const CU::Vector3f& GetLook() const;
		const CU::Vector3f& GetRight() const;
		const CU::Vector3f& GetUp() const;

		const CU::Matrix44f& GetOrientation() const;
		const CU::Matrix44f& GetProjection() const;

	private:
		void UpdateScreenShake(const float aDeltaTime);

	private:
		unsigned int myCameraID;
		
		CU::Matrix44f myMatrixOrientation;
		CU::Vector3f myLook;
		CU::Vector3f myRight;
		CU::Vector3f myUp;

		CU::Vector3f myShakeOffset;
		CU::Vector3f myShakeIntensity;

		float mySpeed;
		float myScreenShakeTimer;
		bool myHasResetPostShake;
	};

}}