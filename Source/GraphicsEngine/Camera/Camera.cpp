#include "stdafx.h"
#include "Camera.h"
#include "../Framework/Framework.h"
#include "../GraphicsEngineInterface.h"
#include "../DirectXFramework/CDirectXMathHelper.h"

#define PIf       3.14159265358979323846
#define DEGREES_TO_RADIANS static_cast<float>(PIf / 180.0f)
#define CAM_NEAR 0.01f
#define CAM_FAR 5000.f

using namespace hse::gfx;

CCamera::CCamera()
	: myFrustum(myCBufferData.myCameraOrientation, myCBufferData.myProjection, CAM_NEAR, CAM_FAR)
	, myNear(CAM_NEAR)
	, myFar(CAM_FAR)
{
}


CCamera::~CCamera()
{
}

bool hse::gfx::CCamera::Init()
{
	CreateProjection(CAM_NEAR, CAM_FAR);
	myFrustum.SetFov(myFOV);
	myFrustum.Update();
	return true;
}

void hse::gfx::CCamera::UpdateBuffers(const CU::Matrix44f& aCameraOrientation)
{
	myCBufferData.myCameraOrientation = aCameraOrientation;
	myCBufferData.myView = myCBufferData.myCameraOrientation.GetProperInverse();
	CU::Vector3f pos = aCameraOrientation.GetPosition();
	myCBufferData.myCameraPosition = { pos.x, pos.y, pos.z, 1.0f };
	myFrustum.Update();
}

void hse::gfx::CCamera::CreateProjection(const float aNearPlane, const float aFarPlane)
{
	if (CFramework::GetRenderAPI() == eRenderAPI::DirectX11)
	{
		CU::Vector2f resolution = CGraphicsEngineInterface::GetResolution();
		myCBufferData.myProjection = myCBufferData.myProjection.CreateProjectionMatrix(aNearPlane, aFarPlane, resolution.x, resolution.y, myFOV * DEGREES_TO_RADIANS);
	}
}

bool hse::gfx::CCamera::Intersects(CFrustumCollider& aCollider)
{
	return myFrustum.Intersects(aCollider);
}

bool hse::gfx::CCamera::Intersects(const CU::Vector3f aPosition, const float aRadius)
{
	return myFrustum.Intersects(aPosition, aRadius);
}
