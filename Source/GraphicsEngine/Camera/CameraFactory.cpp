#include "stdafx.h"
#include "CameraFactory.h"
#include "Camera.h"
#include "CameraInstance.h"
#include "../ResourceManager/ResourceManager.h"
#include "../DirectXFramework/API/DX11ConstantBuffer.h"
#include "../Scene/Scene.h"

using namespace hse::gfx;

CCameraFactory* CCameraFactory::ourInstance = nullptr;

CCameraFactory::CCameraFactory(CScene* aScene)
	: myScene(aScene)
{
}


CCameraFactory::~CCameraFactory()
{
}

void hse::gfx::CCameraFactory::Create(CScene* aScene)
{
	assert(ourInstance == nullptr && "Instance already created!");
	ourInstance = hse_new(CCameraFactory(aScene));
}

void hse::gfx::CCameraFactory::Destroy()
{
	SAFE_DELETE(ourInstance);
}

CCameraInstance hse::gfx::CCameraFactory::CreateCameraAtPosition(const CU::Vector3f& aPos, float aFOV)
{
	CCameraInstance instance;

	CResourceManager* resourceManager = CResourceManager::Get();

	if (resourceManager->CameraExists("MainCamera"))
	{
		instance.myCameraID = resourceManager->GetCameraID("MainCamera");
	}
	else
	{
		unsigned int cameraID = resourceManager->GetCameraID("MainCamera");
		instance.myCameraID = cameraID;
		instance.myMatrixOrientation.SetPosition(aPos);
		CCamera& camera(resourceManager->GetCameraWithID(cameraID));
		camera.myFOV = aFOV;
		camera.Init();

		unsigned int constantBufferID = resourceManager->GetConstantBufferID("MainCamera"); 
		camera.myCBufferID = constantBufferID;
	}
	return instance;
}
