#include "stdafx.h"
#include "Frustum.h"
#include "../../../CommonUtilities/Matrix44.h"
#include "FrustumCollider.h"

namespace hse { namespace gfx {

	CFrustum::CFrustum(const CommonUtilities::Matrix44<float>& aCameraOrientation, const CommonUtilities::Matrix44<float>& aProjection, float aNear, float aFar)
		: myOrientation(aCameraOrientation)
		, myProjection(aProjection)
		, myFrustum90(aNear, aFar, 90.f)
		, myFar(aFar)
		, myNear(aNear)
	{
	}

	CFrustum::~CFrustum()
	{
	}

	void CFrustum::Update()
	{
		myOrientationInverse = CU::Matrix44f::InverseSimple(myOrientation);
		myProjectionInverse = CU::Matrix44f::InverseSimple(myProjection);
	}

	void CFrustum::SetFov(const float aFov)
	{
		myFrustum90 = CU::Intersection::FovFrustum(myNear, myFar, aFov);
	}

	bool CFrustum::Intersects(CFrustumCollider& aCollider)
	{
		CU::Vector4<float> position = CU::Vector4<float>(aCollider.myCenter.x, aCollider.myCenter.y, aCollider.myCenter.z, 1.f) * myOrientationInverse * myProjectionInverse;
		return myFrustum90.Inside({ position.x, position.y, position.z }, aCollider.myRadius);
	}
	bool CFrustum::Intersects(const CU::Vector3f& aPosition, const float aRadius)
	{
		CU::Vector4<float> position = CU::Vector4<float>(aPosition.x, aPosition.y, aPosition.z, 1.f) * myOrientationInverse * myProjectionInverse;
		return myFrustum90.Inside({ position.x, position.y, position.z }, aRadius);
	}
}}