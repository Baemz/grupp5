#pragma once
#include "..\CommonUtilities\Vector.h"

namespace hse { namespace gfx {

	class CFrustumCollider
	{
		friend class CFrustum; 
	public:
		CFrustumCollider();
		~CFrustumCollider();
		CFrustumCollider(const CU::Vector3f& aPostion, const float aRadius);

		void SetRadius(const float aRadius);
		void SetPosition(const CU::Vector3f& aPostion);

		const float GetRadius() const;
		const CU::Vector3f& GetPosition() const;

	private:
		CU::Vector3f myCenter;
		float myRadius;
	};
}}