#pragma once
#include "../../../CommonUtilities/Matrix44.h"
#include "../../../CommonUtilities/FovFrustum.h"

namespace hse { namespace gfx {
	class CFrustumCollider;
	class CFrustum
	{
		friend class CCamera;
	public:
		~CFrustum();

		void Update();
		void SetFov(const float aFov);

		bool Intersects(CFrustumCollider& aCollider); // Overload with different types of bounding-colliders

		bool Intersects(const CU::Vector3f& aPosition, const float aRadius);

	private:

	private:
		CFrustum() = delete;
		CFrustum(const CommonUtilities::Matrix44<float>& aCameraOrientation, const CommonUtilities::Matrix44<float>& aProjection, float aNear, float aFar);
		const CommonUtilities::Matrix44<float>& myOrientation;
		const CommonUtilities::Matrix44<float>& myProjection;
		CommonUtilities::Matrix44<float> myOrientationInverse;
		CommonUtilities::Matrix44<float> myProjectionInverse;

		float myFar;
		float myNear;
		CU::Intersection::FovFrustum myFrustum90; 
	};

}}