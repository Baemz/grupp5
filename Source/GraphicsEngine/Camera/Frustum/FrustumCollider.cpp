#include "stdafx.h"
#include "FrustumCollider.h"

hse::gfx::CFrustumCollider::CFrustumCollider()
	: myCenter(0.f, 0.f, 0.f)
	, myRadius(100.f)
{
}

hse::gfx::CFrustumCollider::~CFrustumCollider()
{
}

hse::gfx::CFrustumCollider::CFrustumCollider(const CU::Vector3f& aPostion, const float aRadius)
{
	myCenter = aPostion;
	myRadius = aRadius;
}

void hse::gfx::CFrustumCollider::SetRadius(const float aRadius)
{
	myRadius = aRadius;
}

void hse::gfx::CFrustumCollider::SetPosition(const CU::Vector3f & aPostion)
{
	myCenter = aPostion;
}

const float hse::gfx::CFrustumCollider::GetRadius() const
{
	return myRadius;
}

const CU::Vector3f & hse::gfx::CFrustumCollider::GetPosition() const
{
	return myCenter;
}
