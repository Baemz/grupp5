#include "stdafx.h"
#include "CameraInstance.h"
#include "GraphicsEngineInterface.h"

#include <iostream>

//For screenpos purposes
#include "Camera.h"
#include "ResourceManager/ResourceManager.h"

//For shake
#include "../CommonUtilities/Random.h"

using namespace hse::gfx;

CCameraInstance::CCameraInstance()
	: myCameraID(UINT_MAX)
	, mySpeed(0.01f)
	, myScreenShakeTimer(0.0f)
	, myHasResetPostShake(true)
{
	myRight.x = myMatrixOrientation[0];
	myRight.y = myMatrixOrientation[1];
	myRight.z = myMatrixOrientation[2];
	myUp.x = myMatrixOrientation[4];
	myUp.y = myMatrixOrientation[5];
	myUp.z = myMatrixOrientation[6];
	myLook.x = myMatrixOrientation[8];
	myLook.y = myMatrixOrientation[9];
	myLook.z = myMatrixOrientation[10];
}


CCameraInstance::~CCameraInstance()
{
}

void hse::gfx::CCameraInstance::SetPosition(const CU::Vector3f& aPosition)
{
	myMatrixOrientation.SetPosition(aPosition);
}


void hse::gfx::CCameraInstance::Move(const CU::Vector3f & aAxis, float aSpeed)
{
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() + (aAxis * aSpeed));
}

void hse::gfx::CCameraInstance::Strafe(float aSpeed)
{
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() + (myRight * aSpeed));
}

void hse::gfx::CCameraInstance::MoveForward(float aSpeed)
{
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() + (myLook * aSpeed));
}

void hse::gfx::CCameraInstance::VerticalMovement(float aSpeed)
{
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() + (myUp * aSpeed));
}

void hse::gfx::CCameraInstance::Rotate(const CU::Vector3f& aDir)
{
	CU::Vector3f aCameraDir = myMatrixOrientation * aDir;
	CU::Vector3f pos = myMatrixOrientation.GetPosition();
	CU::Matrix44f x = myMatrixOrientation.CreateRotateAroundX(aCameraDir.x);
	CU::Matrix44f y = myMatrixOrientation.CreateRotateAroundY(aCameraDir.y);
	CU::Matrix44f z = myMatrixOrientation.CreateRotateAroundZ(aCameraDir.z);

	myMatrixOrientation *= x*y*z;

	myRight.x = myMatrixOrientation[0];
	myRight.y = myMatrixOrientation[1];
	myRight.z = myMatrixOrientation[2];
	myUp.x = myMatrixOrientation[4];
	myUp.y = myMatrixOrientation[5];
	myUp.z = myMatrixOrientation[6];
	myLook.x = myMatrixOrientation[8];
	myLook.y = myMatrixOrientation[9];
	myLook.z = myMatrixOrientation[10];

	myMatrixOrientation.SetPosition(pos);
}

void hse::gfx::CCameraInstance::ShakeOverTime(const float aTime, const CU::Vector3f& aIntensity)
{
	myScreenShakeTimer = aTime;
	myShakeIntensity = aIntensity;
	myShakeOffset = { 0.0f };
	myHasResetPostShake = false;
}

void hse::gfx::CCameraInstance::StopShake()
{
	myScreenShakeTimer = 0.0f;
	myShakeOffset = { 0.0f };
}

void hse::gfx::CCameraInstance::UpdateScreenShake(const float aDeltaTime)
{
	if (myScreenShakeTimer > 0.0f)
	{
		myScreenShakeTimer -= aDeltaTime;
		float quakeAmtX = (CU::GetRandomPercentage() / 100.0f) * std::sin(myShakeIntensity.x) * 2 - myShakeIntensity.x;
		float quakeAmtY = (CU::GetRandomPercentage() / 100.0f) * std::sin(myShakeIntensity.y) * 2 - myShakeIntensity.y;
		float quakeAmtZ = (CU::GetRandomPercentage() / 100.0f) * std::sin(myShakeIntensity.z) * 2 - myShakeIntensity.z;
		myShakeOffset += myRight * quakeAmtX;
		myShakeOffset += myUp * quakeAmtY;
		myShakeOffset += myLook * quakeAmtZ;
	}
	else if (!myHasResetPostShake)
	{
		StopShake();
	}
}

void hse::gfx::CCameraInstance::Update(const float aDeltaTime)
{
	UpdateScreenShake(aDeltaTime);

	//Local basis vectors
	myRight.x = myMatrixOrientation[0];
	myRight.y = myMatrixOrientation[1];
	myRight.z = myMatrixOrientation[2];
	myUp.x = myMatrixOrientation[4];
	myUp.y = myMatrixOrientation[5];
	myUp.z = myMatrixOrientation[6];
	myLook.x = myMatrixOrientation[8];
	myLook.y = myMatrixOrientation[9];
	myLook.z = myMatrixOrientation[10];
}

void hse::gfx::CCameraInstance::UseForRendering()
{
	//Quick fix for sure, but at least the camera's position remains constant.
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() + myShakeOffset);
	CGraphicsEngineInterface::AddToScene(*this);
	myMatrixOrientation.SetPosition(myMatrixOrientation.GetPosition() - myShakeOffset);
}

const CU::Vector2f hse::gfx::CCameraInstance::GetScreenPosition(const CU::Vector3f& aWorldPosition) const
{
	CU::Vector4f viewPos = myMatrixOrientation.GetFastInverse() * CU::Vector4f(aWorldPosition, 1.0f);
	CU::Vector4f projectionPos = GetProjection() * viewPos;
	projectionPos.x /= projectionPos.w;
	projectionPos.y /= projectionPos.w;
	return { projectionPos.x, projectionPos.y };
}

const CU::Vector3f hse::gfx::CCameraInstance::GetPosition() const
{
	return myMatrixOrientation.GetPosition();
}

const CU::Vector3f& hse::gfx::CCameraInstance::GetLook() const
{
	return myLook;
}

const CU::Vector3f& hse::gfx::CCameraInstance::GetRight() const
{
	return myRight;
}

const CU::Vector3f& hse::gfx::CCameraInstance::GetUp() const
{
	return myUp;
}

const CU::Matrix44f & hse::gfx::CCameraInstance::GetOrientation() const
{
	return myMatrixOrientation;
}

const CU::Matrix44f & hse::gfx::CCameraInstance::GetProjection() const
{
	return CResourceManager::Get()->GetCameraWithID(myCameraID).GetProjection();
}
