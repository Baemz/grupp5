#pragma once
#include "Frustum/Frustum.h"

namespace hse { namespace gfx {

	class CFrustumCollider;
	class CCameraInstance;
	class CCamera
	{
		friend class CCameraFactory;
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CSpriteRenderer3D;
		friend class CParticleRenderer;
		friend class CDebugRenderer;
		friend class CSkyboxRenderer;
		friend class CInputComponent;
		friend class CScene;

	public:
		CCamera();
		~CCamera();

		bool Init();
		void UpdateBuffers(const CU::Matrix44f& aCameraOrientation);

		const CU::Matrix44f& GetProjection() { return myCBufferData.myProjection; }

	private:
		struct SCameraBufferData
		{
			CU::Matrix44f myCameraOrientation;
			CU::Matrix44f myView;
			CU::Matrix44f myProjection;
			CU::Vector4f myCameraPosition;
		};

	private:
		SCameraBufferData myCBufferData;
		CFrustum myFrustum;
		unsigned int myCBufferID;
		float myFOV;
		float myNear;
		float myFar;

		void CreateProjection(const float aNearPlane = 0.001f, const float aFarPlane = 1000.f);
		bool Intersects(CFrustumCollider& aCollider); // Overload with the different types of colliders.
		bool Intersects(const CU::Vector3f aPosition, const float aRadius);
	};

}}