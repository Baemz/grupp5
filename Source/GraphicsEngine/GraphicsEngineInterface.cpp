#include "stdafx.h"
#include "GraphicsEngineInterface.h"
#include "GraphicsEngine.h"
#include "Scene/Scene.h"
#include "WindowHandler/WindowHandler.h"

using namespace hse::gfx;
CGraphicsEngine* CGraphicsEngineInterface::myGraphicsEngine = nullptr;
const CU::Vector2f CGraphicsEngineInterface::myTargetResolution = { 1920.f, 1080.f };

void CGraphicsEngineInterface::AddToScene(const CModelInstance& aInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddModelInstance(aInstance);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CCameraInstance& aCameraInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.SetCamera(aCameraInstance);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CSprite& aSprite)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddSprite(aSprite);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CSprite3D & a3DSprite)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddSprite3D(a3DSprite);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CPointLightInstance& aPLInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddPointLightInstance(aPLInstance);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CSpotLightInstance& aSLInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddSpotLightInstance(aSLInstance);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CParticleEmitterInstance& aParticleEmittorInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddParticleEmittorInstance(aParticleEmittorInstance);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CStreakEmitterInstance& aStreakEmitterInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddStreakEmittorInstance(aStreakEmitterInstance);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CText& aText)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddText(aText);
	}
}

void hse::gfx::CGraphicsEngineInterface::AddToScene(const CShaderEffectInstance & aShaderEffectInstance)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.AddShaderEffectInstance(aShaderEffectInstance);
	}
}

void hse::gfx::CGraphicsEngineInterface::UseForRendering(const CEnvironmentalLight& aEnvLight)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.SetEnvironmentalLight(aEnvLight);
	}
}

void hse::gfx::CGraphicsEngineInterface::EnableCursor()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myWindowHandler->EnableCursor();
	}
}

void hse::gfx::CGraphicsEngineInterface::DisableCursor()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myWindowHandler->DisableCursor();
	}
}

void hse::gfx::CGraphicsEngineInterface::ChangeParticleBuffer()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->myScene.ChangeParticleBuffer();
	}
}

const bool hse::gfx::CGraphicsEngineInterface::IsFullscreen()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->IsFullscreen();
	}

	return false;
}

void hse::gfx::CGraphicsEngineInterface::ToggleFullscreen()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleFullscreen();
	}
}

void hse::gfx::CGraphicsEngineInterface::TakeScreenshot(const wchar_t* aDestinationPath)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->TakeScreenshot(aDestinationPath);
	}
}

const CU::Vector2f hse::gfx::CGraphicsEngineInterface::GetResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->GetResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f hse::gfx::CGraphicsEngineInterface::GetFullResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->GetFullResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f hse::gfx::CGraphicsEngineInterface::GetWindowedResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->GetWindowedResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f hse::gfx::CGraphicsEngineInterface::GetFullWindowedResolution()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->myWindowHandler->GetFullWindowedResolution();
	}
	else
	{
		return CU::Vector2f();
	}
}

const CU::Vector2f& hse::gfx::CGraphicsEngineInterface::GetTargetResolution()
{
	return myTargetResolution;
}

const float hse::gfx::CGraphicsEngineInterface::GetRatio()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->GetRatio();
	}
	else
	{
		return 0.0f;
	}
}

#ifdef _DEBUG
void hse::gfx::CGraphicsEngineInterface::FXAA_TEST()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->DOFXAATEST();
	}
}
#endif
void hse::gfx::CGraphicsEngineInterface::ToggleSSAO()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleSSAO();
	}
}
void hse::gfx::CGraphicsEngineInterface::ToggleColorGrading()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleColorGrading();
	}
}
void hse::gfx::CGraphicsEngineInterface::ToggleLinearFog()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleLinearFog();
	}
}
void hse::gfx::CGraphicsEngineInterface::ToggleFXAA()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleFXAA();
	}
}
void hse::gfx::CGraphicsEngineInterface::ToggleBloom()
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->ToggleBloom();
	}
}
void hse::gfx::CGraphicsEngineInterface::SetResolution(const CU::Vector2f& aNewResolution)
{
	if (myGraphicsEngine)
	{
		myGraphicsEngine->SetResolution(aNewResolution);
	}
}

const bool hse::gfx::CGraphicsEngineInterface::IsSSAOEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsSSAOEnabled();
	}

	return false;
}

const bool hse::gfx::CGraphicsEngineInterface::IsColorGradingEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsColorGradingEnabled();
	}

	return false;
}

const bool hse::gfx::CGraphicsEngineInterface::IsLinearFogEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsLinearFogEnabled();
	}

	return false;
}

const bool hse::gfx::CGraphicsEngineInterface::IsBloomEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsBloomEnabled();
	}

	return false;
}

const bool hse::gfx::CGraphicsEngineInterface::IsFXAAEnabled()
{
	if (myGraphicsEngine)
	{
		return myGraphicsEngine->IsFXAAEnabled();
	}

	return false;
}
