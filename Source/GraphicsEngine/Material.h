#pragma once

#define MAX_TEXTURE_SLOT 10
#define MAX_CBUFFER_SLOT_VS 18
#define MAX_CBUFFER_SLOT_PS 17
#define STANDARD_NUM_CBUFFERS_VS 2
#define STANDARD_NUM_CBUFFERS_PS 3

namespace hse { namespace gfx {

	class CDX11Texture;
	class CDX11ConstantBuffer;

	enum ERenderMode
	{
		Deferred = 0,
		Forward,
	};

	enum ELoadingState
	{
		Unloaded,
		Loading,
		Loaded,
	};

	struct SMaterialData
	{

		bool useDeferredMode;
	};

	class CMaterial
	{
		friend class CModelFactory;
	public:
		CMaterial();
		~CMaterial();

		const bool Init(const std::string& aMaterial);
		bool Bind();
		const ERenderMode GetRenderMode() const;
		const bool IsLoading() const;
		const bool IsLoaded() const;

	private:
		void AddVertexShader(const std::string& aShader, const std::string& aEntryPoint);
		void AddPixelShader(const std::string& aShader, const std::string& aEntryPoint);
		void AddTextureAtSlot(const std::string& aTexture, const unsigned int aSlot); // Max slot is 10

		struct STexData
		{
			std::string path;
			unsigned int slot;
		};
		struct SMatData 
		{
			ERenderMode myRenderMode;
			std::vector<STexData> myTextureSlots;
		};

	private:
		CDX11Texture* myTextures;
		unsigned int myVertexShaderID;
		unsigned int myPixelShaderID;
		volatile ELoadingState myLoadingState;
		ERenderMode myRenderMode;
	};
}}