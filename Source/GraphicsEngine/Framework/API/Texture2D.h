#pragma once
namespace hse { namespace gfx {

	class CTexture2D
	{
	public:
		CTexture2D();
		virtual ~CTexture2D();

		virtual void Bind() = 0;

	private:

		virtual bool CreateTexture(const wchar_t* aFilePath) = 0;
		virtual bool CreateTexture(const char * aFilePath) = 0;
	};
}}