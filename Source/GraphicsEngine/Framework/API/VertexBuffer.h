#pragma once
namespace hse { namespace gfx {
	class CVertexBuffer
	{
	public:
		CVertexBuffer();
		virtual ~CVertexBuffer();
		virtual void Bind() = 0;
		virtual bool CreatePrimitiveTriangleBuffer() = 0;

		inline const unsigned int GetVertexCount() { return myVertexCount; };
	protected:
		unsigned int myStride;
		unsigned int myVertexCount;
		unsigned int myOffset;
	};

}}