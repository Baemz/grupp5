#pragma once
#include "../CommonUtilities/Vector2.h"

namespace hse
{
	class CEngine;
}
namespace hse { namespace gfx {

	class CModelInstance;
	class CCameraInstance;
	class CGraphicsEngine;
	class CSprite;
	class CSprite3D;
	class CEnvironmentalLight;
	class CPointLightInstance;
	class CSpotLightInstance;
	class CParticleEmitterInstance;
	class CStreakEmitterInstance;
	class CShaderEffectInstance;
	class CText;

	class CGraphicsEngineInterface
	{
		friend CEngine;
		friend class CWindowHandler;
	public:

		static void AddToScene(const CModelInstance& aInstance);
		static void AddToScene(const CCameraInstance& aCameraInstance);
		static void AddToScene(const CSprite& aSprite);
		static void AddToScene(const CSprite3D& a3DSprite);
		static void AddToScene(const CPointLightInstance& aPLInstance);
		static void AddToScene(const CSpotLightInstance& aPLInstance);
		static void AddToScene(const CParticleEmitterInstance& aParticleEmittorInstance);
		static void AddToScene(const CStreakEmitterInstance& aStreakEmitterInstance);
		static void AddToScene(const CText& aText);
		static void AddToScene(const CShaderEffectInstance& aShaderEffectInstance);
		static void UseForRendering(const CEnvironmentalLight& aEnvLight);

		static void EnableCursor();
		static void DisableCursor();

		static void ChangeParticleBuffer();

		static const bool IsFullscreen();
		static void ToggleFullscreen();
		static void TakeScreenshot(const wchar_t* aDestinationPath);

		static const CU::Vector2f GetResolution();
		static const CU::Vector2f GetFullResolution();
		static const CU::Vector2f GetWindowedResolution();
		static const CU::Vector2f GetFullWindowedResolution();
		static const CU::Vector2f& GetTargetResolution();
		static const float GetRatio();

#ifdef _DEBUG
		static void FXAA_TEST();
#endif
		static void SetResolution(const CU::Vector2f& aNewResolution);
		
		static const bool IsSSAOEnabled();
		static const bool IsColorGradingEnabled();
		static const bool IsLinearFogEnabled();
		static const bool IsBloomEnabled();
		static const bool IsFXAAEnabled();
		static void ToggleSSAO();
		static void ToggleColorGrading();
		static void ToggleLinearFog();
		static void ToggleFXAA();
		static void ToggleBloom();

	private:
		static const CU::Vector2f myTargetResolution;


		CGraphicsEngineInterface() = delete;
		~CGraphicsEngineInterface() = delete;
		static CGraphicsEngine* myGraphicsEngine;
	};
}}