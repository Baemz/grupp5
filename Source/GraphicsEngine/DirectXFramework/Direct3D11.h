#pragma once
#include "../Framework/Framework.h"
#include "API/DX11FullscreenTexture.h"
//#include <atomic>

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilState;
struct ID3D11BlendState;
struct ID3D11SamplerState;
struct ID3D11RasterizerState;

namespace hse { namespace gfx {

	class CWindowHandler;

	class CDirect3D11 : public CFramework
	{
	public:
		CDirect3D11();
		~CDirect3D11();

		bool Init(CWindowHandler& aWindowHandler, const bool aStartInFullscreen, const bool aUseVSync) override;

		void BeginFrame(float aClearColor[4]) override;
		void EndFrame() override;
		void TakeScreenshot(const wchar_t* aDestinationPath) override;
		void ActivateScreenTarget() override;
		void ToggleFullscreen() override;
		void SetResolution(const unsigned int aWidth, const unsigned int aHeight) override;

		ID3D11Device* GetDevice() { return myDevice; };
		ID3D11DeviceContext* GetContext() { return myContext; };

		void SetDefaultDepth();
		void DisableDepth();
		void EnableReadOnlyDepth();

		void EnableAlphablend();
		void EnableAdditiveblend();
		void DisableBlending();

		void SetShouldRenderWireFrame();
		const bool GetShouldRenderWireFrame() const;
		void SetRasterStateWireframe(const bool aState) const;

		inline static CDirect3D11* GetAPI() { return (CDirect3D11*)ourFramework; }

	private:
		bool CreateDepthBuffers();
		void CreateAlphablend();
		bool CreateSamplerState();
		bool CreateRasterState();

	private:
		bool myShouldRenderWireFrame;
		bool myUseVSync;
		CWindowHandler* myWindowHandler;
		IDXGISwapChain* mySwapChain;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		CDX11FullscreenTexture* myScreenTarget;
		ID3D11DepthStencilState* myDepthState2D;
		ID3D11DepthStencilState* myDepthStateParticle;
		ID3D11DepthStencilState* myDepthState;
		ID3D11BlendState* myAlphablendState;
		ID3D11BlendState* myAdditiveblendState;
		ID3D11BlendState* myDisabledBlendState;
		ID3D11SamplerState* mySamplerState;
		ID3D11RasterizerState* myRasterStateNormal;
		ID3D11RasterizerState* myRasterStateWireframe;
		int mySwapChainIsInFullscreen;
	};
}}