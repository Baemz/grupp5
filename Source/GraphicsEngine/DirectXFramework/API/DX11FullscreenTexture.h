#pragma once

struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Texture2D;
struct ID3D11ShaderResourceView;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;
struct D3D11_VIEWPORT;
enum DXGI_FORMAT;
namespace hse { namespace gfx {

	class CDX11Texture;

	class CDX11FullscreenTexture
	{
	public:
		CDX11FullscreenTexture();
		~CDX11FullscreenTexture();

		bool Init(const CU::Vector2f& aSize, const DXGI_FORMAT aFormat, bool aCreateDepth = false, bool aBindableDepth = false);
		bool Init(ID3D11Texture2D* aTexture, bool aCreateDepth = false);
		bool Init(const wchar_t* aTexturePath, const CU::Vector2f& aSize, const DXGI_FORMAT aFormat, bool aCreateDepth = false);

		void ClearTexture(const CU::Vector4f& aClearColor);
		void SetAsActiveTarget();
		void SetAsActiveTarget(ID3D11DepthStencilView * aDepth);
		void SetAsResourceOnSlot(unsigned int aSlot);
		void SetDepthAsResourceOnSlot(unsigned int aSlot);
		const CU::Vector2f& GetResolution() const { return myResolution; };

	private:
		CU::Vector2f myResolution;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		ID3D11Texture2D* myTexture;
		ID3D11ShaderResourceView* myShaderResource;
		ID3D11ShaderResourceView* myDShaderResource;
		ID3D11RenderTargetView* myRenderTarget;
		ID3D11DepthStencilView* myDepthStencil;
		D3D11_VIEWPORT* myViewPort;
	};
}}
