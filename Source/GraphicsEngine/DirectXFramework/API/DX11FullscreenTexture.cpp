#include "stdafx.h"
#include "DX11FullscreenTexture.h"
#include "../Direct3D11.h"

#include "../DirectXTK/DDSTextureLoader.h"

namespace hse { namespace gfx {

	CDX11FullscreenTexture::CDX11FullscreenTexture()
		: myTexture(nullptr)
		, myShaderResource(nullptr)
		, myRenderTarget(nullptr)
		, myDepthStencil(nullptr)
		, myViewPort(nullptr)
		, myDShaderResource(nullptr)
	{
		myDevice = CDirect3D11::GetAPI()->GetDevice();
		myContext = CDirect3D11::GetAPI()->GetContext();
	}


	CDX11FullscreenTexture::~CDX11FullscreenTexture()
	{
		SAFE_RELEASE(myTexture);
		SAFE_RELEASE(myRenderTarget);
		SAFE_RELEASE(myDepthStencil);
		SAFE_RELEASE(myShaderResource);
		SAFE_RELEASE(myDShaderResource);
		hse_delete(myViewPort);
	}

	bool CDX11FullscreenTexture::Init(const CU::Vector2f& aSize, const DXGI_FORMAT aFormat, bool aCreateDepth, bool aBindableDepth)
	{
		D3D11_TEXTURE2D_DESC textureDesc;

		textureDesc.Width = static_cast<UINT>(aSize.x);
		textureDesc.Height = static_cast<UINT>(aSize.y);
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = aFormat;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.SampleDesc.Quality = 0;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		HRESULT result = myDevice->CreateTexture2D(&textureDesc, nullptr, &myTexture);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create empty texture in CDX11FullscreenTexture.");
			return false;
		}

		if (aCreateDepth)
		{
			ID3D11Texture2D* depthTexture;
			D3D11_TEXTURE2D_DESC depthDesc = {};
			depthDesc.Width = static_cast<UINT>(aSize.x);
			depthDesc.Height = static_cast<UINT>(aSize.y);
			depthDesc.ArraySize = 1;
			depthDesc.SampleDesc.Count = 1;
			depthDesc.SampleDesc.Quality = 0;
			depthDesc.MipLevels = 1;
			depthDesc.CPUAccessFlags = 0;
			depthDesc.MiscFlags = 0;
			if (aBindableDepth)
			{
				depthDesc.Format = DXGI_FORMAT_R32_TYPELESS;
				depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
			}
			else
			{
				depthDesc.Format = DXGI_FORMAT_D32_FLOAT;
				depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			}
			result = myDevice->CreateTexture2D(&depthDesc, nullptr, &depthTexture);
			if (FAILED(result))
			{
				ENGINE_LOG("DX11-ERROR! Could not create depth-texture.");
				return false;
			}
			if (aBindableDepth)
			{
				D3D11_DEPTH_STENCIL_VIEW_DESC dsvdesc;
				dsvdesc.Flags = 0;
				dsvdesc.Format = DXGI_FORMAT_D32_FLOAT;
				dsvdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
				dsvdesc.Texture2D.MipSlice = 0;
				result = myDevice->CreateDepthStencilView(depthTexture, &dsvdesc, &myDepthStencil);
			}
			else
			{
				result = myDevice->CreateDepthStencilView(depthTexture, nullptr, &myDepthStencil);
			}
			if (FAILED(result))
			{
				ENGINE_LOG("DX11-ERROR! Could not create depth-stencil.");
				return false;
			}
			if (aBindableDepth)
			{
				D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
				srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				srvDesc.Texture2D.MostDetailedMip = 0;
				srvDesc.Texture2D.MipLevels = 1;
				result = myDevice->CreateShaderResourceView(depthTexture, &srvDesc, &myDShaderResource);
				if (FAILED(result))
				{
					ENGINE_LOG("DX11-ERROR! Could not create depth-srv.");
					return false;
				}
			}
			depthTexture->Release();
		}

		result = myDevice->CreateRenderTargetView(myTexture, nullptr, &myRenderTarget);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create rendertargetview.");
			return false;
		}


		result = myDevice->CreateShaderResourceView(myTexture, nullptr, &myShaderResource);

		myViewPort = hse_new(D3D11_VIEWPORT());
		myViewPort->TopLeftX = 0;
		myViewPort->TopLeftY = 0;
		myViewPort->Width = aSize.x;
		myViewPort->Height = aSize.y;
		myViewPort->MinDepth = 0.0f;
		myViewPort->MaxDepth = 1.0f;

		myResolution = aSize;

		return true;
	}

	bool CDX11FullscreenTexture::Init(ID3D11Texture2D* aTexture, bool aCreateDepth)
	{
		myTexture = aTexture;
		HRESULT result;
		D3D11_TEXTURE2D_DESC textureDesc = {};
		myTexture->GetDesc(&textureDesc);

		if (aCreateDepth)
		{
			ID3D11Texture2D* depthTexture = nullptr;
			D3D11_TEXTURE2D_DESC depthDesc = {};
			depthDesc.Width = textureDesc.Width;
			depthDesc.Height = textureDesc.Height;
			depthDesc.ArraySize = 1;
			depthDesc.Format = DXGI_FORMAT_D32_FLOAT;
			depthDesc.SampleDesc.Count = 1;
			depthDesc.SampleDesc.Quality = 1;
			depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			result = myDevice->CreateTexture2D(&depthDesc, nullptr, &depthTexture);
			if (FAILED(result))
			{
				ENGINE_LOG("DX11-ERROR! Could not create depth-texture.");
				return false;
			}

			result = myDevice->CreateDepthStencilView(depthTexture, nullptr, &myDepthStencil);
			if (FAILED(result))
			{
				ENGINE_LOG("DX11-ERROR! Could not create depth-stencil.");
				return false;
			}
			depthTexture->Release();
		}

		result = myDevice->CreateRenderTargetView(myTexture, nullptr, &myRenderTarget);
		if (FAILED(result))
		{
			ENGINE_LOG("DX11-ERROR! Could not create rendertargetview.");
			return false;
		}

		myViewPort = hse_new(D3D11_VIEWPORT());
		myViewPort->TopLeftX = 0;
		myViewPort->TopLeftY = 0;
		myViewPort->Width = static_cast<FLOAT>(textureDesc.Width);
		myViewPort->Height = static_cast<FLOAT>(textureDesc.Height);
		myViewPort->MinDepth = 0.0f;
		myViewPort->MaxDepth = 1.0f;

		myResolution.x = static_cast<FLOAT>(textureDesc.Width);
		myResolution.y = static_cast<FLOAT>(textureDesc.Height);
		return true;
	}

	bool CDX11FullscreenTexture::Init(const wchar_t * aTexturePath, const CU::Vector2f & aSize, const DXGI_FORMAT aFormat, bool aCreateDepth)
	{
		if (Init(aSize, aFormat, aCreateDepth))
		{
			HRESULT result = DirectX::CreateDDSTextureFromFile(myDevice, aTexturePath, nullptr, &myShaderResource);
			if (FAILED(result))
			{
				RESOURCE_LOG("Could not create texture [%s]", aTexturePath);
				return false;
			}
			return true;
		}
		return false;
	}

	void CDX11FullscreenTexture::ClearTexture(const CU::Vector4f& aClearColor)
	{
		float clearColor[4] = { aClearColor.x ,aClearColor.y, aClearColor.z, aClearColor.w };
		myContext->ClearRenderTargetView(myRenderTarget, clearColor);
		if (myDepthStencil != nullptr)
		{
			myContext->ClearDepthStencilView(myDepthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		}
	}

	void CDX11FullscreenTexture::SetAsActiveTarget()
	{
		myContext->OMSetRenderTargets(1, &myRenderTarget, myDepthStencil);
		myContext->RSSetViewports(1, myViewPort);
	}
	void CDX11FullscreenTexture::SetAsActiveTarget(ID3D11DepthStencilView* aDepth)
	{
		myContext->OMSetRenderTargets(1, &myRenderTarget, aDepth);
		myContext->RSSetViewports(1, myViewPort);
	}

	void CDX11FullscreenTexture::SetAsResourceOnSlot(unsigned int aSlot)
	{
		myContext->PSSetShaderResources(aSlot, 1, &myShaderResource);
	}

	void CDX11FullscreenTexture::SetDepthAsResourceOnSlot(unsigned int aSlot)
	{
		if (myDShaderResource != nullptr)
		{
			myContext->PSSetShaderResources(aSlot, 1, &myDShaderResource);
		}
	}

}}