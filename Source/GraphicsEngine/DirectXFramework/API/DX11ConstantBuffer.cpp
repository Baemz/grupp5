#include "stdafx.h"
#include "DX11ConstantBuffer.h"
#include "../Direct3D11.h"

using namespace hse::gfx;

CDX11ConstantBuffer::CDX11ConstantBuffer()
	: myCBuffer(nullptr)
	, myIsLoaded(false)
{
	myDevice = CDirect3D11::GetAPI()->GetDevice();
	myContext = CDirect3D11::GetAPI()->GetContext();
}


CDX11ConstantBuffer::~CDX11ConstantBuffer()
{
	SAFE_RELEASE(myCBuffer);
}

void hse::gfx::CDX11ConstantBuffer::BindVS(unsigned char aRegisterIndex)
{
	myContext->VSSetConstantBuffers(aRegisterIndex, 1, &myCBuffer);
}

void hse::gfx::CDX11ConstantBuffer::BindGS(unsigned char aRegisterIndex)
{
	myContext->GSSetConstantBuffers(aRegisterIndex, 1, &myCBuffer);
}

void hse::gfx::CDX11ConstantBuffer::BindPS(unsigned char aRegisterIndex)
{
	myContext->PSSetConstantBuffers(aRegisterIndex, 1, &myCBuffer);
}

const bool hse::gfx::CDX11ConstantBuffer::IsLoaded() const
{
	return myIsLoaded;
}

struct test
{
	CU::Matrix44f myCameraOrientation;
	CU::Matrix44f myToCamera;
	CU::Matrix44f myProjection;
};

void hse::gfx::CDX11ConstantBuffer::CreateOrUpdateBuffer(unsigned int aSize, void* someData)
{
	mySize = aSize;

	HRESULT result;
	if (myCBuffer == nullptr)
	{
		D3D11_BUFFER_DESC bufferDesc = {};
		bufferDesc.ByteWidth = aSize;
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		result = myDevice->CreateBuffer(&bufferDesc, nullptr, &myCBuffer);
		if (FAILED(result))
		{
			RESOURCE_LOG("ERROR! Could not create CBuffer.");
		}
		myIsLoaded = true;
	}

	D3D11_MAPPED_SUBRESOURCE subData = {};
	result = myContext->Map(myCBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &subData);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! Cannot change data on constant-buffer.");
	}
	memcpy(subData.pData, someData, static_cast<size_t>(mySize));
	myContext->Unmap(myCBuffer, 0);
}
