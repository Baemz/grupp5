#include "stdafx.h"
#include "DX11Shader.h"
#include "../Direct3D11.h"
#include "../../../EngineCore/FileWatcher/FileWatcherWrapper.h"
#include <d3dcompiler.h>
#pragma comment(lib, "d3dcompiler.lib")


using namespace hse::gfx;

CDX11Shader::CDX11Shader()
	: myType(eShaderType::NONE)
	, myInputLayout(nullptr)
	, myVertexShader(nullptr)
	, myPixelShader(nullptr)
	, myGeometryShader(nullptr)
	, myHasInitialized(false)
	, myHasInitializedOnce(false)
	, myIsLoading(false)
{
	myDevice = CDirect3D11::GetAPI()->GetDevice();
	myContext = CDirect3D11::GetAPI()->GetContext();
}


CDX11Shader::~CDX11Shader()
{
	SAFE_RELEASE(myInputLayout);
	SAFE_RELEASE(myVertexShader);
	SAFE_RELEASE(myPixelShader);
	SAFE_RELEASE(myGeometryShader);
}

void hse::gfx::CDX11Shader::InitVertex(const char* aPath, const char* aEntryPoint)
{
	InitVertex(aPath, aEntryPoint, nullptr);
}

void hse::gfx::CDX11Shader::InitPixel(const char* aPath, const char* aEntryPoint)
{
	InitPixel(aPath, aEntryPoint, nullptr);
}

void CDX11Shader::InitVertex(const char* aPath, const char* aEntryPoint, const D3D_SHADER_MACRO* aDefines)
{
	myIsLoading = true;
	if (myHasInitializedOnce == false)
	{
		CFileWatcherWrapper::AddFileToWatch(this, aPath, &CDX11Shader::ShaderChangedCallback);
		myHasInitializedOnce = true;
	}
	if (myHasInitialized)
	{
		return;
	}
	myType = eShaderType::Vertex;

	if (aDefines != nullptr)
	{
		myDefine = aDefines[0].Definition;
		myDefineName = aDefines[0].Name;
	}

	ID3D10Blob* dataBlob = nullptr;
	CompileShader(aPath, aEntryPoint, dataBlob);
	if (dataBlob == nullptr)
	{
		ENGINE_LOG("ERROR! Shader could not be loaded.");
		return;
	}

	void* vertexShaderByteCode = dataBlob->GetBufferPointer();
	SIZE_T vertexBytecodeSize = dataBlob->GetBufferSize();

	HRESULT result = myDevice->CreateVertexShader(vertexShaderByteCode, vertexBytecodeSize, nullptr, &myVertexShader);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! Shader could not be loaded.");
		return;
	}

	CreateLayout(dataBlob);
	myPath = aPath;
	myEntryPoint = aEntryPoint;
	dataBlob->Release();

	ENGINE_LOG("[%s] compiled successfully.", aPath);
	myHasInitialized = true;
	myIsLoading = false;
}

void CDX11Shader::InitPixel(const char* aPath, const char* aEntryPoint, const D3D_SHADER_MACRO* aDefines)
{
	myIsLoading = true;
	if (myHasInitializedOnce == false)
	{
		CFileWatcherWrapper::AddFileToWatch(this, aPath, &CDX11Shader::ShaderChangedCallback);
		myHasInitializedOnce = true;
	}
	if (myHasInitialized)
	{
		// Print warning
		return;
	}
	myType = eShaderType::Pixel;

	if (aDefines != nullptr)
	{
		myDefine = aDefines[0].Definition;
		myDefineName = aDefines[0].Name;
	}

	ID3D10Blob* dataBlob = nullptr;
	CompileShader(aPath, aEntryPoint, dataBlob);
	if (dataBlob == nullptr)
	{
		// PRINT ERROR
		ENGINE_LOG("ERROR! [%s] Could not be compiled.", aPath);
		return;
	}

	void* pixelShaderByteCode = dataBlob->GetBufferPointer();
	SIZE_T pixelBytecodeSize = dataBlob->GetBufferSize();

	HRESULT result = myDevice->CreatePixelShader(pixelShaderByteCode, pixelBytecodeSize, nullptr, &myPixelShader);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! [%s] Could not be created.", aPath);
	}

	myPath = aPath;
	myEntryPoint = aEntryPoint;

	ENGINE_LOG("[%s] compiled successfully.", aPath);

	dataBlob->Release();
	myHasInitialized = true;
	myIsLoading = false; 
}

void hse::gfx::CDX11Shader::InitGeometry(const char* aPath, const char* aEntryPoint)
{
	myIsLoading = true;
	if (myHasInitializedOnce == false)
	{
		CFileWatcherWrapper::AddFileToWatch(this, aPath, &CDX11Shader::ShaderChangedCallback);
		myHasInitializedOnce = true;
	}
	if (myHasInitialized)
	{
		return;
	}
	myType = eShaderType::Geometry;

	ID3D10Blob* dataBlob = nullptr;
	CompileShader(aPath, aEntryPoint, dataBlob);
	if (dataBlob == nullptr)
	{
		// PRINT ERROR
		ENGINE_LOG("ERROR! [%s] Could not be compiled.", aPath);
		return;
	}

	void* geometryShaderByteCode = dataBlob->GetBufferPointer();
	SIZE_T geometryBytecodeSize = dataBlob->GetBufferSize();

	HRESULT result = myDevice->CreateGeometryShader(geometryShaderByteCode, geometryBytecodeSize, nullptr, &myGeometryShader);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! [%s] Could not be created.", aPath);
	}

	myPath = aPath;
	myEntryPoint = aEntryPoint;

	ENGINE_LOG("[%s] compiled successfully.", aPath);

	dataBlob->Release();
	myHasInitialized = true;
	myIsLoading = false;
}

void CDX11Shader::Bind()
{
	switch (myType)
	{
		case eShaderType::Vertex:
			if (myVertexShader && myInputLayout)
			{
				myContext->IASetInputLayout(myInputLayout);
				myContext->VSSetShader(myVertexShader, nullptr, 0);
			}
			return;

		case eShaderType::Pixel:
			if (myPixelShader)
			{
				myContext->PSSetShader(myPixelShader, nullptr, 0);
			}
			return;
		case eShaderType::Geometry:
			if (myGeometryShader)
			{
				myContext->GSSetShader(myGeometryShader, nullptr, 0);
			}
			return;
	}
}

const bool hse::gfx::CDX11Shader::IsLoading() const
{
	return myIsLoading;
}

void CDX11Shader::CompileShader(const char* aPath, const char* aEntryPoint, ID3D10Blob*& aDataBlob)
{
	const char* shaderVersion = "";
	switch (myType)
	{
		case eShaderType::Vertex:
			shaderVersion = "vs_5_0";
			break;
		case eShaderType::Pixel:
			shaderVersion = "ps_5_0";
			break;
		case eShaderType::Geometry:
			shaderVersion = "gs_5_0";
			break;
	}
	
	ID3D10Blob* pErrorMsg = nullptr;
#ifdef _RETAIL
	DWORD shaderFlags = D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif
#ifdef _RELEASE
	DWORD shaderFlags = D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif
#ifndef _RETAIL
#ifndef _RELEASE
	DWORD shaderFlags = D3DCOMPILE_SKIP_OPTIMIZATION | D3DCOMPILE_DEBUG;
#endif
#endif

	const D3D_SHADER_MACRO defines[] = 
	{
		{ myDefineName.c_str(), myDefine.c_str() },
		{ nullptr, nullptr },
	};

	wchar_t wstring[4096];
	HRESULT result = D3DCompileFromFile(ConvertCharArrayToLPCWSTR(aPath, (wchar_t*)(&wstring), 4096), defines, D3D_COMPILE_STANDARD_FILE_INCLUDE, aEntryPoint, shaderVersion, shaderFlags, 0, &aDataBlob, &pErrorMsg);
	if (FAILED(result))
	{
		if (pErrorMsg)
		{
			_RPT1(_CRT_WARN, "Compilation error: %s\n", (char*)pErrorMsg->GetBufferPointer());
			ENGINE_LOG("ERROR! [%s] failed to compile with error:\n %s", aPath, (char*)pErrorMsg->GetBufferPointer());
			SAFE_RELEASE(pErrorMsg);
		}
		aDataBlob = nullptr;
	}
}

void hse::gfx::CDX11Shader::CreateLayout(ID3D10Blob*& aDataBlob)
{
	ID3D11ShaderReflection* vertexShaderReflection = nullptr;
	if (FAILED(D3DReflect(aDataBlob->GetBufferPointer(), aDataBlob->GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&vertexShaderReflection)))
	{
		return;
	}

	D3D11_SHADER_DESC shaderDesc;
	vertexShaderReflection->GetDesc(&shaderDesc);

	std::vector<D3D11_INPUT_ELEMENT_DESC> inputLayoutDesc;
	for (unsigned int i = 0; i < shaderDesc.InputParameters; i++)
	{
		D3D11_SIGNATURE_PARAMETER_DESC paramDesc;
		vertexShaderReflection->GetInputParameterDesc(i, &paramDesc);

		D3D11_INPUT_ELEMENT_DESC elementDesc;
		elementDesc.SemanticName = paramDesc.SemanticName;
		elementDesc.SemanticIndex = paramDesc.SemanticIndex;
		elementDesc.InputSlot = 0;
		elementDesc.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
		elementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		elementDesc.InstanceDataStepRate = 0;

		if (paramDesc.Mask == 1)
		{
			if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32_UINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32_SINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32_FLOAT;
		}
		else if (paramDesc.Mask <= 3)
		{
			if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32G32_UINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32G32_SINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32G32_FLOAT;
		}
		else if (paramDesc.Mask <= 7)
		{
			if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32_UINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32_SINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT;
		}
		else if (paramDesc.Mask <= 15)
		{
			if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32A32_UINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.Format = DXGI_FORMAT_R32G32B32A32_SINT;
			else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		}

		inputLayoutDesc.push_back(elementDesc);
	}

	HRESULT result = myDevice->CreateInputLayout(&inputLayoutDesc[0], static_cast<UINT>(inputLayoutDesc.size()), aDataBlob->GetBufferPointer(), aDataBlob->GetBufferSize(), &myInputLayout);
	if (FAILED(result))
	{
		ENGINE_LOG("ERROR! Could not reflect shader layout.");
	}
	vertexShaderReflection->Release();
}

bool hse::gfx::CDX11Shader::ShaderChangedCallback(void* aThis, const char* aShaderChanged)
{
	aShaderChanged;
	Sleep(100);
	CDX11Shader& thisShader = *static_cast<CDX11Shader*>(aThis);
	thisShader.myIsLoading = true;
	thisShader.myHasInitialized = false;

	if (thisShader.myType == eShaderType::Pixel)
	{
		SAFE_RELEASE(thisShader.myPixelShader);
		thisShader.InitPixel(thisShader.myPath.c_str(), thisShader.myEntryPoint.c_str());
	}
	else if (thisShader.myType == eShaderType::Geometry)
	{
		SAFE_RELEASE(thisShader.myGeometryShader);
		thisShader.InitGeometry(thisShader.myPath.c_str(), thisShader.myEntryPoint.c_str());
	}
	else
	{
		SAFE_RELEASE(thisShader.myInputLayout);
		SAFE_RELEASE(thisShader.myVertexShader);
		thisShader.InitVertex(thisShader.myPath.c_str(), thisShader.myEntryPoint.c_str());
	}

	thisShader.myIsLoading = false;
	ENGINE_LOG("[%s] has been reloaded.", aShaderChanged);
	return true;
}
