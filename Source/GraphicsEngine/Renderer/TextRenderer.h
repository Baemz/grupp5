#pragma once
#include <map>
#include "..\CommonUtilities\GrowingArray.h"
#include "../Text/Text.h"

struct ID3D11Device;
struct ID3D11DeviceContext;

namespace hse
{
	namespace gfx
	{
		class CFont;
		class CCameraInstance;

		class CTextRenderer
		{
		public:
			CTextRenderer();
			~CTextRenderer();

			static CTextRenderer* GetInstance();

			void Init();
			void Destroy();

			bool InitFont(const std::string& aFontName);
			float GetTextWidth(const CText& aText);

			bool Render(const CU::GrowingArray<CText, unsigned int>& aTexts, const CU::Vector2f& aResolution);

		private:
			static CTextRenderer* ourInstance;

			bool RenderText(const CText& aText, const CU::Vector2f& aResolution, const CU::Matrix44f& aTransform) const;

			ID3D11Device* myDevice;
			ID3D11DeviceContext* myDeviceContext;
			std::unordered_map<std::string, CFont*> myFonts;
		};
	}
}
