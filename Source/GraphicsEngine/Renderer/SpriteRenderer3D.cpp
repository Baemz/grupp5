#include "stdafx.h"
#include "SpriteRenderer3D.h"
#include "Sprite/Sprite.h"
#include "Sprite/SpriteFactory.h"
#include "Model/Loading/ModelFactory.h"
#include "ResourceManager/ResourceManager.h"
#include "Sprite/TexturedQuad/TexturedQuad.h"

#include "../Camera/Camera.h"
#include "GraphicsEngineInterface.h"

namespace hse {
	namespace gfx {

		CSpriteRenderer3D::CSpriteRenderer3D()
			: myContext(nullptr)
		{
		}


		CSpriteRenderer3D::~CSpriteRenderer3D()
		{
		}


		void CSpriteRenderer3D::Init()
		{
			myContext = CDirect3D11::GetAPI()->GetContext();
			myCBufferID = CResourceManager::Get()->GetConstantBufferID("SpriteRenderer3D");
			mySpriteEffectCBufferID = CResourceManager::Get()->GetConstantBufferID("SpriteEffectCBufferID");
		}


		void CSpriteRenderer3D::Render(CCameraInstance& aCameraInstance, CU::GrowingArray<hse::gfx::CSprite3D, unsigned int>& aListToRender)
		{
			if (aCameraInstance.myCameraID == UINT_MAX)
			{
				return;
			}
			if (aListToRender.Size() <= 0)
			{
				return;
			}

			CDirect3D11::GetAPI()->DisableDepth();
			CDirect3D11::GetAPI()->EnableAlphablend();

			myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			CResourceManager* resourceManager(CResourceManager::Get());

			CCamera& camera(resourceManager->GetCameraWithID(aCameraInstance.myCameraID));
			CDX11ConstantBuffer& cameraCBuffer(resourceManager->GetConstantBufferWithID(camera.myCBufferID));
			cameraCBuffer.SetData(camera.myCBufferData);
			cameraCBuffer.BindVS(0);

			SSprite3DData instanceData;
			CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));
			CDX11ConstantBuffer& spriteEffectCBuffer(resourceManager->GetConstantBufferWithID(mySpriteEffectCBufferID));

			const CU::Vector2f& resolution(CGraphicsEngineInterface::GetTargetResolution());

			for (unsigned int index = 0; index < aListToRender.Size(); ++index)
			{
				const hse::gfx::CSprite3D& currentSprite(aListToRender[index]);

				if (currentSprite.myTexturedQuadID == UINT_MAX)
				{
					continue;
				}

				const CSpriteFactory& spriteFactory(CModelFactory::Get()->mySpriteFactory);

				const CTexturedQuad& texQuad = spriteFactory.GetTexturedQuadWithID(currentSprite.myTexturedQuadID);

				if (texQuad.myVertexBufferID == UINT_MAX)
				{
					continue;
				}
				if (texQuad.myIndexBufferID == UINT_MAX)
				{
					continue;
				}
				if (texQuad.myPixelShaderID == UINT_MAX)
				{
					continue;
				}
				if (texQuad.myVertexShaderID == UINT_MAX)
				{
					continue;
				}

				CDX11VertexBuffer& vBuffer = resourceManager->GetVertexBufferWithID(texQuad.myVertexBufferID);
				CDX11IndexBuffer& iBuffer = resourceManager->GetIndexBufferWithID(texQuad.myIndexBufferID);

				vBuffer.Bind();
				iBuffer.Bind();

				CDX11Shader& pShader = resourceManager->GetShaderWithID(texQuad.myPixelShaderID);
				CDX11Shader& vShader = resourceManager->GetShaderWithID(texQuad.myVertexShaderID);

				vShader.Bind();
				pShader.Bind();

				if (texQuad.myTextureID != UINT_MAX)
				{
					CDX11Texture& texture = resourceManager->GetTextureWithID(texQuad.myTextureID);
					texture.Bind();
				}
				else
				{
					ENGINE_LOG("WARNING! Texture for Sprite could not bind. Textures may appear inaccurate.");
				}

				instanceData.myOrientation = currentSprite.myOrientation;
				instanceData.myTextureSizeAndTargetSize.x = texQuad.myTextureSize.x;
				instanceData.myTextureSizeAndTargetSize.y = texQuad.myTextureSize.y;
				instanceData.myTextureSizeAndTargetSize.z = resolution.x;
				instanceData.myTextureSizeAndTargetSize.w = resolution.y;
				instanceData.myPositionAndRotation.x = currentSprite.myPosition.x;
				instanceData.myPositionAndRotation.y = currentSprite.myPosition.y;
				instanceData.myPositionAndRotation.z = currentSprite.myRotation;
				instanceData.myPositionAndRotation.w = 1.f;
				instanceData.myColor = currentSprite.myColor;

				instanceData.mySizeAndPivot.x = currentSprite.myScale.x;
				instanceData.mySizeAndPivot.y = currentSprite.myScale.y;
				instanceData.mySizeAndPivot.z = currentSprite.myPivot.x;
				instanceData.mySizeAndPivot.w = currentSprite.myPivot.y;
				instanceData.myCrop.x = currentSprite.myCrop.x;
				instanceData.myCrop.y = currentSprite.myCrop.y;
				instanceData.myCrop.z = currentSprite.myCrop.z;
				instanceData.myCrop.w = currentSprite.myCrop.w;

				instanceCBuffer.SetData(instanceData);
				instanceCBuffer.BindVS(1);

				myGlowAndPadding[0] = currentSprite.myGlow;
				
				spriteEffectCBuffer.SetData(myGlowAndPadding);
				spriteEffectCBuffer.BindPS(0);

				myContext->DrawIndexed(iBuffer.GetIndexCount(), 0, 0);
			}

			CDirect3D11::GetAPI()->DisableBlending();
			CDirect3D11::GetAPI()->SetDefaultDepth();
		}
	}
}