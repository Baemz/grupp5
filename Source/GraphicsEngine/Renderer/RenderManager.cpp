#include "stdafx.h"
#include "RenderManager.h"
#include "Framework\Framework.h"
#include "../DirectXFramework/API/DX11FullscreenTexture.h"
#include "../DirectXFramework/API/DX11GBuffer.h"

namespace hse { namespace gfx {

	SRenderOptions CRenderManager::myRenderOptions = SRenderOptions();

	CRenderManager::CRenderManager()
		: myIntermediateTexture(nullptr)
		, myIntermediateTexture2(nullptr)
		, myHalfSizeTexture(nullptr)
		, myHalfSizeTexture2(nullptr)
		, myQuaterSizeTexture(nullptr)
		, myQuaterSizeTexture2(nullptr)
		, myEighthSizeTexture(nullptr)
		, myEighthSizeTexture2(nullptr)
		, myGBuffer(nullptr)
		, myAfterToneMapTexture(nullptr)
		, myAfterToneMapTexture2(nullptr)
		, myTonemapTextures(nullptr)
		, mySixteenthSizeTexture(nullptr)
		, mySixteenthSizeTexture2(nullptr)
		, mySSAOTexture(nullptr)
		, mySSAOBlurredTexture(nullptr)
		, mySSAADownsampleTexture(nullptr)
		, myShouldReinit(false)
	{
	}

	CRenderManager::~CRenderManager()
	{
		hse_delete(myIntermediateTexture);
		hse_delete(myIntermediateTexture2);
		hse_delete(myHalfSizeTexture);
		hse_delete(myHalfSizeTexture2);
		hse_delete(myQuaterSizeTexture);
		hse_delete(myQuaterSizeTexture2);
		hse_delete(myEighthSizeTexture);
		hse_delete(myEighthSizeTexture2);
		hse_delete(myGBuffer);
		hse_delete(myAfterToneMapTexture);
		hse_delete(myAfterToneMapTexture2);
		hse_delete(myTonemapTextures);
		hse_delete(mySixteenthSizeTexture);
		hse_delete(mySixteenthSizeTexture2);
		hse_delete(mySSAOTexture);
		hse_delete(mySSAOBlurredTexture);
		hse_delete(myShadowBuffer);
		hse_delete(mySSAADownsampleTexture);
	}

	void CRenderManager::SetRenderOptions(const SRenderOptions& aRenderOptions)
	{
		myRenderOptions = aRenderOptions;
	}

	void CRenderManager::ReInit(CFramework* aFramework, const CU::Vector2f& aResolution)
	{
		DestroyContent();
		Init(aFramework, aResolution);
	}

	void CRenderManager::DestroyContent()
	{
		hse_delete(myIntermediateTexture);
		hse_delete(myIntermediateTexture2);
		hse_delete(myHalfSizeTexture);
		hse_delete(myHalfSizeTexture2);
		hse_delete(myQuaterSizeTexture);
		hse_delete(myQuaterSizeTexture2);
		hse_delete(myEighthSizeTexture);
		hse_delete(myEighthSizeTexture2);
		hse_delete(myGBuffer);
		hse_delete(myAfterToneMapTexture);
		hse_delete(myAfterToneMapTexture2);
		hse_delete(myTonemapTextures);
		hse_delete(mySixteenthSizeTexture);
		hse_delete(mySixteenthSizeTexture2);
		hse_delete(mySSAOTexture);
		hse_delete(mySSAOBlurredTexture);
		hse_delete(myShadowBuffer);
		hse_delete(mySSAADownsampleTexture);
		myFramework = nullptr;

		myForwardRenderer.Destroy();
		myDeferredRenderer.Destroy();
		myPostFXRenderer.Destroy();
		myDebugRenderer.Destroy();
	}

	void CRenderManager::Init(CFramework* aFramework, const CU::Vector2f& aResolution)
	{
		myResolution = aResolution;
		myLastTarget = nullptr;
		myNewTarget = nullptr;

		myFramework = aFramework;
		myForwardRenderer.Init();
		myDeferredRenderer.Init();
		myDebugRenderer.Init();
		myRenderer2D.Init();
		mySpriteRenderer3D.Init();
		myParticleRenderer.Init();
		myPostFXRenderer.Init();
		myTextRenderer.Init();
		mySkyboxRenderer.Init();


		myIntermediateTexture = hse_new(CDX11FullscreenTexture());
		myIntermediateTexture2 = hse_new(CDX11FullscreenTexture());
		myHalfSizeTexture = hse_new(CDX11FullscreenTexture());
		myHalfSizeTexture2 = hse_new(CDX11FullscreenTexture());
		myQuaterSizeTexture = hse_new(CDX11FullscreenTexture());
		myQuaterSizeTexture2 = hse_new(CDX11FullscreenTexture());
		myEighthSizeTexture = hse_new(CDX11FullscreenTexture());
		myEighthSizeTexture2 = hse_new(CDX11FullscreenTexture());
		myAfterToneMapTexture = hse_new(CDX11FullscreenTexture());
		myAfterToneMapTexture2 = hse_new(CDX11FullscreenTexture());
		mySixteenthSizeTexture = hse_new(CDX11FullscreenTexture());
		mySixteenthSizeTexture2 = hse_new(CDX11FullscreenTexture());
		mySSAOTexture = hse_new(CDX11FullscreenTexture());
		mySSAOBlurredTexture = hse_new(CDX11FullscreenTexture());
		mySSAADownsampleTexture = hse_new(CDX11FullscreenTexture());

		myShadowBuffer = hse_new(CDX11FullscreenTexture());
		myShadowBuffer->Init(myResolution, DXGI_FORMAT_R32_FLOAT, true, true);


		DXGI_FORMAT format = DXGI_FORMAT_R16G16B16A16_FLOAT;
		mySSAADownsampleTexture->Init(myResolution , format, false);

		CU::Vector2f resolution;
		if (myRenderOptions.myAAType == EAntiAliasingType::SSAA)
		{
			resolution =  (myResolution * 2.f);
		}
		else
		{
			resolution = myResolution;
		}

		myGBuffer = hse_new(CDX11GBuffer());
		myGBuffer->Init(resolution);

		myIntermediateTexture->Init(resolution, format, true);
		myIntermediateTexture2->Init(resolution, format, true);
		mySSAOTexture->Init(resolution, DXGI_FORMAT_R32_FLOAT, false);
		mySSAOBlurredTexture->Init(resolution, DXGI_FORMAT_R32_FLOAT, false);
		myAfterToneMapTexture->Init(resolution, DXGI_FORMAT_R8G8B8A8_UNORM, false);
		myAfterToneMapTexture2->Init(resolution, DXGI_FORMAT_R8G8B8A8_UNORM, false);
		myHalfSizeTexture->Init((resolution * 0.5f), format, false);
		myHalfSizeTexture2->Init((resolution * 0.5f), format, false);
		myQuaterSizeTexture->Init((resolution * 0.25f), format, false);
		myQuaterSizeTexture2->Init((resolution * 0.25f), format, false);
		myEighthSizeTexture->Init((resolution * (1 / 16.f)), format, false);
		myEighthSizeTexture2->Init((resolution * (1 / 16.f)), format, false);
		mySixteenthSizeTexture->Init((resolution * (1 / 32.f)), DXGI_FORMAT_R8G8B8A8_UNORM, false);
		mySixteenthSizeTexture2->Init((resolution * (1 / 32.f)), DXGI_FORMAT_R8G8B8A8_UNORM, false);

		CU::Vector2f half(GetClosestPow2(resolution * 0.5f));
		std::vector<CU::Vector2f> sizes;
		sizes.push_back(half);
		do
		{
			half.x *= 0.5f;
			CU::Clamp(half.x, 1.f, resolution.x);
			half.y *= 0.5f;
			CU::Clamp(half.y, 1.f, resolution.y);
			sizes.push_back(half);
		} while (half.x > 1.0f && half.y > 1.0f);
		myNumTonemapTextures = static_cast<unsigned int>(sizes.size());
		myTonemapTextures = hse_newArray(CDX11FullscreenTexture, myNumTonemapTextures);
		for (unsigned int i = 0; i < myNumTonemapTextures; ++i)
		{
			myTonemapTextures[i].Init(sizes[i], format, false);
		}
	}

	void CRenderManager::RenderFrame(SRenderData& aRenderData, const CU::Vector2f& aResolution)
	{
		if (myShouldReinit)
		{
			ReInit(myFramework, myResolution);
			myShouldReinit = false;
		}

#ifdef _RETAIL
		CU::Vector4f clearColor = { 0.f, 0.f, 0.f, 1.f };
#else
		CU::Vector4f clearColor = { 0.f, 1.f, 0.f, 1.f };
#endif
		const bool hasDeferredData(aRenderData.myListToRender.Size() > 0);
		const bool hasForwardData(aRenderData.myTransparentListToRender.Size() > 0);
		const bool hasParticles(aRenderData.myParticleEmittorsToRender.Size() > 0);
		const bool has3DSprites(aRenderData.my3DSpritesToRender.Size() > 0);
		const bool hasRenderData(hasDeferredData || hasForwardData || has3DSprites);

		// Render scene
		myGBuffer->ClearBuffer();
		myGBuffer->SetAsRenderTarget();
		if (hasDeferredData)
		{
#ifndef _RETAIL
			if (CDirect3D11::GetAPI()->GetShouldRenderWireFrame())
			{
				CDirect3D11::GetAPI()->SetRasterStateWireframe(true);
				myDeferredRenderer.CollectData(aRenderData);
				CDirect3D11::GetAPI()->SetRasterStateWireframe(false);
			}
			else
#endif
			{
				myDeferredRenderer.CollectData(aRenderData);
			}
		}

		myShadowBuffer->ClearTexture({ 0.f, 0.f, 0.f, 0.f });
		myShadowBuffer->SetAsActiveTarget();
		myDeferredRenderer.DoShadows(aRenderData);

		// SSAO
		CU::Vector4f clearColorSSAO = { 1.f, 1.f, 1.f, 1.f };
		if (myRenderOptions.myUseSSAO)
		{
			mySSAOTexture->ClearTexture(clearColorSSAO);
			mySSAOTexture->SetAsActiveTarget();
			myGBuffer->BindSSAOResources();
			if (hasDeferredData)
			{
				myPostFXRenderer.Render(EFXType::SSAO, { nullptr, nullptr });
			}
			mySSAOBlurredTexture->ClearTexture(clearColorSSAO);
			mySSAOBlurredTexture->SetAsActiveTarget();
			if (hasDeferredData)
			{
				myPostFXRenderer.Render(EFXType::SSAOBlur, { mySSAOTexture, nullptr });
			}
		}
		else
		{
			mySSAOBlurredTexture->ClearTexture(clearColorSSAO);
		}
		
		// Light-pass
		CU::Vector4f clearColorLightPass = { 0.f, 0.f, 0.f, 1.f };
		myIntermediateTexture->ClearTexture(clearColorLightPass);
		myIntermediateTexture->SetAsActiveTarget();
		myGBuffer->BindAsResource();
		mySSAOBlurredTexture->SetAsResourceOnSlot(6);
		myShadowBuffer->SetDepthAsResourceOnSlot(8);
		if (hasDeferredData)
		{
			myDeferredRenderer.LightPass(aRenderData);
		}
		
		// Linear fog
		if (myRenderOptions.myUseLinearFog)
		{
			myIntermediateTexture2->ClearTexture(clearColor);
			myIntermediateTexture2->SetAsActiveTarget();
			myIntermediateTexture->SetAsResourceOnSlot(0);
			if (hasDeferredData)
			{
				myDeferredRenderer.DoLinearFog();
				myDeferredRenderer.DoEmissive(aRenderData);
			}
			myIntermediateTexture2->SetAsActiveTarget(myGBuffer->GetDepth());
			myLastTarget = myIntermediateTexture2;
			myNewTarget = myIntermediateTexture;
		}
		else
		{
			if (hasDeferredData)
			{
				myDeferredRenderer.DoEmissive(aRenderData);
			}
			myIntermediateTexture->SetAsActiveTarget(myGBuffer->GetDepth());
			myLastTarget = myIntermediateTexture;
			myNewTarget = myIntermediateTexture2;
		}

		// Skybox
		mySkyboxRenderer.RenderFrame(aRenderData);

		// Forward Renderer
		if (hasForwardData)
		{
			myForwardRenderer.Render(aRenderData);
		}

		//Particles
		if (hasParticles)
		{
			myParticleRenderer.Render(aRenderData.myCameraInstance, aRenderData.myParticleEmittorsToRender, aRenderData.myStreakEmittorsToRender);
		}

		// 3D Sprites
		if (has3DSprites)
		{
			mySpriteRenderer3D.Render(aRenderData.myCameraInstance, aRenderData.my3DSpritesToRender);
		}
		
		// Post processing
		if (hasRenderData)
		{
			DoBloom();
			myAfterToneMapTexture->ClearTexture(clearColor);
			DoTonemapping();
			DoColorGrading();
			if (myRenderOptions.myAAType == EAntiAliasingType::FXAA)
			{
				DoFXAA();
			}
			else if (myRenderOptions.myAAType == EAntiAliasingType::SSAA)
			{
				mySSAADownsampleTexture->SetAsActiveTarget();
				myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
				myLastTarget = mySSAADownsampleTexture;
			}
		}

		// 2D Sprites
#ifndef _RETAIL
		if (CDirect3D11::GetAPI()->GetShouldRenderWireFrame())
		{
			CDirect3D11::GetAPI()->SetRasterStateWireframe(true);
			myDebugRenderer.Render(aRenderData);
			CDirect3D11::GetAPI()->SetRasterStateWireframe(false);
		}
		else
#endif
		{
			myDebugRenderer.Render(aRenderData);
		}
		myRenderer2D.Render(aRenderData.mySpritesToRender);

		// Fullscreen Effects
		/*for (unsigned int aEffectIndex = 0; aEffectIndex < aRenderData.myShaderEffectsToRender.Size(); ++aEffectIndex)
		{
				CDX11FullscreenTexture* midTarget = myLastTarget;
				myNewTarget->SetAsActiveTarget();
				myPostFXRenderer.RenderEffect(aRenderData.myShaderEffectsToRender[aEffectIndex], myLastTarget);
				myLastTarget = myNewTarget;
				myNewTarget = midTarget;

				myNewTarget->SetAsActiveTarget();
				myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
				midTarget = myLastTarget;
				myLastTarget = myNewTarget;
				myNewTarget = midTarget;
		}*/

		// Text
		myTextRenderer.Render(aRenderData.myTextsToRender, aResolution);

		// Copy to backbuffer
		if (myLastTarget != nullptr)
		{
			myFramework->ActivateScreenTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
		}
	}

	void CRenderManager::ToggleFXAA()
	{
		switch (myRenderOptions.myAAType)
		{
		case EAntiAliasingType::NONE:
			myRenderOptions.myAAType = EAntiAliasingType::FXAA;
			break;
		case EAntiAliasingType::FXAA:
			myRenderOptions.myAAType = EAntiAliasingType::SSAA;
			break;
		case EAntiAliasingType::SSAA:
			myRenderOptions.myAAType = EAntiAliasingType::NONE;
			break;
		}
		myShouldReinit = true;
	}

	void CRenderManager::DoBloom()
	{
		if (myRenderOptions.myUseBloom == false)
		{
			myNewTarget->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
			myLastTarget = myNewTarget;
			myNewTarget = nullptr;
			return;
		}

		myHalfSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Luminance, { myLastTarget, nullptr });

		// (1/4)
		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Copy, { myHalfSizeTexture, nullptr });

		myQuaterSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture, nullptr });

		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture2, nullptr });

		myQuaterSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture, nullptr });

		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture2, nullptr });

		// (1/8)

		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Copy, { myQuaterSizeTexture, nullptr });
		
		myEighthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical8, { myEighthSizeTexture, nullptr });
		
		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal8, { myEighthSizeTexture2, nullptr });
		
		myEighthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical8, { myEighthSizeTexture, nullptr });
		
		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal8, { myEighthSizeTexture2, nullptr });

		// (1/16)

		mySixteenthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Copy, { myEighthSizeTexture, nullptr });
		
		mySixteenthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical8, { mySixteenthSizeTexture, nullptr });
		
		mySixteenthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal8, { mySixteenthSizeTexture2, nullptr });
		
		mySixteenthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical8, { mySixteenthSizeTexture, nullptr });
		
		mySixteenthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal8, { mySixteenthSizeTexture2, nullptr });
		
		myEighthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Add, { mySixteenthSizeTexture, myEighthSizeTexture });

		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical8, { myEighthSizeTexture2, nullptr });

		myEighthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal8, { myEighthSizeTexture, nullptr });

		myEighthSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical8, { myEighthSizeTexture2, nullptr });

		myEighthSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal8, { myEighthSizeTexture, nullptr });

		// Up-scale
		myQuaterSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Add, { myEighthSizeTexture2, myQuaterSizeTexture });

		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture2, nullptr });

		myQuaterSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture, nullptr }); 
		
		myQuaterSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurVertical4, { myQuaterSizeTexture2, nullptr });

		myQuaterSizeTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::GaussianBlurHorizontal4, { myQuaterSizeTexture, nullptr });

		myHalfSizeTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Copy, { myQuaterSizeTexture2, nullptr });

		myNewTarget->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::BloomAdd, { myLastTarget, myHalfSizeTexture });

		myLastTarget = myNewTarget;
		myNewTarget = nullptr;
	}
	void CRenderManager::DoFXAA()
	{
		myAfterToneMapTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::FXAA, { myAfterToneMapTexture2, nullptr });
		myLastTarget = myAfterToneMapTexture;
		myNewTarget = myAfterToneMapTexture2;
	}
	void CRenderManager::DoTonemapping()
	{
		if (myRenderOptions.myUseHDR == false)
		{
			myAfterToneMapTexture->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myLastTarget, nullptr });
			myLastTarget = myAfterToneMapTexture;
			myNewTarget = myAfterToneMapTexture2;
			return;
		}

		for (unsigned int i = 0; i < myNumTonemapTextures; ++i)
		{
			myTonemapTextures[i].SetAsActiveTarget();
			if (i == 0)
			{
				myPostFXRenderer.Render(EFXType::TonemapLum, { myLastTarget, nullptr });
			}
			else
			{
				myPostFXRenderer.Render(EFXType::Copy, { &myTonemapTextures[i - 1], nullptr });
			}
		}

		myAfterToneMapTexture->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::Tonemap, { myLastTarget, &myTonemapTextures[myNumTonemapTextures - 1] });
		myLastTarget = myAfterToneMapTexture;
		myNewTarget = myAfterToneMapTexture2;
	}

	void CRenderManager::DoColorGrading()
	{
		if (myRenderOptions.myUseColorGrading == false)
		{
			myAfterToneMapTexture2->SetAsActiveTarget();
			myPostFXRenderer.Render(EFXType::Copy, { myAfterToneMapTexture, nullptr });
			myLastTarget = myAfterToneMapTexture2;
			myNewTarget = myAfterToneMapTexture;
			return;
		}
		myAfterToneMapTexture2->SetAsActiveTarget();
		myPostFXRenderer.Render(EFXType::ColorGrading, { myAfterToneMapTexture, nullptr });
		myLastTarget = myAfterToneMapTexture2;
		myNewTarget = myAfterToneMapTexture;
	}

	const CU::Vector2f CRenderManager::GetClosestPow2(const CU::Vector2f& aVector)
	{
		unsigned int x = static_cast<unsigned int>(aVector.x);
		unsigned int y = static_cast<unsigned int>(aVector.y);

		x--;
		x |= x >> 1;
		x |= x >> 2;
		x |= x >> 4;
		x |= x >> 8;
		x |= x >> 16;
		x++;

		y--;
		y |= y >> 1;
		y |= y >> 2;
		y |= y >> 4;
		y |= y >> 8;
		y |= y >> 16;
		y++;

		/*y |= (y >> 1);
		y |= (y >> 2);
		y |= (y >> 4);
		y |= (y >> 8);
		y |= (y >> 16);*/

		return CU::Vector2f(static_cast<float>(x), static_cast<float>(y));
	}
}}