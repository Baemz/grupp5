#pragma once

struct ID3D11DeviceContext;
namespace hse { namespace gfx {

	class CCameraInstance;
	class CParticleEmitterInstance;
	class CStreakEmitterInstance;
	class CParticleRenderer
	{
	public:
		CParticleRenderer();
		~CParticleRenderer();

		void Init();
		void Render(CCameraInstance& aCamera, CU::GrowingArray<CParticleEmitterInstance, unsigned int>& aParticleListToRender,
			CU::GrowingArray<CStreakEmitterInstance, unsigned int>& aStreakListToRender);

	private:
		void RenderParticles(CCameraInstance& aCamera, CU::GrowingArray<CParticleEmitterInstance, unsigned int>& aListToRender);
		void RenderStreaks(CCameraInstance& aCamera, CU::GrowingArray<CStreakEmitterInstance, unsigned int>& aListToRender);

		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
		};
		unsigned int myCBufferID;
		ID3D11DeviceContext* myContext;
	};
}}
