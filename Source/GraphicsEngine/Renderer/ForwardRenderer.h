#pragma once
#include "../CommonUtilities/Matrix.h"

struct ID3D11DeviceContext;

namespace hse { namespace gfx {
	
	class CCameraInstance;
	class CModelInstance;
	class CDirectionalLight;
	class CEnvironmentalLight;
	struct SRenderData;

	

	class CForwardRenderer
	{
	public:
		CForwardRenderer();
		~CForwardRenderer();

		void Destroy();

		void Init();
		void Render(SRenderData& aRenderData);

	private:
		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
		};
		// FLYTTA UR RENDERER PLS --HAMPUS DO IT--
		CDirectionalLight* myDirLight;

		unsigned int myCBufferID;
		unsigned int myPointLightCBufferID;
		unsigned int mySpotLightCBufferID;
		ID3D11DeviceContext* myContext;
	};
}}
