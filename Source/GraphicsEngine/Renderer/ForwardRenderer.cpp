#include "stdafx.h"
#include "ForwardRenderer.h"
#include "../Model/ModelInstance.h"
#include "../Model/Model.h"
#include "../ResourceManager/ResourceManager.h"
#include "../Camera/CameraInstance.h"
#include "../Camera/Camera.h"
#include "Lights/DirectionalLight.h"
#include "Lights/EnvironmentalLight.h"
#include "Model/Loading/ModelFactory.h"
#include "Material.h"

using namespace hse::gfx;

CForwardRenderer::CForwardRenderer()
	: myContext(nullptr)
	, myPointLightCBufferID(UINT_MAX)
	, mySpotLightCBufferID(UINT_MAX)
{
}


CForwardRenderer::~CForwardRenderer()
{
	// FLYTTA UR RENDERER PLS --HAMPUS DO IT--
	hse_delete(myDirLight);
}

void CForwardRenderer::Destroy()
{
	hse_delete(myDirLight);
}

void CForwardRenderer::Init()
{
	myContext = CDirect3D11::GetAPI()->GetContext();
	myCBufferID = CResourceManager::Get()->GetConstantBufferID("ForwardRenderer");
	myPointLightCBufferID = CResourceManager::Get()->GetConstantBufferID("PLightForwardRenderer");
	mySpotLightCBufferID = CResourceManager::Get()->GetConstantBufferID("SLightForwardRenderer");

	// FLYTTA UR RENDERER PLS --HAMPUS DO IT--
	myDirLight = hse_new(CDirectionalLight());
}

void CForwardRenderer::Render(SRenderData& aRenderData)
{
	if (myContext == nullptr)
	{
		return;
	}
	if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
	{
		return;
	}

	CDirect3D11::GetAPI()->EnableReadOnlyDepth();
	CDirect3D11::GetAPI()->EnableAlphablend();
	myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	CResourceManager* resourceManager = CResourceManager::Get();
	CModelFactory* modelFactory = CModelFactory::Get();
	CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
	CDX11ConstantBuffer& cameraCBuffer(resourceManager->GetConstantBufferWithID(camera.myCBufferID));
	cameraCBuffer.SetData(camera.myCBufferData);
	cameraCBuffer.BindVS(0);

	SInstanceBufferData instanceData;
	CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));

	if (aRenderData.myEnvLight.myTextureID != UINT_MAX)
	{
		CDX11Texture& envtexture = resourceManager->GetTextureWithID(aRenderData.myEnvLight.myTextureID);
		envtexture.Bind();
		myDirLight->SetEnvLightMipCount(envtexture.GetMipCount());
	}
	myDirLight->SetDataAndBind();

	for (unsigned int i = 0; i < aRenderData.myTransparentListToRender.Size(); ++i)
	{
		CModelInstance& instance = aRenderData.myTransparentListToRender[i];
		if (instance.myModelID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
			continue;
		}
		if (instance.myMaterialID == UINT_MAX)
		{
			continue;
		}
		CModel& model(resourceManager->GetModelWithID(instance.myModelID));


		if (model.myVertexBufferID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName);
			continue;
		}

		CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
		CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));

		CMaterial& mat(modelFactory->GetMaterialWithID(instance.myMaterialID)); 
		if (mat.IsLoading())
		{
			continue;
		}
		mat.Bind();

		CU::Matrix44f orientation(instance.myOrientation);
		orientation.SetScale(instance.myScale);
		instanceData.myToWorld = orientation;
		instanceCBuffer.SetData(instanceData);
		instanceCBuffer.BindVS(1);

		vb.Bind();
		ib.Bind();

		myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
	}
	CDirect3D11::GetAPI()->SetDefaultDepth();
	CDirect3D11::GetAPI()->DisableBlending();
}
