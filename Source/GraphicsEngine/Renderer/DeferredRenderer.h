#pragma once
#include "..\CommonUtilities\Matrix.h"
#include "../CommonUtilities/Timer.h"

struct ID3D11DeviceContext;
namespace hse { namespace gfx {

	class CCameraInstance;
	class CModelInstance;
	class CDirectionalLight;
	class CEnvironmentalLight;
	class CDX11Shader;
	class CDX11VertexBuffer;
	class CDX11IndexBuffer;
	class CDirectionalLight;
	class CDeferredRenderer
	{
	public:
		CDeferredRenderer();
		~CDeferredRenderer();

		void Destroy();


		void Init();
		void CollectData(const SRenderData& aRenderData);
		void LightPass(const SRenderData& aRenderData);
		void DoEmissive(const SRenderData& aRenderData);
		void DoLinearFog();
		void DoShadows(const SRenderData& aRenderData);

	private:

		struct SInstanceBufferData
		{
			CU::Matrix44f myToWorld;
			float useFog = 1;
			float pad[3];
		};

		struct SShadowBufferData
		{
			CU::Matrix44f depthMVP;
		}; 
		struct SShadowBufferMatrices
		{
			CU::Matrix44f depthView;
			CU::Matrix44f depthProj;
		};

	private:
		CU::Matrix44f myShadowProjection;
		CU::Matrix44f myDirLightView;
		CDirectionalLight* dirlight;
		unsigned int myCBufferID;
		unsigned int myShadowCBufferID;
		unsigned int myShadowCBuffer2ID;
		unsigned int myPointLightCBufferID;
		unsigned int mySpotLightCBufferID;
		ID3D11DeviceContext* myContext;
		CDX11Shader* myDirLightShader;
		CDX11Shader* myEnvLightShader;
		CDX11Shader* myEmissiveLightShader;
		CDX11Shader* myVertexShader; 
		CDX11Shader* myLinearFogShader;
		CDX11Shader* myShadowPSShader;
		CDX11Shader* myShadowVSShader;
		CDX11VertexBuffer* myVBuffer;
		CDX11IndexBuffer* myIBuffer;

		unsigned int myTimerCBufferID;
		CU::Timer myTimer;
		float myElapsedTime[4];
	};
}}
