#include "stdafx.h"
#include "SkyboxRenderer.h"
#include "Utilities\RenderStructs.h"
#include "Model\Model.h"
#include "ResourceManager\ResourceManager.h"
#include "Camera\Camera.h"

namespace hse { namespace gfx {
	CSkyboxRenderer::CSkyboxRenderer()
		: myContext(nullptr)
	{
	}


	CSkyboxRenderer::~CSkyboxRenderer()
	{
	}

	void CSkyboxRenderer::Init()
	{
		myContext = CDirect3D11::GetAPI()->GetContext();
		myCBufferID = CResourceManager::Get()->GetConstantBufferID("SkyboxRenderer");
	}

	void CSkyboxRenderer::RenderFrame(SRenderData& aRenderData)
	{
		if ((myContext == nullptr) || (aRenderData.myListToRender.Size() == 0))
		{
			return;
		}
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		CResourceManager* resourceManager = CResourceManager::Get();

		if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
		camera.UpdateBuffers(aRenderData.myCameraInstance.myMatrixOrientation);

		
		CDX11ConstantBuffer& cameraCBuffer(resourceManager->GetConstantBufferWithID(camera.myCBufferID));
		cameraCBuffer.SetData(camera.myCBufferData);
		cameraCBuffer.BindVS(0);

		SInstanceBufferData instanceData;
		CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));

		// The first index of models is reserved for skybox
		CModelInstance& instance(aRenderData.myListToRender[0]);
		if (instance.IsLoaded() == false)
		{
			return;
		}
		if (instance.myModelID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, skybox model not found.", instance.myModelName.c_str());
			return;
		}
		CModel& model(resourceManager->GetModelWithID(instance.myModelID));

		if (model.myVertexBufferID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName);
			return;
		}
		if (model.myPixelShaderID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, PShader not found.", instance.myModelName);
			return;
		}
		if (model.myVertexShaderID == UINT_MAX)
		{
			ENGINE_LOG("[%s] could not render, VShader not found.", instance.myModelName);
			return;
		}

		CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
		CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));
		CDX11Shader& vertexShader(resourceManager->GetShaderWithID(model.myVertexShaderID));
		CDX11Shader& pixelShader(resourceManager->GetShaderWithID(model.myPixelShaderID));
		if (pixelShader.IsLoading())
		{
			return;
		}
		if (vertexShader.IsLoading())
		{
			return;
		}

		instanceData.myToWorld = instance.myOrientation;
		instanceCBuffer.SetData(instanceData);
		instanceCBuffer.BindVS(1);

		for (unsigned int textureSlot = 0; textureSlot < 6; ++textureSlot)
		{
			if (model.myTextureID[textureSlot] != UINT_MAX)
			{
				CDX11Texture& texture(resourceManager->GetTextureWithID(model.myTextureID[textureSlot]));
				texture.Bind();
			}
		}

		vertexShader.Bind();
		pixelShader.Bind();

		vb.Bind();
		ib.Bind();
		myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		CDirect3D11::GetAPI()->SetDefaultDepth();

	}
}}