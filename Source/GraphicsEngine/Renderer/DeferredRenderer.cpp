#include "stdafx.h"
#include "DeferredRenderer.h"
#include "Camera\Camera.h"
#include "Model\ModelInstance.h"
#include "Model\Model.h"
#include "Lights\DirectionalLight.h"

#include "DirectXFramework\API\DX11Shader.h"
#include "Lights\LightFactory.h"
#include "Utilities\LightStructs.h"
#include "Lights\PointLight.h"
#include "Lights\SpotLight.h"
#include "DirectXFramework\CDirectXMathHelper.h"
#include "Material.h"

namespace hse { namespace gfx {

	CDeferredRenderer::CDeferredRenderer()
	{
		myElapsedTime[0] = 0.0f;
	}


	CDeferredRenderer::~CDeferredRenderer()
	{
		hse_delete(myVertexShader);
		hse_delete(myEnvLightShader);
		hse_delete(myDirLightShader);
		hse_delete(myVBuffer);
		hse_delete(myIBuffer);
		hse_delete(dirlight);
		hse_delete(myEmissiveLightShader);
		hse_delete(myLinearFogShader);
		hse_delete(myShadowPSShader);
		hse_delete(myShadowVSShader);
	}

	void CDeferredRenderer::Destroy()
	{
		hse_delete(myVertexShader);
		hse_delete(myEnvLightShader);
		hse_delete(myDirLightShader);
		hse_delete(myVBuffer);
		hse_delete(myIBuffer);
		hse_delete(dirlight);
		hse_delete(myEmissiveLightShader);
		hse_delete(myLinearFogShader);
		hse_delete(myShadowPSShader);
		hse_delete(myShadowVSShader);
	}

	void CDeferredRenderer::Init()
	{
		myContext = CDirect3D11::GetAPI()->GetContext();
		myCBufferID = CResourceManager::Get()->GetConstantBufferID("DeferredRenderer");
		myPointLightCBufferID = CResourceManager::Get()->GetConstantBufferID("DeferredRendererPL");
		mySpotLightCBufferID = CResourceManager::Get()->GetConstantBufferID("DeferredRendererSL");
		myShadowCBufferID = CResourceManager::Get()->GetConstantBufferID("DeferredRendererShadow");
		myShadowCBuffer2ID = CResourceManager::Get()->GetConstantBufferID("DeferredRendererShadow2");
		myTimerCBufferID = CResourceManager::Get()->GetConstantBufferID("DeferredTimeBuffer");

		dirlight = hse_new(CDirectionalLight());
		myVertexShader = hse_new(CDX11Shader());
		myVBuffer = hse_new(CDX11VertexBuffer());
		myIBuffer = hse_new(CDX11IndexBuffer());
		myVBuffer->CreateQuad();
		myIBuffer->CreateQuadIndex();

		{
			const D3D_SHADER_MACRO define[] =
			{
				{ "FX_VERTEX", "1" },
				{ nullptr, nullptr }
			};
			myVertexShader->InitVertex("Data/Shaders/PostFX.hlsl", "VSMain", define);
		}

		myDirLightShader = hse_new(CDX11Shader());
		myEnvLightShader = hse_new(CDX11Shader());
		myEmissiveLightShader = hse_new(CDX11Shader());
		myLinearFogShader = hse_new(CDX11Shader());
		myShadowPSShader = hse_new(CDX11Shader());
		myShadowVSShader = hse_new(CDX11Shader());
		myDirLightShader->InitPixel("Data/Shaders/DirectionalLightPS.hlsl", "PSMain");
		myEnvLightShader->InitPixel("Data/Shaders/EnvironmentLightPS.hlsl", "PSMain");
		myEmissiveLightShader->InitPixel("Data/Shaders/EmissiveLightPS.hlsl", "PSMain");
		myLinearFogShader->InitPixel("Data/Shaders/FogPS.hlsl", "PSMain");
		myShadowPSShader->InitPixel("Data/Shaders/ShadowPass.hlsl", "PSMain");
		myShadowVSShader->InitVertex("Data/Shaders/ShadowPass.hlsl", "VSMain");

		myShadowProjection = CreateOrthogonalMatrixLH(50.f, 50.f, 0.001f, 5000.f);
		//myShadowProjection = CreateProjectionMatrixLH(75.f, 16 / 9.f, 0.01f, 5000.f);
	}

	void CDeferredRenderer::LightPass(const SRenderData& aRenderData)
	{
		if (myVertexShader->IsLoading())
		{
			return;
		}
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		CDirect3D11::GetAPI()->EnableAdditiveblend();
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myVBuffer->Bind();
		myIBuffer->Bind();
		myVertexShader->Bind();

		CResourceManager* resourceManager = CResourceManager::Get();
		CLightFactory* lightFactory = CLightFactory::Get();

		// ENVIRONMENTAL-LIGHT
		if (aRenderData.myEnvLight.myTextureID != UINT_MAX)
		{
			CDX11Texture& envtexture = resourceManager->GetTextureWithID(aRenderData.myEnvLight.myTextureID);
			dirlight->SetEnvLightMipCount(envtexture.GetMipCount());
			dirlight->SetDataAndBind();
			envtexture.Bind();
			myEnvLightShader->Bind();
			myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
		}

		// DIRECTIONAL LIGHT
		myDirLightShader->Bind();
		dirlight->SetDataAndBind();

		SShadowBufferMatrices depthMVP;
		CDX11ConstantBuffer& depthCBuffer(resourceManager->GetConstantBufferWithID(myShadowCBufferID));
		depthCBuffer.BindPS(2);

		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);

		// POINTLIGHTS
		for (unsigned int i = 0; i < aRenderData.myPointLightData.Size(); ++i)
		{
			CPointLightInstance instance(aRenderData.myPointLightData[i]);
			if (instance.myPointLightID == UINT_MAX)
			{
				continue;
			}
			CPointLight& pl(lightFactory->GetPointLightWithID(instance.myPointLightID));
			CDX11Shader& pShader(resourceManager->GetShaderWithID(pl.myPixelShader));

			SPointLightData plData;
			plData.myColor = pl.myColor;
			plData.myIntensity = instance.myIntensity;
			plData.myPosition = CU::Vector4f(instance.myPosition, 1.f);
			plData.myRange = pl.myRange;
			CDX11ConstantBuffer& cBuffer(resourceManager->GetConstantBufferWithID(myPointLightCBufferID));
			cBuffer.SetData(plData);
			cBuffer.BindPS(0);
			pShader.Bind();
			myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
		}

		// SPOTLIGHTS
		for (unsigned int i = 0; i < aRenderData.mySpotLightData.Size(); ++i)
		{
			const CSpotLightInstance& instance(aRenderData.mySpotLightData[i]);
			if (instance.mySpotLightID == UINT_MAX)
			{
				continue;
			}
			CSpotLight& sl(lightFactory->GetSpotLightWithID(instance.mySpotLightID));
			CDX11Shader& pShader(resourceManager->GetShaderWithID(sl.myPixelShader));

			SSpotLightData slData;
			slData.myColor = sl.myColor;
			slData.myIntensity = instance.myIntensity;
			slData.myPosition = CU::Vector4f(instance.myPosition, 1.f);
			slData.myRange = sl.myRange;
			slData.myAngle = sl.myAngle;
			slData.myDirection = instance.myDirection;
			CDX11ConstantBuffer& cBuffer(resourceManager->GetConstantBufferWithID(mySpotLightCBufferID));
			cBuffer.SetData(slData);
			cBuffer.BindPS(0);
			pShader.Bind();
			myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
		}

		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	void CDeferredRenderer::DoEmissive(const SRenderData& aRenderData)
	{
		aRenderData;

		if (myVertexShader->IsLoading())
		{
			return;
		}
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		CDirect3D11::GetAPI()->EnableAdditiveblend();
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myVBuffer->Bind();
		myIBuffer->Bind();
		myVertexShader->Bind();

		myEmissiveLightShader->Bind();
		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);

		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	void CDeferredRenderer::DoLinearFog()
	{
		CDirect3D11::GetAPI()->DisableBlending();
		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		myLinearFogShader->Bind();
		myContext->DrawIndexed(myIBuffer->GetIndexCount(), 0, 0);
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	void CDeferredRenderer::DoShadows(const SRenderData& aRenderData)
	{
		if (myContext == nullptr)
		{
			return;
		}
		if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		if (myShadowVSShader->IsLoading() || myShadowPSShader->IsLoading())
		{
			return;
		}

		CResourceManager* resourceManager = CResourceManager::Get();
		CDX11ConstantBuffer& cBuffer(resourceManager->GetConstantBufferWithID(myShadowCBufferID));

		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myShadowVSShader->Bind();
		myShadowPSShader->Bind();

		SShadowBufferData mvp;
		CU::Vector3f lightDir(dirlight->myLightData.myToLightDirection);
		myDirLightView = LookAtDX(lightDir, { 0.f, 0.f, 0.f }, { 0.f, 1.0f, 0.f });

		for (unsigned int i = 1; i < aRenderData.myListToRender.Size(); ++i)
		{
			const CModelInstance& instance = aRenderData.myListToRender[i];
			if (instance.myModelID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
				continue;
			}
			if (instance.myMaterialID == UINT_MAX)
			{
				continue;
			}
			CModel& model(resourceManager->GetModelWithID(instance.myModelID));

			if (model.myVertexBufferID == UINT_MAX || model.myIndexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName);
				continue;
			}

			CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
			CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));

			CU::Matrix44f orientation(instance.myOrientation);
			orientation.SetScale(instance.myScale);
			mvp.depthMVP = orientation * myDirLightView * myShadowProjection;
			cBuffer.SetData(mvp);
			cBuffer.BindVS(1);
			vb.Bind();
			ib.Bind();
			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}
	}

	void CDeferredRenderer::CollectData(const SRenderData& aRenderData)
	{
		if (myContext == nullptr)
		{
			return;
		}
		if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		if (aRenderData.myListToRender.Empty())
		{
			return;
		}

		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		CResourceManager* resourceManager = CResourceManager::Get();
		CModelFactory* modelFactory = CModelFactory::Get();

		// Camera ConstantBuffer
		CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
		camera.UpdateBuffers(aRenderData.myCameraInstance.myMatrixOrientation);
		CDX11ConstantBuffer& cameraCBuffer(resourceManager->GetConstantBufferWithID(camera.myCBufferID));
		cameraCBuffer.SetData(camera.myCBufferData);
		cameraCBuffer.BindVS(0);

		// Time ConstantBuffer
		CDX11ConstantBuffer& timeBuffer(resourceManager->GetConstantBufferWithID(myTimerCBufferID));
		STimeData tData = aRenderData.myTimeData;
		timeBuffer.SetData(tData);
		timeBuffer.BindPS(3);

		SInstanceBufferData instanceData;
		CDX11ConstantBuffer& instanceCBuffer(resourceManager->GetConstantBufferWithID(myCBufferID));

		CDirect3D11::GetAPI()->SetDefaultDepth();

		// Start at 1 because index [0] is reserved for skybox.
		for (unsigned int i = 1; i < aRenderData.myListToRender.Size(); ++i)
		{
			const CModelInstance& instance = aRenderData.myListToRender[i];
			if (instance.myModelID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, model not found.", instance.myModelName.c_str());
				continue;
			}
			if (instance.myMaterialID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, material not found.", instance.myModelName.c_str());
				continue;
			}
			CModel& model(resourceManager->GetModelWithID(instance.myModelID));


			if (model.myVertexBufferID == UINT_MAX || model.myIndexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[%s] could not render, VBuffer not found.", instance.myModelName);
				continue;
			}

			CDX11VertexBuffer& vb(resourceManager->GetVertexBufferWithID(model.myVertexBufferID));
			CDX11IndexBuffer& ib(resourceManager->GetIndexBufferWithID(model.myIndexBufferID));

			CMaterial& mat(modelFactory->GetMaterialWithID(instance.myMaterialID));
			if (mat.IsLoading())
			{
				continue;
			}
			mat.Bind();

			CU::Matrix44f orientation(instance.myOrientation);
			orientation.SetScale(instance.myScale);
			instanceData.myToWorld = orientation;

			if (instance.IsAffectedByFog())
			{
				instanceData.useFog = 1;
			}
			else
			{
				instanceData.useFog = 0;
			}

			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(1);


			vb.Bind();
			ib.Bind();
			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}

		cameraCBuffer.BindPS(0);
	}

}}