#include "stdafx.h"
#include "DebugRenderer.h"
#include "DirectXFramework\Direct3D11.h"
#include "DirectXFramework\API\DX11Shader.h"
#include "ResourceManager\ResourceManager.h"
#include "Model\Loading\ModelFactory.h"
#include "Model\Model.h"
#include "Camera\Camera.h"
#include "Material.h"


namespace hse { namespace gfx {
	CDebugRenderer::CDebugRenderer()
		: myContext(nullptr)
		, myLineVBuffer(nullptr)
		, myCubeVBuffer(nullptr)
		, myLineVertexShader(nullptr)
		, myLinePixelShader(nullptr)
		, myLineGShader(nullptr)
		, myCubeGShader(nullptr)
		{
		}

	CDebugRenderer::~CDebugRenderer()
	{
		Destroy();
	}

	void CDebugRenderer::Destroy()
	{
		hse_delete(myLineVBuffer);
		hse_delete(myCubeVBuffer);
		hse_delete(myLineVertexShader);
		hse_delete(myLinePixelShader);
		hse_delete(myLineGShader);
		hse_delete(myCubeGShader);
		hse_delete(my2DLineVertexShader);
		hse_delete(my2DLineVBuffer);
		hse_delete(myRect3DVBuffer);
		hse_delete(my2DLineGShader);
		hse_delete(my3DRectGShader);
	}

	void CDebugRenderer::Init()
	{
		myContext = CDirect3D11::GetAPI()->GetContext();
		myLineVBuffer = hse_new(CDX11VertexBuffer());
		myCubeVBuffer = hse_new(CDX11VertexBuffer());
		my2DLineVBuffer = hse_new(CDX11VertexBuffer());
		myRect3DVBuffer = hse_new(CDX11VertexBuffer());
		myLineVertexShader = hse_new(CDX11Shader());
		my2DLineVertexShader = hse_new(CDX11Shader());
		myLinePixelShader = hse_new(CDX11Shader());
		myLineGShader = hse_new(CDX11Shader());
		myCubeGShader = hse_new(CDX11Shader());
		my2DLineGShader = hse_new(CDX11Shader());
		my3DRectGShader = hse_new(CDX11Shader());

		myLinePixelShader->InitPixel("Data/Shaders/DebugShapesPS.hlsl", "PSMain");
		myLineVertexShader->InitVertex("Data/Shaders/DebugShapesVS.hlsl", "VSMain");
		my2DLineVertexShader->InitVertex("Data/Shaders/DebugShapes2DVS.hlsl", "VSMain");

		myLineGShader->InitGeometry("Data/Shaders/DebugShapesLinesGS.hlsl", "GSMain");
		my3DRectGShader->InitGeometry("Data/Shaders/DebugShapesRectangle3DGS.hlsl", "GSMain");
		my2DLineGShader->InitGeometry("Data/Shaders/DebugShapes2DLinesGS.hlsl", "GSMain");
		myCubeGShader->InitGeometry("Data/Shaders/DebugShapesCubesGS.hlsl", "GSMain");

		my2DLineVBuffer->CreateDebugBuffer(MAX_DEBUG2DLINES , EBufferUsage::DYNAMIC);
		myLineVBuffer->CreateDebugBuffer(MAX_DEBUGLINES , EBufferUsage::DYNAMIC);
		myCubeVBuffer->CreateDebugBuffer(MAX_DEBUGCUBES, EBufferUsage::DYNAMIC);
		myRect3DVBuffer->CreateDebugBuffer(MAX_DEBUGRECTANGLE3D, EBufferUsage::DYNAMIC);
		
		myCBufferID = CResourceManager::Get()->GetConstantBufferID("DebugRenderer");
	}


	void CDebugRenderer::Render(SRenderData& aRenderData)
	{
		if (myContext == nullptr)
		{
			return;
		}
		if (aRenderData.myCameraInstance.myCameraID == UINT_MAX)
		{
			return;
		}
		if (myLinePixelShader->IsLoading())
		{
			return;
		}
		if (myLineVertexShader->IsLoading())
		{
			return;
		}
		if (myLineGShader->IsLoading())
		{
			return;
		}
		if (myCubeGShader->IsLoading())
		{
			return;
		}

		CDirect3D11::GetAPI()->EnableReadOnlyDepth();
		CDirect3D11::GetAPI()->DisableBlending();

		//2DLines
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
		if (aRenderData.myDebugData.myDebug2DLineData.Size() > MAX_DEBUG2DLINES)
		{
			ENGINE_LOG("WARNING! You are trying to render more 2D-Lines than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebug2DLineData.Size(), MAX_DEBUG2DLINES);
		}
		my2DLineVBuffer->SetData(aRenderData.myDebugData.myDebug2DLineData.GetRawData());
		my2DLineVBuffer->Bind();

		my2DLineVertexShader->Bind();
		myLinePixelShader->Bind();

		my2DLineGShader->Bind();

		myContext->Draw(aRenderData.myDebugData.myDebug2DLineData.Size(), 0);

		//Not 2D
		CResourceManager* resourceManager = CResourceManager::Get();
		CCamera& camera(resourceManager->GetCameraWithID(aRenderData.myCameraInstance.myCameraID));
		CDX11ConstantBuffer& cameraCBuffer(resourceManager->GetConstantBufferWithID(camera.myCBufferID));
		cameraCBuffer.SetData(camera.myCBufferData);
		cameraCBuffer.BindVS(0);
		cameraCBuffer.BindGS(0);

		myLineVertexShader->Bind();

		//LINES
		if (aRenderData.myDebugData.myDebugLineData.Size() > MAX_DEBUGLINES)
		{
			ENGINE_LOG("WARNING! You are trying to render more Lines than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebugLineData.Size(), MAX_DEBUGLINES);
		}
		myLineVBuffer->SetData(aRenderData.myDebugData.myDebugLineData.GetRawData());
		myLineVBuffer->Bind();

		myLineGShader->Bind();

		myContext->Draw(aRenderData.myDebugData.myDebugLineData.Size(), 0);

		//CUBES
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
		if (aRenderData.myDebugData.myDebugCubeData.Size() > MAX_DEBUGCUBES)
		{
			ENGINE_LOG("WARNING! You are trying to render more cubes than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebugCubeData.Size(), MAX_DEBUGCUBES);
		}
		myCubeVBuffer->SetData(aRenderData.myDebugData.myDebugCubeData.GetRawData());
		myCubeVBuffer->Bind();

		myCubeGShader->Bind();

		myContext->Draw(aRenderData.myDebugData.myDebugCubeData.Size(), 0);
		
		//RECTANGLE 3D
		if (aRenderData.myDebugData.myDebugRectangle3DData.Size() > MAX_DEBUGRECTANGLE3D)
		{
			ENGINE_LOG("WARNING! You are trying to render more 3DRectangles than the maximum allowed, everything passed the limit is cut: Trying to print ( %u / %u )", aRenderData.myDebugData.myDebugRectangle3DData.Size(), MAX_DEBUGRECTANGLE3D);
		}
		myRect3DVBuffer->SetData(aRenderData.myDebugData.myDebugRectangle3DData.GetRawData());
		myRect3DVBuffer->Bind();

		my3DRectGShader->Bind();

		myContext->Draw(aRenderData.myDebugData.myDebugRectangle3DData.Size(), 0);

		RenderSpheres(aRenderData);

		myContext->GSSetShader(nullptr, nullptr, 0);
		CDirect3D11::GetAPI()->SetDefaultDepth();
	}

	/* PRIVATE FUNCTIONS */

	void CDebugRenderer::RenderSpheres(SRenderData& aRenderData)
	{
		myContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myContext->GSSetShader(nullptr, nullptr, 0);

		CModelFactory* modelFactoryPtr = CModelFactory::Get();
		CResourceManager* resourceMngPtr = CResourceManager::Get();
		if (resourceMngPtr == nullptr)
		{
			ENGINE_LOG("[CDebugRenderer] ERROR! resource manager is not created.");
			return;
		}
		if (modelFactoryPtr == nullptr)
		{
			ENGINE_LOG("[CDebugRenderer] ERROR! model factory is not created.");
			return;
		}
		CModelInstance* currInstancePtr(nullptr);
		CModel* currModelPtr(nullptr);
		CDX11ConstantBuffer& instanceCBuffer(resourceMngPtr->GetConstantBufferWithID(myCBufferID));
		SInstanceBufferData instanceData;

		for (unsigned int i(0); i < aRenderData.myDebugData.myDebugSphereData.Size(); ++i)
		{
			currInstancePtr = &aRenderData.myDebugData.myDebugSphereData[i];
			if (currInstancePtr->IsLoading())
			{
				continue;
			}
			if (currInstancePtr->myModelID == UINT_MAX)
			{
				ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because model ID is invalid. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
				continue;
			}
			if (currInstancePtr->myMaterialID == UINT_MAX)
			{
				ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because material ID is invalid. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
				continue;
			}

			currModelPtr = &resourceMngPtr->GetModelWithID(currInstancePtr->myModelID);
			if (currModelPtr->myVertexBufferID == UINT_MAX)
			{
				ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because it vertex buffer ID is invalid. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
				continue;
			}

			CDX11VertexBuffer& vb(resourceMngPtr->GetVertexBufferWithID(currModelPtr->myVertexBufferID));
			CDX11IndexBuffer& ib(resourceMngPtr->GetIndexBufferWithID(currModelPtr->myIndexBufferID));
			CMaterial& mat(modelFactoryPtr->GetMaterialWithID(currInstancePtr->myMaterialID));
			CU::Matrix44f orientation(currInstancePtr->myOrientation);
			orientation.SetScale(currInstancePtr->myScale);
			instanceData.myToWorld = orientation;

			if (!mat.Bind())
			{
				ENGINE_LOG("[CDebugRenderer] WARNING! Failed to render a debug model because the material could not be bound. Model name: \"%s\".", currInstancePtr->myModelName.c_str());
				continue;
			}
			instanceCBuffer.SetData(instanceData);
			instanceCBuffer.BindVS(1);

			vb.Bind();
			ib.Bind();

			myContext->DrawIndexed(ib.GetIndexCount(), 0, 0);
		}
	}
}}