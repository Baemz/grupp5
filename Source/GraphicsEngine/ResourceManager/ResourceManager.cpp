﻿#include "stdafx.h"
#include "ResourceManager.h"
#include "../Model/Model.h"
#include "../Camera/Camera.h"
#include "../DirectXFramework/API/DX11VertexBuffer.h"
#include "../DirectXFramework/API/DX11IndexBuffer.h"
#include "../DirectXFramework/API/DX11ConstantBuffer.h"
#include "../DirectXFramework/API/DX11Shader.h"
#include "../DirectXFramework/API/DX11Texture2D.h"
#include <mutex>

namespace hse
{
	namespace gfx
	{
		namespace ResourceManager🔒
		{
			std::recursive_mutex resourceMutex;
		}
	}
}

using namespace hse::gfx;

CResourceManager* CResourceManager::ourInstance = nullptr;

void CResourceManager::Create()
{
	assert(ourInstance == nullptr && "Instance already created.");
	CDX11Texture::CreateDefaultTextures();
	ourInstance = hse_new(CResourceManager());
}

void CResourceManager::Destroy()
{
	CDX11Texture::DestroyDefaultTextures();
	hse_delete(ourInstance);
}

CResourceManager* CResourceManager::Get()
{
	return ourInstance;
}

CResourceManager::CResourceManager()
	: myCurrentlyFreeModelID(0)
	, myCurrentlyFreeConstantBufferID(0)
	, myCurrentlyFreeCameraID(0)
	, myCurrentlyFreeIndexBufferID(0)
	, myCurrentlyFreeVertexBufferID(0)
	, myCurrentlyFreeShaderID(0)
	, myCurrentlyFreeTextureID(0)
{
	myModels = hse_newArray(CModel, MAX_MODEL_COUNT);
	myCameras = hse_newArray(CCamera, MAX_CAMERA_COUNT);
	myVertexBuffers = hse_newArray(CDX11VertexBuffer, MAX_BUFFER_COUNT);
	myIndexBuffers = hse_newArray(CDX11IndexBuffer, MAX_BUFFER_COUNT);
	myConstantBuffers = hse_newArray(CDX11ConstantBuffer, MAX_BUFFER_COUNT);
	myShaders = hse_newArray(CDX11Shader, MAX_SHADER_COUNT);
	myTextures = hse_newArray(CDX11Texture, MAX_TEXTURE_COUNT);
}


CResourceManager::~CResourceManager()
{
	hse_delete(myModels);
	hse_delete(myCameras);
	hse_delete(myShaders);
	hse_delete(myVertexBuffers);
	hse_delete(myIndexBuffers);
	hse_delete(myConstantBuffers);
	hse_delete(myTextures);
}


bool CResourceManager::ModelExists(const char* aModelFilePath) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedModelIDs.find(aModelFilePath) != myAssignedModelIDs.end())
	{
		return true;
	}
	return false;
}

bool hse::gfx::CResourceManager::UnloadModel(const char* aModelFilePath)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (ModelExists(aModelFilePath) == false)
	{
		return true;
	}
	const unsigned int modelID = GetModelID(aModelFilePath);
	UnloadIndexBuffer(modelID);
	UnloadVertexBuffer(modelID);
	myModels[modelID] = CModel();
	myAssignedModelIDs.erase(aModelFilePath);
	return true;
}

unsigned int CResourceManager::GetModelID(const char* aModelFilePath)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedModelIDs.find(std::string(aModelFilePath)) != myAssignedModelIDs.end())
	{
		return myAssignedModelIDs[std::string(aModelFilePath)];
	}
	else
	{
		if (myCurrentlyFreeModelID > (MAX_MODEL_COUNT - 1))
		{
			assert(false && "Too many models created");
			RESOURCE_LOG("WARNING! Too many models created.");
			return UINT_MAX;
		}
		myAssignedModelIDs[std::string(aModelFilePath)] = myCurrentlyFreeModelID;
		return myCurrentlyFreeModelID++;
	}
}

const CModel& CResourceManager::GetModelWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myModels[aID];
}

CModel& CResourceManager::GetModelWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myModels[aID];
}

unsigned int CResourceManager::GetVertexBufferID(const unsigned int aModelID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedVertexBufferIDs.find(aModelID) != myAssignedVertexBufferIDs.end())
	{
		return myAssignedVertexBufferIDs[aModelID];
	}
	else
	{
		if (myCurrentlyFreeVertexBufferID > (MAX_BUFFER_COUNT - 1))
		{
			assert(false && "Too many vertex-buffers created");
			RESOURCE_LOG("WARNING! Too many vertex-buffers created");
			return UINT_MAX;
		}
		myAssignedVertexBufferIDs[aModelID] = myCurrentlyFreeVertexBufferID;
		return myCurrentlyFreeVertexBufferID++;
	}
}

bool hse::gfx::CResourceManager::UnloadVertexBuffer(const unsigned int aModelID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	const unsigned int bufferID = GetVertexBufferID(aModelID);
	myVertexBuffers[bufferID] = CDX11VertexBuffer();
	myAssignedVertexBufferIDs.erase(aModelID);
	return true;
}

const CDX11VertexBuffer& CResourceManager::GetVertexBufferWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myVertexBuffers[aID];
}

CDX11VertexBuffer& CResourceManager::GetVertexBufferWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myVertexBuffers[aID];
}

unsigned int hse::gfx::CResourceManager::GetIndexBufferID(const unsigned int aModelID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedIndexBufferIDs.find(aModelID) != myAssignedIndexBufferIDs.end())
	{
		return myAssignedIndexBufferIDs[aModelID];
	}
	else
	{
		if (myCurrentlyFreeIndexBufferID > (MAX_BUFFER_COUNT - 1))
		{
			assert(false && "Too many index-buffers created");
			RESOURCE_LOG("WARNING! Too many index-buffers created");
			return UINT_MAX;
		}
		myAssignedIndexBufferIDs[aModelID] = myCurrentlyFreeIndexBufferID;
		return myCurrentlyFreeIndexBufferID++;
	}
}

bool hse::gfx::CResourceManager::UnloadIndexBuffer(const unsigned int aModelID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	const unsigned int bufferID = GetIndexBufferID(aModelID);
	myIndexBuffers[bufferID] = CDX11IndexBuffer();
	myAssignedIndexBufferIDs.erase(aModelID);
	return true;
}

const CDX11IndexBuffer& hse::gfx::CResourceManager::GetIndexBufferWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myIndexBuffers[aID];
}

CDX11IndexBuffer& hse::gfx::CResourceManager::GetIndexBufferWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myIndexBuffers[aID];
}

unsigned int hse::gfx::CResourceManager::GetConstantBufferID(const char* aObjectRef)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedConstantBufferIDs.find(std::string(aObjectRef)) != myAssignedConstantBufferIDs.end())
	{
		return myAssignedConstantBufferIDs[std::string(aObjectRef)];
	}
	else
	{
		if (myCurrentlyFreeConstantBufferID > (MAX_BUFFER_COUNT - 1))
		{
			assert(false && "Too many constant-buffers created");
			RESOURCE_LOG("WARNING! Too many constant-buffers created");
			return UINT_MAX;
		}
		myAssignedConstantBufferIDs[std::string(aObjectRef)] = myCurrentlyFreeConstantBufferID;
		return myCurrentlyFreeConstantBufferID++;
	}
}

bool hse::gfx::CResourceManager::CameraExists(const char* aCameraName) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedCameraIDs.find(aCameraName) != myAssignedCameraIDs.end())
	{
		return true;
	}
	return false;
}

unsigned int hse::gfx::CResourceManager::GetCameraID(const char* aCameraName)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedCameraIDs.find(std::string(aCameraName)) != myAssignedCameraIDs.end())
	{
		return myAssignedCameraIDs[std::string(aCameraName)];
	}
	else
	{
		if (myCurrentlyFreeCameraID > (MAX_MODEL_COUNT - 1))
		{
			assert(false && "Too many models created");
			RESOURCE_LOG("WARNING! Too many models created");
			return UINT_MAX;
		}
		myAssignedCameraIDs[std::string(aCameraName)] = myCurrentlyFreeCameraID;
		return myCurrentlyFreeCameraID++;
	}
}

const CCamera& hse::gfx::CResourceManager::GetCameraWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myCameras[aID];
}

CCamera& hse::gfx::CResourceManager::GetCameraWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myCameras[aID];
}

const CDX11ConstantBuffer& hse::gfx::CResourceManager::GetConstantBufferWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myConstantBuffers[aID];
}

CDX11ConstantBuffer& hse::gfx::CResourceManager::GetConstantBufferWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myConstantBuffers[aID];
}

bool hse::gfx::CResourceManager::ShaderExists(const char* aFilePath) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedShaderIDs.find(aFilePath) != myAssignedShaderIDs.end())
	{
		return true;
	}
	return false;
}

unsigned int CResourceManager::GetShaderID(const char* aFilePath)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedShaderIDs.find(std::string(aFilePath)) != myAssignedShaderIDs.end())
	{
		return myAssignedShaderIDs[std::string(aFilePath)];
	}
	else
	{
		if (myCurrentlyFreeShaderID > (MAX_SHADER_COUNT - 1))
		{
			assert(false && "Too many shaders created");
			RESOURCE_LOG("WARNING! Too many shaders created");
			return UINT_MAX;
		}
		myAssignedShaderIDs[std::string(aFilePath)] = myCurrentlyFreeShaderID;
		return myCurrentlyFreeShaderID++;
	}
}

const CDX11Shader& CResourceManager::GeShaderWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myShaders[aID];
}

CDX11Shader& CResourceManager::GetShaderWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myShaders[aID];
}

bool hse::gfx::CResourceManager::TextureExists(const wchar_t* aFilePath)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedTextureIDs.find(aFilePath) != myAssignedTextureIDs.end())
	{
		return true;
	}
	return false;
}

bool hse::gfx::CResourceManager::TextureExists(const char* aFilePath)
{
	wchar_t wstring[4096];
	return TextureExists(ConvertCharArrayToLPCWSTR(aFilePath, (wchar_t*)(&wstring), 4096));
}

bool hse::gfx::CResourceManager::TextureExistsCreateIfNot(const char* aFilePath, unsigned int & aTextureID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (TextureExists(aFilePath) == true)
	{
		return true;
	}

	aTextureID = GetTextureID(aFilePath);

	return false;
}

unsigned int hse::gfx::CResourceManager::GetTextureID(const wchar_t* aFilePath)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	if (myAssignedTextureIDs.find(std::wstring(aFilePath)) != myAssignedTextureIDs.end())
	{
		return myAssignedTextureIDs[std::wstring(aFilePath)];
	}
	else
	{
		if (myCurrentlyFreeTextureID > (MAX_TEXTURE_COUNT - 1))
		{
			assert(false && "Too many textures created");
			RESOURCE_LOG("WARNING! Too many textures created");
			return UINT_MAX;
		}
		myAssignedTextureIDs[std::wstring(aFilePath)] = myCurrentlyFreeTextureID;
		return myCurrentlyFreeTextureID++;
	}
}

unsigned int hse::gfx::CResourceManager::GetTextureID(const char * aFilePath)
{
	wchar_t wstring[4096];
	return GetTextureID(ConvertCharArrayToLPCWSTR(aFilePath, (wchar_t*)(&wstring), 4096));
}

const CDX11Texture& hse::gfx::CResourceManager::GetTextureWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myTextures[aID];
}

CDX11Texture& hse::gfx::CResourceManager::GetTextureWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ResourceManager🔒::resourceMutex);
	return myTextures[aID];
}
