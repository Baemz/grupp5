#pragma once

namespace hse { namespace gfx {

		class CStreakEmitter
		{
			friend class CParticleFactory;
			friend class CParticleRenderer;
			friend class CScene;
		public:
			CStreakEmitter();
			~CStreakEmitter();

		private:
			unsigned int myVertexShaderID;
			unsigned int myPixelShaderID;
			unsigned int myGeometryShaderID;
			unsigned int myTextureID;
			CDX11VertexBuffer* myVBuffer;
		};

}}