#include "stdafx.h"
#include "ParticleEmitter.h"
#include "DirectXFramework\API\DX11VertexBuffer.h"

namespace hse { namespace gfx {

	CParticleEmitter::CParticleEmitter()
		: myTextureID(UINT_MAX)
		, myVertexShaderID(UINT_MAX)
		, myGeometryShaderID(UINT_MAX)
		, myPixelShaderID(UINT_MAX)
	{
		myVBuffer = hse_new(CDX11VertexBuffer());
	}


	CParticleEmitter::~CParticleEmitter()
	{
		hse_delete(myVBuffer);
	}
}}