#pragma once
#include "ParticleStructs.h"
namespace hse { namespace gfx {

	class CDX11VertexBuffer;
	class CParticleEmitter
	{
		friend class CParticleFactory;
		friend class CParticleRenderer;
	public:
		CParticleEmitter();
		~CParticleEmitter();

	private:
		SParticleJsonData myJsonData;
		unsigned int myVertexShaderID;
		unsigned int myPixelShaderID;
		unsigned int myGeometryShaderID;
		unsigned int myTextureID;
		CDX11VertexBuffer* myVBuffer;
	};

}}