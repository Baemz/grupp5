#include "stdafx.h"
#include "ParticleEmitterInstance.h"
#include "ParticleFactory.h"
#include "GraphicsEngineInterface.h"

namespace hse { namespace gfx {

	CParticleEmitterInstance::CParticleEmitterInstance()
		: myEmitterID(UINT_MAX)
		, mySpawnTimer(0.f)
		, myDurationTimer(0.f)
		, myParentOrientation(nullptr)
	{
	}

	CParticleEmitterInstance::~CParticleEmitterInstance()
	{
	}

	void CParticleEmitterInstance::Init(const char* aParticleJson, CU::Vector3f* aParentPosition, CU::Quaternion* aParentOrientation, const CU::Vector3f aBoxOffset)
	{
		if (CParticleFactory::Get() != nullptr)
		{
			myEmitterID = CParticleFactory::Get()->CreateEmittor(aParticleJson, myJsonData);

			if (myEmitterID != UINT_MAX)
			{
				myFrustumCollider.SetPosition({ 0.f, 0.f, 0.f });
				myFrustumCollider.SetRadius(1.f);
				myParentOrientation = aParentOrientation;
				myParentPosition = aParentPosition;
				myBoxOffset = aBoxOffset;

				myParticles.Init(static_cast<unsigned int>(myJsonData.startLifetime * myJsonData.emissionRateOverTime));
				myIsEmitting = true;
			}
			else
			{
				myIsEmitting = false;
			}
		}
		else
		{
			ENGINE_LOG("WARNING! No ParticleFactory created.");
		}
	}

	void CParticleEmitterInstance::Render()
	{
		CGraphicsEngineInterface::AddToScene(*this);
	}

	void CParticleEmitterInstance::SetPosition(const CU::Vector3f & aPosition)
	{
		myOrientation.SetPosition(aPosition);
	}

	void CParticleEmitterInstance::SpawnParticles(const float aDeltaTime)
	{
		if (myIsEmitting == false)
		{
			return;
		}

		bool spawnedParticle(false);
		while (mySpawnTimer >= (1 / myJsonData.emissionRateOverTime))
		{
			spawnedParticle = true;

			// handle different types of emit-shapes (currently only box)
			CU::Vector3f direction(0.f, 0.f, -1.f);
			CU::Quaternion q;
			q.RotateLocal(myJsonData.direction);
			direction = q * direction;
			direction.Normalize();

			CU::Vector3f boxMin(-0.5f + myBoxOffset.x, -0.5f + myBoxOffset.y, -0.5f + myBoxOffset.z);
			boxMin.x *= myJsonData.spawnPositionLimits.x;
			boxMin.y *= myJsonData.spawnPositionLimits.y;
			boxMin.z *= myJsonData.spawnPositionLimits.z;
			CU::Vector3f boxMax(0.5f + myBoxOffset.x, 0.5f + myBoxOffset.y, 0.5f + myBoxOffset.z);
			boxMax.x *= myJsonData.spawnPositionLimits.x;
			boxMax.y *= myJsonData.spawnPositionLimits.y;
			boxMax.z *= myJsonData.spawnPositionLimits.z;
			
			if (myParentPosition != nullptr)
			{
				myOrientation.SetPosition(*myParentPosition);
			}

			if (myParentOrientation != nullptr)
			{
				direction = *myParentOrientation * direction;
				boxMin = *myParentOrientation * boxMin;
				boxMax = *myParentOrientation * boxMax;
			}
			else
			{
				boxMin = q * boxMin;
				boxMax = q * boxMax;
			}

			CU::Vector3f offset(CU::GetRandomInRange(boxMin.x, boxMax.x), CU::GetRandomInRange(boxMin.y, boxMax.y), CU::GetRandomInRange(boxMin.z, boxMax.z));

			CU::Vector4f startColor;
			startColor = myJsonData.startColor;

			bool gradient = false;
			if (myJsonData.useColorOverLife)
			{
				if (myJsonData.randomTwoGradients)
				{
					int randGrad = CU::GetRandomInRange(0, 100);
					if (randGrad < 50)
					{
						gradient = false;
						startColor = myJsonData.colorgrad1[0];
					}
					else
					{
						gradient = true;
						startColor = myJsonData.colorgrad2[0];
					}
				}
				else
				{
					startColor = myJsonData.colorgrad1[0];
				}
			}

			float rotateVel = 0.f;
			if (myJsonData.useRotationOverLifetime)
			{
				rotateVel = CU::GetRandomInRange(myJsonData.rotationOverLifeMin.z, myJsonData.rotationOverLifeMax.z);
			}
			else
			{
			}

			float startRotation = 0.f; 
			startRotation = CU::GetRandomInRange(myJsonData.startRotationMin, myJsonData.startRotationMax);


			myParticles.Add(SParticle({ myOrientation.GetPosition() + offset , 1.0f }, startColor, direction, myJsonData.startSize, startRotation, gradient, rotateVel));
			mySpawnTimer -= (1 / myJsonData.emissionRateOverTime);

		}
		if (spawnedParticle)
		{
			mySpawnTimer = 0.f;
		}
		mySpawnTimer += aDeltaTime;
	}

	static float Round(float f)
	{
		return floorf(f * 100) / 100; // C++11
	}

	void CParticleEmitterInstance::Update(const float aDeltaTime)
	{
		if (myIsEmitting == false && myParticles.Size() <= 0)
		{
			return;
		}
		if (myJsonData.looping == true || myDurationTimer <= myJsonData.duration)
		{
			if (myParticles.Size() < static_cast<unsigned int>(myJsonData.maxParticles))
			{
				SpawnParticles(aDeltaTime);
			}
		}
		if (myJsonData.looping == false && myIsEmitting != false)
		{
			myDurationTimer += aDeltaTime;
			if (myDurationTimer >= myJsonData.duration)
			{
				myIsEmitting = false;
			}
		}

		for (unsigned int i = myParticles.Size(); i-- > 0;)
		{
			if (myParticles[i].myCurrentLifeTime >= myJsonData.startLifetime)
			{
				myParticles.RemoveCyclicAtIndex(i);
			}
			else
			{
				const float lifetimePercent(myParticles[i].myCurrentLifeTime / myJsonData.startLifetime);
				myParticles[i].myPosition += CU::Vector4f(myParticles[i].myDirection, 1.0f) * myJsonData.startSpeed * aDeltaTime;
				myParticles[i].myCurrentLifeTime += aDeltaTime;

				if (myJsonData.useSizeCurve)
				{
					myParticles[i].mySize = myJsonData.startSize * (myJsonData.startSizeCurve + (myJsonData.endSize - myJsonData.startSizeCurve) * (lifetimePercent));
				}

				float lifetimeClamped = Round(lifetimePercent);
				unsigned int index = static_cast<unsigned int>(lifetimeClamped * 100);
				if (myJsonData.useColorOverLife)
				{
					if (myParticles[i].myGradient == false)
					{
						myParticles[i].myColor = myJsonData.colorgrad1[index];
					}
					else
					{
						myParticles[i].myColor = myJsonData.colorgrad2[index];
					}
				}
				if (myJsonData.useRotationOverLifetime)
				{
					myParticles[i].myRotation += aDeltaTime * myParticles[i].myRotationVelocity;
				}
			}
		}
	}
}}