#pragma once
#include "..\Camera\Frustum\FrustumCollider.h"
#include "..\Utilities\VertexStructs.h"
#include "..\CommonUtilities\GrowingArray.h"
#include "..\CommonUtilities\Matrix.h"
#include "ParticleStructs.h"

namespace hse { namespace gfx{

	class CParticleEmitterInstance
	{
		friend class CParticleFactory;
		friend class CParticleRenderer;
		friend class CScene;
	public:
		CParticleEmitterInstance();
		~CParticleEmitterInstance();

		void Init(const char* aParticleJson, CU::Vector3f* aParentPosition = nullptr, CU::Quaternion* aParentOrientation = nullptr, const CU::Vector3f aBoxOffset = { 0.0f, 0.0f, 0.0f });
		void Update(const float aDeltaTime);
		void Render();
		const bool IsEmitting() const { return myIsEmitting; };
		const bool IsEmpty() const { return (myParticles.Size() <= 0); };

		void SetPosition(const CU::Vector3f& aPosition);

		const unsigned int GetEmitterID() const { return myEmitterID; };

	private:

		void SpawnParticles(const float aDeltaTime);

	private:
		CU::GrowingArray<SParticle, unsigned int> myParticles;
		CU::Matrix44f myOrientation;
		CFrustumCollider myFrustumCollider;
		SParticleJsonData myJsonData;
		float mySpawnTimer;
		float myDurationTimer;
		unsigned int myEmitterID;
		CU::Quaternion* myParentOrientation;
		CU::Vector3f* myParentPosition;
		CU::Vector3f myBoxOffset;
		bool myIsEmitting;
	};

}}