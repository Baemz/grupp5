#pragma once
#include <array>

namespace hse { namespace gfx {

	struct SParticleJsonData
	{
		bool looping;
		bool randomTwoGradients;
		bool useColorOverLife;
		float duration;
		float startDelay;
		float startLifetime;
		float startSpeed;
		float startSize;
		float startSizeMin;
		float startSizeMax;
		float startSizeCurve;
		float endSize;
		bool useSizeOverLifetime;
		bool useSizeCurve;
		bool useRotationOverLifetime;
		float startRotationMin;
		float startRotationMax;
		float gravityModifier;
		int maxParticles;
		float emissionRateOverTime;
		CU::Vector3f velocityOverLife;
		CU::Vector3f direction;
		CU::Vector3f spawnPositionLimits;
		CU::Vector3f rotationOverLife;
		CU::Vector3f rotationOverLifeMin;
		CU::Vector3f rotationOverLifeMax;
		CU::Vector4f startColor;
		std::array<CU::Vector4f, 100> colorgrad1;
		std::array<CU::Vector4f, 100> colorgrad2;
		std::string textureFileName;
	};
}}