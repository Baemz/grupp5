#pragma once

// STD-includes
#include <map>
#include <vector>
#include <array>
#include <algorithm>
#include <string>
#include <functional>
#include <cassert>
#include <cmath>

// CommonUtilities-includes
#include "../CommonUtilities/Matrix.h"
#include "../CommonUtilities/Vector.h"
#include "../CommonUtilities/Stopwatch.h"
#include "../CommonUtilities/GrowingArray.h"
#include "..\CommonUtilities\CommonMath.h"
#include "..\CommonUtilities\Random.h"
#include "..\CommonUtilities\Sort.h"

// // -DX11- // //
#include <d3d11.h>
#include "DirectXFramework/Direct3D11.h"
#include "DirectXFramework/API/DX11VertexBuffer.h"
#include "DirectXFramework/API/DX11IndexBuffer.h"
#include "DirectXFramework/API/DX11ConstantBuffer.h"
#include "DirectXFramework/API/DX11Shader.h"
#include "DirectXFramework/API/DX11Texture2D.h"

// GraphicsEngine-includes
#include "Utilities/VertexStructs.h"
#include "Utilities/miscutil.h"
#include "Utilities/RenderStructs.h"
#include "DirectXFramework/CDirectXMathHelper.h"
#include "GraphicsEngineInterface.h"
#include "Model\Loading\ModelFactory.h"
#include "ResourceManager\ResourceManager.h"

// EngineCore-includes
#include "../EngineCore/MemoryPool/MemoryPool.h"
#include "../EngineCore/Logger/Logger.h"

// Defines

#define SAFE_RELEASE(ptr) if (ptr != nullptr){ ptr->Release(); ptr = nullptr; }
#define SAFE_LRELEASE(ptr) if (ptr != nullptr){ ptr->release(); ptr = nullptr; }
#define SAFE_DELETE(ptr) hse_delete(ptr)
#define MAX_INSTANCE_COUNT 1000

#ifndef MATH_PI
#define MATH_PI (3.141592654f)
#endif