#pragma once
#include "Model/ModelInstance.h"
#include "Camera/CameraInstance.h"
#include "Lights/EnvironmentalLight.h"
#include "../CommonUtilities/GrowingArray.h"
#include "Sprite/Sprite.h"
#include "Sprite/Sprite3D.h"
#include "Particles/ParticleEmitterInstance.h"
#include "Text/Text.h"
#include "Lights/PointLightInstance.h"
#include "Lights/SpotLightInstance.h"
#include "ShaderEffects/ShaderEffectInstance.h"
#include "Particles/StreakEmitterInstance.h"

namespace hse { namespace gfx {

	struct STimeData
	{
		float myThreadDeltaTime;
		float myThreadTotalTime;
		float myApplicationTotalTime;
	private:
		float pad;
	};

	struct SRenderData
	{
		struct SDebugData
		{
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebugLineData;
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebugCubeData;
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebug2DLineData;
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebugRectangle3DData;
			CU::GrowingArray<CModelInstance, unsigned int> myDebugSphereData;

			void Clean()
			{
				myDebugLineData.RemoveAll();
				myDebugCubeData.RemoveAll();
				myDebug2DLineData.RemoveAll();
				myDebugRectangle3DData.RemoveAll();
				myDebugSphereData.RemoveAll();
			}
		};
		SDebugData myDebugData;

		STimeData myTimeData;
		CU::GrowingArray<hse::gfx::CModelInstance, unsigned int> myListToRender;
		CU::GrowingArray<hse::gfx::CModelInstance, unsigned int> myTransparentListToRender;
		CU::GrowingArray<CSprite, unsigned int> mySpritesToRender;
		CU::GrowingArray<CSprite3D, unsigned int> my3DSpritesToRender;
		CU::GrowingArray<CParticleEmitterInstance, unsigned int> myParticleEmittorsToRender;
		CU::GrowingArray<CStreakEmitterInstance, unsigned int> myStreakEmittorsToRender;
		CU::GrowingArray<CShaderEffectInstance, unsigned int> myShaderEffectsToRender;
		CU::GrowingArray<CText, unsigned int> myTextsToRender;
		CU::GrowingArray<CPointLightInstance, unsigned int> myPointLightData;
		CU::GrowingArray<CSpotLightInstance, unsigned int> mySpotLightData;
		CCameraInstance myCameraInstance;
		CEnvironmentalLight  myEnvLight;
	};
}}