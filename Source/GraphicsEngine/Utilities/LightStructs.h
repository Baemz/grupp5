#pragma once
#include "../CommonUtilities/Vector.h"

struct SPointLightData
{
	CU::Vector4f myPosition;
	CU::Vector3f myColor;
	float myRange;
	float myIntensity;
	float pad[3];
};

struct SSpotLightData
{
	CU::Vector4f myPosition;
	CU::Vector3f myColor;
	float myAngle;
	CU::Vector3f myDirection;
	float myRange;
	float myIntensity;
	float pad[3];
};