#pragma once
#include "../../CommonUtilities/Vector.h"

struct SNoneEffectData
{
	CU::Vector4f myPadding;
};

struct SGlitchScreenData
{
	SGlitchScreenData()
		: myResolution({ 1920.0f, 1080.0f })
		, myUVNoiseDistance(0.25f)
		, myElapsedTime(0.0f)
		, myLineThresholdMultiplier(1.0f)
		, myBlockThresholdMultiplier(1.0f)
		, myDistortionLineMultipler(1.0f)
		, myNoiseAbberationAlpha(0.0f)
	{
	}

	CU::Vector2f myResolution;
	float myElapsedTime;
	float myUVNoiseDistance;
	float myLineThresholdMultiplier;
	float myBlockThresholdMultiplier;
	float myDistortionLineMultipler;
	float myNoiseAbberationAlpha;
};

struct SStaticScreenData
{
	float myIntensity = 0.5f;
	float myElapsedTime = 0.0f;
	float myDistortionLineMultiplier = 0.5f;
	float padding;
};