#pragma once
#include <string>

struct ID3D11Device;
struct ID3D11DeviceContext;
struct IFW1FontWrapper;
struct IDWriteFactory;
struct IDWriteFontCollectionLoader;
struct IDWriteFontCollection;
struct IDWriteTextFormat;
struct IDWriteTextLayout;

namespace hse
{
	namespace gfx
	{
		class CFont
		{
		public:
			CFont();
			~CFont();

			bool Init(ID3D11Device* aDevice, ID3D11DeviceContext* aDeviceContext, const char* aFontName);
			bool Render(const char* aText, float aSize, const CU::Vector2f& aPosition, unsigned int aColor, const CU::Matrix44f& aTransform, const CU::Vector2f& aWordWrapBoxSize, const bool aAlignCenter);
			bool Render(const wchar_t* aText, float aSize, const CU::Vector2f& aPosition, unsigned int aColor, const CU::Matrix44f& aTransform, const CU::Vector2f& aWordWrapBoxSize, const bool aAlignCenter);

			const std::wstring& GetName() const;
			float SetDataAndGetWidth(const std::string& aText, const float aSize, const CU::Vector2f& aWordWrapBoxSize, const bool aAlignCenter);

		private:
			struct SFontCreation
			{
				SFontCreation()
					: myFactory(nullptr)
					, myFontCollectionLoader(nullptr)
					, myFontCollection(nullptr)
					, myTextFormat(nullptr)
					, myTextLayout(nullptr)
				{}

				~SFontCreation();

				IDWriteFactory* myFactory;
				IDWriteFontCollectionLoader* myFontCollectionLoader;
				IDWriteFontCollection* myFontCollection;
				IDWriteTextFormat* myTextFormat;
				IDWriteTextLayout* myTextLayout;
			};

			bool UpdateCustomFont(const wchar_t* aText, float aSize, const CU::Vector2f& aWordWrapBoxSize, const bool aAlignCenter);

			std::wstring myFontName;
			std::wstring myFontPath;
			bool myIsUsingCustomFont;
			std::wstring myTextLastRender;
			float mySizeLastRender;
			CU::Vector2f myWordWrapBoxLastRender;
			bool myAlignCenterLastRender;

			ID3D11Device* myDevice;
			ID3D11DeviceContext* myDeviceContext;
			SFontCreation myFontCreation;
			IFW1FontWrapper* myFont;
		};
	}
}
