#pragma once
#include <windows.h>
#include <atomic>

namespace hse { namespace gfx {

	struct SWindowData;
	class CWindowHandler
	{
	public:
		CWindowHandler();
		~CWindowHandler();

		bool Init(const SWindowData& someWindowData);
		void SetResolution(CU::Vector2ui aResolution);
		static LRESULT CALLBACK WinProc(HWND aWindowHandle, UINT aWinMessage, WPARAM aWPARAM, LPARAM aLPARAM);

		HWND GetWindowHandle();

		inline const bool IsFullscreen() { return myIsFullscreen; }
		const CU::Vector2f& GetValidResolution() const;
		const CU::Vector2f& GetFullResolution() const;
		const CU::Vector2f& GetFullWindowedResolution() const;
		const CU::Vector2f& GetWindowedResolution() const;
		void SetFullscreenState(const bool aIsFullscreen);

		void EnableCursor() const;
		void DisableCursor() const;

	private:
		HWND myHWND;
		CU::Vector2f myWindowedResolution;
		CU::Vector2f myFullWindowedResolution;
		CU::Vector2f myFullscreenResolution;
		std::atomic_bool myIsFullscreen;
		bool myIsBorderless;
	};

}}