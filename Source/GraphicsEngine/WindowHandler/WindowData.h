#pragma once
#include <string>

namespace hse {namespace gfx {
	struct SWindowData
	{
		std::string myWindowName;
		unsigned int myWidth;
		unsigned int myHeight;
		bool myStartInFullScreen; 
		bool myUseBloom;
		bool myUseFXAA;
		bool myUseColorGrading;
		bool myUseVSync;
		void* myHWND = nullptr;
	};
}}