#include "stdafx.h"
#include "WindowHandler.h"
#include "WindowData.h"

#include "../GraphicsEngineInterface.h"
#include "../Launcher/resource.h"

using namespace hse::gfx;

CWindowHandler::CWindowHandler()
{
}


CWindowHandler::~CWindowHandler()
{
}

LRESULT CALLBACK CWindowHandler::WinProc(HWND aWindowHandle, UINT aWinMessage, WPARAM aWPARAM, LPARAM aLPARAM)
{
	if (aWinMessage == WM_DESTROY || aWinMessage == WM_CLOSE)
	{
		PostQuitMessage(0);
		return 0;
	}
	else if (aWinMessage == WM_SIZE || aWinMessage == WM_MOVE)
	{
		return 0;
	}

	return DefWindowProc(aWindowHandle, aWinMessage, aWPARAM, aLPARAM);
}

bool CWindowHandler::Init(const SWindowData& someWindowData)
{
	WNDCLASS windowClass = {};
	windowClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = CWindowHandler::WinProc;
	windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
	HINSTANCE hInstance(GetModuleHandleW(nullptr));
	if (hInstance != nullptr)
	{
		windowClass.hIcon = (HICON)LoadImageW(hInstance, MAKEINTRESOURCEW(IDI_ICON1), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
	}
	ShowCursor(FALSE);
	windowClass.lpszClassName = "HSEEngine";
	RegisterClass(&windowClass);

	DWORD dwStyle(0);
	// Overlapped
	dwStyle = WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE;
	myIsBorderless = false;

	myIsFullscreen = someWindowData.myStartInFullScreen;

	HWND hwnd = nullptr;
	hwnd = reinterpret_cast<HWND>(someWindowData.myHWND);

	if (hwnd == nullptr)
	{
		myHWND = CreateWindow("HSEEngine",
			someWindowData.myWindowName.c_str(),
			dwStyle,
			0,
			0,
			someWindowData.myWidth,
			someWindowData.myHeight,
			nullptr, nullptr, nullptr, nullptr);
	}
	else
	{
		myHWND = hwnd;
	}
	

	SetResolution(CU::Vector2ui(someWindowData.myWidth, someWindowData.myHeight));

	if (myIsFullscreen == true)
	{
		RECT clipy;
		GetWindowRect(myHWND, &clipy);
		ClipCursor(&clipy);
	}

	ShowWindow(myHWND, SW_SHOW);
	SetForegroundWindow(myHWND);
	SetFocus(myHWND);

	return true;
}

void CWindowHandler::SetResolution(CU::Vector2ui aResolution)
{
	::SetWindowPos(myHWND, nullptr, 0, 0, (int)aResolution.x, (int)aResolution.y, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	myFullscreenResolution.x = (float)GetSystemMetrics(SM_CXSCREEN);
	myFullscreenResolution.y = (float)GetSystemMetrics(SM_CYSCREEN);

	if (myIsBorderless)
	{
		myWindowedResolution = myFullscreenResolution;
	}
	else 
	{
		RECT r;
		GetClientRect(myHWND, &r);
		const int horizontal(r.right - r.left);
		const int vertical(r.bottom - r.top);

		myWindowedResolution.x = (float)horizontal;
		myWindowedResolution.y = (float)vertical;
	}

	//myWidth = (float)aResolution.x;
	//myHeight = (float)aResolution.y;
	myFullWindowedResolution.x = (float)aResolution.x;
	myFullWindowedResolution.y = (float)aResolution.y;
}

HWND CWindowHandler::GetWindowHandle()
{
	return myHWND;
}

const CU::Vector2f& hse::gfx::CWindowHandler::GetValidResolution() const
{
	if (myIsFullscreen) return myFullscreenResolution;
	else return myWindowedResolution;
}

const CU::Vector2f& hse::gfx::CWindowHandler::GetFullResolution() const
{
	return myFullscreenResolution;
}

const CU::Vector2f& hse::gfx::CWindowHandler::GetFullWindowedResolution() const
{
	return myFullWindowedResolution;
}

const CU::Vector2f& hse::gfx::CWindowHandler::GetWindowedResolution() const
{
	return myWindowedResolution;
}

void hse::gfx::CWindowHandler::SetFullscreenState(const bool aIsFullscreen)
{
	myIsFullscreen = aIsFullscreen;

	if (myIsFullscreen == true)
	{
		RECT clipy;
		GetWindowRect(myHWND, &clipy);
		ClipCursor(&clipy);
	}
	else
	{
		ClipCursor(nullptr);
	}
}

void hse::gfx::CWindowHandler::EnableCursor() const
{
	while (ShowCursor(TRUE) < 0) continue;
}

void hse::gfx::CWindowHandler::DisableCursor() const
{
	while (ShowCursor(FALSE) >= 0) continue;
}
