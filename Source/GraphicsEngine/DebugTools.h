#pragma once
#include "Utilities\VertexStructs.h"
#include "..\CommonUtilities\GrowingArray.h"
#include <mutex>

#define DT_DRAW_LINE_2_ARGS(aStartPos, aEndPos) hse::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos);
#define DT_DRAW_LINE_3_ARGS(aStartPos, aEndPos, aStartColorRGB) hse::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB);
#define DT_DRAW_LINE_4_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB) hse::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB);
#define DT_DRAW_LINE_5_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness) hse::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness);
#define DT_DRAW_LINE_6_ARGS(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness, aEndThickness) hse::gfx::CDebugTools::Get()->DrawLine(aStartPos, aEndPos, aStartColorRGB, aEndColorRGB, aStartThickness, aEndThickness);

#define DT_DRAW_SPHERE_2_ARGS(aPos, aColor) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor);
#define DT_DRAW_SPHERE_3_ARGS(aPos, aColor, aRadious) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, aRadious);
#define DT_DRAW_SPHERE_4_ARGS(aPos, aColor, aRadious, aRotation) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, aRadious, aRotation);
#define DT_DRAW_SPHERE_5_ARGS(aPos, aColor, aRadious, aRotation, aRingCount) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, aRadious, aRotation, aRingCount);
#define DT_DRAW_SPHERE_6_ARGS(aPos, aColor, aRadious, aRotation, aRingCount, aSliceCount) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, aColor, aRadious, aRotation, aRingCount, aSliceCount);

#define DT_DRAW_SPHERE_DEF_COLOR_1_ARGS(aPos) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, { -1.0f });
#define DT_DRAW_SPHERE_DEF_COLOR_2_ARGS(aPos, aRadious) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, { -1.0f }, aRadious);
#define DT_DRAW_SPHERE_DEF_COLOR_3_ARGS(aPos, aRadious, aRotation) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, { -1.0f }, aRadious, aRotation);
#define DT_DRAW_SPHERE_DEF_COLOR_4_ARGS(aPos, aRadious, aRotation, aRingCount) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, { -1.0f }, aRadious, aRotation, aRingCount);
#define DT_DRAW_SPHERE_DEF_COLOR_5_ARGS(aPos, aRadious, aRotation, aRingCount, aSliceCount) hse::gfx::CDebugTools::Get()->DrawSphere(aPos, { -1.0f }, aRadious, aRotation, aRingCount, aSliceCount);

namespace hse { namespace gfx {

	class CModelInstance;

	class CDebugTools
	{
	public:
		~CDebugTools();

		static void Create();
		static void Destroy();
		static CDebugTools* Get();

		void DrawLine(CU::Vector3f aStartPos, CU::Vector3f aEndPos, CU::Vector3f aStartColorRGB = { 1.f, 1.f, 1.f }, CU::Vector3f aEndColorRGB = { 1.f, 1.f, 1.f },
			float aStartThickness = 1.0f, float aEndThickness = 1.0f);
		void DrawLine2D(CU::Vector2f aStartScreenPos, CU::Vector2f aEndScreenPos, CU::Vector3f aStartColorRGB = { 1.f, 1.f, 1.f }, CU::Vector3f aEndColorRGB = { 1.f, 1.f, 1.f });
		void DrawCube(const CU::Vector3f aPos, const float aSize = 1.f, const CU::Vector3f aCubeRGB = { 1.f, 1.f, 1.f });
		void DrawRectangle3D(const CU::Vector3f aPos, const CU::Vector3f aSizes = { 1.f, 1.f, 1.f }, const CU::Vector3f aCubeRGB = { 1.f, 1.f, 1.f });
		void DrawText2D(const std::string& aText, const CU::Vector2f& aPosition = CU::Vector2f(0.0f, 0.0f), float aSize = 24.0f,
			const CU::Vector3f& aColor = CU::Vector3f(1.0f, 1.0f, 1.0f));
		void DrawSphere(const CU::Vector3f& aPos, const CU::Vector3f& aColor, const CU::Vector3f& aRotation = CU::Vector3f(),
			const float aRadious = 1.0f, const unsigned int aRingCount = 10, const unsigned int aSliceCount = 10);

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebug2DLineData() const;
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetDebugCubeData() const;
		const CU::GrowingArray<CModelInstance, unsigned int>& GetDebugSphereData() const;
		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& GetRectangle3DData();

		void ChangeUpdateBuffer();
		void ChangeRenderBuffer();

	private:
		struct SDebugDataBuffer
		{
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebugLineData;
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebugCubeData;
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebug2DLineData;
			CU::GrowingArray<SSimpleDebugVertex, unsigned int> myDebugRectangle3DData;
			CU::GrowingArray<CModelInstance, unsigned int> myDebugSphereData;

			bool myIsRead = false;

			void RemoveAll()
			{
				myDebugLineData.RemoveAll();
				myDebugCubeData.RemoveAll();
				myDebug2DLineData.RemoveAll();
				myDebugRectangle3DData.RemoveAll();
				myDebugSphereData.RemoveAll();
			}
		};

		CDebugTools();

	private:
		static CDebugTools* ourInstance;
		SDebugDataBuffer myDebugDataBuffers[3];

		unsigned char myFreeIndex;
		unsigned char myReadIndex;
		unsigned char myWriteIndex;

		std::mutex myBufferLock;
	};
}}