#pragma once
#include "../CommonUtilities/Matrix.h"
#include "../Camera/Frustum/FrustumCollider.h"

namespace hse { namespace gfx {
	
	class CModelInstance
	{
		friend class CModelFactory;
		friend class CForwardRenderer;
		friend class CSkyboxRenderer;
		friend class CScene;
		friend class CDeferredRenderer;
		friend class CDebugRenderer;
	public:
		CModelInstance();
		CModelInstance(const CModelInstance& aModelInstance);
		~CModelInstance();

		void operator=(const CModelInstance& aModelInstance);

		enum class ELoadingState : char
		{
			Unloaded = 0,
			Loading = 1,
			Loaded = 2,
			FailedLoading = 3,
		};

		void Init();
		void InitCustomSphere(const CU::Vector3f& aColor, const float aRadious, const unsigned int aRingCount, const unsigned int aPointPerRingCount);

		void Move(const CU::Vector3f& aValue);
		void SetScale(const CU::Vector3f& aScale);
		void SetScaleForInit(const CU::Vector3f& aScale);
		void Rotate(float aValue);
		void Rotate(const CU::Vector3f& aRotation);

		void SetPosition(const CU::Vector3f& aPosition);
		void SetOrientation(const CU::Matrix44f& aOrientation);
		void SetModelPath(const std::string& aModelPath);
		void SetMaterialPath(const std::string& aMaterialPath);
		const bool IsAffectedByFog() const { return myAffectedByFog; };
		void SetAffectedByFog(const bool aAffectedByFog);
		const bool IsCullable() const { return myIsCullable; };
		void SetIsCullable(const bool aIsCullable);
		void UpdateColliderPosition();

		const bool IsLoaded() const;
		const bool IsLoading() const;
		const bool LoadingFailed() const;
		const std::string& GetModelPath() const;
		const CU::Vector3f GetPosition() const;
		const CU::Matrix44f GetOrientation() const;
		const float GetZValue() const;
		unsigned int GetModelID() const { return myModelID; };
		inline const float GetRadious() const { return myRadious; }


		void Render();

	private:
		std::string myModelName;
		std::string myModelPath;
		std::string myMaterialPath;
		CU::Matrix44f myOrientation;
		CU::Vector3f myScaleToUseForInit;
		CU::Vector3f myScale;
		CFrustumCollider myFrustumCollider;
		unsigned int myModelID;
		unsigned int myMaterialID;
		volatile ELoadingState myLoadingState;
		float myRadious;
		bool myAffectedByFog;
		bool myIsCullable;
	};
}}