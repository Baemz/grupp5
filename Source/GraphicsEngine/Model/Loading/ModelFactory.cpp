﻿#include "stdafx.h"
#include "ModelFactory.h"
#include "../Model.h"
#include "../../DirectXFramework/API/DX11VertexBuffer.h"
#include "../../DirectXFramework/API/DX11IndexBuffer.h"
#include "../../DirectXFramework/API/DX11Shader.h"
#include "../../DirectXFramework/API/DX11Texture2D.h"
#include "../../ResourceManager/ResourceManager.h"
#include "../EngineCore/FileWatcher/FileWatcherWrapper.h"
#include <thread>
#include <mutex>
#include "Material.h"

using namespace hse::gfx;

namespace hse { namespace gfx {
	namespace ModelFactory🔒
	{
		std::recursive_mutex modelFactoryMutex;
	}
}}


CModelFactory* CModelFactory::ourInstance = nullptr;

bool hse::gfx::CModelFactory::ModelChangedCallback(void* aThis, const char* aModelChanged)
{
	CModelFactory& thisModelFactory = *static_cast<CModelFactory*>(aThis);

	if (thisModelFactory.ReloadModel(aModelChanged) == false)
	{
		RESOURCE_LOG("ERROR! Failed to reload model \"%s\" when changed", aModelChanged);
	}

	return true;
}

bool hse::gfx::CModelFactory::MaterialChangedCallback(void* aThis, const char* aMaterialChanged)
{
	CModelFactory& thisModelFactory = *static_cast<CModelFactory*>(aThis);
	START_TIMER(loadTimer);
	int time = 0;
	unsigned int matID(thisModelFactory.GetMaterialID(aMaterialChanged));
	CMaterial& mat(thisModelFactory.GetMaterialWithID(matID));

	mat.myLoadingState = ELoadingState::Loading;

	if (mat.Init(aMaterialChanged) == false)
	{
		thisModelFactory.RemoveMaterialRefCount(matID);
		mat.myLoadingState = ELoadingState::Unloaded;
		END_TIMER(loadTimer, time);
		RESOURCE_LOG("WARNING! [%s] couldn't be reloaded.", aMaterialChanged);
		return false;
	}

	END_TIMER(loadTimer, time);
	RESOURCE_LOG("[%s] took %i ms to reload.", aMaterialChanged, time);
	mat.myLoadingState = ELoadingState::Loaded;
	return true;
}

CModelFactory::CModelFactory(hse::CFileWatcherWrapper& aFileWatcher)
	: myFileWatcher(aFileWatcher)
	, myCurrentlyFreeMaterialID(0)
{
	myFBXLoader.Init();
	myMaterials = hse_newArray(CMaterial, MAX_MATERIAL_COUNT);
}

CModelFactory::~CModelFactory()
{
	hse_delete(myMaterials);
}

bool hse::gfx::CModelFactory::LoadModel(const char* aFilePath, const char* aMaterialFilePath, CModelInstance* aNewInstance)
{
	CResourceManager* resourceManager = CResourceManager::Get();

	START_TIMER(loadTimer);
	int time = 0;

	CLoaderModel* loaderModel = nullptr;
	if (myFBXLoader.LoadModel(aFilePath, &loaderModel) == false)
	{
		END_TIMER(loadTimer, time);
		RESOURCE_LOG("[MODEL] WARNING! (%s) could not be loaded. Model might be rendered incorrectly or not at all", aFilePath);
		return false;
	}

	// Model
	unsigned int modelID = resourceManager->GetModelID(aFilePath);
	CModel& model(resourceManager->GetModelWithID(modelID));

	// Vertex
	unsigned int vertexBufferID = resourceManager->GetVertexBufferID(modelID);
	CDX11VertexBuffer& vertexBuffer = resourceManager->GetVertexBufferWithID(vertexBufferID);
	vertexBuffer.CreateBuffer(loaderModel->myMeshes[0]->myVerticies, loaderModel->myMeshes[0]->myVertexBufferSize, loaderModel->myMeshes[0]->myVertexCount);
	model.myVertexBufferID = vertexBufferID;

	// Index
	unsigned int indexBufferID = resourceManager->GetIndexBufferID(modelID);
	CDX11IndexBuffer& indexBuffer = resourceManager->GetIndexBufferWithID(indexBufferID);
	indexBuffer.CreateBuffer(loaderModel->myMeshes[0]->myIndexes);
	model.myIndexBufferID = indexBufferID;

	if (aMaterialFilePath != nullptr)
	{
		// Material
		std::string materialPath(aMaterialFilePath);
		if (MaterialExists(materialPath.c_str()))
		{
			aNewInstance->myMaterialID = GetMaterialID(materialPath.c_str());
		}
		else
		{
			unsigned int matID(GetMaterialID(materialPath.c_str()));
			CMaterial& mat(GetMaterialWithID(matID));
			mat.myLoadingState = ELoadingState::Loading;
			if (mat.Init(materialPath.c_str()) == false)
			{
				RemoveMaterialRefCount(matID);
				mat.myLoadingState = ELoadingState::Unloaded;
				RESOURCE_LOG("WARNING! [%s] couldn't be loaded, using default material.", materialPath.c_str());
				UseDefaultMaterialID(aNewInstance);
			}
			else
			{
				aNewInstance->myMaterialID = matID;
				CFileWatcherWrapper::AddFileToWatch(this, materialPath.c_str(), &CModelFactory::MaterialChangedCallback);
				mat.myLoadingState = ELoadingState::Loaded;
			}
		}
	}

	if (aNewInstance != nullptr)
	{
		model.myMaxRadius = loaderModel->myMaxRadius;
		aNewInstance->myModelID = modelID;
		aNewInstance->myRadious = model.myMaxRadius;
	}

	END_TIMER(loadTimer, time);
	RESOURCE_LOG("[MODEL] (%s) took %i ms to load.", aFilePath, time);

	loaderModel->Destroy();
	hse_delete(loaderModel);
	model.myIsLoaded = true;

	return true;
}

bool hse::gfx::CModelFactory::ReloadModel(const char * aFilePath)
{
	if (LoadModel(aFilePath, nullptr, nullptr) == true)
	{
		return true;
	}

	return false;
}

void hse::gfx::CModelFactory::UseDefaultTexture(CModel& aModel)
{
	CResourceManager* resourceManager = CResourceManager::Get();
	
	const wchar_t* defaultTexture = L"Data/Models/Misc/defaultTexture.dds";
	if (resourceManager->TextureExists(defaultTexture))
	{
		aModel.myTextureID[0] = resourceManager->GetTextureID(defaultTexture);
	}
	else
	{
		unsigned int textureID = resourceManager->GetTextureID(defaultTexture);
		CDX11Texture& texture(resourceManager->GetTextureWithID(textureID));
		if (texture.CreateTexture(defaultTexture, ETextureType::Diffuse) == false)
		{
			assert(false && "Default Textures could not be loaded");
		}
		aModel.myTextureID[0] = textureID;
	}
}

void hse::gfx::CModelFactory::UseDefaultMaterialID(CModelInstance* aNewInstance)
{
	std::string materialPath("Data/Materials/DEFAULT.material");
	if (MaterialExists(materialPath.c_str()))
	{
		aNewInstance->myMaterialID = GetMaterialID(materialPath.c_str());
	}
	else
	{
		unsigned int matID(GetMaterialID(materialPath.c_str()));
		CMaterial& mat(GetMaterialWithID(matID));
		mat.myLoadingState = ELoadingState::Loading;
		if (mat.Init(materialPath.c_str()) == false)
		{
			RemoveMaterialRefCount(matID);
			mat.myLoadingState = ELoadingState::Unloaded;
			RESOURCE_LOG("Error! [%s] couldn't be loaded. CHECK WITH PORGAMMERS!", materialPath.c_str());
		}
		else
		{
			CFileWatcherWrapper::AddFileToWatch(this, materialPath.c_str(), &CModelFactory::MaterialChangedCallback);
			aNewInstance->myMaterialID = matID;
		}
		mat.myLoadingState = ELoadingState::Loaded;
	}
}

bool hse::gfx::CModelFactory::UnloadMaterial(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ModelFactory🔒::modelFactoryMutex);
	myMaterials[aID] = CMaterial();
	myMaterialRefCounts[aID] = 0;
	for (auto& val : myAssignedMaterialDs)
	{
		if (val.second == aID)
		{
			myAssignedMaterialDs.erase(val.first);
			break;
		}
	}
	myFreedIDs.push(aID);

	return true;
}

unsigned int hse::gfx::CModelFactory::GetMaterialID(const std::string & aMaterialFilePath)
{
	std::unique_lock<std::recursive_mutex> lg(ModelFactory🔒::modelFactoryMutex);
	if (myAssignedMaterialDs.find(std::string(aMaterialFilePath)) != myAssignedMaterialDs.end())
	{
		myMaterialRefCounts[myAssignedMaterialDs[std::string(aMaterialFilePath)]] += 1;
		return myAssignedMaterialDs[std::string(aMaterialFilePath)];
	}
	else
	{
		if (myCurrentlyFreeMaterialID > (MAX_MATERIAL_COUNT - 1) && myFreedIDs.empty() != true)
		{
			assert(false && "Too many materials created");
			RESOURCE_LOG("WARNING! Too many materials created.");
			return UINT_MAX;
		}

		unsigned int freeID = UINT_MAX;
		if (myFreedIDs.empty() == false)
		{
			freeID = myFreedIDs.front();
			myFreedIDs.pop();
		}
		else
		{
			freeID = myCurrentlyFreeMaterialID++;
		}

		myAssignedMaterialDs[std::string(aMaterialFilePath)] = freeID;
		myMaterialRefCounts[freeID] += 1;
		return freeID;
	}
}

const CMaterial& hse::gfx::CModelFactory::GetMaterialWithID(const unsigned int aID) const
{
	std::unique_lock<std::recursive_mutex> lg(ModelFactory🔒::modelFactoryMutex);
	return myMaterials[aID];
}

CMaterial& hse::gfx::CModelFactory::GetMaterialWithID(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ModelFactory🔒::modelFactoryMutex);
	return myMaterials[aID];
}

void hse::gfx::CModelFactory::RemoveMaterialRefCount(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ModelFactory🔒::modelFactoryMutex);
	myMaterialRefCounts[aID] -= 1;
	if (myMaterialRefCounts[aID] <= 0)
	{
		UnloadMaterial(aID);
	}
}

void hse::gfx::CModelFactory::AddMaterialRefCount(const unsigned int aID)
{
	std::unique_lock<std::recursive_mutex> lg(ModelFactory🔒::modelFactoryMutex);
	myMaterialRefCounts[aID] += 1;
}

bool hse::gfx::CModelFactory::MaterialExists(const std::string & aMaterialFilePath) const
{
	std::unique_lock<std::recursive_mutex> lg(ModelFactory🔒::modelFactoryMutex);
	if (myAssignedMaterialDs.find(aMaterialFilePath) != myAssignedMaterialDs.end())
	{
		return true;
	}
	return false;
}

void CModelFactory::Create(hse::CFileWatcherWrapper& aFileWatcher)
{
	if (ourInstance != nullptr)
	{
		assert(false && "Instance already created.");
		return;
	}
	ourInstance = hse_new(CModelFactory(aFileWatcher));
}

void CModelFactory::Destroy()
{
	hse_delete(ourInstance);
}

CModelInstance CModelFactory::CreateModel(const char* aFilePath)
{
	CModelInstance newInstance;
	
	CreateModelExisting(aFilePath, newInstance, "Data/Materials/DEFAULT.material");

	return newInstance;
}

void hse::gfx::CModelFactory::CreateModelExisting(const std::string aFilePath, CModelInstance& aInstance, const std::string aMaterialFilePath)
{
	aInstance.myModelName = GetFilename(std::string(aFilePath));
	if (FileExists(aFilePath.c_str()) == false)
	{
		RESOURCE_LOG("[MODEL] ERROR! (%s) does not exist.", aFilePath.c_str());
		aInstance.myLoadingState = CModelInstance::ELoadingState::FailedLoading;
		return;
	}

	CResourceManager* resourceManager = CResourceManager::Get();

	if (resourceManager->ModelExists(aFilePath.c_str()))
	{
		aInstance.myModelID = resourceManager->GetModelID(aFilePath.c_str());
	}
	else
	{
		if (LoadModel(aFilePath.c_str(), aMaterialFilePath.c_str(), &aInstance) == false)
		{
			aInstance.myLoadingState = CModelInstance::ELoadingState::FailedLoading;
			return;
		}

		myFileWatcher.AddFileToWatch(this, aFilePath.c_str(), &ModelChangedCallback);
	}
	if (aInstance.myModelID != UINT_MAX)
	{
		CModel& model(resourceManager->GetModelWithID(aInstance.myModelID));
		while (model.myIsLoaded == false)
		{
			std::this_thread::yield();
		}
		const float maxRadius = model.myMaxRadius;
		aInstance.myFrustumCollider.SetRadius(maxRadius * aInstance.myScaleToUseForInit.GetLargestComponent());
	}
	aInstance.myMaterialID = GetMaterialID(aMaterialFilePath);
	aInstance.myLoadingState = CModelInstance::ELoadingState::Loaded;
}

void hse::gfx::CModelFactory::CreateCustomSphere(CModelInstance& aInstance, const CU::Vector3f& aColor, const float aRadius, const unsigned int aRingCount, const unsigned int aSliceCount)
{
	if (aRadius <= 0.0f)
	{
		ENGINE_LOG("[CModelFactory] ERROR! Failed to load a custom sphere because the radious was %f.", aRadius);
		return;
	}
	if (aRingCount < 2)
	{
		ENGINE_LOG("[CModelFactory] ERROR! Failed to load a custom sphere because amount of the rings (%i) is too small.", aRingCount);
		return;
	}
	if (aSliceCount < 3)
	{
		ENGINE_LOG("[CModelFactory] ERROR! Failed to load a custom sphere because amount of the points per ring (%i) is too small.", aSliceCount);
		return;
	}

	CResourceManager* resourceManager = CResourceManager::Get();
	if (resourceManager == nullptr)
	{
		RESOURCE_LOG("[CModelFactory] ERROR! Failed to create a custom sphere because resource manager was nullptr.");
		return;
	}

	// Model

	const std::string modelName = "customSphere_[" + std::to_string(aColor.x) + ", " + std::to_string(aColor.y) + ", " + std::to_string(aColor.z) + "]" +
		"_" + std::to_string(aRadius) + "_" + std::to_string(aRingCount) + "_" + std::to_string(aSliceCount);

	const bool modelExists(resourceManager->ModelExists(modelName.c_str()));
	START_TIMER(loadTimer);
	if (modelExists)
	{
		aInstance.myModelID = resourceManager->GetModelID(modelName.c_str());
	}
	else
	{
		const unsigned int modelID = resourceManager->GetModelID(modelName.c_str());
		CModel& model(resourceManager->GetModelWithID(modelID));

		const unsigned int vertexBufferID = resourceManager->GetVertexBufferID(modelID);
		CDX11VertexBuffer& vertexBuffer = resourceManager->GetVertexBufferWithID(vertexBufferID);
		CU::GrowingArray<unsigned int, unsigned int> indexList;
		vertexBuffer.CreateSphere(indexList, aColor, aRadius, aRingCount, aSliceCount);
		model.myVertexBufferID = vertexBufferID;

		const unsigned int indexBufferID = resourceManager->GetIndexBufferID(modelID);
		CDX11IndexBuffer& indexBuffer = resourceManager->GetIndexBufferWithID(indexBufferID);
		indexBuffer.CreateBuffer(indexList);
		model.myIndexBufferID = indexBufferID;
		indexList.RemoveAll();

		aInstance.myModelID = modelID;
	}

	// Material

	const std::string materialPath("Data/Materials/DEFAULT_COLOR.material");
	if (MaterialExists(materialPath.c_str()))
	{
		aInstance.myMaterialID = GetMaterialID(materialPath.c_str());
	}
	else
	{
		const unsigned int matID(GetMaterialID(materialPath.c_str()));
		CMaterial& mat(GetMaterialWithID(matID));
		if (mat.Init(materialPath.c_str()) == false)
		{
			RemoveMaterialRefCount(matID);
			RESOURCE_LOG("WARNING! [%s] couldn't be loaded, using default material.", materialPath.c_str());
		}
		else
		{
			aInstance.myMaterialID = matID;
		}
	}

	int time = 0;
	END_TIMER(loadTimer, time);
	if (!modelExists)
	{
		RESOURCE_LOG("[MODEL] Sphere took %i ms to load. Sphere name: \"%s\".", time, modelName.c_str());
	}

	aInstance.myModelName = modelName.c_str();
	aInstance.myLoadingState = CModelInstance::ELoadingState::Loaded;
}

CModelInstance CModelFactory::CreatePrimitiveTriangle()
{
	CModelInstance instance;
	CResourceManager* resourceManager = CResourceManager::Get();

	if (resourceManager->ModelExists("triangle"))
	{
		instance.myModelID = resourceManager->GetModelID("triangle");
	}
	else
	{
		START_TIMER(loadTimer);
		unsigned int modelID = resourceManager->GetModelID("triangle");
		CModel& model(resourceManager->GetModelWithID(modelID));

		unsigned int vertexBufferID = resourceManager->GetVertexBufferID(modelID);
		CDX11VertexBuffer& vertexBuffer = resourceManager->GetVertexBufferWithID(vertexBufferID);
		vertexBuffer.CreatePrimitiveTriangleBuffer();
		model.myVertexBufferID = vertexBufferID;


		if (resourceManager->ShaderExists("Data/Shaders/PrimitiveVertexShader.hlsl"))
		{
			model.myVertexShaderID = resourceManager->GetShaderID("Data/Shaders/PrimitiveVertexShader.hlsl");
		}
		else
		{
			unsigned int vertexShaderID = resourceManager->GetShaderID("Data/Shaders/PrimitiveVertexShader.hlsl");
			CDX11Shader& vertexShader(resourceManager->GetShaderWithID(vertexShaderID));
			vertexShader.InitVertex("Data/Shaders/PrimitiveVertexShader.hlsl", "VSMain");
			model.myVertexShaderID = vertexShaderID;
		}


		if (resourceManager->ShaderExists("Data/Shaders/PrimitivePixelShader.hlsl"))
		{
			model.myPixelShaderID = resourceManager->GetShaderID("Data/Shaders/PrimitivePixelShader.hlsl");
		}
		else
		{
			unsigned int pixelShaderID = resourceManager->GetShaderID("Data/Shaders/PrimitivePixelShader.hlsl");
			CDX11Shader& pixelShader(resourceManager->GetShaderWithID(pixelShaderID));
			pixelShader.InitPixel("Data/Shaders/PrimitivePixelShader.hlsl", "PSMain");
			model.myPixelShaderID = pixelShaderID;
		}


		instance.myModelID = modelID;

		int time = 0;
		END_TIMER(loadTimer, time);
		RESOURCE_LOG("[MODEL] Cube took %i ms to load.", time);
	}
	
	instance.myModelName = "Triangle";
	instance.myLoadingState = CModelInstance::ELoadingState::Loaded;
	return instance;
}

CModelInstance hse::gfx::CModelFactory::CreateCube()
{
	CModelInstance instance;

	CResourceManager* resourceManager = CResourceManager::Get();

	if (resourceManager->ModelExists("cube"))
	{
		instance.myModelID = resourceManager->GetModelID("cube");
	}
	else
	{
		START_TIMER(loadTimer);
		unsigned int modelID = resourceManager->GetModelID("cube");
		CModel& model(resourceManager->GetModelWithID(modelID));

		unsigned int vertexBufferID = resourceManager->GetVertexBufferID(modelID);
		CDX11VertexBuffer& vertexBuffer = resourceManager->GetVertexBufferWithID(vertexBufferID);
		vertexBuffer.CreateCube();
		model.myVertexBufferID = vertexBufferID;

		unsigned int indexBufferID = resourceManager->GetIndexBufferID(modelID);
		CDX11IndexBuffer& indexBuffer = resourceManager->GetIndexBufferWithID(indexBufferID);
		indexBuffer.CreateCubeIndex();
		model.myIndexBufferID = indexBufferID;

		unsigned int vertexShaderID = resourceManager->GetShaderID("Data/Shaders/PrimitiveVertexShader.hlsl");
		CDX11Shader& vertexShader(resourceManager->GetShaderWithID(vertexShaderID));
		vertexShader.InitVertex("Data/Shaders/PrimitiveVertexShader.hlsl", "VSMain");
		model.myVertexShaderID = vertexShaderID;

		unsigned int pixelShaderID = resourceManager->GetShaderID("Data/Shaders/PrimitivePixelShader.hlsl");
		CDX11Shader& pixelShader(resourceManager->GetShaderWithID(pixelShaderID));
		pixelShader.InitPixel("Data/Shaders/PrimitivePixelShader.hlsl", "PSMain");
		model.myPixelShaderID = pixelShaderID;

		instance.myModelID = modelID;

		int time = 0;
		END_TIMER(loadTimer, time);
		RESOURCE_LOG("[MODEL] Cube took %i ms to load.", time);
	}

	instance.myModelName = "Cube";
	instance.myLoadingState = CModelInstance::ELoadingState::Loaded;
	return instance;
}

CModelInstance hse::gfx::CModelFactory::CreateTexturedCube(const wchar_t* aTexturePath)
{
	CModelInstance instance;

	CResourceManager* resourceManager = CResourceManager::Get();

	if (resourceManager->ModelExists("texturedCube"))
	{
		instance.myModelID = resourceManager->GetModelID("texturedCube");
	}
	else
	{
		START_TIMER(loadTimer);
		unsigned int modelID = resourceManager->GetModelID("texturedCube");
		CModel& model(resourceManager->GetModelWithID(modelID));

		unsigned int vertexBufferID = resourceManager->GetVertexBufferID(modelID);
		CDX11VertexBuffer& vertexBuffer = resourceManager->GetVertexBufferWithID(vertexBufferID);
		vertexBuffer.CreateTexturedCube();
		model.myVertexBufferID = vertexBufferID;

		unsigned int indexBufferID = resourceManager->GetIndexBufferID(modelID);
		CDX11IndexBuffer& indexBuffer = resourceManager->GetIndexBufferWithID(indexBufferID);
		indexBuffer.CreateCubeIndex();
		model.myIndexBufferID = indexBufferID;

		unsigned int vertexShaderID = resourceManager->GetShaderID("Data/Shaders/TexturedPrimitiveVS.hlsl");
		CDX11Shader& vertexShader(resourceManager->GetShaderWithID(vertexShaderID));
		vertexShader.InitVertex("Data/Shaders/TexturedPrimitiveVS.hlsl", "VSMain");
		model.myVertexShaderID = vertexShaderID;

		unsigned int pixelShaderID = resourceManager->GetShaderID("Data/Shaders/TexturedPrimitivePS.hlsl");
		CDX11Shader& pixelShader(resourceManager->GetShaderWithID(pixelShaderID));
		pixelShader.InitPixel("Data/Shaders/TexturedPrimitivePS.hlsl", "PSMain");
		model.myPixelShaderID = pixelShaderID;

		unsigned int textureID = resourceManager->GetTextureID(aTexturePath);
		CDX11Texture& texture(resourceManager->GetTextureWithID(textureID));
		texture.CreateTexture(aTexturePath, ETextureType::Diffuse);
		model.myTextureID[0] = textureID;

		instance.myModelID = modelID;
		int time = 0;
		END_TIMER(loadTimer, time);
		RESOURCE_LOG("[MODEL] TexturedCube took %i ms to load.", time);
	}

	instance.myModelName = "TexturedCube";
	instance.myLoadingState = CModelInstance::ELoadingState::Loaded;
	return instance;
}

hse::gfx::CModelInstance hse::gfx::CModelFactory::CreateSkyboxCube(const char* aSkyboxPath)
{
	CModelInstance instance;

	CResourceManager* resourceManager = CResourceManager::Get();

	if (resourceManager->ModelExists("InvertedCube"))
	{
		instance.myModelID = resourceManager->GetModelID("InvertedCube");
	}
	else
	{
		START_TIMER(loadTimer);
		unsigned int modelID = resourceManager->GetModelID("InvertedCube");
		CModel& model(resourceManager->GetModelWithID(modelID));

		unsigned int vertexBufferID = resourceManager->GetVertexBufferID(modelID);
		CDX11VertexBuffer& vertexBuffer = resourceManager->GetVertexBufferWithID(vertexBufferID);
		vertexBuffer.CreateCube();
		model.myVertexBufferID = vertexBufferID;

		unsigned int indexBufferID = resourceManager->GetIndexBufferID(modelID);
		CDX11IndexBuffer& indexBuffer = resourceManager->GetIndexBufferWithID(indexBufferID);
		indexBuffer.CreateInvertedCubeIndex();
		model.myIndexBufferID = indexBufferID;

		unsigned int vertexShaderID = resourceManager->GetShaderID("Data/Shaders/SkyboxVS.hlsl");
		CDX11Shader& vertexShader(resourceManager->GetShaderWithID(vertexShaderID));
		vertexShader.InitVertex("Data/Shaders/SkyboxVS.hlsl", "VSMain");
		model.myVertexShaderID = vertexShaderID;

		unsigned int pixelShaderID = resourceManager->GetShaderID("Data/Shaders/SkyboxPS.hlsl");
		CDX11Shader& pixelShader(resourceManager->GetShaderWithID(pixelShaderID));
		pixelShader.InitPixel("Data/Shaders/SkyboxPS.hlsl", "PSMain");
		model.myPixelShaderID = pixelShaderID;

		//const wchar_t* texturePath = L"Data/Models/Misc/test_cubemap.dds";

		if (resourceManager->TextureExists(aSkyboxPath))
		{
			model.myTextureID[0] = resourceManager->GetTextureID(aSkyboxPath);
		}
		else
		{
			unsigned int textureID = resourceManager->GetTextureID(aSkyboxPath);
			CDX11Texture& texture(resourceManager->GetTextureWithID(textureID));
			if (texture.CreateTexture(aSkyboxPath, ETextureType::Skymap) == false)
			{
				RESOURCE_LOG("(%s) could not be loaded, textures may appear inaccurate.", aSkyboxPath);
			}
			else
			{
				model.myTextureID[0] = textureID;
			}
		}

		instance.myModelID = modelID;
		int time = 0;
		END_TIMER(loadTimer, time);
		RESOURCE_LOG("[MODEL] InvertedCube took %i ms to load.", time);
	}

	instance.myModelName = "InvertedCube";
	instance.myLoadingState = CModelInstance::ELoadingState::Loaded;
	return instance;
}

unsigned int hse::gfx::CModelFactory::CreateSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef)
{
	return mySpriteFactory.CreateSprite(aSpritePath, aTextureSizeRef);
}

unsigned int hse::gfx::CModelFactory::Create3DSprite(const char * aSpritePath, CU::Vector2f & aTextureSizeRef)
{
	return mySpriteFactory.Create3DSprite(aSpritePath, aTextureSizeRef);
}
