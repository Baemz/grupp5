#pragma once
#include "../ModelInstance.h"
#include "../../FBXLoader/FBXLoader.h"
#include "../../Sprite/SpriteFactory.h"
#include <queue>

#define MAX_MATERIAL_COUNT 500

namespace hse
{
	class CFileWatcherWrapper;
};

namespace hse { namespace gfx {

	class CScene;
	class CModel;
	class CMaterial;
	class CModelFactory
	{
		friend class CRenderer2D;
		friend class CSpriteRenderer3D;
	public:
		~CModelFactory();

		static void Create(hse::CFileWatcherWrapper& aFileWatcher);
		static void Destroy();
		static CModelFactory* Get() { return ourInstance; };

		CModelInstance CreateModel(const char* aFilePath);
		void CreateModelExisting(const std::string aFilePath, CModelInstance& aInstance, const std::string aMaterialFilePath);
		void CreateCustomSphere(CModelInstance& aInstance, const CU::Vector3f& aColor, const float aRadious, const unsigned int aRingCount, const unsigned int aPointPerRingCount);
		CModelInstance CreatePrimitiveTriangle();
		CModelInstance CreateCube();
		CModelInstance CreateTexturedCube(const wchar_t* aTexturePath);
		CModelInstance CreateSkyboxCube(const char* aSkyboxPath = "Data/Models/Misc/test_cubemap.dds");
		unsigned int CreateSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef);
		unsigned int Create3DSprite(const char* aSpritePath, CU::Vector2f& aTextureSizeRef);
		CMaterial& GetMaterialWithID(const unsigned int aID);
		void RemoveMaterialRefCount(const unsigned int aID);
		void AddMaterialRefCount(const unsigned int aID);

	private:
		static bool ModelChangedCallback(void* aThis, const char* aModelChanged);
		static bool MaterialChangedCallback(void* aThis, const char* aMaterialChanged);

		CModelFactory(hse::CFileWatcherWrapper& aFileWatcher);
		bool LoadModel(const char* aFilePath, const char* aMaterialFilePath, CModelInstance* aNewInstance);
		bool ReloadModel(const char* aFilePath);
		void UseDefaultTexture(CModel& aModel);
		void UseDefaultMaterialID(CModelInstance* aNewInstance);

		bool MaterialExists(const std::string& aMaterialFilePath) const;
		bool UnloadMaterial(const unsigned int aID);
		unsigned int GetMaterialID(const std::string& aMaterialFilePath);
		const CMaterial& GetMaterialWithID(const unsigned int aID) const;

	private:
		static CModelFactory* ourInstance;

		CSpriteFactory mySpriteFactory;
		CFBXLoader myFBXLoader;
		std::map<std::string, unsigned int> myAssignedMaterialDs;
		std::map<unsigned int, unsigned int> myMaterialRefCounts;
		std::queue<unsigned int> myFreedIDs;
		unsigned int myCurrentlyFreeMaterialID;
		CMaterial* myMaterials;
		hse::CFileWatcherWrapper& myFileWatcher;

	};
}}
