#include "stdafx.h"
#include "ModelInstance.h"
#include "GraphicsEngineInterface.h"
#include "..\EngineCore\WorkerPool\WorkerPool.h"
#include "Loading\ModelFactory.h"

using namespace hse::gfx;

CModelInstance::CModelInstance()
	: myModelID(UINT_MAX)
	, myMaterialID(UINT_MAX)
	, myModelName("")
	, myLoadingState(ELoadingState::Unloaded)
	, myScale(1.f, 1.f, 1.f)
	, myRadious(0.0f)
	, myAffectedByFog(true)
	, myMaterialPath("Data/Materials/DEFAULT.material")
	, myIsCullable(true)
{
}

hse::gfx::CModelInstance::CModelInstance(const CModelInstance& aModelInstance)
{
	myModelName = aModelInstance.myModelName;
	myModelPath = aModelInstance.myModelPath;
	myOrientation = aModelInstance.myOrientation;
	myScaleToUseForInit = aModelInstance.myScaleToUseForInit;
	myScale = aModelInstance.myScale;
	myFrustumCollider = aModelInstance.myFrustumCollider;
	myModelID = aModelInstance.myModelID;
	myMaterialID = aModelInstance.myMaterialID;
	myLoadingState = aModelInstance.myLoadingState;
	myAffectedByFog = aModelInstance.myAffectedByFog;
	myIsCullable = aModelInstance.myIsCullable;

	CModelFactory* modelFactory = CModelFactory::Get();
	if (modelFactory && myMaterialID != UINT_MAX)
	{
		modelFactory->AddMaterialRefCount(myMaterialID);
	}
}


CModelInstance::~CModelInstance()
{
	CModelFactory* modelFactory = CModelFactory::Get();
	if (modelFactory && myMaterialID != UINT_MAX)
	{
		modelFactory->RemoveMaterialRefCount(myMaterialID);
	}
}

void hse::gfx::CModelInstance::operator=(const CModelInstance & aModelInstance)
{
	myModelName = aModelInstance.myModelName;
	myModelPath = aModelInstance.myModelPath;
	myOrientation = aModelInstance.myOrientation;
	myScaleToUseForInit = aModelInstance.myScaleToUseForInit;
	myScale = aModelInstance.myScale;
	myFrustumCollider = aModelInstance.myFrustumCollider;
	myModelID = aModelInstance.myModelID;
	myMaterialID = aModelInstance.myMaterialID;
	myLoadingState = aModelInstance.myLoadingState;
	myAffectedByFog = aModelInstance.myAffectedByFog;
	myIsCullable = aModelInstance.myIsCullable;

	CModelFactory* modelFactory = CModelFactory::Get();
	if (modelFactory && myMaterialID != UINT_MAX)
	{
		modelFactory->AddMaterialRefCount(myMaterialID);
	}
}

void hse::gfx::CModelInstance::Init()
{
	CModelFactory* modelFactory = CModelFactory::Get();
	if (modelFactory)
	{
		if (myLoadingState == ELoadingState::Unloaded && myModelPath != "")
		{
			myLoadingState = ELoadingState::Loading;
 			WORKER_POOL_QUEUE_WORK(&CModelFactory::CreateModelExisting, CModelFactory::Get(), myModelPath, std::ref<CModelInstance>(*this), myMaterialPath);
		}
	}
	else
	{
		ENGINE_LOG("ERROR! No ModelFactory created.");
	}
	
}

void hse::gfx::CModelInstance::InitCustomSphere(const CU::Vector3f& aColor, const float aRadious, const unsigned int aRingCount, const unsigned int aPointPerRingCount)
{
	if (CModelFactory::Get() != nullptr)
	{
		if (myLoadingState == ELoadingState::Unloaded)
		{
			myLoadingState = ELoadingState::Loading;
			WORKER_POOL_QUEUE_WORK(&CModelFactory::CreateCustomSphere, CModelFactory::Get(), std::ref<CModelInstance>(*this), aColor, aRadious, aRingCount, aPointPerRingCount);
		}
		else
		{
			ENGINE_LOG("[CModelInstance] WARNING! Tried to load a custom sphere on a model that is already loaded or is currently loading its model. The custom sphere will not be loaded.");
		}
	}
	else
	{
		ENGINE_LOG("ERROR! No ModelFactory created.");
	}
}

void hse::gfx::CModelInstance::Move(const CU::Vector3f & aValue)
{
	myOrientation.SetPosition(myOrientation.GetPosition() + aValue);
	myFrustumCollider.SetPosition(myOrientation.GetPosition());
}

void hse::gfx::CModelInstance::SetScale(const CU::Vector3f& aScale)
{
	myScale = aScale;
}

void hse::gfx::CModelInstance::SetScaleForInit(const CU::Vector3f & aScale)
{
	myScaleToUseForInit = aScale;
}

void hse::gfx::CModelInstance::Rotate(float aValue)
{
	CU::Vector3f aPos = myOrientation.GetPosition();
	myOrientation *= myOrientation.CreateRotateAroundY(aValue);
	myOrientation.SetPosition(aPos);
}

void hse::gfx::CModelInstance::Rotate(const CU::Vector3f& aRotation)
{
	CU::Vector3f aPrevPos = myOrientation.GetPosition();
	myOrientation *= myOrientation.CreateRotateAroundX(aRotation.y);
	myOrientation *= myOrientation.CreateRotateAroundY(aRotation.x);
	myOrientation *= myOrientation.CreateRotateAroundZ(aRotation.z);
	myOrientation.SetPosition(aPrevPos);
}

void hse::gfx::CModelInstance::SetPosition(const CU::Vector3f& aPosition)
{
	myOrientation.SetPosition(aPosition);
	myFrustumCollider.SetPosition(aPosition);
}

const CU::Matrix44f hse::gfx::CModelInstance::GetOrientation() const
{
	return myOrientation;
}

void hse::gfx::CModelInstance::SetOrientation(const CU::Matrix44f & aOrientation)
{
	myOrientation = aOrientation;
	myFrustumCollider.SetPosition(myOrientation.GetPosition());
}

void hse::gfx::CModelInstance::SetModelPath(const std::string& aModelPath)
{
	myModelPath = aModelPath;
}

void hse::gfx::CModelInstance::SetMaterialPath(const std::string & aMaterialPath)
{
	myMaterialPath = aMaterialPath;
}

void hse::gfx::CModelInstance::SetAffectedByFog(const bool aAffectedByFog)
{
	myAffectedByFog = aAffectedByFog;
}
void hse::gfx::CModelInstance::SetIsCullable(const bool aIsCullable)
{
	myIsCullable = aIsCullable;
}

void hse::gfx::CModelInstance::UpdateColliderPosition()
{
	myFrustumCollider.SetPosition(myOrientation.GetPosition());
}

const bool hse::gfx::CModelInstance::IsLoaded() const
{
	if (myLoadingState == ELoadingState::Loaded)
	{
		return true;
	}
	return false;
}

const bool hse::gfx::CModelInstance::IsLoading() const
{
	return (myLoadingState == ELoadingState::Loading);
}

const bool hse::gfx::CModelInstance::LoadingFailed() const
{
	return (myLoadingState == ELoadingState::FailedLoading);
}

const std::string& hse::gfx::CModelInstance::GetModelPath() const
{
	return myModelPath;
}

const CU::Vector3f hse::gfx::CModelInstance::GetPosition() const
{
	return myOrientation.GetPosition();
}

const float hse::gfx::CModelInstance::GetZValue() const
{
	return myOrientation[14];
}

void hse::gfx::CModelInstance::Render()
{
	if (myLoadingState == ELoadingState::Loaded && myModelID != UINT_MAX)
	{
		CGraphicsEngineInterface::AddToScene(*this);
	}
}
