#pragma once

namespace hse { namespace gfx {

	class CDX11VertexBuffer;
	class CDX11Shader;
	class CDX11IndexBuffer;
	class CDX11Texture;

	class CModel
	{
		friend class CModelFactory;
		friend class CResourceManager;
		friend class CForwardRenderer;
		friend class CDeferredRenderer;
		friend class CSkyboxRenderer;
		friend class CDebugRenderer;
	public:
		~CModel();

	private:
		CModel();
		volatile bool myIsLoaded;
		float myMaxRadius;
		unsigned int myVertexShaderID;
		unsigned int myPixelShaderID;
		unsigned int myVertexBufferID;
		unsigned int myIndexBufferID;

		unsigned int myAlternateTexture0;
		unsigned int myAlternateTexture1;

		unsigned int myTextureID[6];
	};

}}