#include "stdafx.h"
#include "Model.h"
#include "Loading/ModelFactory.h"

using namespace hse::gfx;

hse::gfx::CModel::~CModel()
{
	for (unsigned int i = 0; i < 6; ++i)
	{
		myTextureID[i] = UINT_MAX;
	}
}

CModel::CModel()
	: myPixelShaderID(UINT_MAX)
	, myVertexBufferID(UINT_MAX)
	, myVertexShaderID(UINT_MAX)
	, myIndexBufferID(UINT_MAX)
	, myTextureID{ UINT_MAX, UINT_MAX, UINT_MAX, UINT_MAX, UINT_MAX, UINT_MAX }
	, myAlternateTexture0(UINT_MAX), myAlternateTexture1(UINT_MAX)
	, myMaxRadius(0.f)
	, myIsLoaded(false)
{
}
