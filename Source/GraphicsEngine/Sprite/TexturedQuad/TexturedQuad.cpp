#include "stdafx.h"
#include "TexturedQuad.h"

namespace hse { namespace gfx {

	CTexturedQuad::CTexturedQuad()
		: myVertexBufferID(UINT_MAX)
		, myIndexBufferID(UINT_MAX)
		, myTextureID(UINT_MAX)
		, myVertexShaderID(UINT_MAX)
		, myPixelShaderID(UINT_MAX)
	{
	}


	CTexturedQuad::~CTexturedQuad()
	{
	}
}}
