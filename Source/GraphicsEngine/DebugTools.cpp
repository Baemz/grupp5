#include "stdafx.h"
#include "DebugTools.h"
#include "Utilities\VertexStructs.h"
namespace hse {
	namespace gfx {

		CDebugTools* CDebugTools::ourInstance = nullptr;
		CDebugTools::CDebugTools()
			: myFreeIndex(0)
			, myReadIndex(1)
			, myWriteIndex(2)
		{
			myDebugDataBuffers[myFreeIndex].myDebug2DLineData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebug2DLineData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebug2DLineData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugLineData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebugLineData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebugLineData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugCubeData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebugCubeData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebugCubeData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugRectangle3DData.Reserve(200000);
			myDebugDataBuffers[myReadIndex].myDebugRectangle3DData.Reserve(200000);
			myDebugDataBuffers[myWriteIndex].myDebugRectangle3DData.Reserve(200000);

			myDebugDataBuffers[myFreeIndex].myDebugSphereData.Reserve(20000);
			myDebugDataBuffers[myReadIndex].myDebugSphereData.Reserve(20000);
			myDebugDataBuffers[myWriteIndex].myDebugSphereData.Reserve(20000);
		}


		CDebugTools::~CDebugTools()
		{
		}

		void CDebugTools::Create()
		{
			ourInstance = hse_new(CDebugTools());
		}

		void CDebugTools::Destroy()
		{
			hse_delete(ourInstance);
		}

		CDebugTools* CDebugTools::Get()
		{
			return ourInstance;
		}

		void CDebugTools::DrawLine(CU::Vector3f aStartPos, CU::Vector3f aEndPos, CU::Vector3f aStartColorRGB, CU::Vector3f aEndColorRGB, float aStartThickness, float aEndThickness)
		{
			SSimpleDebugVertex lineStart;
			lineStart.myPosition.x = aStartPos.x;
			lineStart.myPosition.y = aStartPos.y;
			lineStart.myPosition.z = aStartPos.z;
			lineStart.myPosition.w = 1.f;
			lineStart.myColor.r = aStartColorRGB.r;
			lineStart.myColor.g = aStartColorRGB.g;
			lineStart.myColor.b = aStartColorRGB.b;
			lineStart.myColor.a = 1.f;
			lineStart.Thicc = aStartThickness;

			SSimpleDebugVertex lineEnd;
			lineEnd.myPosition.x = aEndPos.x;
			lineEnd.myPosition.y = aEndPos.y;
			lineEnd.myPosition.z = aEndPos.z;
			lineEnd.myPosition.w = 1.f;
			lineEnd.myColor.r = aEndColorRGB.r;
			lineEnd.myColor.g = aEndColorRGB.g;
			lineEnd.myColor.b = aEndColorRGB.b;
			lineEnd.myColor.a = 1.f;
			lineEnd.Thicc = aEndThickness;

			myDebugDataBuffers[myWriteIndex].myDebugLineData.Add(lineStart);
			myDebugDataBuffers[myWriteIndex].myDebugLineData.Add(lineEnd);
		}

		void CDebugTools::DrawLine2D(CU::Vector2f aStartScreenPos, CU::Vector2f aEndScreenPos, CU::Vector3f aStartColorRGB, CU::Vector3f aEndColorRGB)
		{
			SSimpleDebugVertex lineStart;
			lineStart.myPosition.x = aStartScreenPos.x;
			lineStart.myPosition.y = aStartScreenPos.y;
			lineStart.myPosition.z = 1.f;
			lineStart.myPosition.w = 1.f;
			lineStart.myColor.r = aStartColorRGB.r;
			lineStart.myColor.g = aStartColorRGB.g;
			lineStart.myColor.b = aStartColorRGB.b;
			lineStart.myColor.a = 1.f;
			lineStart.Thicc = 1.f;

			SSimpleDebugVertex lineEnd;
			lineEnd.myPosition.x = aEndScreenPos.x;
			lineEnd.myPosition.y = aEndScreenPos.y;
			lineEnd.myPosition.z = 1.f;
			lineEnd.myPosition.w = 1.f;
			lineEnd.myColor.r = aEndColorRGB.r;
			lineEnd.myColor.g = aEndColorRGB.g;
			lineEnd.myColor.b = aEndColorRGB.b;
			lineEnd.myColor.a = 1.f;
			lineEnd.Thicc = 1.f;

			myDebugDataBuffers[myWriteIndex].myDebug2DLineData.Add(lineStart);
			myDebugDataBuffers[myWriteIndex].myDebug2DLineData.Add(lineEnd);
		}

		void CDebugTools::DrawCube(const CU::Vector3f aPos, const float aSize, const CU::Vector3f aCubeRGB)
		{
			SSimpleDebugVertex cubePos;
			cubePos.myPosition = { aPos, 1.f };
			cubePos.myColor = { aCubeRGB, 1.f };
			cubePos.Thicc = aSize;
			myDebugDataBuffers[myWriteIndex].myDebugCubeData.Add(cubePos);
		}

		void CDebugTools::DrawRectangle3D(const CU::Vector3f aPos, const CU::Vector3f aSizes, const CU::Vector3f aRectangleRGB)
		{
			SSimpleDebugVertex rectanglePos;
			rectanglePos.myPosition = { aPos, aSizes.x };
			rectanglePos.myColor = { aRectangleRGB, aSizes.y };
			rectanglePos.Thicc = aSizes.z;
			myDebugDataBuffers[myWriteIndex].myDebugRectangle3DData.Add(rectanglePos);
		}

		void CDebugTools::DrawText2D(const std::string& aText, const CU::Vector2f& aPosition, float aSize, const CU::Vector3f& aColor)
		{
			CText newText;
			newText.Init("Comic Sans MS", aText, aSize, { 0.0f }, aPosition, CText::ColorFloatToUint(aColor));
			newText.Render();
		}
		
		void CDebugTools::DrawSphere(const CU::Vector3f& aPos, const CU::Vector3f& aColor, const CU::Vector3f& aRotation,
			const float aRadious, const unsigned int aRingCount, const unsigned int aSliceCount)
		{
			myDebugDataBuffers[myWriteIndex].myDebugSphereData.Add(CModelInstance());

			CModelInstance& instance_ref(myDebugDataBuffers[myWriteIndex].myDebugSphereData.GetLast());
			instance_ref.InitCustomSphere(aColor, aRadious, aRingCount, aSliceCount);
			instance_ref.SetPosition(aPos);
			instance_ref.Rotate(aRotation);
		}

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& CDebugTools::GetDebug2DLineData() const
		{
			return myDebugDataBuffers[myReadIndex].myDebug2DLineData;
		}

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& CDebugTools::GetDebugCubeData() const
		{
			return myDebugDataBuffers[myReadIndex].myDebugCubeData;
		}

		const CU::GrowingArray<SSimpleDebugVertex, unsigned int>& CDebugTools::GetRectangle3DData()
		{
			return myDebugDataBuffers[myReadIndex].myDebugRectangle3DData;
		}

		const CU::GrowingArray<hse::gfx::CModelInstance, unsigned int>& CDebugTools::GetDebugSphereData() const
		{
			return myDebugDataBuffers[myReadIndex].myDebugSphereData;
		}

		void CDebugTools::ChangeUpdateBuffer()
		{
			std::unique_lock<std::mutex> bufferLock(myBufferLock);
			Swap(myWriteIndex, myFreeIndex);
			bufferLock.unlock();
			myDebugDataBuffers[myWriteIndex].RemoveAll();
			myDebugDataBuffers[myWriteIndex].myIsRead = false;
		}

		void CDebugTools::ChangeRenderBuffer()
		{
			std::unique_lock<std::mutex> bufferLock(myBufferLock);
			if (myDebugDataBuffers[myFreeIndex].myIsRead == false)
			{
				Swap(myReadIndex, myFreeIndex);
			}
			bufferLock.unlock();
			myDebugDataBuffers[myReadIndex].myIsRead = true;
		}

	}
}