#include "stdafx.h"
#include "..\EngineCore\CrashReport.h"
#include "..\EngineCore\Engine.h"
#include "..\Game\Game.h"
#include "..\EngineCore\CommandLineManager\CommandLineManager.h"

using namespace std::placeholders;

#ifndef _RETAIL
#define USE_CONSOLE
#endif // !_RETAIL

LONG WINAPI ExceptionFilterFunction(_EXCEPTION_POINTERS* aExceptionP)
{
	hse::CreateMiniDump(aExceptionP);
	return EXCEPTION_EXECUTE_HANDLER;
}


void Run(hse::CEngine& aEngine, hse::SEngineStartParams& aStartParam, CGame& aGame);
int WINAPI wWinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nShowCmd)
{
	SetUnhandledExceptionFilter(ExceptionFilterFunction);

#ifdef USE_CONSOLE
	AllocConsole();
	FILE* filePointerPointer1 = new FILE();
	SetWindowPos(GetConsoleWindow(), nullptr, 1920, 0, 800, 640, SWP_NOSIZE | SWP_NOZORDER);
	freopen_s(&filePointerPointer1, "conin$", "r", stdin);
	FILE* filePointerPointer2 = new FILE();
	freopen_s(&filePointerPointer2, "conout$", "w", stdout);
	FILE* filePointerPointer3 = new FILE();
	freopen_s(&filePointerPointer3, "conout$", "w", stderr);
#endif 


	hInstance; hPrevInstance; lpCmdLine; nShowCmd;
	{
		hse::CEngine engine;
		CGame game;
		hse::SEngineStartParams startParams;
		Run(engine, startParams, game);
	}

	FreeConsole();

	return 0;
}

void Run(hse::CEngine& aEngine, hse::SEngineStartParams& aStartParam, CGame& aGame)
{
	__try
	{
		hse::CCommandLineManager::Init();
		aStartParam.initCallback = std::bind(&CGame::Init, &aGame);
		aStartParam.updateCallback = std::bind(&CGame::Update, &aGame, _1);
		aEngine.Init(aStartParam);
		aEngine.Start();
	}
	__except (ExceptionFilterFunction(GetExceptionInformation()))
	{

	}
}