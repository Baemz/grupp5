#pragma once
#include <Windows.h>
#include <thread>
#include <sstream>

namespace CommonUtilities
{
	class ThreadHelper
	{
	public:
		static void SetThreadName(const std::thread& aThread, const std::string aNewThreadName)
		{
			SetThreadName(aThread.get_id(), aNewThreadName);
		}
		static void SetThreadName(const std::thread* aThread, const std::string aNewThreadName)
		{
			SetThreadName(aThread->get_id(), aNewThreadName);
		}
		static void SetThreadName(const std::thread::id& aThreadId, const std::string aNewThreadName)
		{
			std::stringstream threadId;
			threadId << aThreadId;
			SetThreadNameWindowsFunction(std::stoul(threadId.str()), aNewThreadName.c_str());
		}

	private:
		//  
		// Usage: SetThreadName ((DWORD)-1, "MainThread");  
		//  
		static const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)  
		typedef struct tagTHREADNAME_INFO
		{
			DWORD dwType; // Must be 0x1000.  
			LPCSTR szName; // Pointer to name (in user addr space).  
			DWORD dwThreadID; // Thread ID (-1=caller thread).  
			DWORD dwFlags; // Reserved for future use, must be zero.  
		} THREADNAME_INFO;
#pragma pack(pop)  
		static void SetThreadNameWindowsFunction(DWORD dwThreadID, const char* threadName) {
			THREADNAME_INFO info;
			info.dwType = 0x1000;
			info.szName = threadName;
			info.dwThreadID = dwThreadID;
			info.dwFlags = 0;
#pragma warning(push)  
#pragma warning(disable: 6320 6322)  
			__try {
				RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
			}
			__except (EXCEPTION_EXECUTE_HANDLER) {
			}
#pragma warning(pop)  
		}
	};
}

namespace CU = CommonUtilities;
