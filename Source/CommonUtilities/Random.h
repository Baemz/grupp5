#pragma once

namespace CommonUtilities
{
	void InitRandom();
	void InitRandom(const unsigned int aSeed);
	long long GetRandom();

	float GetRandomPercentage();
	float GetRandomInRange(const float aMinValue, const float aMaxValue);
	int GetRandomInRange(const int aMinValue, const int aMaxValue);
}

namespace CU = CommonUtilities;