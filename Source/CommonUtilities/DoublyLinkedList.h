#pragma once
#include "DoublyLinkedListNode.h"

namespace CommonUtilities
{

	template <class T>
	class DoublyLinkedList
	{
	public:
		//Skapar en tom lista
		DoublyLinkedList();

		//Frig�r allt minne som listan allokerat
		~DoublyLinkedList();

		//Returnerar antalet element i listan
		int GetSize() const;

		//Returnerar f�rsta noden i listan, eller nullptr om listan �r tom
		DoublyLinkedListNode<T>* GetFirst() const;

		//Returnerar sista noden i listan, eller nullptr om listan �r tom
		DoublyLinkedListNode<T>* GetLast() const;

		//Skjuter in ett nytt element f�rst i listan
		void InsertFirst(const T& aValue);

		//Skjuter in ett nytt element sist i listan
		void InsertLast(const T& aValue);

		//Skjuter in ett nytt element innan aNode
		void InsertBefore(DoublyLinkedListNode<T> *aNode, const T &aValue);

		//Skjuter in ett nytt element efter aNode
		void InsertAfter(DoublyLinkedListNode<T> *aNode, const T &aValue);

		//Plockar bort noden ur listan och frig�r minne. (Det �r ok att anta att
		//aNode �r en nod i listan, och inte fr�n en annan lista)
		void Remove(DoublyLinkedListNode<T> *aNode);

		//Hittar f�rsta elementet i listan som har ett visst v�rde. J�mf�relsen 
		//g�rs med operator==. Om inget element hittas returneras nullptr.
		DoublyLinkedListNode<T> *FindFirst(const T &aValue);

		//Hittar sista elementet i listan som har ett visst v�rde. J�mf�relsen 
		//g�rs med operator==. Om inget element hittas returneras nullptr.
		DoublyLinkedListNode<T> *FindLast(const T &aValue);

		//Plockar bort f�rsta elementet i listan som har ett visst v�rde. 
		//J�mf�relsen g�rs med operator==. Om inget element hittas g�rs ingenting. 
		//Returnerar true om ett element plockades bort, och false annars.
		bool RemoveFirst(const T &aValue);

		void RemoveFirstNode();
		void RemoveLastNode();

		//Plockar bort sista elementet i listan som har ett visst v�rde. 
		//J�mf�relsen g�rs med operator==. Om inget element hittas g�rs ingenting. 
		//Returnerar true om ett element plockades bort, och false annars.
		bool RemoveLast(const T &aValue);

	private:
		DoublyLinkedListNode<T>* myFirstNode;
		DoublyLinkedListNode<T>* myLastNode;
		int mySize;
	};

	template<class T>
	inline DoublyLinkedList<T>::DoublyLinkedList()
	{
		mySize = 0;
		myFirstNode = nullptr;
		myLastNode = nullptr;
	}

	template<class T>
	inline DoublyLinkedList<T>::~DoublyLinkedList()
	{
		if (myFirstNode != nullptr && myLastNode != nullptr)
		{
			DoublyLinkedListNode<T>* node = myLastNode;
			while (node != nullptr)
			{
				DoublyLinkedListNode<T>* node2 = node;
				node = node->myPreviousNode;
				delete node2;
				node2 = nullptr;
			}
			myFirstNode = nullptr;
			myLastNode = nullptr;
		}
		
	}

	template<class T>
	inline int DoublyLinkedList<T>::GetSize() const
	{
		return mySize;
	}

	template<class T>
	inline DoublyLinkedListNode<T>* DoublyLinkedList<T>::GetFirst() const
	{
		return myFirstNode;
	}

	template<class T>
	inline DoublyLinkedListNode<T>* DoublyLinkedList<T>::GetLast() const
	{
		return myLastNode;
	}

	template<class T>
	inline void DoublyLinkedList<T>::InsertFirst(const T& aValue)
	{
		DoublyLinkedListNode<T>* node = new DoublyLinkedListNode<T>(aValue);
		if (myFirstNode == nullptr)
		{
			myFirstNode = node;
			myLastNode = node;
		}
		else
		{
			myFirstNode->myPreviousNode = node;
			node->myNextNode = myFirstNode;
			myFirstNode = node;
		}
		++mySize;
	}

	template<class T>
	inline void DoublyLinkedList<T>::InsertLast(const T & aValue)
	{
		DoublyLinkedListNode<T>* node = new DoublyLinkedListNode<T>(aValue);
		if (myLastNode == nullptr)
		{
			myFirstNode = node;
			myLastNode = node;
		}
		else
		{
			myLastNode->myNextNode = node;
			node->myPreviousNode = myLastNode;
			myLastNode = node;
		}
		++mySize;
	}

	template<class T>
	inline void DoublyLinkedList<T>::InsertBefore(DoublyLinkedListNode<T>* aNode, const T& aValue)
	{
		DoublyLinkedListNode<T>* node = new DoublyLinkedListNode<T>(aValue);

		if (aNode->myPreviousNode == nullptr)
		{
			aNode->myPreviousNode = node;
			node->myNextNode = aNode;
			myFirstNode = node;
		}
		else
		{
			node->myPreviousNode = aNode->myPreviousNode;
			node->myPreviousNode->myNextNode = node;
			node->myNextNode = aNode;
			aNode->myPreviousNode = node;
		}

		++mySize;
	}

	template<class T>
	inline void DoublyLinkedList<T>::InsertAfter(DoublyLinkedListNode<T>* aNode, const T& aValue)
	{
		DoublyLinkedListNode<T>* node = new DoublyLinkedListNode<T>(aValue);

		if (aNode->myNextNode == nullptr)
		{
			node->myPreviousNode = aNode;
			aNode->myNextNode = node;
			myLastNode = node;
		}
		else
		{
			node->myNextNode = aNode->myNextNode;
			node->myNextNode->myPreviousNode = node;
			node->myPreviousNode = aNode;
			aNode->myNextNode = node;
		}

		++mySize;
	}

	template<class T>
	inline void DoublyLinkedList<T>::Remove(DoublyLinkedListNode<T>* aNode)
	{
		if (aNode == nullptr)
		{
			return;
		}
		else if (aNode == myFirstNode && aNode == myLastNode)
		{
			delete aNode;
			aNode = nullptr;
			myFirstNode = nullptr;
			myLastNode = nullptr;
			--mySize;
			return;
		}
		else if (aNode == myFirstNode)
		{
			myFirstNode->myNextNode->myPreviousNode = nullptr;
			myFirstNode = myFirstNode->myNextNode;
		}
		else if (aNode == myLastNode)
		{
			myLastNode->myPreviousNode->myNextNode = nullptr;
			myLastNode = myLastNode->myPreviousNode;
		}
		else
		{
			aNode->myPreviousNode->myNextNode = aNode->myNextNode;
			aNode->myNextNode->myPreviousNode = aNode->myPreviousNode;
		}
		delete aNode;
		--mySize;
	}

	template<class T>
	inline DoublyLinkedListNode<T>* DoublyLinkedList<T>::FindFirst(const T& aValue)
	{
		DoublyLinkedListNode<T>* node = myFirstNode;
		while (node != nullptr)
		{
			if (node->GetValue() == aValue)
			{
				return node;
			}
			node = node->myNextNode;
		}
		return nullptr;
	}

	template<class T>
	inline DoublyLinkedListNode<T>* DoublyLinkedList<T>::FindLast(const T& aValue)
	{
		DoublyLinkedListNode<T>* node = myLastNode;
		while (node != nullptr)
		{
			if (node->GetValue() == aValue)
			{
				return node;
			}
			node = node->myPreviousNode;
		}
		return nullptr;
	}

	template<class T>
	inline bool DoublyLinkedList<T>::RemoveFirst(const T& aValue)
	{
		DoublyLinkedListNode<T>* node = myFirstNode;
		while (node != nullptr)
		{
			if (node->GetValue() == aValue)
			{
				if (node == myLastNode)
				{
					RemoveLastNode();
				}
				else if (node == myFirstNode)
				{
					RemoveFirstNode();
				}
				else
				{
					node->myPreviousNode->myNextNode = node->myNextNode;
					node->myNextNode->myPreviousNode = node->myPreviousNode;

					delete node;
					node = nullptr;

				}
				--mySize;
				return true;
			}
			node = node->myNextNode;
		}
		return false;
	}

	template<class T>
	inline void DoublyLinkedList<T>::RemoveFirstNode()
	{
		DoublyLinkedListNode<T>* node = myFirstNode;
		if (node->myNextNode != nullptr)
		{
			myFirstNode = node->myNextNode;
			myFirstNode->myPreviousNode = nullptr;
			delete node;
			node = nullptr;
		}
		else
		{
			delete myFirstNode;
			myFirstNode = nullptr;
		}
		--mySize;

	}

	template<class T>
	inline void DoublyLinkedList<T>::RemoveLastNode()
	{
		DoublyLinkedListNode<T>* node = myLastNode;
		if (node->myPreviousNode != nullptr)
		{
			myLastNode = node->myPreviousNode;
			myLastNode->myNextNode = nullptr;
			delete node;
			node = nullptr;
		}
		else
		{
			delete myLastNode;
			myLastNode = nullptr;
		}
		--mySize;
	}

	template<class T>
	inline bool DoublyLinkedList<T>::RemoveLast(const T& aValue)
	{
		DoublyLinkedListNode<T>* node = myLastNode;
		while (node != nullptr)
		{
			if (node->GetValue() == aValue)
			{
				if (node == myLastNode)
				{
					RemoveLastNode();
				}
				else if (node == myFirstNode)
				{
					RemoveFirstNode();
				}
				else
				{
					node->myPreviousNode->myNextNode = node->myNextNode;
					node->myNextNode->myPreviousNode = node->myPreviousNode;

					delete node;
					node = nullptr;
				}
				--mySize;
				return true;
			}
			node = node->myPreviousNode;
		}
		return false;
	}
}