#include "Intersection.h"
#include <assert.h>
#include "Macros.h"


namespace CommonUtilities
{
	namespace Collision
	{
		bool PointInsideSphere(const Sphere& aSphere, const Vector3f aPoint)
		{
			return (aSphere.myCenter - aPoint).Length2() < aSphere.myRadius * aSphere.myRadius;
		}

		bool PointInsideAABB(const AABB3D& aAABB, const Vector3f& aPoint)
		{
			if (aPoint.x < aAABB.myMin.x)
			{
				return false;
			}
			if (aPoint.x > aAABB.myMax.x)
			{
				return false;
			}
			if (aPoint.y < aAABB.myMin.y)
			{
				return false;
			}
			if (aPoint.y > aAABB.myMax.y)
			{
				return false;
			}
			if (aPoint.z < aAABB.myMin.z)
			{
				return false;
			}
			if (aPoint.z > aAABB.myMax.z)
			{
				return false;
			}
			return true;
		}

		bool IntersectionPlaneLine(const Plane& aPlane, const Line3D& aLine, Vector3f& aOurIntersectionPoint)
		{
			//Om linjen �r parallell med planet, returneras false och aOutIntersectionPoint �ndras inte
			//Om linjen inte �r parallell med planet skrivs sk�rningspunkten till aOutIntersectionPoint och true returneras
			
			float pointAndNormal = aLine.myDirection.Dot(aPlane.myNormal);

			if (pointAndNormal == 0.f)
			{
				return false;
			}

			float tDistance = (aPlane.myPoint.Dot(aPlane.myNormal) - aLine.myPoint.Dot(aPlane.myNormal)) / pointAndNormal;
			aOurIntersectionPoint = aLine.myPoint + aLine.myDirection * tDistance;

			return true;
		}

		bool IntersectionAABBLine(const AABB3D& aAABB, const Line3D& aLine)
		{
			const Vector3f& point = aLine.myPoint;
			const Vector3f& direction = aLine.myDirection;
			const Vector3f& min = aAABB.myMin;
			const Vector3f& max = aAABB.myMax;

			// IntersectionPoint
			Vector3f iPoint;

			iPoint = point;
			if (direction.x != 0.f)
			{
				iPoint = point + direction * ((min.x - point.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.x != 0.f)
			{
				iPoint = point + direction * ((max.x - point.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = point;
			if (direction.y != 0.f)
			{
				iPoint = point + direction * ((min.y - point.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.y != 0.f)
			{
				iPoint = point + direction * ((max.y - point.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = point;
			if (direction.z != 0.f)
			{
				iPoint = point + direction * ((min.z - point.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.z != 0.f)
			{
				iPoint = point + direction * ((max.z - point.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			return false;
		}

		bool IntersectionSphereLine(const Sphere& aSphere, const Line3D& aLine)
		{
			const Vector3f& point = aLine.myPoint;
			const Vector3f& direction = aLine.myDirection.GetNormalized();
			const Vector3f& center = aSphere.myCenter;

			float length = (center - point).Dot(direction);
			Vector3f closestPoint = point + direction * length;
			Vector3f distance = aSphere.myCenter - closestPoint;
			return distance.Length2() < aSphere.myRadius * aSphere.myRadius;
		}

		bool IntersectionSphereSphere(const Sphere& aSphere1, const Sphere& aSphere2)
		{
			/*
			const Vector3f distance = aSphere1.center - aSphere2.center;
			const float distanceLength = distance.Length();
			*/

			return ((aSphere1.myCenter - aSphere2.myCenter).Length() < aSphere1.myRadius + aSphere2.myRadius);
		}
		
		bool IntersectionCircleCircle(const Circle& aCircle1, const Circle& aCircle2)
		{
			CU::Vector2f pos1 = CU::Vector2f(aCircle1.myCenter.x / (1080.0f / 1920.0f), aCircle1.myCenter.y);
			CU::Vector2f pos2 = CU::Vector2f(aCircle2.myCenter.x / (1080.0f / 1920.0f), aCircle2.myCenter.y);
			return ((pos1 - pos2).Length() < aCircle1.myRadius + aCircle2.myRadius);
		}


		bool IntersectionCircleVShape(const Circle& aCircle, const Line3D& aLLine,const Line3D& aRLine)
		{
			assert(aLLine.myPoint.x == aRLine.myPoint.x && aLLine.myPoint.y == aRLine.myPoint.y);

			CU::Vector2f circleCenter = CU::Vector2f(aCircle.myCenter.x, aCircle.myCenter.y);
			CU::Vector2f lineIntersectPoint = CU::Vector2f(aLLine.myPoint.x, aLLine.myPoint.y);
			CU::Vector2f lNormal = -1.f * CU::Vector2f( -aLLine.myDirection.y, aLLine.myDirection.x);
			CU::Vector2f rNormal = CU::Vector2f(-aRLine.myDirection.y, aRLine.myDirection.x);
			CU::Vector2f intersectionToCircle = circleCenter - lineIntersectPoint;
			return ((intersectionToCircle).Dot(lNormal) < 0.f && (intersectionToCircle).Dot(rNormal) < 0.f);
		}
		bool IntersectionAABBCircle(const AABB & aRect, const Circle & aCircle)
		{
			float deltaX = aCircle.myCenter.x - MAX(aRect.myMax.x, MIN(aCircle.myCenter.x, aRect.myMin.x + aRect.myWidth));
			float deltaY = aCircle.myCenter.y - MAX(aRect.myMax.y, MIN(aCircle.myCenter.y, aRect.myMin.y + aRect.myHeight));
			return (deltaX * deltaX + deltaY * deltaY) < (aCircle.myRadius * aCircle.myRadius);
		}
		bool IntersectionRayAABB(const Ray & aRay, const AABB3D & aAABB)
		{
			const Vector3f& origin = aRay.myOrigin;
			const Vector3f& direction = aRay.myDirection;
			const Vector3f& min = aAABB.myMin;
			const Vector3f& max = aAABB.myMax;

			// IntersectionPoint
			Vector3f iPoint;

			iPoint = origin;
			if (direction.x != 0.f)
			{
				iPoint = origin + direction * ((min.x - origin.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.x != 0.f)
			{
				iPoint = origin + direction * ((max.x - origin.x) / direction.x);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = origin;
			if (direction.y != 0.f)
			{
				iPoint = origin + direction * ((min.y - origin.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.y != 0.f)
			{
				iPoint = origin + direction * ((max.y - origin.y) / direction.y);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			iPoint = origin;
			if (direction.z != 0.f)
			{
				iPoint = origin + direction * ((min.z - origin.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}
			if (direction.z != 0.f)
			{
				iPoint = origin + direction * ((max.z - origin.z) / direction.z);
			}
			if (iPoint.x >= min.x && iPoint.x <= max.x && iPoint.y >= min.y && iPoint.y <= max.y && iPoint.z >= min.z && iPoint.z <= max.z)
			{
				return true;
			}

			return false;
		}
	}
}
