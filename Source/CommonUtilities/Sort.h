#pragma once
#include <vector>
#include "GrowingArray.h"

namespace CommonUtilities
{
	template<class T>
	inline void SelectionSort(std::vector<T>& aVector)
	{
		T minValueIndex;
		for (size_t i = 0; i < aVector.size(); ++i)
		{
			minValueIndex = i;
			for (size_t j = i + 1; j < aVector.size(); ++j)
			{
				if (aVector[j] < aVector[minValueIndex])
				{
					minValueIndex = j;
				}
			}

			if (minValueIndex != i)
			{
				T swapValue = aVector[i];
				aVector[i] = aVector[minValueIndex];
				aVector[minValueIndex] = swapValue;
			}
		}
	}

	template<class T>
	inline void BubbleSort(std::vector<T>& aVector)
	{
		size_t bound = aVector.size() - 1;
		size_t newBound = 0;
		for (size_t i = 0; i < aVector.size(); ++i)
		{
			for (size_t j = 0; j < bound; ++j)
			{
				if (aVector[j + 1] < aVector[j])
				{
					T swapValue = aVector[j];
					aVector[j] = aVector[j + 1];
					aVector[j + 1] = swapValue;
					newBound = j;
				}
			}
			bound = newBound;
		}
	}

	template<class T>
	inline void QuickSort(std::vector<T>& aVector)
	{
		Partition(aVector, 0, aVector.size());
	}

	template <class T>
	void Partition(std::vector<T>& aVector, size_t aFrom, size_t aTo)
	{
		if (aTo - aFrom <= 1 || aVector.empty())
		{
			return;
		}

		size_t pivot = aFrom;
		size_t forwardIndex = pivot + 1;
		size_t backwardIndex = aTo - 1;

		while (true)
		{
			while (forwardIndex < aTo && !(aVector[pivot] < aVector[forwardIndex]))
			{
				++forwardIndex;
			}
			while (pivot < backwardIndex && !(aVector[backwardIndex] < aVector[pivot]))
			{
				--backwardIndex;
			}

			if (forwardIndex >= backwardIndex)
			{
				break;
			}

			T swapValue = aVector[forwardIndex];
			aVector[forwardIndex] = aVector[backwardIndex];
			aVector[backwardIndex] = swapValue;
		}

		for (size_t index = pivot; index < backwardIndex; ++index)
		{
			T swapValue = aVector[index];
			aVector[index] = aVector[index + 1];
			aVector[index + 1] = swapValue;
		}
		Partition(aVector, aFrom, backwardIndex);
		Partition(aVector, backwardIndex + 1, aTo);
	}

	template<class T>
	inline void QuickSort(GrowingArray<T>& aVector)
	{
		Partition(aVector, 0, aVector.size());
	}

	template <class T>
	void Partition(GrowingArray<T>& aVector, unsigned int aFrom, unsigned int aTo)
	{
		if (aTo - aFrom <= 1 || aVector.Size() <= 0)
		{
			return;
		}

		unsigned int pivot = aFrom;
		unsigned int forwardIndex = pivot + 1;
		unsigned int backwardIndex = aTo - 1;

		while (true)
		{
			while (forwardIndex < aTo && !(aVector[pivot] < aVector[forwardIndex]))
			{
				++forwardIndex;
			}
			while (pivot < backwardIndex && !(aVector[backwardIndex] < aVector[pivot]))
			{
				--backwardIndex;
			}

			if (forwardIndex >= backwardIndex)
			{
				break;
			}

			T swapValue = aVector[forwardIndex];
			aVector[forwardIndex] = aVector[backwardIndex];
			aVector[backwardIndex] = swapValue;
		}

		for (unsigned int index = pivot; index < backwardIndex; ++index)
		{
			T swapValue = aVector[index];
			aVector[index] = aVector[index + 1];
			aVector[index + 1] = swapValue;
		}
		Partition(aVector, aFrom, backwardIndex);
		Partition(aVector, backwardIndex + 1, aTo);
	}
	

	template<class T>
	inline void MergeSort(std::vector<T>& aVector)
	{
		MergeSort(std::begin(aVector), std::end(aVector));
	}

	template<class T>
	inline void MergeSort(T aIteratorBegin, T aIteratorEnd)
	{
		std::size_t length = std::distance(aIteratorBegin, aIteratorEnd);
		if (length <= 1)
		{
			return;
		}

		size_t mid = length / 2;
		T midPoint = std::next(aIteratorBegin, mid);

		MergeSort(aIteratorBegin, midPoint);
		MergeSort(midPoint, aIteratorEnd);

		Merge(aIteratorBegin, midPoint, aIteratorEnd);
	}
	template<class T>
	inline void Merge(T aBegin, T aMidPoint, T aEnd)
	{
		typedef std::vector<typename std::iterator_traits<T>::value_type> vector;
		
		vector vec(std::make_move_iterator(aBegin), std::make_move_iterator(aEnd));
		vector::iterator beginAlt = std::begin(vec);
		vector::iterator endAlt = std::end(vec);
		vector::iterator midAlt = std::next(beginAlt, std::distance(aBegin, aMidPoint));


		vector::iterator left = beginAlt;
		vector::iterator right = midAlt;
		T iter = aBegin;

		while (left < midAlt && right < endAlt)
		{
			if (*left < *right)
			{
				*iter = *left;
				++iter;
				++left;
			}
			else
			{
				*iter = *right;
				++iter;
				++right;
			}
		}
		while (left < midAlt)
		{
			*iter = *left;
			++iter;
			++left;
		}
		while (right < endAlt)
		{
			*iter = *right;
			++iter;
			++right;
		}
	}
	
}
