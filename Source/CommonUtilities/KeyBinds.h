#pragma once
namespace CommonUtilities
{

	class CKeyBinds
	{
	public:
		struct SHotKeys
		{
			unsigned char myMove_FORWARD;
			unsigned char myMove_RIGHT;
			unsigned char myMove_BACKWARDS;
			unsigned char myMove_LEFT;
			unsigned char myMove_UP;
			unsigned char myMove_DOWN;
			unsigned char myRoll_RIGHT;
			unsigned char myRoll_LEFT;
			unsigned char myBoost;
			unsigned char myShoot;
			unsigned char myMissile;
			unsigned char myPause;
			unsigned char myF1;

		};

		~CKeyBinds() = default;
		static const SHotKeys& GetHotKeys();
		static const bool Create(const char * aDataFilePath);
		static void Destroy();

	private:
		CKeyBinds(const char * aDataFilePath);
		void LoadKeyBindData(const char * aDataFilePath);

		static CKeyBinds* myInstance;
		static SHotKeys myHotKeys;
	};

}
namespace CU = CommonUtilities;