#pragma once
#include <typeinfo>
#include <map>
#include <mutex>

namespace CommonUtilities
{
	template <class Key, unsigned int DictionarySize>
	class Dictionary
	{
	public:
		Dictionary() = default;
		~Dictionary();

		Dictionary& operator=(const Dictionary& aCopy);

		template <class Value>
		void SetValue(const Key& aKey, const Value& aValue);

		//Please note: Heavier than Johnny Hendricks on his off-season!
		template <class Value>
		void RemoveValue(const Key& aKey);

		template <class Value>
		Value* GetValue(const Key& aKey);

	private:
		struct SBucketData
		{
			SBucketData()
				: myData(nullptr)
				, myIndex(0)
				, myTypeSize(0)
			{}

			SBucketData(void* aData, unsigned int aIndex, std::size_t aTypeSize)
				: myData(aData)
				, myIndex(aIndex)
				, myTypeSize(aTypeSize)
			{}

			void* myData;
			unsigned int myIndex;
			std::size_t myTypeSize;
		};
		struct SEqualAndDestruct
		{
			SEqualAndDestruct()
				: myEqual(nullptr)
				, myDestructor(nullptr)
			{}

			SEqualAndDestruct(const std::function<void(void* aObject0, const void* aObject1)>& aEqual, std::function<void(const void* aObject)> aDestructor)
				: myEqual(aEqual)
				, myDestructor(aDestructor)
			{}

			std::function<void(void* aObject0, const void* aObject1)> myEqual;
			std::function<void(const void* aObject)> myDestructor;
		};

		std::map<Key, SBucketData> myData;
		std::map<Key, const std::type_info*> myDataTypes;
		std::map<void*, size_t> myDataTypeSizes;
		std::map<void*, SEqualAndDestruct> myDataOperators;
		char myBucket[DictionarySize] = { 0 };
		unsigned int myIndex = 0;

		void* myLastUsedAdress = nullptr;

		mutable std::mutex myMutex;
	};

	template<class Key, unsigned int DictionarySize>
	inline Dictionary<Key, DictionarySize>::~Dictionary()
	{
		std::unique_lock<std::mutex> lg(myMutex);

		for (auto& dataPair : myData)
		{
			void* data(dataPair.second.myData);
			myDataOperators[data].myDestructor(data);
		}
	}

	template<class Key, unsigned int DictionarySize>
	inline Dictionary<Key, DictionarySize>& Dictionary<Key, DictionarySize>::operator=(const Dictionary& aCopy)
	{
		std::unique_lock<std::mutex> lg(myMutex, std::defer_lock);
		std::unique_lock<std::mutex> lgCopy(aCopy.myMutex, std::defer_lock);
		std::lock(lg, lgCopy);

		myData.clear();
		myDataTypeSizes.clear();

		void* lastAddress(nullptr);
		for (auto& dataPair : aCopy.myData)
		{
			SBucketData& currentBucketData(myData[dataPair.first]);
			currentBucketData = dataPair.second;

			void* oldAddress(&myBucket[currentBucketData.myIndex]);
			currentBucketData.myData = oldAddress;
			if (dataPair.second.myData == aCopy.myLastUsedAdress)
			{
				lastAddress = currentBucketData.myData;
			}

			void* currentData(currentBucketData.myData);

			myDataOperators[currentData].myEqual(currentData, dataPair.second.myData);
		}

		assert("Last address not set!" && !((aCopy.myIndex != 0) && (lastAddress == nullptr)));
		
		myDataTypes = aCopy.myDataTypes;

		for (auto& dataTypePair : myData)
		{
			myDataTypeSizes[dataTypePair.second.myData] = dataTypePair.second.myTypeSize;
		}

		myIndex = aCopy.myIndex;
		myLastUsedAdress = lastAddress;

		return *this;
	}

	template<class Key, unsigned int DictionarySize>
	template<class Value>
	inline void Dictionary<Key, DictionarySize>::SetValue(const Key & aKey, const Value & aValue)
	{
		std::unique_lock<std::mutex> lg(myMutex);

		if (myData.count(aKey) == 1)
		{
			if (myDataTypes[aKey] == &typeid(Value))
			{
				*static_cast<Value*>(myData[aKey].myData) = aValue;
			}
			else
			{
#ifndef _DEBUG
				assert("Value doesn't match type currently held by key!" && false);
#endif
			}
		}
		else
		{
			if (myIndex + sizeof(Value) > DictionarySize)
			{
				assert("Opaque dictionary ran out of memory!" && false);
			}

			void* newData(new(&myBucket[myIndex]) Value(aValue));
			myData[aKey] = SBucketData(newData, myIndex, sizeof(Value));
			myDataTypes[aKey] = &typeid(Value);
			myDataTypeSizes[newData] = sizeof(Value);

			auto equal(
				[](void* aObject0, const void* aObject1)
			{
				aObject0; // Sometimes gives "unreferenced variable"
				aObject1;
				*static_cast<Value*>(aObject0) = *static_cast<const Value*>(aObject1);
			});
			auto destructor(
				[](const void* aObject)
			{
				aObject; // Sometimes gives "unreferenced variable"
				static_cast<const Value*>(aObject)->~Value();
			});

			myDataOperators.insert({ newData, SEqualAndDestruct(equal, destructor) });
			myIndex += sizeof(Value);

			myLastUsedAdress = newData;
		}
	}

	template<class Key, unsigned int DictionarySize>
	template<class Value>
	inline void Dictionary<Key, DictionarySize>::RemoveValue(const Key & aKey)
	{
		std::unique_lock<std::mutex> lg(myMutex);

		if (myData.count(aKey) == 0)
		{
#ifndef _DEBUG
			assert("Dictionary slot is already undefined" && false);
#endif
			return;
		}
		else
		{
			if (myDataTypes[aKey] != &typeid(Value))
			{
#ifndef _DEBUG
				assert("Dictionary slot has not same type as type trying to remove" && false);
#endif
				return;
			}

			void* currentPointer = myData[aKey].myData;

			((Value*)currentPointer)->~Value();

			while (currentPointer != myLastUsedAdress)
			{
				char* nextPointer = static_cast<char*>(currentPointer) + myDataTypeSizes[currentPointer];

				if (myDataTypeSizes.count(nextPointer) == 1)
				{
					//Reroute object back down the bucket
					memcpy(currentPointer, nextPointer, myDataTypeSizes[nextPointer]);

					//Set size of next object
					myDataTypeSizes[currentPointer] = myDataTypeSizes[nextPointer];
					myDataOperators[currentPointer] = myDataOperators[nextPointer];

					//Set adress to next object
					currentPointer = static_cast<void*>(nextPointer);
				}
				else
				{
					myLastUsedAdress = currentPointer;
					break;
				}
			}

			myIndex -= sizeof(Value);

			myDataTypeSizes.erase(myData[aKey].myData);
			myDataOperators.erase(myData[aKey].myData);
			myDataTypes.erase(aKey);
			myData.erase(aKey);
		}
	}

	template<class Key, unsigned int DictionarySize>
	template<class Value>
	inline Value * Dictionary<Key, DictionarySize>::GetValue(const Key & aKey)
	{
		std::unique_lock<std::mutex> lg(myMutex);

		if (myData.count(aKey) == 1)
		{
			if (myDataTypes[aKey] == &typeid(Value))
			{
				return static_cast<Value*>(myData[aKey].myData);
			}
			else
			{
#ifndef _DEBUG
				assert("Tried to get the wrong type of value" && false);
#endif
				return nullptr;
			}
		}
		else
		{
			return nullptr;
		}
	}
};

namespace CU = CommonUtilities;