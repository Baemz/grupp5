#pragma once
#include "Vector3.h"
#include "Vector4.h"
#pragma warning(disable : 4201)
namespace CommonUtilities
{
	template<typename T>
	class Plane
	{
	public:
		Plane();
		Plane(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2);
		Plane(const Vector3<T>& aPoint0, const Vector3<T>& aNormal);
		~Plane();

		void InitWith3Points(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2);

		void InitWithPointAndNormal(const Vector3<T>& aPoint, const Vector3<T>& aNormal);

		bool Inside(const Vector3<T>& aPosition) const;
		void Normalize();

		int ClassifySpherePlane(Vector3<T> aSpherePosition, float aSphereRadius) const;

		float PlaneDotCoord(const Vector3f& aPos) const
		{
			return myFirstPoint.Dot(aPos);
		}
		float DotCoord(const Vector3<T>& aPos) const
		{
			return myABCD.x * aPos.x + myABCD.y * aPos.y + myABCD.z * aPos.z + myABCD.w;
		}

		Vector3<T> GetNormal() const;

		CommonUtilities::Vector3<T> myFirstPoint;
		CommonUtilities::Vector3<T> mySecondPoint;
		CommonUtilities::Vector3<T> myThirdPoint;
		CommonUtilities::Vector3<T> myNormal;

		union
		{
			struct 
			{
				float a, b, c, d;
			};
			CommonUtilities::Vector4<T> myABCD;
		};

	private:
		void CalculateNormal();
	};

	template <typename T>
	inline int Plane<T>::ClassifySpherePlane(Vector3<T> aSpherePosition, float aSphereRadius) const
	{
		float distance = (aSpherePosition.Dot(myNormal)) - myABCD.w;

		// completely on the front side
		if (distance >= aSphereRadius)
		{
			return 1;
		}

		// completely on the backside (aka "inside")
		if (distance <= -aSphereRadius)
		{
			return -1;
		}

		//sphere intersects the plane
		return 0;
	}
	
	template<typename T>
	inline Plane<T>::Plane()
	{
	}
	
	template<typename T>
	inline Plane<T>::Plane(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2)
	{
		myFirstPoint = aPoint0;
		mySecondPoint = aPoint1;
		myThirdPoint = aPoint2;
		CalculateNormal();
		myABCD.x = myNormal.x;
		myABCD.y = myNormal.y;
		myABCD.z = myNormal.z;
		myABCD.w = myFirstPoint.Dot(myNormal);
	}

	template<typename T>
	inline Plane<T>::Plane(const Vector3<T>& aPoint, const Vector3<T>& aNormal)
	{
		myFirstPoint = aPoint;
		myABCD.x = aNormal.x;
		myABCD.y = aNormal.y;
		myABCD.z = aNormal.z;
		myNormal = aNormal;
		myABCD.w = myFirstPoint.Dot(myNormal);
	}
	
	template<typename T>
	inline Plane<T>::~Plane()
	{
	}
	
	template<typename T>
	inline void Plane<T>::InitWith3Points(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2)
	{
		myFirstPoint = aPoint0;
		mySecondPoint = aPoint1;
		myThirdPoint = aPoint2;
		CalculateNormal();

		myABCD.x = myNormal.x;
		myABCD.y = myNormal.y;
		myABCD.z = myNormal.z;
		myABCD.w = myFirstPoint.Dot(myNormal);
	}

	template<typename T>
	inline void Plane<T>::InitWithPointAndNormal(const Vector3<T>& aPoint, const Vector3<T>& aNormal)
	{
		myFirstPoint = aPoint;
		myNormal = aNormal;
		myABCD.x = myNormal.x;
		myABCD.y = myNormal.y;
		myABCD.z = myNormal.z;
		myABCD.w = myFirstPoint.Dot(myNormal);
	}

	template<typename T>
	inline bool Plane<T>::Inside(const Vector3<T>& aPosition) const
	{
		if (((aPosition - myFirstPoint).Dot(myNormal)) == 0)
		{
			return true;
		}
		else if (((aPosition - myFirstPoint).Dot(myNormal)) < 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	inline void Plane<T>::Normalize()
	{
		CU::Vector3<T> normal(myABCD.x, myABCD.y, myABCD.z);
		float length = normal.Length();
		
		myABCD.x /= length;
		myABCD.y /= length;
		myABCD.z /= length;
	}

	template<typename T>
	inline void Plane<T>::CalculateNormal()
	{
		CommonUtilities::Vector3<T> firstDirection = (mySecondPoint - myFirstPoint);
		CommonUtilities::Vector3<T> secondDirection = (myThirdPoint - mySecondPoint);

		myNormal = firstDirection.Cross(secondDirection);
	}

	template <typename T>
	Vector3<T> Plane<T>::GetNormal() const
	{
		return Vector3<T>(myABCD.x, myABCD.y, myABCD.z);
	}
}
namespace CU = CommonUtilities;
#pragma warning(default : 4201)