#pragma once


namespace CommonUtilities
{
	static constexpr float Pif = 3.1415926535f;
	static constexpr int Pii = 3; // Average of the infamous number PI
	static constexpr double Pid = 3.1415926535897932384;

	inline const float ToRadians(const float aDegree)
	{
		return (aDegree * (Pif / 180.f));
	}

	inline const float ToDegrees(const float aRadian)
	{
		return (aRadian * (180.f / Pif));
	}

	template<class T>
	inline void Clamp(T& aVal, const T& min, const T& max)
	{
		aVal = (aVal < max) ? ((aVal > min) ? aVal : min) : max;
	}

	template <typename T, typename T2>
	__forceinline T Lerp(const T& aMin, const T& aMax, const T2& aCurrentValue)
	{
		return aMin + (aMax - aMin) * aCurrentValue;
	}

	template <typename T>
	__forceinline T SmoothStep(const T& aMin, const T& aMax, const T& aPercentage)
	{
		const float percentSquared = aPercentage * aPercentage;
		return aMin + (aMax - aMin) * (static_cast<T>(-2.0) * percentSquared * aPercentage + static_cast<T>(3.0) * percentSquared);
	}

	template <typename T>
	__forceinline T SmootherStep(const T& aMin, const T& aMax, const T& aPercentage)
	{
		const float cubicPercent = std::pow(aPercentage, 3);
		return aMin + (aMax - aMin) * (static_cast<T>(6.0) * cubicPercent * aPercentage * aPercentage - static_cast<T>(15.0) * cubicPercent * aPercentage + static_cast<T>(10.0) * cubicPercent);
	}

}
namespace CU = CommonUtilities;