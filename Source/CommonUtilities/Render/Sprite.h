#pragma once
#include "Vector.h"
#include "Drawable.h"

namespace Tga2D
{
	class CSprite;
	class CSpriteBatch;
}

class Sprite : public Drawable
{

public:
	Sprite();
	Sprite(const Sprite& aSprite);
	Sprite(const char* aSpritePath);
	~Sprite();

	Sprite& operator=(const Sprite& aSprite);

	virtual void Render() override;

	void SetPivot(const CommonUtilities::Vector2f& aPivot);
	const CommonUtilities::Vector2f GetPivot() const;

	void SetColor(const CommonUtilities::Vector4f& aColor);
	const CommonUtilities::Vector4f GetColor() const;

	const Tga2D::CSprite* GetSprite() const;
	Tga2D::CSprite* GetSprite();

	void SetShouldRender(bool aShould);
	bool GetShouldRender() const;

 	void SetBatch(Tga2D::CSpriteBatch * aBatch);

private:
	bool myIsBatched;
	Tga2D::CSpriteBatch * mySpriteBatch;
protected:
	Tga2D::CSprite* mySprite;
};


