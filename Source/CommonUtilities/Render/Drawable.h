#pragma once
#include "Vector2.h"
#include "Vector3.h"

class Camera;

class Drawable
{
public:
	Drawable();
	virtual ~Drawable();

	virtual void PreRender();
	virtual void Render() = 0;
	virtual void Update() {};

	void SetPosition(const CommonUtilities::Vector3f & aPosition);
	const CommonUtilities::Vector3f & GetPosition() const;
	void SetSize(const CommonUtilities::Vector2f & aSize);
	const CommonUtilities::Vector2f & GetSize() const;
	void SetRotation(const float aRotation);
	float GetRotation() const;

protected:
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector2f mySize;
	float myRotation;

	CommonUtilities::Vector3f myRenderPosition;
	CommonUtilities::Vector2f myRenderSize;
	float myRenderRotation;

	friend class Camera;
};

