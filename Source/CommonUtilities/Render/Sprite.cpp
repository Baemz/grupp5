#include "stdafx.h"
#include "Sprite.h"
#include <tga2d/sprite/sprite.h>
#include <tga2d/sprite/sprite_batch.h>
#include "Camera.h"

Sprite::Sprite()
	: mySprite(nullptr),
	mySpriteBatch(nullptr),
	myIsBatched(false)
{
}

 Sprite::Sprite(const Sprite & aSprite)
 {
	 if (aSprite.GetSprite() != nullptr)
	 {
		 mySprite = new Tga2D::CSprite(*aSprite.GetSprite());
	 }
	 else
	 {
		 mySprite = nullptr;
	 }

	 mySpriteBatch = aSprite.mySpriteBatch;
	 myIsBatched = aSprite.myIsBatched;
 }

Sprite::Sprite(const char* aSpritePath)
	: mySpriteBatch(nullptr),
	myIsBatched(false)
{
	std::string path = aSpritePath;
	size_t pngFixPos = path.find(".png");
	if (pngFixPos != std::string::npos)
	{
		path = path.substr(0, pngFixPos) + ".dds";
	}
	mySprite = new Tga2D::CSprite(path.c_str());
}


Sprite::~Sprite()
{
	SAFE_DELETE(mySprite);
}

void Sprite::SetPivot(const Vector2f& aPivot)
{
	mySprite->SetPivot(Tga2D::Vector2f(aPivot.x, aPivot.y));
}

const Vector2f Sprite::GetPivot() const
{
	return Vector2f(mySprite->GetPivot().x, mySprite->GetPivot().y);
}

void Sprite::SetColor(const Vector4f& aColor)
{
	mySprite->SetColor(Tga2D::CColor(aColor.x, aColor.y, aColor.z, aColor.w));
}

const Vector4f Sprite::GetColor() const
{
	return Vector4f(mySprite->GetColor().myR, mySprite->GetColor().myG, mySprite->GetColor().myB, mySprite->GetColor().myA);
}

const Tga2D::CSprite* Sprite::GetSprite() const
{
	return mySprite;
}

Tga2D::CSprite* Sprite::GetSprite()
{
	return mySprite;
}

void Sprite::SetShouldRender(bool aShould)
{
	mySprite->SetShouldRender(aShould);
}

bool Sprite::GetShouldRender() const
{
	return mySprite->GetShouldRender();
}

void Sprite::SetBatch(Tga2D::CSpriteBatch * aBatch)
{
	mySpriteBatch = aBatch;
	myIsBatched = (mySpriteBatch != nullptr);
}


Sprite& Sprite::operator=(const Sprite& aSprite)
{
	if (aSprite.GetSprite() != nullptr)
	{
		mySprite = new Tga2D::CSprite(*aSprite.GetSprite());
	}
	else
	{
		mySprite = nullptr;
	}

	mySpriteBatch = aSprite.mySpriteBatch;
	myIsBatched = aSprite.myIsBatched;
	return *this;
}

void Sprite::Render()
{
	//Culling
	if ((myRenderPosition.x < -0.2f)
		|| (myRenderPosition.x > 1.2f)
		|| (myRenderPosition.y < -0.2f)
		|| (myRenderPosition.y > 1.2f))
	{
		return;
	}
	mySprite->SetPosition({ myRenderPosition.x, myRenderPosition.y });
	mySprite->SetSizeRelativeToImage(myRenderSize);
	mySprite->SetRotation(myRenderRotation);
	mySprite->Render();
}
