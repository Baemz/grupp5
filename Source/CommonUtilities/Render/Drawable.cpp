#include "stdafx.h"
#include "Drawable.h"
#include "Camera.h"

Drawable::Drawable()
	: myPosition({ 0.f, 0.f, 0.f }),
	mySize({1.f, 1.f}),
	myRotation(0.f)
{
}


Drawable::~Drawable()
{
}
void Drawable::PreRender()
{
	myRenderPosition = Camera::GetInstance().ConvertToProjectedScreenSpace(GetPosition());
	myRenderSize = GetSize();
	myRenderRotation = GetRotation();
}
void Drawable::SetPosition(const CommonUtilities::Vector3f & aPosition)
{
	myPosition = aPosition;
}

const CommonUtilities::Vector3f & Drawable::GetPosition() const
{
	return myPosition;
}

void Drawable::SetSize(const CommonUtilities::Vector2f & aSize)
{
	mySize = aSize;
}

const CommonUtilities::Vector2f & Drawable::GetSize() const
{
	return mySize;
}

void Drawable::SetRotation(const float aRotation)
{
	myRotation = aRotation;
}

float Drawable::GetRotation() const
{
	return myRotation;
}