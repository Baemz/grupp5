#pragma once
#include <chrono>

#define START_TIMER(ID) std::chrono::steady_clock::time_point ID = std::chrono::steady_clock::now();

#define END_TIMER(ID, aInt) {std::chrono::steady_clock::time_point endTime = std::chrono::steady_clock::now(); \
typedef std::chrono::milliseconds milliseconds;\
milliseconds ms = std::chrono::duration_cast<milliseconds>(endTime - ID);\
aInt = static_cast<int>(ms.count());}