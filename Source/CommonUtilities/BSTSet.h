#pragma once

namespace CommonUtilities
{
	//BST-SET
	template <class T>
	class BSTSetNode;

	template <class T> 
	class BSTSet
	{
	public:
		BSTSet();
		~BSTSet();

		//Returnerar Root-noden.
		const BSTSetNode<T>* GetRoot() const;

		//Returnerar true om elementet finns i m�ngden, och false annars.
		bool HasElement(const T& aValue) const;

		//Stoppar in elementet i m�ngden, om det inte redan finns d�r. G�r 
		//ingenting annars.
		void Insert(const T& aValue);

		//Plockar bort elementet ur m�ngden, om det finns. G�r ingenting annars.
		void Remove(const T& aValue);

		//Balanserar tr�det med DSW-metod
		void DSWBalance();

	private:
		void DSWCreateBackbone();
		void DSWCreatePerfectTree();
		void Compact(int aAmountToRotatePerNode);
		void RotateNode(BSTSetNode<T>* aCurrentNode, BSTSetNode<T>* aParentNode);

		BSTSetNode<T>* myRootNode;
		int mySize;
	};

	template<class T>
	inline BSTSet<T>::BSTSet()
	{
		myRootNode = nullptr;
		mySize = 0;
	}

	template<class T>
	inline BSTSet<T>::~BSTSet()
	{
		if (myRootNode != nullptr && myRootNode->DeleteChilds())
		{
			delete myRootNode;
			myRootNode = nullptr;
		}
	}

	template<class T>
	inline const BSTSetNode<T>* BSTSet<T>::GetRoot() const
	{
		return myRootNode;
	}

	template<class T>
	inline bool BSTSet<T>::HasElement(const T& aValue) const
	{
		BSTSetNode<T>* currentNode = myRootNode;
		while (currentNode != nullptr)
		{
			if ((!(currentNode->myValue < aValue) && !(aValue < currentNode->myValue)))
			{
				return true;
			}

			if (currentNode->myValue < aValue)
			{
				currentNode = currentNode->myRightChildNode;
			}
			else if (aValue < currentNode->myValue)
			{
				currentNode = currentNode->myLeftChildNode;
			}
			else
			{
				return false;
			}

		}
		return false;
	}

	template<class T>
	inline void BSTSet<T>::Insert(const T& aValue)
	{
		if (HasElement(aValue))
		{
			return;
		}
		else
		{
			if (myRootNode == nullptr)
			{
				myRootNode = new BSTSetNode<T>(aValue);
			}
			else
			{
				myRootNode->Insert(new BSTSetNode<T>(aValue));
			}
			++mySize;
		}
	}

	template<class T>
	inline void BSTSet<T>::Remove(const T& aValue)
	{
		if (myRootNode == nullptr)
		{
			return;
		}

		BSTSetNode<T>* node = nullptr;
		if (!(myRootNode->myValue < aValue) && !(aValue < myRootNode->myValue))
		{
			BSTSetNode<T> root(aValue);
			root.myLeftChildNode = myRootNode;
			node = myRootNode->Remove(aValue, &root);
			myRootNode = root.myLeftChildNode;
		}
		else
		{
			node = myRootNode->Remove(aValue, nullptr);
		}

		if (node != nullptr)
		{
			delete node;
			--mySize;
		}
	}

	template<class T>
	inline void BSTSet<T>::DSWBalance()
	{
		DSWCreateBackbone();
		DSWCreatePerfectTree();
	}

	template<class T>
	inline void BSTSet<T>::DSWCreateBackbone()
	{
		BSTSetNode<T> *currentNode = myRootNode;
		BSTSetNode<T> *parentNode = nullptr;
		while (currentNode != nullptr)
		{
			if (currentNode->myLeftChildNode != nullptr)
			{
				BSTSetNode<T> *leftChildRightmostNode = currentNode->myLeftChildNode;
				while (leftChildRightmostNode->myRightChildNode != nullptr)
				{
					leftChildRightmostNode = leftChildRightmostNode->myRightChildNode;
				}
				leftChildRightmostNode->myRightChildNode = currentNode;
				if (parentNode != nullptr)
				{
					parentNode->myRightChildNode = currentNode->myLeftChildNode;
					currentNode->myLeftChildNode = nullptr;
					currentNode = parentNode->myRightChildNode;
				}
				else
				{
					myRootNode = currentNode->myLeftChildNode;
					currentNode->myLeftChildNode = nullptr;
					currentNode = myRootNode;
				}
			}
			else
			{
				parentNode = currentNode;
				currentNode = currentNode->myRightChildNode;
			}
		}
	}

	template<class T>
	inline void BSTSet<T>::DSWCreatePerfectTree()
	{
		int m = powf(2, floor(log2(mySize + 1))) - 1;
		Compact(mySize - m);
		while (m > 1)
		{
			m /= 2;
			Compact(m);
		}
	}

	template<class T>
	inline void BSTSet<T>::Compact(int aAmountToRotatePerNode)
	{
		int rotationsDone = 0;
		BSTSetNode<T> *currNode = myRootNode;
		BSTSetNode<T> *parentNode = nullptr;
		while (rotationsDone != aAmountToRotatePerNode)
		{
			BSTSetNode<T> *nextParent = currNode->myRightChildNode;
			RotateNode(currNode, parentNode);
			parentNode = nextParent;
			currNode = nextParent->myRightChildNode;
			rotationsDone++;
		}
	}

	template<class T>
	inline void BSTSet<T>::RotateNode(BSTSetNode<T>* aCurrentNode, BSTSetNode<T>* aParentNode)
	{
		if (aParentNode == nullptr)
		{
			myRootNode = aCurrentNode->myRightChildNode;
		}
		BSTSetNode<T> *newRightChildOnRotatedNode = aCurrentNode->myRightChildNode->myLeftChildNode;
		aCurrentNode->myRightChildNode->myLeftChildNode = aCurrentNode;
		if (aParentNode != nullptr)
		{
			aParentNode->myRightChildNode = aCurrentNode->myRightChildNode;
		}
		aCurrentNode->myRightChildNode = newRightChildOnRotatedNode;
	}



	//BST-NODE
	template <class T>
	class BSTSetNode
	{
	public:
		BSTSetNode(const BSTSetNode&) = delete;
		BSTSetNode &operator=(const BSTSetNode&) = delete;

		const T& GetData() const;
		const BSTSetNode<T>* GetLeft() const;
		const BSTSetNode<T>* GetRight() const;

	private:
		friend class BSTSet<T>;
		BSTSetNode() :
			myLeftChildNode(nullptr),
			myRightChildNode(nullptr)
		{
		}
		BSTSetNode(const T& aValue) :
			myValue(aValue),
			myLeftChildNode(nullptr),
			myRightChildNode(nullptr)
		{
		}
		~BSTSetNode()
		{
			myLeftChildNode = nullptr;
			myRightChildNode = nullptr;
		}

		bool DeleteChilds();
		void Insert(BSTSetNode<T>* aNode);
		BSTSetNode<T>* GetLeftestNode();
		BSTSetNode<T>* Remove(const T& aValue, BSTSetNode<T>* aParent);


		T myValue;
		BSTSetNode* myLeftChildNode;
		BSTSetNode* myRightChildNode;
	};

	template<class T>
	inline BSTSetNode<T>* BSTSetNode<T>::GetLeftestNode()
	{
		if (myLeftChildNode == nullptr)
		{
			return this;
		}
		return myLeftChildNode->GetLeftestNode();
	}

	template<class T>
	inline const T& BSTSetNode<T>::GetData() const
	{
		return myValue;
	}

	template<class T>
	inline const BSTSetNode<T>* BSTSetNode<T>::GetLeft() const
	{
		return myLeftChildNode;
	}

	template<class T>
	inline const BSTSetNode<T>* BSTSetNode<T>::GetRight() const
	{
		return myRightChildNode;
	}

	template<class T>
	inline void BSTSetNode<T>::Insert(BSTSetNode<T>* aNode)
	{
		if (aNode == nullptr)
		{
			return;
		}
		if (myValue < aNode->myValue)
		{
			if (myRightChildNode == nullptr)
			{
				myRightChildNode = aNode;
			}
			else
			{
				myRightChildNode->Insert(aNode);
			}
		}
		else if (aNode->myValue < myValue)
		{
			if (myLeftChildNode == nullptr)
			{
				myLeftChildNode = aNode;
			}
			else
			{
				myLeftChildNode->Insert(aNode);
			}
		}
	}

	template<class T>
	inline BSTSetNode<T>* BSTSetNode<T>::Remove(const T& aValue, BSTSetNode<T>* aParent)
	{
		if (aValue < myValue)
		{
			if (myLeftChildNode != nullptr)
			{
				return myLeftChildNode->Remove(aValue, this);
			}
			return nullptr;
		}
		else if (myValue < aValue)
		{
			if (myRightChildNode != nullptr)
			{
				return myRightChildNode->Remove(aValue, this);
			}
			return nullptr;
		}
		else
		{
			if (myLeftChildNode != nullptr && myRightChildNode != nullptr)
			{
				myValue = myRightChildNode->GetLeftestNode()->myValue;
				return myRightChildNode->Remove(myValue, this);
			}
			else if (aParent->myLeftChildNode == this)
			{
				aParent->myLeftChildNode = (myLeftChildNode != nullptr) ? myLeftChildNode : myRightChildNode;
				return this;
			}
			else if (aParent->myRightChildNode == this)
			{
				aParent->myRightChildNode = (myLeftChildNode != nullptr) ? myLeftChildNode : myRightChildNode;
				return this;
			}
		}
	}

	template<class T>
	inline bool BSTSetNode<T>::DeleteChilds()
	{
		if ((myLeftChildNode != nullptr && myLeftChildNode->DeleteChilds()))
		{
			delete myLeftChildNode;
			myLeftChildNode = nullptr;

		}

		if ((myRightChildNode != nullptr && myRightChildNode->DeleteChilds()))
		{
			delete myRightChildNode;
			myRightChildNode = nullptr;
		}

		if (myRightChildNode == nullptr && myLeftChildNode == nullptr)
		{
			return true;
		}
		return false;
	}
}