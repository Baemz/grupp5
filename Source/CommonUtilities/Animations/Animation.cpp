#include "stdafx.h"
#include "Animation.h"
#include "Timer.h"
#include "Sprite.h"
#include <tga2d/engine.h>
#include <cassert>
#include "Camera.h"

Animation::Animation()
{
}

Animation::Animation(const Animation& aAnimation)
{
	if (aAnimation.GetSprite() != nullptr)
	{
		mySprite = new Tga2D::CSprite(*aAnimation.GetSprite());
	}
	else
	{
		mySprite = nullptr;
	}
}


Animation::~Animation()
{
}

Animation& Animation::operator=(const Animation& aAnimation)
{
	if (aAnimation.GetSprite() != nullptr)
	{
		mySprite = new Tga2D::CSprite(*aAnimation.GetSprite());
	}
	else
	{
		mySprite = nullptr;
	}
	myShouldLoop = aAnimation.myShouldLoop;
	myFrameCount = aAnimation.myFrameCount;
	myCurrentFrame = aAnimation.myCurrentFrame;
	myCurrentFrameOnRow = aAnimation.myCurrentFrameOnRow;
	myAnimationSpeed = aAnimation.myAnimationSpeed;
	myAnimationTimer = aAnimation.myAnimationTimer;
	myFrameSize = aAnimation.myFrameSize;
	myFramePosition = aAnimation.myFramePosition;
	myFrameStartPos = aAnimation.myFrameStartPos;
	return *this;
}

void Animation::Init(Sprite* aSpriteSheet, bool aShouldLoop, const unsigned int aFrameCount, const float aAnimationSpeed, const Vector2f& aFrameSize, const Vector2f& aFrameStartPos)
{
	if (aSpriteSheet != nullptr)
	{
		*dynamic_cast<Sprite*>(this) = *aSpriteSheet;
		myShouldLoop = aShouldLoop;
		myFrameCount = aFrameCount - 1;
		myAnimationSpeed = aAnimationSpeed;
		myCurrentFrame = 0;
		myCurrentFrameOnRow = 0;
		myAnimationTimer = 0.0f;

		myFrameSize.x = aFrameSize.x / static_cast<float>(GetSprite()->GetImageSize().x);
		myFrameSize.y = aFrameSize.y / static_cast<float>(GetSprite()->GetImageSize().y);

		myFrameStartPos.x = aFrameStartPos.x / static_cast<float>(GetSprite()->GetImageSize().x);
		myFrameStartPos.y = aFrameStartPos.y / static_cast<float>(GetSprite()->GetImageSize().y);

		myFramePosition = myFrameStartPos;

		GetSprite()->SetUVScale({ myFrameSize.x, myFrameSize.y });
		GetSprite()->SetSizeRelativeToImage({ myFrameSize.x, myFrameSize.y });
		SetPivot({ 0.5f, 1.0f });
	}
	else
	{
		assert(false && "aSpriteSheet was nullptr");
	}
}

void Animation::Update()
{
	if (myShouldLoop == false && (myCurrentFrame == (myFrameCount + 1)))
	{
		return;
	}

	const float deltaTime = CommonUtilities::Timer::GetInstance()->GetDeltaTime();

	if (myAnimationTimer >= myAnimationSpeed)
	{
		if (myFrameStartPos.x + (myFrameSize.x * myCurrentFrameOnRow) >= 1.0f)
		{
			myCurrentFrameOnRow = 0;
			myFramePosition.y += myFrameSize.y;
			myFramePosition.x = myFrameStartPos.x;
		}
		else
		{
			myFramePosition.x = myFrameStartPos.x + (myFrameSize.x * myCurrentFrameOnRow);
		}

		if (myShouldLoop && (myCurrentFrame > myFrameCount))
		{
			myCurrentFrame = 0;
			myCurrentFrameOnRow = 0;
			myFramePosition.y = myFrameStartPos.y;
			myFramePosition.x = myFrameStartPos.x;
		}
		else
		{
			++myCurrentFrame;
			++myCurrentFrameOnRow;
		}
		myAnimationTimer = 0.0f;
	}
	else
	{
		myAnimationTimer += deltaTime;
	}
}
void Animation::Render()
{
	GetSprite()->SetUVOffset({ myFramePosition.x, myFramePosition.y });
	myRenderSize = myRenderSize * myFrameSize;
	Sprite::Render();
}

void Animation::ResetAnimation()
{
	myCurrentFrame = 0;
	myCurrentFrameOnRow = 0;
	myFramePosition = myFrameStartPos;
	myAnimationTimer = 0.0f;
}

void Animation::SetLoopState(const bool aLoopState)
{
	myShouldLoop = aLoopState;
}
