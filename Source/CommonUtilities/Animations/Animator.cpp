#include "stdafx.h"
#include "Animator.h"

Animator::Animator()
	: myCurrentState("Idle")
{
}


Animator::~Animator()
{
}

void Animator::Update()
{
	for (auto& iterator : myAnimations)
	{
		if (iterator.first == myCurrentState)
		{
			iterator.second.SetPosition(myPosition);
			iterator.second.SetRotation(myRotation);
			iterator.second.SetSize(mySize);
			iterator.second.Update();
		}
	}
}

void Animator::PreRender()
{
	Drawable::PreRender();
	for (auto& iterator : myAnimations)
	{
		if (iterator.first == myCurrentState)
		{
			iterator.second.PreRender();
		}
	}
}

void Animator::Render()
{
	for (auto& iterator : myAnimations)
	{
		if (iterator.first == myCurrentState)
		{
			iterator.second.Render();
		}
	}
}

void Animator::SetState(const std::string& aState)
{
	if (myCurrentState != aState)
	{
		myCurrentState = aState;
		myAnimations[aState].ResetAnimation();
	}
}

void Animator::AddAnimation(const AnimationData aData)
{
	if (mySpriteSheets[aData.texturePath].GetSprite() == nullptr)
	{
		mySpriteSheets[aData.texturePath] = Sprite(aData.texturePath.c_str());
	}

	Animation anim;
	anim.Init(&mySpriteSheets[aData.texturePath], aData.shouldLoop, aData.frameCount, aData.animationSpeed, aData.frameSize, aData.startFramePosition);
	myAnimations[aData.state] = anim;
}

bool Animator::SetAnimationLoop(const std::string& aAnimation, const bool aLoopState)
{
	for (auto& iterator : myAnimations)
	{
		if (iterator.first == aAnimation)
		{
			iterator.second.SetLoopState(aLoopState);
			return true;
		}
	}
	return false;
}
