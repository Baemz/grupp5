#pragma once
#include "Animation.h"
#include "Sprite.h"
#include <map>

struct AnimationData
{
	bool shouldLoop;
	unsigned int frameCount;
	float animationSpeed;
	Vector2f frameSize;
	Vector2f startFramePosition;
	std::string state;
	std::string texturePath;
};

class Animator : public Drawable
{
public:
	Animator();
	~Animator();

	void PreRender() override;
	void Render() override;
	void Update() override;

	void SetState(const std::string& aState);
	const std::string& GetState() const { return myCurrentState; }

	void AddAnimation(const AnimationData aData);
	bool SetAnimationLoop(const std::string& aAnimation, const bool aLoopState);
private:
	std::string myCurrentState;
	std::map<std::string, Animation> myAnimations;
	std::map<std::string, Sprite> mySpriteSheets;
};

