#pragma once
#include "Sprite.h"

class Sprite;
class Animator;
class Animation : public Sprite
{
public:
	Animation();
	~Animation(); 
	Animation(const Animation& aAnimation);
	Animation& operator=(const Animation& aSprite);

private:
	friend Animator;
	void Init(Sprite* aSpriteSheet, bool aShouldLoop, const unsigned int aFrameCount, const float aAnimationSpeed, const Vector2f& aFrameSize, const Vector2f& aFrameStartPos);
	void Update() override;
	void Render() override;
	void ResetAnimation();
	void SetLoopState(const bool aLoopState);

	bool myShouldLoop;
	unsigned int myFrameCount;
	unsigned int myCurrentFrame;
	unsigned int myCurrentFrameOnRow;
	float myAnimationSpeed;
	float myAnimationTimer;
	CommonUtilities::Vector2f myFrameSize;
	CommonUtilities::Vector2f myFramePosition;
	CommonUtilities::Vector2f myFrameStartPos;
//	Sprite* mySpriteSheet;

};

