#pragma once

namespace CommonUtilities
{
	template <class T>
	class BSTSet;
	template <class T>
	class BSTNode
	{
	public:

		void Insert(BSTNode<T>* aNode);
		BSTNode<T>* GetLeftestNode(BSTNode<T>* aParent);
		BSTNode<T>* Remove(const T& aValue, BSTNode<T>* aParent);

	private:
		friend class BSTSet<T>;
		BSTNode() :
			myLeftChildNode(nullptr),
			myRightChildNode(nullptr)
		{
		}

		BSTNode(const T& aValue) :
			myValue(aValue),
			myLeftChildNode(nullptr),
			myRightChildNode(nullptr)
		{
		}
		~BSTNode()
		{
			delete myLeftChildNode;
			delete myRightChildNode;
			myLeftChildNode = nullptr;
			myRightChildNode = nullptr;
		}

		T myValue;
		BSTNode* myLeftChildNode;
		BSTNode* myRightChildNode;
	};

	template<class T>
	inline BSTNode<T>* BSTNode<T>::GetLeftestNode(BSTNode<T>* aParent)
	{
		if (myLeftChildNode == nullptr)
		{
			return this;
		}
		aParent = this;
		return myLeftChildNode->GetLeftestNode(this);
	}


	template<class T>
	inline void BSTNode<T>::Insert(BSTNode<T>* aNode)
	{
		if (aNode == nullptr)
		{
			return;
		}
		if (myValue < aNode->myValue)
		{
			if (myRightChildNode == nullptr)
			{
				myRightChildNode = aNode;
			}
			else
			{
				myRightChildNode->Insert(aNode);
			}
		}
		else if (aNode->myValue < myValue)
		{
			if (myLeftChildNode == nullptr)
			{
				myLeftChildNode = aNode;
			}
			else
			{
				myLeftChildNode->Insert(aNode);
			}
		}
	}

	template<class T>
	inline BSTNode<T>* BSTNode<T>::Remove(const T& aValue, BSTNode<T>* aParent)
	{
		if (myValue < aValue)
		{
			if (myRightChildNode != nullptr)
			{
				return myRightChildNode->Remove(aValue, this);
			}
			return nullptr;
		}
		else if (aValue < myValue)
		{
			if (myLeftChildNode != nullptr)
			{
				return myLeftChildNode->Remove(aValue, this);
			}
			return nullptr;
		}
		else
		{
			if (myLeftChildNode != nullptr && myRightChildNode != nullptr)
			{
				BSTNode<T>* nodeParent = this;
				BSTNode<T>* minNode = myRightChildNode->GetLeftestNode(nodeParent);

				if (aParent->myLeftChildNode == nodeParent)
				{
					aParent->myLeftChildNode = minNode;
				}
				else
				{
					aParent->myRightChildNode = minNode;
				}
				minNode->myLeftChildNode = myLeftChildNode;

				if (minNode != myRightChildNode)
				{
					if (minNode->myRightChildNode != nullptr)
					{
						nodeParent->Insert(minNode->myRightChildNode);
					}

					minNode->myRightChildNode = myRightChildNode;
					nodeParent->myLeftChildNode = nullptr;
				}
			}
			else if (aParent->myLeftChildNode == this)
			{
				if (myLeftChildNode != nullptr)
				{
					aParent->myLeftChildNode = myLeftChildNode;
				}
				else
				{
					aParent->myLeftChildNode = myRightChildNode;
				}
			}
			else if (aParent->myRightChildNode == this)
			{
				if (myLeftChildNode != nullptr)
				{
					aParent->myRightChildNode = myLeftChildNode;
				}
				else
				{
					aParent->myRightChildNode = myRightChildNode;
				}
			}
			return this;
		}
	}
}