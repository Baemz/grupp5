#pragma once
#include <assert.h>
#include <algorithm>

namespace CommonUtilities
{
	template<typename Type, typename CountType = unsigned short>
	class GrowingArray
	{
	public:
		using ValueType = Type;
		using SizeType = CountType;

		GrowingArray();
		explicit GrowingArray(CountType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);
		GrowingArray(const GrowingArray& aGrowingArray);
		GrowingArray(GrowingArray&& aGrowingArray);

		~GrowingArray();

		GrowingArray& operator=(const GrowingArray& aGrowingArray);
		GrowingArray& operator=(GrowingArray&& aGrowingArray);

		void Init(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag = true);
		void ReInit(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag = true);

		inline Type& operator[](const CountType& aIndex);
		inline const Type& operator[](const CountType& aIndex) const;

		inline void Add(const Type& aObject);
		inline void Add(const GrowingArray& aGrowingArray);
		inline bool AddUnique(const Type& aObject);
		inline void Insert(CountType aIndex, const Type& aObject);
		inline void DeleteCyclic(Type& aObject);
		inline void DeleteCyclicAtIndex(CountType aItemNumber);
		inline void RemoveCyclic(const Type& aObject);
		inline void RemoveCyclicAtIndex(CountType aItemNumber);
		inline CountType Find(const Type& aObject);
		inline void SortAscending();

		inline Type& GetFirst();
		inline const Type& GetFirst() const;
		inline Type& GetLast();
		inline const Type& GetLast() const;

		static const CountType FoundNone = static_cast<CountType>(-1);

		inline void RemoveAll();
		inline void DeleteAll();

		void Optimize();
		__forceinline CountType Size() const;
		__forceinline CountType GetLastIndex() const;
		__forceinline CountType Capacity() const;
		__forceinline bool Empty() const;
		__forceinline bool Full() const;
		inline void Resize(CountType aNewSize);
		inline void Reserve(CountType aNewSize);

		// You should be REALLY careful with these.
		// Get a pointer to the internal data in the array.
		Type* GetRawData() { return myData; };
		// You should be REALLY careful with these.
		// Get invalid object of stored type.
		// Use with decltype, ex:
		// using ArrayType = decltype(myArray.GetInvalidTypeObject());
		Type GetInvalidTypeObject() { return *(Type*(nullptr))};
		// You should be REALLY careful with these.
		// Sets internal array pointer to aStartAddress and sets capacity and size to aAmount.
		// WARNING: Removes any removing/deleting/adding functionality on this array for forever, except the functions that starts with same comment as this one.
		void SetRawData(Type* aStartAddress, CountType aAmount) { SetRawDataReserve(aStartAddress, aAmount, aAmount); };
		// You should be REALLY careful with these.
		// Sets internal array pointer to aStartAddress and capacity to aAmount. Sets size to aAmountAlreadyAdded.
		// WARNING: Removes any removing/deleting/adding functionality on this array for forever, except the functions that starts with same comment as this one.
		void SetRawDataReserve(Type* aStartAddress, CountType aAmount, CountType aAmountAlreadyAdded) { myData = aStartAddress; myCapacity = aAmount; myAmountOfAddedElements = aAmountAlreadyAdded; myDontDeleteAnything = true; };
		// You should be REALLY careful with these.
		// Sets internal array pointer to nullptr and sets capacity and size to 0.
		// WARNING: Memory leak if GetRawData() hasn't been called and returned pointer saved.
		void ReleaseOwnership() { myData = nullptr; myAmountOfAddedElements = 0; myCapacity = 0; };

	private:
		Type* myData = nullptr;
		CountType myAmountOfAddedElements;
		CountType myCapacity;
		bool mySafeModeFlag;
		bool myDontDeleteAnything;

		void ChangeArrayCapacityIfNeeded(const CountType& aIndex);

	};

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray()
	{
		myData = nullptr;
		myAmountOfAddedElements = 0;
		myCapacity = 0;
		mySafeModeFlag = true;
		myDontDeleteAnything = false;
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray(CountType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{
		Init(aNrOfRecommendedItems, aUseSafeModeFlag);
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray(const GrowingArray& aGrowingArray)
	{
 		this->Init(aGrowingArray.myCapacity, aGrowingArray.mySafeModeFlag);
		*this = aGrowingArray;
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::GrowingArray(GrowingArray&& aGrowingArray)
	{
		myAmountOfAddedElements = aGrowingArray.myAmountOfAddedElements;
		myCapacity = aGrowingArray.myCapacity;
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;
		myDontDeleteAnything = aGrowingArray.myDontDeleteAnything;

		myData = aGrowingArray.myData;
		aGrowingArray.myData = nullptr;
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>::~GrowingArray()
	{
		if (myDontDeleteAnything == false)
		{
			delete[] myData;
			myData = nullptr;
		}
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>& GrowingArray<Type, CountType>::operator=(const GrowingArray& aGrowingArray)
	{
		if (aGrowingArray.myCapacity != 0)
		{
			this->ChangeArrayCapacityIfNeeded(aGrowingArray.myCapacity);
		}

		myAmountOfAddedElements = aGrowingArray.myAmountOfAddedElements;
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;
		myDontDeleteAnything = aGrowingArray.myDontDeleteAnything;

		if (aGrowingArray.mySafeModeFlag == true)
		{
			for (CountType dataIndex = static_cast<CountType>(0); dataIndex < myAmountOfAddedElements; dataIndex++)
			{
				myData[static_cast<int>(dataIndex)] = aGrowingArray.myData[static_cast<int>(dataIndex)];
			}
		}
		else
		{
			memcpy(this->myData, aGrowingArray.myData, sizeof(Type) * aGrowingArray.myAmountOfAddedElements);

			myAmountOfAddedElements = aGrowingArray.myAmountOfAddedElements;
		}

		return *this;
	}

	template<typename Type, typename CountType>
	inline GrowingArray<Type, CountType>& GrowingArray<Type, CountType>::operator=(GrowingArray&& aGrowingArray)
	{
		myAmountOfAddedElements = aGrowingArray.myAmountOfAddedElements;
		myCapacity = aGrowingArray.myAmountOfAddedElements;
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;
		myDontDeleteAnything = aGrowingArray.myDontDeleteAnything;

		myData = aGrowingArray.myData;
		aGrowingArray.myData = nullptr;
		
		return *this;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Init(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag)
	{
		ReInit(aNrOfRecomendedItems, aUseSafeModeFlag);
		/*assert("Tried to create an empty or negative GrowingArray!" && (aNrOfRecomendedItems > 0));
		myData = new Type[aNrOfRecomendedItems];

		myAmountOfAddedElements = 0;
		myCapacity = aNrOfRecomendedItems;
		mySafeModeFlag = aUseSafeModeFlag;
		myDontDeleteAnything = false;*/
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::ReInit(CountType aNrOfRecomendedItems, bool aUseSafeModeFlag)
	{
		assert("Tried to create an empty or negative GrowingArray!" && (aNrOfRecomendedItems > 0));

		delete[] myData;
		myData = nullptr;
		//Init(aNrOfRecomendedItems, aUseSafeModeFlag);

		myData = new Type[aNrOfRecomendedItems];

		myAmountOfAddedElements = 0;
		myCapacity = aNrOfRecomendedItems;
		mySafeModeFlag = aUseSafeModeFlag;
		myDontDeleteAnything = false;
	}

	template<typename Type, typename CountType>
	inline Type& GrowingArray<Type, CountType>::operator[](const CountType& aIndex)
	{
		assert(((aIndex >= 0) && (myAmountOfAddedElements > aIndex)) && "Trying to access invalid index!");
		return myData[aIndex];
	}

	template<typename Type, typename CountType>
	inline const Type& GrowingArray<Type, CountType>::operator[](const CountType& aIndex) const
	{
		assert(((aIndex >= 0) && (myAmountOfAddedElements > aIndex)) && "Trying to access invalid index!");
		return myData[aIndex];
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Add(const Type& aObject)
	{
		Insert(myAmountOfAddedElements, aObject);
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Add(const GrowingArray& aGrowingArray)
	{
		for (CountType dataIndex = 0; dataIndex < aGrowingArray.Size(); ++dataIndex)
		{
			Insert(myAmountOfAddedElements, aGrowingArray[dataIndex]);
		}
	}

	template<typename Type, typename CountType>
	inline bool GrowingArray<Type, CountType>::AddUnique(const Type & aObject)
	{
		if (Find(aObject) == FoundNone)
		{
			Add(aObject);
			return true;
		}
		return false;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Insert(CountType aIndex, const Type& aObject)
	{
		assert(((aIndex >= 0) && (myAmountOfAddedElements >= aIndex)) && "Trying to access invalid index!");
		if (myCapacity <= myAmountOfAddedElements)
		{
			ChangeArrayCapacityIfNeeded(myCapacity * static_cast<CountType>(2));
		}

		for (CountType dataIndex = (myAmountOfAddedElements); dataIndex > aIndex; dataIndex--)
		{
			myData[dataIndex] = myData[dataIndex - 1];
		}

		myData[aIndex] = aObject;
		myAmountOfAddedElements++;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::DeleteCyclic(Type& aObject)
	{
		for (CountType dataIndex = 0; dataIndex < myAmountOfAddedElements; dataIndex++)
		{
			if (myData[dataIndex] == aObject)
			{
				DeleteCyclicAtIndex(dataIndex);

				break;
			}
		}
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::DeleteCyclicAtIndex(CountType aItemNumber)
	{
		assert(((aItemNumber >= 0) && (myAmountOfAddedElements > aItemNumber)) && "Trying to access invalid index!");

		if (myDontDeleteAnything == true)
		{
			return;
		}

		delete myData[aItemNumber];
		myAmountOfAddedElements--;
		myData[aItemNumber] = myData[myAmountOfAddedElements];
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::RemoveCyclic(const Type& aObject)
	{
		for (CountType dataIndex = 0; dataIndex < myAmountOfAddedElements; dataIndex++)
		{
			if (myData[dataIndex] == aObject)
			{
				RemoveCyclicAtIndex(dataIndex);

				break;
			}
		}
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::RemoveCyclicAtIndex(CountType aItemNumber)
	{
		assert(((aItemNumber >= 0) && (myAmountOfAddedElements > aItemNumber)) && "Trying to access invalid index!");

		if (myDontDeleteAnything == true)
		{
			return;
		}

		myAmountOfAddedElements--;
		myData[aItemNumber] = myData[myAmountOfAddedElements];
	}

	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::Find(const Type& aObject)
	{
		for (CountType dataIndex = 0; dataIndex < myAmountOfAddedElements; dataIndex++)
		{
			if (myData[dataIndex] == aObject)
			{
				return dataIndex;
			}
		}

		return FoundNone;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::SortAscending()
	{
		std::sort(&myData[0], &myData[myAmountOfAddedElements]);
	}

	template<typename Type, typename CountType>
	inline Type& GrowingArray<Type, CountType>::GetFirst()
	{
		assert("Trying to get first element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[0];
	}

	template<typename Type, typename CountType>
	inline const Type& GrowingArray<Type, CountType>::GetFirst() const
	{
		assert("Trying to get first element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[0];
	}

	template<typename Type, typename CountType>
	inline Type& GrowingArray<Type, CountType>::GetLast()
	{
		assert("Trying to get Last element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[myAmountOfAddedElements - 1];
	}

	template<typename Type, typename CountType>
	inline const Type& GrowingArray<Type, CountType>::GetLast() const
	{
		assert("Trying to get Last element in array when it's empty" && (myAmountOfAddedElements > 0));
		return myData[myAmountOfAddedElements - 1];
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::RemoveAll()
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		/*delete[] myData;
		myData = nullptr;*/
		myAmountOfAddedElements = 0;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::DeleteAll()
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		for (CountType dataIndex = 0; dataIndex < myAmountOfAddedElements; dataIndex++)
		{
			delete myData[dataIndex];
			myData[dataIndex] = nullptr;
		}

		RemoveAll();
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Optimize()
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (myCapacity == myAmountOfAddedElements)
		{
			return;
		}

		Type* allData = myData;
		myData = new Type[myAmountOfAddedElements];
		myCapacity = myAmountOfAddedElements;

		for (CountType dataIndex = 0; dataIndex < myAmountOfAddedElements; dataIndex++)
		{
			myData[dataIndex] = allData[dataIndex];
		}

		delete[] allData;
	}

	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::Size() const
	{
		return myAmountOfAddedElements;
	}

	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::GetLastIndex() const
	{
		if (myAmountOfAddedElements == 0)
		{
			return FoundNone;
		}
		return (myAmountOfAddedElements - 1);
	}
	
	template<typename Type, typename CountType>
	inline CountType GrowingArray<Type, CountType>::Capacity() const
	{
		return myCapacity;
	}

	template<typename Type, typename CountType>
	inline bool GrowingArray<Type, CountType>::Empty() const
	{
		return (myAmountOfAddedElements == 0);
	}

	template<typename Type, typename CountType>
	inline bool GrowingArray<Type, CountType>::Full() const
	{
		return (myAmountOfAddedElements == myCapacity);
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Resize(CountType aNewSize)
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (aNewSize == 0)
		{
			return;
		}

		ChangeArrayCapacityIfNeeded(aNewSize);

		myAmountOfAddedElements = myCapacity;
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::Reserve(CountType aNewSize)
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (aNewSize == 0)
		{
			return;
		}

		if (myCapacity < aNewSize)
		{
			Type* allData = myData;

			myData = new Type[aNewSize];

			myCapacity = aNewSize;

			for (CountType dataIndex = 0; dataIndex < myAmountOfAddedElements; dataIndex++)
			{
				myData[dataIndex] = allData[dataIndex];
			}

			delete[] allData;
		}
	}

	template<typename Type, typename CountType>
	inline void GrowingArray<Type, CountType>::ChangeArrayCapacityIfNeeded(const CountType& aIndex)
	{
		if (myDontDeleteAnything == true)
		{
			return;
		}

		if (myCapacity < aIndex)
		{
			Type* allData = myData;
			
			myData = new Type[aIndex];

			myCapacity = aIndex;

			for (CountType dataIndex = 0; dataIndex < myAmountOfAddedElements; dataIndex++)
			{
				myData[dataIndex] = allData[dataIndex];
			}

			delete[] allData;
		}
		else if (myCapacity > aIndex)
		{
			myAmountOfAddedElements = aIndex;
			myCapacity = aIndex;
		}
		else if (myCapacity == 0)
		{
			if (aIndex == 0)
			{
				myData = new Type[2];

				myCapacity = 2;
			}
			else
			{
				myData = new Type[aIndex];

				myCapacity = aIndex;
			}
		}
	}
};

namespace CU = CommonUtilities;
