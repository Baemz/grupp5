#pragma once
#include "Vector.h"

namespace CommonUtilities
{
	namespace Collision
	{
		struct Circle
		{
			Circle() : myCenter(Vector2f()), myRadius(0.0f)
			{
			}
			Circle(const Vector2f& aCenter, const float aRadius) : myCenter(aCenter), myRadius(aRadius)
			{
			}

			Vector2f myCenter;
			float myRadius;
		};

		struct Sphere
		{
			Sphere(const Vector3f& aCenter, const float aRadius) : myCenter(aCenter), myRadius(aRadius)
			{
			}

			Vector3f myCenter;
			float myRadius;
		};

		struct AABB3D
		{
			AABB3D() = default;

			AABB3D(const Vector3f& aMin, const Vector3f& aMax) : myMin(aMin), myMax(aMax)
			{
			}

			Vector3f myMin;
			Vector3f myMax;

			bool myIsTargeted = false;
		};

		struct AABB
		{
			AABB() {};
			AABB(const Vector2f& aMin, const Vector2f& aMax) : myMin(aMin), myMax(aMax)
			{ 
				myWidth = myMax.x - myMax.x;
				myHeight = myMin.x - myMin.x;
			}
			Vector2f myMin;
			Vector2f myMax;
			float myWidth;
			float myHeight;
		};

		struct Line3D
		{
			Line3D(const Vector3f& aPoint, const Vector3f& aDir) : myPoint(aPoint), myDirection(aDir)
			{
			}

			Vector3f myPoint;
			Vector3f myDirection;
		};

		struct Plane
		{
			Plane(const Vector3f& aPoint, const Vector3f& aNormal) : myPoint(aPoint), myNormal(aNormal)
			{
			}

			

			Vector3f myPoint;
			Vector3f myNormal;
		};

		struct Ray
		{
			Ray(const Vector3f& aOrigin, const Vector3f aDirection) : myOrigin(aOrigin), myDirection(aDirection)
			{
			}

			CU::Vector3f myOrigin;
			CU::Vector3f myDirection;
		};

		bool PointInsideSphere(const Sphere& aSphere, const Vector3f aPoint);
		bool PointInsideAABB(const AABB3D& aAABB, const Vector3f& aPoint);
		bool IntersectionPlaneLine(const Plane& aPlane, const Line3D& aLine, Vector3f& aOurIntersectionPoint);
		bool IntersectionAABBLine(const AABB3D& aAABB, const Line3D& aLine);
		bool IntersectionSphereLine(const Sphere& aSphere, const Line3D& aLine);
		bool IntersectionSphereSphere(const Sphere& aSphere1, const Sphere& aSphere2);
		bool IntersectionCircleCircle(const Circle& aCircle1, const Circle& aCircle2);
		bool IntersectionCircleVShape(const Circle& aCircle, const Line3D& aLLine, const Line3D& aRLine);
		bool IntersectionAABBCircle(const AABB& aRect, const Circle& aCircle);
		bool IntersectionRayAABB(const Ray& aRay, const AABB3D& aAABB);
	};
}
namespace CU = CommonUtilities;
