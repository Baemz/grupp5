#pragma once
#include <windows.h>
#include <xinput.h>
#include "stdint.h"
#pragma comment(lib, "XInput.lib") 
#pragma comment(lib, "Xinput9_1_0.lib")


namespace CommonUtilities
{
	class XboxController
	{
	public:
		XboxController();
		XboxController(int aControllerIndex);
		~XboxController();


		//You MUST call Update() BEFORE you run your input.
		void Update();

		//You MUST call RefreshState() AFTER you've run all of your desired input.
		void RefreshState();


		bool IsXboxControllerConnected();
		void Vibrate(float aLeftMotor = 0.0f, float aRightMotor = 0.0f);
		bool IsButtonDown(const int aButton) const;
		bool WasButtonPressed(const int aButton) const;
		XINPUT_STATE GetState();

		bool IsRightStickInDeadZone() const;
		bool IsLeftStickInDeadZone() const;
		float LeftStickX() const;
		float LeftStickY() const;
		float RightStickX() const;
		float RightStickY() const;

		float LeftTrigger();
		float RightTrigger();

	private:
		XINPUT_STATE myControllerState;
		int myControllerNumber;

		static const int ButtonCount = 14;    
		bool previousButtonStates[ButtonCount];
		bool currentButtonStates[ButtonCount]; 
		bool controllerButtonsDown[ButtonCount];
	};

	static const WORD XINPUT_Buttons[] = {
		XINPUT_GAMEPAD_A,
		XINPUT_GAMEPAD_B,
		XINPUT_GAMEPAD_X,
		XINPUT_GAMEPAD_Y,
		XINPUT_GAMEPAD_DPAD_UP,
		XINPUT_GAMEPAD_DPAD_DOWN,
		XINPUT_GAMEPAD_DPAD_LEFT,
		XINPUT_GAMEPAD_DPAD_RIGHT,
		XINPUT_GAMEPAD_LEFT_SHOULDER,
		XINPUT_GAMEPAD_RIGHT_SHOULDER,
		XINPUT_GAMEPAD_LEFT_THUMB,
		XINPUT_GAMEPAD_RIGHT_THUMB,
		XINPUT_GAMEPAD_START,
		XINPUT_GAMEPAD_BACK
	};

	struct XButtonIDs
	{
		XButtonIDs();

		int A;
		int B;
		int X;
		int Y;

		int DPad_Up;
		int	DPad_Down;
		int	DPad_Left;
		int	DPad_Right;

		int L_Shoulder;
		int	R_Shoulder;

		int L_Thumbstick;
		int R_Thumbstick;

		int Start;
		int Back;
	};
}

extern CommonUtilities::XButtonIDs XboxButtons;
