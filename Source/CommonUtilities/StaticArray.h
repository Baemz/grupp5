#pragma once
#include <assert.h>

namespace CommonUtilities
{
	template <typename T, int size = 10>
	class StaticArray
	{
	public:
		StaticArray();
		StaticArray(const StaticArray& aStaticArray);

		~StaticArray();

		StaticArray& operator=(const StaticArray& aStaticArray);

		inline const T& operator[](const int& aIndex) const;
		inline T& operator[](const int& aIndex);


		// Utility functions
		inline void Insert(int aIndex, T& aObject);
		inline void DeleteAll();

		const int GetSize() const;
	private:
		T* myArray;
	};
	
	template<typename T, int size> inline StaticArray <T, size> ::StaticArray()
	{
		myArray = new T[size];
	}
	
	template<typename T, int size> inline StaticArray<T, size>::StaticArray(const StaticArray& aStaticArray)
	{
		myArray = new T[size];
		for (int i = 0; i < size; i++)
		{
			myArray[i] = aStaticArray[i];
		}
	}
	
	template<typename T, int size>	inline StaticArray<T, size>::~StaticArray()
	{
		delete myArray;
		myArray = nullptr;
	}
	
	template<typename T, int size>	inline StaticArray<T, size>& StaticArray<T, size>::operator=(const StaticArray& aStaticArray)
	{
		assert(size == aStaticArray.GetSize());
		for (int i = 0; i < size; i++)
 		{
			myArray[i] = aStaticArray[i];
 		}
		return *(this);
	}
	
	template<typename T, int size>	inline const T& StaticArray<T, size>::operator[](const int& aIndex) const
	{
		assert((aIndex <= (size - 1)) && "Index out of bounds.");
		assert((aIndex >= 0) && "Index out of bounds.");
		return myArray[aIndex];
	}
	
	template<typename T, int size>	inline T& StaticArray<T, size>::operator[](const int& aIndex)
	{
		assert((aIndex <= (size - 1)) && "Index out of bounds.");
		assert((aIndex >= 0) && "Index out of bounds.");
		return myArray[aIndex];
	}
	
	template<typename T, int size>	inline void StaticArray<T, size>::Insert(int aIndex, T& aObject)
	{
		assert((aIndex <= (size - 1)) && "Index out of bounds.");
		assert((aIndex >= 0) && "Index out of bounds.");

		T objectHolder = myArray[aIndex];
		myArray[aIndex] = aObject;

		for (int i = size - 1; i > aIndex; i--)
		{
			if (i == aIndex + 1)
			{
				myArray[i] = objectHolder;
			}
			else
			{
				myArray[i] = myArray[i - 1];
			}
		}
	}
	
	template<typename T, int size>	inline void StaticArray<T, size>::DeleteAll()
	{
		for (int i = 0; i < size; i++)
		{
			delete myArray[i];
			myArray[i] = nullptr;
		}
	}

	template<typename T, int size>	inline const int StaticArray<T, size>::GetSize() const
	{
		return size;
	}
}
