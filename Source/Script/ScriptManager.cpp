#include "stdafx.h"
#include "ScriptManager.h"
#include "..\EngineCore\FileWatcher\FileWatcherWrapper.h"

CScriptManager* CScriptManager::ourInstance = nullptr;

void CScriptManager::Create()
{
	assert("ScriptManager already created!" && (ourInstance == nullptr));

	if (ourInstance == nullptr)
	{
		ourInstance = hse_new(CScriptManager);
	}
}

void CScriptManager::Destroy()
{
	hse_delete(ourInstance);
}

bool CScriptManager::Init(const std::function<bool(CScriptManager& aScriptManager)>& aRegisterFunctionsFunction, const std::string& aFileName)
{
	NewState(aFileName);

	std::ofstream outputFile(ourDocumentationFilePath, std::ofstream::out | std::ofstream::trunc);
	if (outputFile.good() == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Couldn't open the documentation file \"%s\""
			, ourDocumentationFilePath);
		return false;
	}
	outputFile.close();

	if ((aRegisterFunctionsFunction == nullptr)
		|| (aRegisterFunctionsFunction(*this) == false))
	{
		myLuaState.myStateIsValid = false;
		return false;
	}

	return true;
}

int CScriptManager::FromLUA(lua_State* aState)
{
	SScriptFunctionCallableBase::SFunctionWrapper* function(reinterpret_cast<SScriptFunctionCallableBase::SFunctionWrapper*>(lua_touserdata(aState, lua_upvalueindex(1))));

	int amountReturned(0);
	if (function != nullptr)
	{
		amountReturned = (*function)(aState);
	}
	else
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Nullptr function in FromLUA");
	}

	return amountReturned;
}

bool CScriptManager::CheckLUAError(lua_State& aState, int aResult)
{
	if (aResult != LUA_OK)
	{
		const char* message = lua_tostring(&aState, -1);
		std::string completeMessage("[ScriptManager] ERROR! ");
		completeMessage += message;
		RESOURCE_LOG(completeMessage.c_str());
		lua_pop(&aState, 1);

		return false;
	}

	return true;
}

bool CScriptManager::LoadedFileChanged(void* aScriptManager, const char* aFileChangedName)
{
	CScriptManager* scriptManagerPointer(reinterpret_cast<CScriptManager*>(aScriptManager));
	if (scriptManagerPointer == nullptr)
	{
		return false;
	}

	return scriptManagerPointer->LoadedFileChangedImpl(aFileChangedName);
}

bool CScriptManager::LoadedFileChangedImpl(const char* aFileChangedName)
{
	std::unique_lock<std::mutex> lg(myLuaState.myMutex);

	RESOURCE_LOG("[ScriptManager] File \"%s\" changed, reloading it"
		, aFileChangedName);

	return NewState(aFileChangedName);
}

bool CScriptManager::NewState(const std::string& aFileToOpen)
{
	if (myLuaState.myLuaState != nullptr)
	{
		lua_close(myLuaState.myLuaState);
		myLuaState = nullptr;
	}

	myLuaState.myLuaState = luaL_newstate();

	if (myLuaState.myLuaState == nullptr)
	{
		myLuaState.myStateIsValid = false;
		return false;
	}

	myLuaState.myStateIsValid = true;

	luaL_openlibs(myLuaState.myLuaState);

	if (OpenFile(aFileToOpen.c_str()) == false)
	{
		lua_close(myLuaState.myLuaState);
		myLuaState = nullptr;
		return false;
	}

	return true;
}

bool CScriptManager::OpenFile(const std::string& aFileToOpen)
{
	static bool added(false); // TODO: obviously remove and place in SLuaState
	if (added == false)
	{
		added = true;
		if (hse::CFileWatcherWrapper::AddFileToWatch(this, aFileToOpen.c_str(), &CScriptManager::LoadedFileChanged) == false)
		{
			RESOURCE_LOG("[ScriptManager] ERROR! File \"%s\" doesn't exist, can't add as a lua file"
				, aFileToOpen.c_str());
			myLuaState.myStateIsValid = false;
			return false;
		} // TODO: change file watcher to be able to keep nonexisting files
	}

	if (CheckLUAError(*myLuaState.myLuaState, luaL_loadfile(myLuaState.myLuaState, aFileToOpen.c_str())) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! File \"%s\" doesn't exist or isn't a valid lua file"
			, aFileToOpen.c_str());
		myLuaState.myStateIsValid = false;
		return false;
	}

	if (CheckLUAError(*myLuaState.myLuaState, lua_pcall(myLuaState.myLuaState, 0, LUA_MULTRET, 0)) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! File \"%s\" isn't a valid lua file"
			, aFileToOpen.c_str());
		myLuaState.myStateIsValid = false;
		return false;
	}

	myLuaState.myStateIsValid = true;

	const auto size = myRegisteredFunctions.size();
	for (auto& functionPair : myRegisteredFunctions)
	{
		RegisterFunctionForState(*myLuaState.myLuaState, *functionPair.second);
	}

	return true;
}

void CScriptManager::RegisterFunctionForState(lua_State& aLuaState, SScriptFunctionCallableBase& aFunction)
{
	if (myLuaState.IsValid())
	{
		lua_pushlightuserdata(&aLuaState, &aFunction.myFunctionWrapper);
		lua_pushcclosure(&aLuaState, &CScriptManager::FromLUA, 1);
		lua_setglobal(&aLuaState, aFunction.myFunctionName.c_str());
	}
}

CScriptManager::CScriptManager()
{
}

CScriptManager::~CScriptManager()
{
	if (myLuaState.myLuaState != nullptr)
	{
		lua_close(myLuaState.myLuaState);
		myLuaState = nullptr;
	}
	myLuaState.myStateIsValid = false;

	const auto size = myRegisteredFunctions.size();
	for (auto& functionPair : myRegisteredFunctions)
	{
		hse_delete(functionPair.second);
	}
	myRegisteredFunctions.clear();
}

//
//#include "stdafx.h"
//#include "ScriptManager.h"
//#include "..\EngineCore\FileWatcher\FileWatcherWrapper.h"
//
//CScriptManager* CScriptManager::ourInstance = nullptr;
//
//void CScriptManager::Create()
//{
//	assert(ourInstance == nullptr && "[CScriptManager] Instance already created.");
//	ourInstance = hse_new(CScriptManager());
//
//	ourInstance->myState = nullptr;
//}
//
//void CScriptManager::Destroy()
//{
//	if (ourInstance->myState)
//	{
//		lua_close(ourInstance->myState);
//	}
//	ourInstance->myState = nullptr;
//
//	hse_delete(ourInstance);
//}
//
//void CScriptManager::Init(std::function<void()> aRegisterExternalFunction, const std::string& aFilePath)
//{
//	if (myState != nullptr)
//	{
//		lua_close(myState);
//	}
//
//	myState = luaL_newstate();
//	luaL_openlibs(myState);
//	
//	if (CheckAndPrintError( luaL_dofile(myState, aFilePath.c_str()) ))
//	{
//		return;
//	}
//	
//	if (hse::CFileWatcherWrapper::AddFileToWatch(this, aFilePath.c_str(), &ReloadFile) == false)
//	{
//		SCRIPT_LOG("WARNING! Failed to add a file to file watcher. File name: \"%s\".", aFilePath.c_str());
//	}
//
//	aRegisterExternalFunction();
//}
//
//void CScriptManager::RegisterFunction(const std::string& aName, lua_CFunction aFunction, const std::string& aHelpString)
//{
//	myFunctionDocs[aName];
//	myFunctionDocs[aName] = aHelpString;
//	PrintDocumentation();
//
//	lua_register(myState, aName.c_str(), aFunction);
//}
//
//void CScriptManager::Update()
//{
//}
//
///* PRIVATE FUNCTIONS */
//
//const bool CScriptManager::CheckAndPrintError(const int aError) const
//{
//	if (aError == LUA_OK) return false;
//
//	SCRIPT_LOG("ERROR! %s.", lua_tostring(myState, -1));
//	return true;
//}
//
//void CScriptManager::PrintDocumentation()
//{
//	std::ofstream outputFile("Data/Scripts/Documentation.txt");
//
//	if (outputFile.bad())
//	{
//		SCRIPT_LOG("The documentation file was bad; cannot open.");
//		outputFile.close();
//		return;
//	}
//
//	outputFile.clear();
//	outputFile << "--|Currently Exposed Functions|--\n\n";
//
//	for (auto& s : myFunctionDocs)
//	{
//		outputFile << "Function name: \"" << s.first << "\"\n";
//		outputFile << "Note: " << s.second << "\n";
//		outputFile << "--------------------------------------------------------";
//		outputFile << std::endl;
//	}
//
//	outputFile.close();
//}
//
//bool CScriptManager::ReloadFile(void* aThis, const char* aFileToReload)
//{
//	CScriptManager& thisRef = *(CScriptManager*)aThis;
//
//	std::lock_guard<std::mutex> lockGuard( thisRef.myMutex );
//	thisRef.CheckAndPrintError(luaL_dofile(thisRef.myState, aFileToReload));
//
//	return true;
//}
