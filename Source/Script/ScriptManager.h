#pragma once
#include "ScriptIncluder.h"
#include <mutex>
#include <fstream>

namespace hse
{
	class CMemoryPool;
}

class CScriptManager
{
	friend hse::CMemoryPool;

public:
	static void Create();
	static void Destroy();

	static inline CScriptManager* Get() { return ourInstance; };

	bool Init(const std::function<bool(CScriptManager& aScriptManager)>& aRegisterFunctionsFunction, const std::string& aFileName);

	template<typename FunctionType>
	bool RegisterFunction(const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aDocumentationText);

	template<typename... Args>
	bool CallFunction(const std::string& aFunctionName, Args&&... aArgs);

private:
	struct SScriptFunctionCallableBase
	{
		struct SFunctionWrapper
		{
			int operator()(lua_State* aState)
			{
				return myFunctionWrapper(aState);
			}

			std::function<int(lua_State*)> myFunctionWrapper;
		};

		SScriptFunctionCallableBase() = default;
		~SScriptFunctionCallableBase() = default;

		SFunctionWrapper myFunctionWrapper;
		std::string myFunctionName;

#ifndef _RETAIL
		std::string myDocumentationText;
#endif // !_RETAIL
	};
	template<typename FunctionType>
	class SScriptFunctionCallable : public SScriptFunctionCallableBase
	{
		friend CScriptManager;

	public:
		~SScriptFunctionCallable() = default;

	private:
		SScriptFunctionCallable(std::function<FunctionType> aFunction)
			: myFunction(aFunction)
		{};

		std::function<FunctionType> myFunction;

	};

	template <typename T>
	struct function_traits
		: public function_traits<decltype(&T::operator())>
	{};

	template <typename ClassType, typename ReturnType, typename... Args>
	struct function_traits<ReturnType(ClassType::*)(Args...) const>
	{
		enum { AmountOfArgs = sizeof...(Args) };

		using tuple_type = std::tuple<Args...>;

		using result_type = ReturnType;

		template <size_t i>
		struct arg
		{
			using type = typename std::tuple_element<i, std::tuple<Args...>>::type;
		};
	};

	template<std::size_t TypeIndex = 0, typename... Tuple>
	inline typename std::enable_if_t<TypeIndex == sizeof...(Tuple), std::size_t>
		GetCustomTypeForTuple(lua_State&, std::tuple<Tuple...>&)
	{
		constexpr std::size_t tupleSize(sizeof...(Tuple));
		return tupleSize;
	}

	template<std::size_t TypeIndex = 0, typename... Tuple>
	inline typename std::enable_if_t<TypeIndex < sizeof...(Tuple), std::size_t>
		GetCustomTypeForTuple(lua_State& aLuaState, std::tuple<Tuple...>& aTuple)
	{
		constexpr std::size_t argIndex((sizeof...(Tuple) - 1) - TypeIndex);
		if (GetCustomType(aLuaState, std::get<argIndex>(aTuple)) == false)
		{
			return argIndex;
		}

		return GetCustomTypeForTuple<TypeIndex + 1, Tuple...>(aLuaState, aTuple);
	}

	template<std::size_t TypeIndex = 0, typename... Tuple>
	inline typename std::enable_if_t<TypeIndex == sizeof...(Tuple), int>
		PushCustomTypeForTuple(lua_State&, std::tuple<Tuple...>&)
	{
		return 0;
	}

	template<std::size_t TypeIndex = 0, typename... Tuple>
	inline typename std::enable_if_t<TypeIndex < sizeof...(Tuple), int>
		PushCustomTypeForTuple(lua_State& aLuaState, std::tuple<Tuple...>& aTuple)
	{
		const int amountOfReturnTypes(PushCustomType(aLuaState, std::get<TypeIndex>(aTuple)));

		return (amountOfReturnTypes + PushCustomTypeForTuple<TypeIndex + 1, Tuple...>(aLuaState, aTuple));
	}

	// _____________________________________________
	// Replace with std::apply when moved to C++17
	template<typename Function, typename Tuple, size_t... I>
	auto ApplyImpl(Function& aFunction, Tuple& aTuple, std::index_sequence<I ...>)
	{
		aTuple; // Compile warning without this if the tuple is empty
		return aFunction(std::get<I>(aTuple)...);
	}

	template<typename Function, typename Tuple>
	auto ApplyImpl(Function& aFunction, Tuple& aTuple)
	{
		static constexpr auto size = std::tuple_size<Tuple>::value;
		return ApplyImpl(aFunction, aTuple, std::make_index_sequence<size>{});
	}
	// _____________________________________________

	template<typename ReturnType, typename Function, typename Tuple>
	std::enable_if_t<std::is_same<ReturnType, void>::value == false, int>
		Apply(lua_State& aLuaState, Function& aFunction, Tuple& aTuple)
	{
		static constexpr auto size = std::tuple_size<Tuple>::value;
		return PushCustomType(aLuaState, ApplyImpl(aFunction, aTuple, std::make_index_sequence<size>{}));
	}

	template<typename ReturnType, typename Function, typename Tuple>
	std::enable_if_t<std::is_same<ReturnType, void>::value == true, int>
		Apply(lua_State&, Function& aFunction, Tuple& aTuple)
	{
		static constexpr auto size = std::tuple_size<Tuple>::value;
		ApplyImpl(aFunction, aTuple, std::make_index_sequence<size>{});
		return 0;
	}

	struct SLuaState
	{
		bool IsValid() const
		{
			return ((myLuaState != nullptr) && (myStateIsValid == true));
		}

		SLuaState& operator=(const std::nullptr_t& aNullptr)
		{
			myLuaState = aNullptr;
			myStateIsValid = !!aNullptr;

			return *this;
		}

		lua_State* myLuaState = nullptr;
		bool myStateIsValid = false;
		std::mutex myMutex;
	};

	static int FromLUA(lua_State* aState);

	static bool CheckLUAError(lua_State& aState, int aResult);

	static bool LoadedFileChanged(void* aScriptManager, const char* aFileChangedName);
	bool LoadedFileChangedImpl(const char* aFileChangedName);

	bool NewState(const std::string& aFileToOpen);
	bool OpenFile(const std::string& aFileToOpen);

	void RegisterFunctionForState(lua_State& aLuaState, SScriptFunctionCallableBase& aFunction);

	template<typename Type>
	bool GetCustomType(lua_State& aLuaState, Type& aTypeReference);

	int PushCustomType(lua_State& aLuaState, const double& aTypeReference);

	int PushCustomType(lua_State& aLuaState, const int& aTypeReference);

	int PushCustomType(lua_State& aLuaState, const char* aTypeReference);

	int PushCustomType(lua_State& aLuaState, const bool& aTypeReference);

	CScriptManager();
	~CScriptManager();

	static CScriptManager* ourInstance;

	static constexpr const char* ourDocumentationFilePath = "Data/Scripts/exposedScriptFunctions.txt";

	std::unordered_map<std::string, SScriptFunctionCallableBase*> myRegisteredFunctions;
	// ____________________________________
	// Place in map with file names after labb
	SLuaState myLuaState;
	// ____________________________________

};

template<typename FunctionType>
inline bool CScriptManager::RegisterFunction(const std::string& aFunctionName, const std::function<FunctionType>& aFunction, const std::string& aDocumentationText)
{
	assert("Tried to register unnamed function to ScriptManager" && (aFunctionName != ""));
	assert("You forgot your documentation text! (Registering function in ScriptManager)" && (aDocumentationText != ""));
	assert("Your documentation text is probably not complete! Make it more descriptive. (Registering function in ScriptManager)" && (aDocumentationText.size() > 20));

	SScriptFunctionCallable<FunctionType>* newFunction(hse_new(SScriptFunctionCallable<FunctionType>(aFunction)));

	newFunction->myFunctionName = aFunctionName;
#ifndef _RETAIL
	newFunction->myDocumentationText = aDocumentationText;
#endif // !_RETAIL

	using functionTypes = function_traits<decltype(std::function<FunctionType>())>;

	SScriptFunctionCallable<FunctionType>& function(*newFunction);
	SScriptFunctionCallableBase::SFunctionWrapper functionWrapper;
	functionWrapper.myFunctionWrapper =
		[this, function](lua_State* aLuaState) -> int
	{
		if (aLuaState == nullptr)
		{
			RESOURCE_LOG("[ScriptManager] ERROR! Invalid lua state in RegisterFunction lambda, probably memory corruption somewhere");
			return 0;
		}

		const int amountOfArgumentsPassed(lua_gettop(aLuaState));
		constexpr std::size_t amountOfArgumentsWanted(functionTypes::AmountOfArgs);

		if (amountOfArgumentsPassed != amountOfArgumentsWanted)
		{
			lua_settop(aLuaState, 0);
			RESOURCE_LOG("[ScriptManager] ERROR! Wrong amount of arguments passed to \"%s\". Got %i, expected %llu"
				, function.myFunctionName.c_str(), amountOfArgumentsPassed, amountOfArgumentsWanted);
			return 0;
		}

		functionTypes::tuple_type argsTuple;
		const std::size_t argExitedAt(GetCustomTypeForTuple(*aLuaState, argsTuple));
		if (argExitedAt != functionTypes::AmountOfArgs)
		{
			lua_settop(aLuaState, 0);
			RESOURCE_LOG("[ScriptManager] ERROR! Argument type at %llu passed to \"%s\" is wrong"
				, argExitedAt, function.myFunctionName.c_str());
			return 0;
		}

		const int amountOfReturnTypes(Apply<functionTypes::result_type>(*aLuaState, function.myFunction, argsTuple));

		return amountOfReturnTypes;
	};

	newFunction->myFunctionWrapper = functionWrapper;

	// TODO: go through all states when there are multiple of them
	RegisterFunctionForState(*myLuaState.myLuaState, function);

	std::ofstream outputFile(ourDocumentationFilePath, std::ofstream::out | std::ofstream::app);
	if (outputFile.good() == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Couldn't open the documentation file \"%s\""
			, ourDocumentationFilePath);
		hse_delete(newFunction);
		return false;
	}
	outputFile << aDocumentationText << std::endl;
	outputFile << std::endl;
	outputFile.close();

	myRegisteredFunctions.insert({ aFunctionName, newFunction });

	return true;
}

template<typename ...Args>
inline bool CScriptManager::CallFunction(const std::string& aFunctionName, Args&&... aArgs)
{
	// TODO: add file name to argument, for correct lua_state
	std::unique_lock<std::mutex> lg(myLuaState.myMutex);

	if (myLuaState.IsValid() == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! \"%s\" doesn't have a valid lua state"
			, "TODO: FILENAME");
		return false;
	}

	lua_settop(myLuaState.myLuaState, 0);
	if (lua_getglobal(myLuaState.myLuaState, aFunctionName.c_str()) == 0)
	{
		lua_settop(myLuaState.myLuaState, 0);
		RESOURCE_LOG("[ScriptManager] ERROR! \"%s\" doesn't exist, getting as function"
			, aFunctionName.c_str());
		return false;
	}

	if (lua_isfunction(myLuaState.myLuaState, -1) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! \"%s\" isn't a function"
			, aFunctionName.c_str());
		return false;
	}

	std::tuple<Args...> arguments(aArgs...);

	const int amountOfArguments(PushCustomTypeForTuple(*myLuaState.myLuaState, arguments));

	if (CheckLUAError(*myLuaState.myLuaState, lua_pcall(myLuaState.myLuaState, amountOfArguments, 0, 0)) == false)
	{
		RESOURCE_LOG("[ScriptManager] ERROR! Something went wrong when calling function \"%s\""
			, aFunctionName.c_str());
		return false;
	}

	return true;
}

#define LUA_GET_CUSTOM_TYPE(aType, aValue, aCast)	\
if (lua_is##aType(&aLuaState, -1))					\
{													\
	aValue = aCast(lua_to##aType(&aLuaState, -1));	\
	lua_pop(&aLuaState, 1);							\
	return true;									\
}													\
													\
return false;

template<typename Type>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, Type& aTypeReference)
{
	static_assert(false, "One of your functions arguments isn't supported");
	// Compile error here?
	// Add it!
	return false;
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, const char*& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(string, aTypeReference, );
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, int& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, static_cast<int>);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, double& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, );
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, float& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(number, aTypeReference, static_cast<float>);
}

template<>
inline bool CScriptManager::GetCustomType(lua_State& aLuaState, bool& aTypeReference)
{
	LUA_GET_CUSTOM_TYPE(boolean, aTypeReference, !!);
}

#undef LUA_GET_CUSTOM_TYPE

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const double& aTypeReference)
{
	lua_pushnumber(&aLuaState, aTypeReference);
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const int& aTypeReference)
{
	lua_pushnumber(&aLuaState, static_cast<double>(aTypeReference));
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const char* aTypeReference)
{
	lua_pushstring(&aLuaState, aTypeReference);
	return 1;
}

inline int CScriptManager::PushCustomType(lua_State& aLuaState, const bool& aTypeReference)
{
	lua_pushboolean(&aLuaState, aTypeReference);
	return 1;
}

//
//#pragma once
//#include <map>
//#include <string>
//#include <functional>
//#include <mutex>
//
//#include "lua.hpp"
//
//class CScriptManager
//{
//public:
//	~CScriptManager() = default;
//	static void Create();
//	static void Destroy();
//	static CScriptManager* Get() { return ourInstance; }
//	void Init(std::function<void()> aRegisterExternalFunction, const std::string& aFilePath);
//	void RegisterFunction(const std::string& aName, lua_CFunction aFunction, const std::string& aHelpString);
//
//	template <typename... Args>
//	void CallFunction(const std::string & aName, Args && ...aArgs);
//
//	void Update();
//
//private:
//	CScriptManager() = default;
//
//	const bool CheckAndPrintError(const int aError) const; //Will return true if there is an error.
//	void PrintDocumentation();
//	static bool ReloadFile(void* aThis, const char* aFileToReload);
//
//	template<typename T>
//	void PushCustomType(lua_State* aLuaState, T& aTypeReference);
//
//	template<>
//	void PushCustomType(lua_State* aLuaState, const char*& aTypeReference);
//
//	template<>
//	void PushCustomType(lua_State* aLuaState, std::string& aTypeReference);
//
//	template<>
//	void PushCustomType(lua_State* aLuaState, lua_CFunction& aTypeReference);
//
//	template<typename...>
//	inline void PushParameters() { }
//
//	template <typename T, typename... Args>
//	void PushParameters(T aValue, Args && ...aArgs);
//
//private:
//	static CScriptManager* ourInstance;
//
//	std::map<std::string, std::string> myFunctionDocs;
//	lua_State* myState;
//	std::mutex myMutex;
//};
//
//#define PUSH_VALUE_TO_LUA(aType, aValue)	\
//lua_push##aType(aLuaState, aValue)
//
//
//template<typename... Args>
//inline void CScriptManager::CallFunction(const std::string& aName, Args&& ...aArgs)
//{
//	std::lock_guard<std::mutex> lockGuard(myMutex);
//
//	if (myState == nullptr)
//	{
//		SCRIPT_LOG("[CScriptManager] ERROR! Tried to call a LUA function before initializing LUA state.");
//		return;
//	}
//
//	lua_getglobal(myState, aName.c_str());
//
//	const unsigned char argAmount = sizeof...(aArgs);
//	PushParameters(aArgs...);
//
//	CheckAndPrintError(lua_pcall(myState, argAmount, 0, 0));
//}
//
///* PRIVATE FUNCTIONS */
//
//template<>
//inline void CScriptManager::PushCustomType(lua_State* aLuaState, const char*& aTypeReference)
//{
//	PUSH_VALUE_TO_LUA(string, aTypeReference);
//}
//
//template<>
//inline void CScriptManager::PushCustomType(lua_State* aLuaState, std::string& aTypeReference)
//{
//	PUSH_VALUE_TO_LUA(string, aTypeReference.c_str());
//}
//
//template<typename T>
//inline void CScriptManager::PushCustomType(lua_State* aLuaState, T& aTypeReference)
//{
//	static_assert(std::is_arithmetic<T>::value, "Cannot push non-arithmetic type as lua argument.");
//
//	// T is allowed to be only arithmetic.
//	PUSH_VALUE_TO_LUA(number, static_cast<double>(aTypeReference));
//}
//
//template<>
//inline void CScriptManager::PushCustomType(lua_State* aLuaState, lua_CFunction& aTypeReference)
//{
//	PUSH_VALUE_TO_LUA(cfunction, aTypeReference);
//}
//
//template<typename T, typename ...Args>
//inline void CScriptManager::PushParameters(T aValue, Args&& ...aArgs)
//{
//	PushCustomType(myState, aValue);
//	PushParameters(aArgs...);
//}
